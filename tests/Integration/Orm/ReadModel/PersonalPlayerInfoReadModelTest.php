<?php

declare(strict_types=1);

namespace App\Tests\Integration\Orm\ReadModel;

use App\Domain\ReadModel\PersonalPlayerInfo\PersonalPlayerInfo;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\Hand;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\ReadModel\PersonalPlayerInfoReadModel;
use App\TestData\TestPlayers;
use App\Tests\Integration\AbstractKrakBoemIntegrationTest;

final class PersonalPlayerInfoReadModelTest extends AbstractKrakBoemIntegrationTest
{
    /** @var PersonalPlayerInfoReadModel */
    private $readModel;

    /** @var TableIdentifier */
    private $tableIdentifier;

    protected function setUp(): void
    {
        parent::setUp();

        $this->tableIdentifier = $tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('cb3c4b02-2d26-48a8-9773-9ec0e8098a1f')
        );

        /** @var PersonalPlayerInfoReadModel $readModel */
        $readModel = $this->getContainer()->get(PersonalPlayerInfoReadModel::class);
        $this->readModel = $readModel;
        $this->readModel->forgetTable($tableIdentifier);
    }

    /** @test */
    public function itRemovesTrumpCardIfNotPresentAnymore(): void
    {
        // See #100.

        $personalPlayerInfo = PersonalPlayerInfo::forPlayerAtTable(
            Hand::fromCards(new \ArrayIterator([
                new Card(12, Card::HEARTS),
            ])),
            TestPlayers::penningmeester(),
            $this->tableIdentifier
        )->withTrumpCard(new Card(12, Card::HEARTS));

        // Save with trump card.
        $this->readModel->savePersonalPlayerInfo($personalPlayerInfo);

        // Save without trump card.
        $this->readModel->savePersonalPlayerInfo(
            $personalPlayerInfo->withoutTrumpCard()
        );

        $retrieved = $this->readModel->getPersonalPlayerInfo(
            TestPlayers::penningmeester(),
            $this->tableIdentifier
        );

        $this->assertEquals($personalPlayerInfo->withoutTrumpCard(), $retrieved);
    }

    /** @test */
    public function itHidesCardsIfNotShownAnymore(): void
    {
        // See #113.

        $personalPlayerInfo = PersonalPlayerInfo::forPlayerAtTable(
            Hand::fromCards(new \ArrayIterator([
                new Card(12, Card::HEARTS),
            ])),
            TestPlayers::penningmeester(),
            $this->tableIdentifier
        )->withCardsOpen();

        // Save with trump card.
        $this->readModel->savePersonalPlayerInfo($personalPlayerInfo);

        $emptyHandedPlayerInfo = $personalPlayerInfo->emptyHanded();

        // Save without trump card.
        $this->readModel->savePersonalPlayerInfo(
            $emptyHandedPlayerInfo
        );

        $retrieved = $this->readModel->getPersonalPlayerInfo(
            TestPlayers::penningmeester(),
            $this->tableIdentifier
        );

        $this->assertEquals($emptyHandedPlayerInfo, $retrieved);
    }
}
