<?php

declare(strict_types=1);

namespace App\Tests\Integration\Orm\ReadModel;

use App\Domain\ReadModel\WelcomingTable\WelcomingTable;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Invitation\Invitations;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\ReadModel\WelcomingTableReadModel;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class WelcomingTableReadModelTest extends KernelTestCase
{
    /** @var WelcomingTableReadModel */
    private $readModel;

    /** @var TableIdentifier */
    private $tableIdentifier;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        /** @var WelcomingTableReadModel $readModel */
        $readModel = $this->getContainer()->get(WelcomingTableReadModel::class);
        $this->readModel = $readModel;

        $this->tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('eeb9c8b1-4026-4be7-bb3f-652c4e309ce6')
        );

        $readModel->removeWelcomingTable($this->tableIdentifier);
    }

    /** @test */
    public function itSavesWelcomingTable(): void
    {
        $welcomingTable = new WelcomingTable(
            $this->tableIdentifier,
            PlayerName::fromString('Frits'),
            new \DateTimeImmutable('2020-10-28'),
            new Invitations(
                Invitation::fromString('da06511c-18ca-42b7-bc8c-790ea6f4504c')
            ),
            3,
            CardGame::fromString('Wiezen'),
            Language::default()
        );

        $this->readModel->saveWelcomingTable($welcomingTable);
        $actual = $this->readModel->getWelcomingTable($this->tableIdentifier);

        $this->assertEquals($welcomingTable, $actual);
    }

    /** @test */
    public function itUpdatesWelcomingTable(): void
    {
        $welcomingTable = new WelcomingTable(
            $this->tableIdentifier,
            PlayerName::fromString('Frits'),
            new \DateTimeImmutable('2020-10-28'),
            new Invitations(
                Invitation::fromString('da06511c-18ca-42b7-bc8c-790ea6f4504c')
            ),
            3,
            CardGame::fromString('Wiezen'),
            Language::default()
        );

        $this->readModel->saveWelcomingTable($welcomingTable);
        $expected = $welcomingTable->playing(CardGame::fromString('Rikken'));
        $this->readModel->saveWelcomingTable($expected);
        $actual = $this->readModel->getWelcomingTable($this->tableIdentifier);

        $this->assertEquals($expected, $actual);
    }
}
