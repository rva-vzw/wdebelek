<?php

declare(strict_types=1);

namespace App\Tests\Integration\Orm\ReadModel;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Game\PlayedTrick;
use App\Domain\ValueObject\Game\PlayedTricks;
use App\Domain\ValueObject\Game\Trick;
use App\Domain\ValueObject\Player\Team;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\ReadModel\CollectedTricksReadModel;
use App\TestData\TestPlayers;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class CollectedTricksReadModelTest extends KernelTestCase
{
    private TableIdentifier $tableIdentifier;
    private GameIdentifier $gameIdentifier;
    private CollectedTricksReadModel $readModel;

    protected function setUp(): void
    {
        self::bootKernel();

        /** @var CollectedTricksReadModel $readModel */
        $readModel = $this->getContainer()->get(CollectedTricksReadModel::class);
        $this->readModel = $readModel;

        $this->tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('ce9ec9ac-7699-4ff0-b9a6-0be6d42bba98')
        );
        $this->gameIdentifier = GameIdentifier::fromNumber($this->tableIdentifier, GameNumber::first());

        $this->readModel->discardTricks($this->tableIdentifier);
    }

    /** @test */
    public function itRetrievesCollectedTricksByTeam(): void
    {
        $playedTrick1 = new PlayedTrick(
            Trick::empty()->withPlayedCard(TestPlayers::dtl(), new Card(1, Card::SPADES))
                ->withPlayedCard(TestPlayers::ddl(), new Card(2, Card::SPADES))
                ->withPlayedCard(TestPlayers::ddb(), new Card(5, Card::CLUBS))
                ->withPlayedCard(TestPlayers::secretaris(), new Card(4, Card::SPADES)),
            1,
            TestPlayers::dtl()
        );
        $playedTrick2 = new PlayedTrick(
            Trick::empty()->withPlayedCard(TestPlayers::dtl(), new Card(12, Card::SPADES))
                ->withPlayedCard(TestPlayers::ddl(), new Card(3, Card::SPADES))
                ->withPlayedCard(TestPlayers::ddb(), new Card(2, Card::HEARTS))
                ->withPlayedCard(TestPlayers::secretaris(), new Card(13, Card::SPADES)),
            2,
            TestPlayers::secretaris()
        );
        $this->readModel->collectTrick(
            $this->tableIdentifier,
            $this->gameIdentifier,
            $playedTrick1
        );
        $this->readModel->collectTrick(
            $this->tableIdentifier,
            $this->gameIdentifier,
            $playedTrick2
        );
        $this->readModel->discloseAllTricks($this->gameIdentifier);

        $actual = $this->readModel->getDisclosedTricks(
            $this->gameIdentifier,
            Team::withPlayers(TestPlayers::dtl(), TestPlayers::ddl())
        );
        $expected = PlayedTricks::empty()->withPlayedTrick($playedTrick1);

        $this->assertEquals($expected, $actual);
    }
}
