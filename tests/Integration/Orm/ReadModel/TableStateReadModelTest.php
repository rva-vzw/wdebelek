<?php

declare(strict_types=1);

namespace App\Tests\Integration\Orm\ReadModel;

use App\Domain\ReadModel\TableState\TableState;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\ReadModel\TableStateReadModel;
use App\TestData\TestPlayers;
use App\Tests\Integration\AbstractKrakBoemIntegrationTest;

final class TableStateReadModelTest extends AbstractKrakBoemIntegrationTest
{
    private TableStateReadModel $readModel;
    private GameIdentifier $gameIdentifier;
    private TableIdentifier $tableIdentifier;

    protected function setUp(): void
    {
        parent::setUp();

        $this->tableIdentifier = TableIdentifier::withSecret(Secret::fromString('2af6c346-db38-4ee3-bab2-cd0bf07d95af'));

        $this->gameIdentifier = GameIdentifier::fromNumber($this->tableIdentifier, GameNumber::first());

        /** @var TableStateReadModel $readModel */
        $readModel = $this->getContainer()->get(TableStateReadModel::class);
        $this->readModel = $readModel;
        $this->readModel->deleteTableState($this->tableIdentifier);

        $this->readModel->saveTableState(
            TableState::forNewTable($this->tableIdentifier, TestPlayers::ddb())
        );
        $this->readModel->prepareGame(
            $this->tableIdentifier,
            $this->gameIdentifier,
            TestPlayers::dpb(),
            TestPlayers::dpb()
        );

        $this->readModel->saveTableState(
            TableState::forNewTable($this->tableIdentifier, TestPlayers::dpb())->forNewGame(
                $this->gameIdentifier,
                new SimplePlayerIdentifiers(
                    TestPlayers::ddb(),
                    TestPlayers::penningmeester(),
                    TestPlayers::secretaris(),
                    TestPlayers::dpb()
                ),
                52,
                TestPlayers::dpb()
            )
        );
    }

    /** @test */
    public function itRetrievesTableStateBeforeGameStarts(): void
    {
        $tableState = $this->readModel->getTableStateByGame($this->gameIdentifier);

        $this->assertInstanceOf(TableState::class, $tableState);
    }

    /** @test */
    public function itUpdatesTrickNumber(): void
    {
        $this->readModel->logTrick($this->gameIdentifier, 1, TestPlayers::penningmeester());

        $state = $this->readModel->getTableStateByGame($this->gameIdentifier);

        $this->assertEquals(2, $state->getTrickNumber());
    }

    /** @test */
    public function itSavesATableState(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('b00e0002-4bf5-48e7-8691-39a5c80f9dd1'));
        $gameIdentifier = GameIdentifier::fromNumber($tableIdentifier, GameNumber::first());

        $tableState = TableState::forNewTable(
            $tableIdentifier,
            TestPlayers::penningmeester()
        )->forNewGame($gameIdentifier, new SimplePlayerIdentifiers(TestPlayers::penningmeester(), TestPlayers::secretaris()), 52, TestPlayers::secretaris())
        ->playing(CardGame::soloWhist());

        $this->readModel->saveTableState($tableState);
        $retrieved = $this->readModel->getTableState($tableIdentifier);

        $this->assertEquals($tableState, $retrieved);
    }

    /** @test */
    public function itSavesInactiveTableState(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('b00e0002-4bf5-48e7-8691-39a5c80f9dd1'));

        $tableState = TableState::forNewTable(
            $tableIdentifier,
            TestPlayers::penningmeester()
        );

        $this->readModel->saveTableState($tableState);
        $retrieved = $this->readModel->getTableState($tableIdentifier);

        $this->assertEquals($tableState, $retrieved);
    }

    /** @test */
    public function itDoesntClearFirstGameWhenFirstGamePrepared(): void
    {
        $tableState = $this->readModel->getTableState($this->tableIdentifier);

        $this->assertTrue($tableState->isFirstGame());
    }

    /** @test */
    public function itSavesTableStateWithUndisclosedTricks(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('eecb1d31-7d71-4baf-9da8-ee6a8674563c'));
        $tableState = TableState::forNewTable($tableIdentifier, TestPlayers::penningmeester())->withAllCollectedTricksDisclosed();
        $this->readModel->saveTableState($tableState);

        $tableState = $this->readModel->getTableState($tableIdentifier);
        $this->assertTrue($tableState->isCollectedTricksDisclosed());

        $tableState = $tableState->withNoCollectedTricksDisclosed();
        $this->readModel->saveTableState($tableState);

        $tableState = $this->readModel->getTableState($tableIdentifier);
        $this->assertFalse($tableState->isCollectedTricksDisclosed());
    }
}
