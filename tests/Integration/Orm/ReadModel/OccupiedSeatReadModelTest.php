<?php

declare(strict_types=1);

namespace App\Tests\Integration\Orm\ReadModel;

use App\Domain\ReadModel\Exception\NotFoundInReadModel;
use App\Domain\ReadModel\OccupiedSeats\OccupiedSeat;
use App\Domain\ReadModel\PlayerProfile\SimplePlayerProfile;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\Exception\PlayerNotFound;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Player\SimpleNamedPlayer;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\ReadModel\OccupiedSeatReadModel;
use App\TestData\TestPlayers;
use App\Tests\Integration\AbstractKrakBoemIntegrationTest;

final class OccupiedSeatReadModelTest extends AbstractKrakBoemIntegrationTest
{
    /** @var OccupiedSeatReadModel */
    private $readModel;

    /** @var TableIdentifier */
    private $tableIdentifier;

    protected function setUp(): void
    {
        parent::setUp();

        /** @var OccupiedSeatReadModel $readModel */
        $readModel = $this->getContainer()->get(OccupiedSeatReadModel::class);
        $this->readModel = $readModel;

        $this->tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('42d7a0c8-3bcb-472b-8c1d-7e0d5898e5e9')
        );

        $readModel->deleteByTable($this->tableIdentifier);
    }

    /** @test */
    public function itSavesInvitation(): void
    {
        $occupiedSeat = OccupiedSeat::invited(
            $this->tableIdentifier,
            'https://test.rijkvanafdronk.be',
            Seat::fromInteger(1),
            Invitation::fromString('a4f8dbce-7076-4448-a491-328c26f25136'),
            PlayerName::fromString('dpb'),
            Language::default()
        );

        $this->readModel->saveOccupiedSeat(
            $occupiedSeat
        );

        $seats = iterator_to_array($this->readModel->getOccupiedSeats($this->tableIdentifier));

        $this->assertEquals($occupiedSeat, $seats[0]);
    }

    /** @test */
    public function itSavesInitialPlayer(): void
    {
        $occupiedSeat = OccupiedSeat::initial(
            $this->tableIdentifier,
            new SimplePlayerProfile(
                TestPlayers::penningmeester(),
                PlayerName::fromString('penningmeester'),
                Language::default()
            )
        );

        $this->readModel->saveOccupiedSeat(
            $occupiedSeat
        );

        $seats = iterator_to_array($this->readModel->getOccupiedSeats($this->tableIdentifier));

        $this->assertEquals($occupiedSeat, $seats[0]);
    }

    /** @test */
    public function itRetrievesPlayerName(): void
    {
        $occupiedSeat = OccupiedSeat::initial(
            $this->tableIdentifier,
            new SimplePlayerProfile(
                TestPlayers::penningmeester(),
                PlayerName::fromString('penningmeester'),
                Language::default()
            )
        );

        $this->readModel->saveOccupiedSeat(
            $occupiedSeat
        );

        $actual = iterator_to_array($this->readModel->getPlayersAtTable($this->tableIdentifier));

        $expected = [
            new SimpleNamedPlayer(TestPlayers::penningmeester(), PlayerName::fromString('penningmeester')),
        ];

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itKicksAcceptedPlayer(): void
    {
        $occupiedSeat = OccupiedSeat::accepted(
            $this->tableIdentifier,
            new SimplePlayerProfile(
                TestPlayers::penningmeester(),
                PlayerName::fromString('penningmeester'),
                Language::default()
            ),
            PlayerName::fromString('dpb'),
            Seat::fromInteger(1)
        );

        $this->readModel->saveOccupiedSeat(
            $occupiedSeat
        );
        $this->readModel->delete($occupiedSeat);

        $this->expectException(PlayerNotFound::class);
        $this->readModel->getSeatByPlayer($this->tableIdentifier, TestPlayers::penningmeester());
    }

    /** @test */
    public function itPublishesInvitation(): void
    {
        $occupiedSeat = OccupiedSeat::invited(
            $this->tableIdentifier,
            'https://test.rijkvanafdronk.be',
            Seat::fromInteger(1),
            Invitation::fromString('a4f8dbce-7076-4448-a491-328c26f25136'),
            PlayerName::fromString('dpb'),
            Language::default()
        );

        $this->readModel->saveOccupiedSeat(
            $occupiedSeat
        );

        $occupiedSeat = $occupiedSeat->withPublishedInvitation();

        $this->readModel->saveOccupiedSeat($occupiedSeat);

        $actual = $this->readModel->getSeatByOpenInvitation(
            $this->tableIdentifier,
            Invitation::fromString('a4f8dbce-7076-4448-a491-328c26f25136')
        );

        $this->assertEquals($occupiedSeat, $actual);
    }

    /** @test */
    public function itOnlyGetsByOpenInvitation(): void
    {
        $invitation = Invitation::fromString('a4f8dbce-7076-4448-a491-328c26f25136');
        $occupiedSeat = OccupiedSeat::invited(
            $this->tableIdentifier,
            'https://test.rijkvanafdronk.be',
            Seat::fromInteger(1),
            $invitation,
            PlayerName::fromString('dpb'),
            Language::default()
        )->acceptedBy(
            TestPlayers::diu(),
            PlayerName::fromString('diu')
        );
        $this->readModel->saveOccupiedSeat($occupiedSeat);

        $this->expectException(NotFoundInReadModel::class);
        $this->readModel->getSeatByOpenInvitation($this->tableIdentifier, $invitation);
    }
}
