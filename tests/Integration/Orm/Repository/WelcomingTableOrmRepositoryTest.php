<?php

declare(strict_types=1);

namespace App\Tests\Integration\Orm\Repository;

use App\Domain\ReadModel\WelcomingTable\WelcomingTable;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Invitation\Invitations;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Entity\OrmWelcomingTable;
use App\Orm\Repository\WelcomingTableOrmRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class WelcomingTableOrmRepositoryTest extends KernelTestCase
{
    /** @var WelcomingTableOrmRepository */
    private $repository;

    /** @var TableIdentifier */
    private $tableIdentifier;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        /** @var WelcomingTableOrmRepository $repository */
        $repository = $this->getContainer()->get(WelcomingTableOrmRepository::class);
        $this->repository = $repository;

        $this->tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('57cd1e30-2fab-49b3-b8d6-3f7fd8e7d244')
        );

        $this->repository->remove($this->tableIdentifier);
    }

    /** @test */
    public function itRetrievesRecentWelcomingTables(): void
    {
        $welcomingTable = new WelcomingTable(
            $this->tableIdentifier,
            PlayerName::fromString('Frits'),
            new \DateTimeImmutable('2020-10-28'),
            new Invitations(
                Invitation::fromString('da06511c-18ca-42b7-bc8c-790ea6f4504c')
            ),
            3,
            CardGame::fromString('Wiezen'),
            Language::default()
        );

        $this->repository->save(OrmWelcomingTable::fromWelcomingTable($welcomingTable));

        $tables = iterator_to_array(
            $this->repository->getRecentWelcomingTables(new \DateTimeImmutable('2020-10-28'))
        );

        $actual = array_filter(
            $tables,
            function (WelcomingTable $t): bool {
                return $t->getTableIdentifier() == $this->tableIdentifier;
            }
        );

        $this->assertEquals([$welcomingTable], $actual);
    }
}
