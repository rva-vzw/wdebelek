<?php

declare(strict_types=1);

namespace App\Tests\Integration\Orm\Repository;

use App\Domain\ReadModel\GameCounter\GameCounter;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Repository\GameCounterOrmRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class GameCounterOrmRepositoryTest extends KernelTestCase
{
    /** @var GameCounterOrmRepository */
    private $repostitory;

    /** @var TableIdentifier */
    private $tableIdentifier;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        /** @var GameCounterOrmRepository $repository */
        $repository = $this->getContainer()->get(GameCounterOrmRepository::class);
        $this->repostitory = $repository;

        $this->tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('e8800b3c-ff5e-4bf7-b17d-a63cf549be09')
        );

        $this->repostitory->removeForTable($this->tableIdentifier);
    }

    /** @test */
    public function itUpdatesExistingGameCounter(): void
    {
        $gameCounter = GameCounter::createNew($this->tableIdentifier, new \DateTimeImmutable('2020-02-11'));
        $this->repostitory->saveGameCounter($gameCounter);
        $gameCounter = $gameCounter->incrementedAt(new \DateTimeImmutable('2020-02-11'));
        $this->repostitory->saveGameCounter($gameCounter);

        $retrievedCounter = $this->repostitory->findCounterForTable($this->tableIdentifier);
        $this->assertEquals($gameCounter, $retrievedCounter);
    }
}
