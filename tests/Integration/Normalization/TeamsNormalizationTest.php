<?php

declare(strict_types=1);

namespace App\Tests\Integration\Normalization;

use App\Domain\ValueObject\Player\Team;
use App\Domain\ValueObject\Player\Teams;
use App\TestData\TestPlayers;

final class TeamsNormalizationTest extends AbstractNormalizationTest
{
    /** @test */
    public function itNormalizesAndDenormalizesTeams(): void
    {
        $teams = Teams::empty()
            ->withAdditionalTeam(Team::withPlayers(
                TestPlayers::secretaris(),
                TestPlayers::ddb()
            ))
            ->withAdditionalTeam(Team::withPlayers(
                TestPlayers::penningmeester(),
                TestPlayers::dtl()
            ));

        $normalized = $this->normalizer->normalize($teams);
        $denormalized = $this->denormalizer->denormalize($normalized, Teams::class);

        $this->assertEquals($teams, $denormalized);
    }
}
