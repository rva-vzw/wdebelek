<?php

declare(strict_types=1);

namespace App\Tests\Integration\Normalization;

use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\WriteModel\Table\Event\GameChosen;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestPlayers;

final class TableEventNormalizationTest extends AbstractNormalizationTest
{
    /** @test */
    public function itNormalizesGameChosen(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('5d4128ad-194b-455f-afbe-d888313bbeba'));

        $event = new GameChosen(
            $tableIdentifier,
            CardGame::fromString('Traditional Whist'),
            TestPlayers::secretaris()
        );

        $normalized = $this->normalizer->normalize($event);
        $denormalized = $this->denormalizer->denormalize($normalized, GameChosen::class);

        $this->assertEquals($event, $denormalized);
    }
}
