<?php

declare(strict_types=1);

namespace App\Tests\Integration\Normalization;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\Hand;
use App\Domain\ValueObject\Card\Hands;
use App\TestData\TestPlayers;

final class HandNormalizationTest extends AbstractNormalizationTest
{
    /** @test */
    public function itNormalizesHand(): void
    {
        $hand = Hand::fromCards(new \ArrayIterator([
            new Card(1, Card::SPADES),
            new Card(2, Card::SPADES),
        ]));

        $normalizedHand = $this->normalizer->normalize($hand);
        $expected = [1, 2];

        $this->assertEquals($expected, $normalizedHand);
    }

    /** @test */
    public function itNormalizesHands(): void
    {
        $hand1 = Hand::fromCards(new \ArrayIterator([
            new Card(1, Card::SPADES),
            new Card(2, Card::SPADES),
        ]));

        $hand2 = Hand::fromCards(new \ArrayIterator([
            new Card(1, Card::HEARTS),
            new Card(2, Card::HEARTS),
        ]));

        $hands = Hands::empty()
            ->withHand(TestPlayers::penningmeester(), $hand1)
            ->withHand(TestPlayers::secretaris(), $hand2);

        $normalizedHand = $this->normalizer->normalize($hands);
        $expected = [
            TestPlayers::penningmeester()->toString() => [1, 2],
            TestPlayers::secretaris()->toString() => [14, 15],
        ];

        $this->assertEquals($expected, $normalizedHand);
    }
}
