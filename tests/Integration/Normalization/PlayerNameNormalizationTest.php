<?php

declare(strict_types=1);

namespace App\Tests\Integration\Normalization;

use App\Domain\ValueObject\Player\PlayerName;

final class PlayerNameNormalizationTest extends AbstractNormalizationTest
{
    /** @test */
    public function itNormalizesPlayerName(): void
    {
        $playerName = PlayerName::fromString('jeroen');
        $normalized = $this->normalizer->normalize($playerName);

        $this->assertEquals('jeroen', $normalized);
    }
}
