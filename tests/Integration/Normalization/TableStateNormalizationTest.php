<?php

declare(strict_types=1);

namespace App\Tests\Integration\Normalization;

use App\Domain\ReadModel\TableState\TableState;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestPlayers;

final class TableStateNormalizationTest extends AbstractNormalizationTest
{
    /** @test */
    public function itNormalizesTableState(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('4395fc46-6892-4909-a53c-2b115b6b5e12'));

        $tableState = TableState::forNewTable($tableIdentifier, TestPlayers::penningmeester())->forNewGame(
            GameIdentifier::fromNumber(
                $tableIdentifier,
                GameNumber::first()
            ),
            new SimplePlayerIdentifiers(
                TestPlayers::penningmeester(),
                TestPlayers::secretaris(),
                TestPlayers::dtl(),
                TestPlayers::ddb()
            ),
            52,
            TestPlayers::ddb()
        )->waitingForDealer();

        $normalized = $this->normalizer->normalize($tableState);
        $denormalized = $this->denormalizer->denormalize($normalized, TableState::class);

        $this->assertEquals($tableState, $denormalized);
    }
}
