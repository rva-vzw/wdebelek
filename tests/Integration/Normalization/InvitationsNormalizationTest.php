<?php

declare(strict_types=1);

namespace App\Tests\Integration\Normalization;

use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Invitation\Invitations;

final class InvitationsNormalizationTest extends AbstractNormalizationTest
{
    /** @test */
    public function itNormalizesInvitations(): void
    {
        $invitations = new Invitations(
            Invitation::fromString('146c9e58-65dc-4790-a2f5-0626a2df9b11'),
            Invitation::fromString('a123fd84-dbfe-4a44-be16-f3eab3c3be5f')
        );

        $normalized = $this->normalizer->normalize($invitations);
        $this->assertEquals(
            ['146c9e58-65dc-4790-a2f5-0626a2df9b11', 'a123fd84-dbfe-4a44-be16-f3eab3c3be5f'],
            $normalized
        );
        $denormalized = $this->denormalizer->denormalize($normalized, Invitations::class);

        $this->assertEquals($invitations, $denormalized);
    }
}
