<?php

declare(strict_types=1);

namespace App\Tests\Integration\Normalization;

use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\Event\GameStarted;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestPlayers;

final class GameEventNormalizationTest extends AbstractNormalizationTest
{
    /** @test */
    public function itNormalizesGameStarted(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('17c33099-c2f6-4b85-8f62-c9ee5f18fba7'));
        $gameIdentifier = GameIdentifier::fromNumber($tableIdentifier, GameNumber::first());

        $event = new GameStarted(
            $gameIdentifier,
            new SimplePlayerIdentifiers(
                TestPlayers::ddb(),
                TestPlayers::secretaris(),
                TestPlayers::dpb(),
                TestPlayers::dtl()
            ),
            SimplePileOfCards::complete(),
            $tableIdentifier,
            TestPlayers::dtl(),
            TestPlayers::dtl(),
            new \DateTimeImmutable('2021-01-07')
        );

        $normalized = $this->normalizer->normalize($event);
        $denormalized = $this->denormalizer->denormalize($normalized, GameStarted::class);

        $this->assertEquals($event, $denormalized);
    }
}
