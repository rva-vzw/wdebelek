<?php

declare(strict_types=1);

namespace App\Tests\Integration\Normalization;

use App\Domain\ReadModel\PlayingPlayer\PlayingPlayer;
use App\Domain\ReadModel\SeatedPlayer\SimpleSeatedPlayer;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestProfiles;

final class PlayingPlayerNormalizationTest extends AbstractNormalizationTest
{
    /** @test */
    public function itNormalizesFellowPlayer(): void
    {
        $fellowPlayer = PlayingPlayer::create(
            TableIdentifier::withSecret(Secret::fromString('e20cbb11-a308-46f6-ab01-9c4e0e98557b')),
            SimpleSeatedPlayer::fromProfile(
                TestProfiles::penningmeester(),
                Seat::zero()
            )
        );

        $normalized = $this->normalizer->normalize($fellowPlayer);
        $denormalized = $this->denormalizer->denormalize($normalized, PlayingPlayer::class);

        $this->assertEquals($fellowPlayer, $denormalized);
    }
}
