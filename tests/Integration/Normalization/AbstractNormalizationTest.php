<?php

declare(strict_types=1);

namespace App\Tests\Integration\Normalization;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

abstract class AbstractNormalizationTest extends KernelTestCase
{
    /** @var NormalizerInterface */
    protected $normalizer;
    /** @var DenormalizerInterface */
    protected $denormalizer;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        /** @var NormalizerInterface $normalizer */
        $normalizer = $this->getContainer()->get(NormalizerInterface::class);
        $this->normalizer = $normalizer;
        /** @var DenormalizerInterface $denormalizer */
        $denormalizer = $this->getContainer()->get(DenormalizerInterface::class);
        $this->denormalizer = $denormalizer;
    }
}
