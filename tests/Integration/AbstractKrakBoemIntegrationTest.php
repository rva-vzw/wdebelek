<?php

declare(strict_types=1);

namespace App\Tests\Integration;

use RvaVzw\KrakBoem\EventSourcing\EventBus\EventBus;
use RvaVzw\KrakBoem\EventSourcing\EventStore\EventStore;
use RvaVzw\KrakBoem\EventSourcing\EventStore\EventStoreHacks;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class AbstractKrakBoemIntegrationTest extends KernelTestCase
{
    /** @var EventStore */
    protected $eventStore;

    /** @var EventBus */
    protected $eventBus;

    /** @var EventStoreHacks */
    protected $eventStoreHacks;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        /** @var EventStore $eventStore */
        $eventStore = $this->getContainer()->get(EventStore::class);
        $this->eventStore = $eventStore;

        /** @var EventBus $bus */
        $bus = $this->getContainer()->get(EventBus::class);
        $this->eventBus = $bus;

        /** @var EventStoreHacks $eventStoreHacks */
        $eventStoreHacks = $this->getContainer()->get(EventStoreHacks::class);
        $this->eventStoreHacks = $eventStoreHacks;
    }
}
