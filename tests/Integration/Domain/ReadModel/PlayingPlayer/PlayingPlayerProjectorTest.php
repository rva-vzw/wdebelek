<?php

declare(strict_types=1);

namespace App\Tests\Integration\Domain\ReadModel\PlayingPlayer;

use App\Domain\ReadModel\Exception\NotFoundInReadModel;
use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\ReadModel\PlayingPlayer\PlayingPlayer;
use App\Domain\ReadModel\PlayingPlayer\PlayingPlayerProjector;
use App\Domain\ReadModel\PlayingPlayer\PlayingPlayerRepository;
use App\Domain\ReadModel\SeatedPlayer\SeatedPlayer;
use App\Domain\ReadModel\SeatedPlayer\SeatedPlayers;
use App\Domain\ReadModel\SeatedPlayer\SimpleSeatedPlayer;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Game\Event\CardPlayed;
use App\Domain\WriteModel\Game\Event\CardsReceived;
use App\Domain\WriteModel\Game\Event\GameStarted;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestPlayers;
use App\TestData\TestProfiles;
use App\Tests\Integration\AbstractKrakBoemIntegrationTest;

final class PlayingPlayerProjectorTest extends AbstractKrakBoemIntegrationTest
{
    /** @var PlayingPlayerProjector */
    private $projector;
    /** @var PlayingPlayerRepository */
    private $repository;
    /** @var TableIdentifier */
    private $tableIdentifier;

    protected function setUp(): void
    {
        parent::setUp();

        $this->tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('81a30af3-e6c2-4063-98b9-0ff874c98ac9')
        );

        /** @var PlayingPlayerRepository $fellowPlayerOrmRepository */
        $fellowPlayerOrmRepository = $this->getContainer()->get(PlayingPlayerRepository::class);

        $this->repository = $fellowPlayerOrmRepository;
        $this->repository->deleteForTable($this->tableIdentifier);
        $this->projector = new PlayingPlayerProjector(
            $this->repository,
            $this->getSeatedPlayers()
        );
    }

    /** @test */
    public function itCountsReceivedCards(): void
    {
        $gameIdentifier = GameIdentifier::fromNumber(
            $this->tableIdentifier,
            GameNumber::first()
        );
        $this->projector->__invoke(
            new GameStarted(
                $gameIdentifier,
                new SimplePlayerIdentifiers(
                    TestPlayers::ddb(),
                    TestPlayers::penningmeester(),
                    TestPlayers::secretaris(),
                    TestPlayers::dpb()
                ),
                SimplePileOfCards::complete(),
                $this->tableIdentifier,
                TestPlayers::dpb(),
                TestPlayers::ddb(),
                new \DateTimeImmutable('2020-01-07')
            )
        );
        $this->projector->__invoke(new CardsReceived(
            $gameIdentifier,
            TestPlayers::ddb(),
            SimplePileOfCards::complete()->getFirstCards(8),
            $this->tableIdentifier
        ));

        $actual = $this->repository->getPlayingPlayer(
            $this->tableIdentifier,
            TestPlayers::ddb()
        );

        $expected = PlayingPlayer::create(
            $this->tableIdentifier,
            SimpleSeatedPlayer::fromProfile(
                TestProfiles::ddb(),
                Seat::zero()
            )
        )->withAdditionalCards(8);

        $this->assertEquals($expected, $actual);
    }

    public function getSeatedPlayers(): SeatedPlayers
    {
        return new class() implements SeatedPlayers {
            /** @var array<string,SeatedPlayer> */
            private $playersArray;

            public function __construct()
            {
                /** @var array<int, PlayerProfile> $allProfiles */
                $allProfiles = [
                    TestProfiles::ddb(),
                    TestProfiles::penningmeester(),
                    TestProfiles::secretaris(),
                    TestProfiles::dpb(),
                ];

                foreach ($allProfiles as $seatNr => $profile) {
                    $this->playersArray[$profile->getPlayerIdentifier()->toString()] = new SimpleSeatedPlayer(
                        $profile->getPlayerIdentifier(),
                        $profile->getPlayerName(),
                        $profile->getLanguage(),
                        Seat::fromInteger($seatNr)
                    );
                }
            }

            public function getSeatedPlayer(
                TableIdentifier $tableIdentifier,
                PlayerIdentifier $playerIdentifier
            ): SeatedPlayer {
                $key = $playerIdentifier->toString();
                if (array_key_exists($key, $this->playersArray)) {
                    return $this->playersArray[$key];
                }
                throw NotFoundInReadModel::create(PlayerProfile::class, $playerIdentifier);
            }
        };
    }

    /** @test */
    public function itRemovesRevealedTrumpWhenTrumpCardThrown(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('ec0cee14-c29b-44b1-b8e5-fe369057351b'));
        $card = new Card(1, Card::CLUBS);
        $playingPlayer = PlayingPlayer::create(
            $tableIdentifier,
            SimpleSeatedPlayer::fromProfile(
                TestProfiles::ddb(),
                Seat::zero()
            )
        )->withAdditionalCards(13)->withTrumpRevealed(
            $card
        );

        $this->repository->savePlayingPlayer(
            $playingPlayer
        );

        $this->projector->__invoke(new CardPlayed(
            GameIdentifier::fromNumber($tableIdentifier, GameNumber::first()),
            TestPlayers::ddb(),
            $card,
            1,
            $tableIdentifier
        ));

        $retrievedPlayer = $this->repository->getPlayingPlayer($tableIdentifier, TestPlayers::ddb());

        $this->assertNull($retrievedPlayer->getShownTrump());
    }
}
