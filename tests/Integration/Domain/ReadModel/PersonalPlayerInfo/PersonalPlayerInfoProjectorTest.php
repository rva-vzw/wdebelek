<?php

declare(strict_types=1);

namespace App\Tests\Integration\Domain\ReadModel\PersonalPlayerInfo;

use App\Domain\ReadModel\PersonalPlayerInfo\PersonalPlayerInfoProjector;
use App\Domain\ReadModel\PlayerProfile\SimplePlayerProfile;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\Hand;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Game\Event\CardPlayed;
use App\Domain\WriteModel\Game\Event\CardsReceived;
use App\Domain\WriteModel\Game\Event\GameStarted;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\Event\InvitationAccepted;
use App\Domain\WriteModel\Table\Event\TableClaimed;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\ReadModel\PersonalPlayerInfoReadModel;
use App\TestData\TestPlayers;
use App\Tests\Integration\AbstractKrakBoemIntegrationTest;

final class PersonalPlayerInfoProjectorTest extends AbstractKrakBoemIntegrationTest
{
    /** @var PersonalPlayerInfoReadModel */
    private $readModel;

    /** @var PersonalPlayerInfoProjector */
    private $projector;

    /** @var TableIdentifier */
    private $tableIdentifier;

    protected function setUp(): void
    {
        parent::setUp();

        $this->tableIdentifier = $tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('a52058ae-8571-42e7-b5c0-2ee8a2b515f1')
        );

        /** @var PersonalPlayerInfoReadModel $readModel */
        $readModel = $this->getContainer()->get(PersonalPlayerInfoReadModel::class);
        $this->readModel = $readModel;
        $this->readModel->forgetTable($tableIdentifier);

        // FIXME: Maybe I should split up PersonalPlayerInfoReadModel. Or maybe I should get rid of one of the interfaces.
        $this->projector = new PersonalPlayerInfoProjector($readModel, $readModel);
    }

    /** @test */
    public function itRemovesPlayedCardFromHand(): void
    {
        $gameIdentifier = GameIdentifier::fromNumber($this->tableIdentifier, GameNumber::first());

        $this->projector->__invoke(
            new TableClaimed(
                $this->tableIdentifier,
                TestPlayers::secretaris(),
                PlayerName::fromString('secr'),
                Seat::zero(),
                Language::default()
            )
        );
        $this->projector->__invoke(
            InvitationAccepted::byPlayer(
                $this->tableIdentifier,
                Invitation::fromString('e8cb7939-a9c3-4616-80be-5f18b467ac1b'),
                Seat::fromInteger(1),
                new SimplePlayerProfile(
                    TestPlayers::penningmeester(),
                    PlayerName::fromString('penn'),
                    Language::default()
                )
            )
        );
        $this->projector->__invoke(
            new GameStarted(
                $gameIdentifier,
                new SimplePlayerIdentifiers(
                    TestPlayers::penningmeester(),
                    TestPlayers::secretaris()
                ),
                SimplePileOfCards::complete(),
                $this->tableIdentifier,
                TestPlayers::penningmeester(),
                TestPlayers::penningmeester(),
                new \DateTimeImmutable('2021-01-07')
            )
        );
        $this->projector->__invoke(
            new CardsReceived(
                $gameIdentifier,
                TestPlayers::penningmeester(),
                SimplePileOfCards::fromCards(new \ArrayIterator([
                    new Card(1, Card::HEARTS),
                    new Card(2, Card::HEARTS),
                    new Card(3, Card::HEARTS),
                ])),
                $this->tableIdentifier
            )
        );

        $this->projector->__invoke(
            new CardPlayed(
                $gameIdentifier,
                TestPlayers::penningmeester(),
                new Card(2, Card::HEARTS),
                1,
                $this->tableIdentifier
            )
        );

        $expectedHand = Hand::fromCards(new \ArrayIterator([
                    new Card(1, Card::HEARTS),
                    new Card(3, Card::HEARTS),
        ]));
        $actualHand = $this->readModel->getPlayerHand(
            TestPlayers::penningmeester(),
            $this->tableIdentifier,
            $gameIdentifier
        );

        $this->assertEquals($expectedHand, $actualHand);
    }
}
