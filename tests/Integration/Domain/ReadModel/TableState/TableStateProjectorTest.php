<?php

declare(strict_types=1);

namespace App\Tests\Integration\Domain\ReadModel\TableState;

use App\Domain\ReadModel\TableState\TableState;
use App\Domain\ReadModel\TableState\TableStateProjector;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Game\Trick;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Player\TableMates;
use App\Domain\ValueObject\Player\Teams;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Game\Event\CardPlayed;
use App\Domain\WriteModel\Game\Event\CardsReceived;
use App\Domain\WriteModel\Game\Event\GameStarted;
use App\Domain\WriteModel\Game\Event\NonPlayingTrumpRevealed;
use App\Domain\WriteModel\Game\Game;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\Event\GameChosen;
use App\Domain\WriteModel\Table\Event\GamePrepared;
use App\Domain\WriteModel\Table\Event\TableClaimed;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\ReadModel\TableStateReadModel;
use App\TestData\TestPlayers;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class TableStateProjectorTest extends KernelTestCase
{
    /** @var TableStateReadModel */
    private $readModel;

    /** @var TableStateProjector */
    private $projector;

    /** @var TableIdentifier */
    private $tableIdentifier;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        /** @var TableStateReadModel $readModel */
        $readModel = $this->getContainer()->get(TableStateReadModel::class);
        $this->readModel = $readModel;

        // TODO (#51): get rid of the ReadModelUpdater interface.
        $this->projector = new TableStateProjector($readModel, $readModel);

        $this->tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('a52058ae-8571-42e7-b5c0-2ee8a2b515f1')
        );
        $this->readModel->deleteTableState($this->tableIdentifier);
    }

    /** @test */
    public function itUpdatesGameIdentifierWhenGameStarts(): void
    {
        // We assume that a table exists before the first game is started.
        // (This is mainly necessary to configure the table's language)
        $this->projector->__invoke(
            new TableClaimed(
                $this->tableIdentifier,
                TestPlayers::penningmeester(),
                PlayerName::fromString('penningmeester'),
                Seat::zero(),
                Language::default()
            )
        );

        // Start the first game.
        $this->projector->__invoke(
            new GameStarted(
                GameIdentifier::fromNumber($this->tableIdentifier, GameNumber::first()),
                new SimplePlayerIdentifiers(
                    TestPlayers::penningmeester(),
                    TestPlayers::secretaris(),
                    TestPlayers::dtl(),
                    TestPlayers::ddb()
                ),
                SimplePileOfCards::complete(),
                $this->tableIdentifier,
                TestPlayers::ddb(),
                TestPlayers::ddb(),
                new \DateTimeImmutable('2021-01-07 20:56')
            )
        );

        // start 2nd game
        $this->projector->__invoke(
            new GameStarted(
                GameIdentifier::fromNumber($this->tableIdentifier, GameNumber::second()),
                new SimplePlayerIdentifiers(
                    TestPlayers::penningmeester(),
                    TestPlayers::secretaris(),
                    TestPlayers::dtl(),
                    TestPlayers::ddb()
                ),
                SimplePileOfCards::complete(),
                $this->tableIdentifier,
                TestPlayers::penningmeester(),
                TestPlayers::penningmeester(),
                new \DateTimeImmutable('2021-01-07 21:05')
            )
        );

        $actual = $this->readModel->getTableState($this->tableIdentifier);
        $expected = TableState::forNewTable($this->tableIdentifier, TestPlayers::penningmeester())->forNewGame(
            GameIdentifier::fromNumber($this->tableIdentifier, GameNumber::second()),
            new SimplePlayerIdentifiers(
                TestPlayers::secretaris(),
                TestPlayers::dtl(),
                TestPlayers::ddb(),
                TestPlayers::penningmeester(),
            ),
            52,
            TestPlayers::penningmeester()
        );

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itSavesPlayedCard(): void
    {
        $gameIdentifier = GameIdentifier::fromNumber($this->tableIdentifier, GameNumber::first());

        $playerIdentifiers = new SimplePlayerIdentifiers(
            TestPlayers::penningmeester(),
            TestPlayers::secretaris(),
            TestPlayers::dtl(),
            TestPlayers::ddb()
        );

        $this->projector->__invoke(
            new TableClaimed(
                $this->tableIdentifier,
                TestPlayers::penningmeester(),
                PlayerName::fromString('penn'),
                Seat::zero(),
                Language::default()
            )
        );
        $this->projector->__invoke(
            new GameStarted(
                $gameIdentifier,
                $playerIdentifiers,
                SimplePileOfCards::complete(),
                $this->tableIdentifier,
                TestPlayers::ddb(),
                TestPlayers::ddb(),
                new \DateTimeImmutable('2021-01-07')
            )
        );
        $this->projector->__invoke(
            new CardsReceived(
                $gameIdentifier,
                TestPlayers::penningmeester(),
                SimplePileOfCards::empty()->with(new Card(12, Card::HEARTS)),
                $this->tableIdentifier
            )
        );
        $this->projector->__invoke(
            new CardPlayed(
                $gameIdentifier,
                TestPlayers::penningmeester(),
                new Card(12, Card::HEARTS),
                1,
                $this->tableIdentifier
            )
        );

        $actual = $this->readModel->getTableState($this->tableIdentifier);

        $expected = new TableState(
            $gameIdentifier,
            1,
            $this->tableIdentifier,
            0,
            0,
            Teams::allIndividual($playerIdentifiers),
            CardGame::unspecified(),
            false,
            null,
            false,
            TestPlayers::ddb(),
            TestPlayers::ddb(),
            52,
            false
        );

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itKeepsSuspendedTableSuspendedWhenChoosingCardGame(): void
    {
        $tableState = TableState::forNewTable(
            $this->tableIdentifier,
            TestPlayers::penningmeester()
        )->asSuspended();

        $this->readModel->saveTableState(
            $tableState
        );

        $this->projector->__invoke(
            new GameChosen(
                $this->tableIdentifier,
                CardGame::fromString('wiezen'),
                TestPlayers::penningmeester()
            )
        );

        $actual = $this->readModel->getTableState($this->tableIdentifier);

        $this->assertEquals(
            $tableState->playing(CardGame::fromString('wiezen')),
            $actual
        );
    }

    /** @test */
    public function itSavesNonPlayingTrump(): void
    {
        $gameIdentifier = GameIdentifier::fromNumber($this->tableIdentifier, GameNumber::first());

        // Game with 3 players. Trump card is not one of the cards of the players.

        $playerIdentifiers = new SimplePlayerIdentifiers(
            TestPlayers::penningmeester(),
            TestPlayers::secretaris(),
            TestPlayers::dtl(),
        );

        $this->projector->__invoke(
            new TableClaimed(
                $this->tableIdentifier,
                TestPlayers::penningmeester(),
                PlayerName::fromString('penn'),
                Seat::zero(),
                Language::default()
            )
        );
        $this->projector->__invoke(
            new GameStarted(
                $gameIdentifier,
                $playerIdentifiers,
                SimplePileOfCards::complete(),
                $this->tableIdentifier,
                TestPlayers::dtl(),
                TestPlayers::dtl(),
                new \DateTimeImmutable('2021-01-07')
            )
        );
        // No cards are dealt, so any trump we reveal is a non playing trump.
        $this->projector->__invoke(
            new NonPlayingTrumpRevealed(
                $gameIdentifier,
                TestPlayers::dtl(),
                new Card(1, Card::DIAMONDS),
                $this->tableIdentifier
            )
        );

        $actual = $this->readModel->getTableState($this->tableIdentifier);
        $expected = new TableState(
            $gameIdentifier,
            1,
            $this->tableIdentifier,
            0,
            0,
            Teams::allIndividual($playerIdentifiers),
            CardGame::unspecified(),
            false,
            new Card(1, Card::DIAMONDS),
            false,
            TestPlayers::dtl(),
            TestPlayers::dtl(),
            52,
            false
        );

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itPreparesGame(): void
    {
        $tableState = TableState::forNewTable($this->tableIdentifier, TestPlayers::penningmeester());
        $this->readModel->saveTableState($tableState);

        $playerIdentifiers = new SimplePlayerIdentifiers(
            TestPlayers::secretaris(),
            TestPlayers::penningmeester(),
            TestPlayers::dtl(),
            TestPlayers::diu()
        );
        $this->projector->__invoke(
            new GamePrepared(
                $this->tableIdentifier,
                TestPlayers::penningmeester(),
                TestPlayers::secretaris(),
                GameNumber::first(),
                SimplePileOfCards::complete(),
                TableMates::fromPlayers($playerIdentifiers->withLastPlayer(TestPlayers::penningmeester())),
                $playerIdentifiers
            )
        );

        $actual = $this->readModel->getTableState($this->tableIdentifier);
        $expected = new TableState(
            GameIdentifier::fromNumber($this->tableIdentifier, GameNumber::first()),
            // trick number is only set when game starts.
            0,
            $this->tableIdentifier,
            0,
            0,
            // teams are only initialized when game starts.
            Teams::empty(),
            CardGame::unspecified(),
            false,
            null,
            false,
            TestPlayers::penningmeester(),
            TestPlayers::penningmeester(),
            // Total number of cards is only set when game starts.
            0,
            false
        );

        $this->assertEquals($expected, $actual);
    }
}
