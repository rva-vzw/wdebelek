<?php

declare(strict_types=1);

namespace App\Tests\Integration\Domain\ReadModel\RevealedCards;

use App\Domain\ReadModel\PersonalPlayerInfo\PlayerHands;
use App\Domain\ReadModel\PlayersAtTable\PlayersAtTable;
use App\Domain\ReadModel\RevealedCards\RevealedCards;
use App\Domain\ReadModel\RevealedCards\RevealedCardsProjector;
use App\Domain\ReadModel\RevealedCards\RevealedCardsRepository;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\Hand;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Player\SimpleNamedPlayer;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\Event\CardPickedUp;
use App\Domain\WriteModel\Game\Event\HandRevealed;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestPlayers;
use App\Tests\Integration\AbstractKrakBoemIntegrationTest;

final class RevealedCardsProjectorTest extends AbstractKrakBoemIntegrationTest
{
    /** @var RevealedCardsRepository */
    private $repository;

    /** @var PlayerHands */
    private $dummyPlayerHands;

    /** @var PlayersAtTable */
    private $playersAtTableStub;

    protected function setUp(): void
    {
        parent::setUp();

        /** @var RevealedCardsRepository $readModel */
        $readModel = $this->getContainer()->get(RevealedCardsRepository::class);
        $this->repository = $readModel;

        /* @var PlayerHands dummyPlayerHands */
        $this->dummyPlayerHands = $this->createMock(PlayerHands::class);

        $namedPlayers = [
            new SimpleNamedPlayer(TestPlayers::dtl(), PlayerName::fromString('dtl')),
            new SimpleNamedPlayer(TestPlayers::secretaris(), PlayerName::fromString('secr')),
            new SimpleNamedPlayer(TestPlayers::penningmeester(), PlayerName::fromString('penn')),
            new SimpleNamedPlayer(TestPlayers::dpb(), PlayerName::fromString('dpb')),
            new SimpleNamedPlayer(TestPlayers::ddb(), PlayerName::fromString('ddb')),
        ];

        $playersAtTableStub = $this->createMock(PlayersAtTable::class);
        $playersAtTableStub->method('getPlayersAtTable')
            ->willReturn(new \ArrayIterator($namedPlayers));
        /* @var PlayersAtTable $playersAtTable */
        $this->playersAtTableStub = $playersAtTableStub;
    }

    /** @test */
    public function itShowsRevealedCardsToAllPlayers(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('21541751-96ad-4756-a6f4-cbd43ef5fcef')
        );

        $projector = new RevealedCardsProjector(
            $this->dummyPlayerHands,
            $this->repository,
            $this->playersAtTableStub
        );

        $gameIdentifier = GameIdentifier::fromNumber($tableIdentifier, GameNumber::first());
        $cards = SimplePileOfCards::fromCards(new \ArrayIterator([
            new Card(1, Card::CLUBS),
            new Card(2, Card::CLUBS),
            new Card(3, Card::CLUBS),
        ]));

        $projector->__invoke(
            new HandRevealed(
                $gameIdentifier,
                TestPlayers::dtl(),
                Hand::fromCards($cards),
                $tableIdentifier
            )
        );

        $actual = iterator_to_array($this->repository->getRevealedByPlayer(
            TestPlayers::dtl(),
            $gameIdentifier
        ));
        usort($actual, function (RevealedCards $a, RevealedCards $b): int {
            return $a->getRevealedToPlayerIdentifier()->toString() <=> $b->getRevealedToPlayerIdentifier()->toString();
        });

        $expected = [
            new RevealedCards($gameIdentifier, TestPlayers::dtl(), TestPlayers::dtl(), $tableIdentifier, $cards),
            new RevealedCards($gameIdentifier, TestPlayers::dtl(), TestPlayers::secretaris(), $tableIdentifier, $cards),
            new RevealedCards($gameIdentifier, TestPlayers::dtl(), TestPlayers::penningmeester(), $tableIdentifier, $cards),
            new RevealedCards($gameIdentifier, TestPlayers::dtl(), TestPlayers::dpb(), $tableIdentifier, $cards),
            new RevealedCards($gameIdentifier, TestPlayers::dtl(), TestPlayers::ddb(), $tableIdentifier, $cards),
        ];
        usort($expected, function (RevealedCards $a, RevealedCards $b): int {
            return $a->getRevealedToPlayerIdentifier()->toString() <=> $b->getRevealedToPlayerIdentifier()->toString();
        });

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itShowsPickedUpCardToAllPlayers(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('51939b16-b87c-4b2d-9219-ebd83a0a980a')
        );

        $projector = new RevealedCardsProjector(
            $this->dummyPlayerHands,
            $this->repository,
            $this->playersAtTableStub
        );

        $gameIdentifier = GameIdentifier::fromNumber($tableIdentifier, GameNumber::first());

        $projector->__invoke(new CardPickedUp(
            $gameIdentifier,
            TestPlayers::ddb(),
            new Card(2, Card::SPADES),
            1,
            $tableIdentifier
        ));

        $actual = iterator_to_array($this->repository->getRevealedByPlayer(
            TestPlayers::ddb(),
            $gameIdentifier
        ));
        usort($actual, function (RevealedCards $a, RevealedCards $b): int {
            return $a->getRevealedToPlayerIdentifier()->toString() <=> $b->getRevealedToPlayerIdentifier()->toString();
        });

        $cards = SimplePileOfCards::singleCard(new Card(2, Card::SPADES));
        $expected = [
            new RevealedCards($gameIdentifier, TestPlayers::ddb(), TestPlayers::dtl(), $tableIdentifier, $cards),
            new RevealedCards($gameIdentifier, TestPlayers::ddb(), TestPlayers::secretaris(), $tableIdentifier, $cards),
            new RevealedCards($gameIdentifier, TestPlayers::ddb(), TestPlayers::penningmeester(), $tableIdentifier, $cards),
            new RevealedCards($gameIdentifier, TestPlayers::ddb(), TestPlayers::dpb(), $tableIdentifier, $cards),
            new RevealedCards($gameIdentifier, TestPlayers::ddb(), TestPlayers::ddb(), $tableIdentifier, $cards),
        ];
        usort($expected, function (RevealedCards $a, RevealedCards $b): int {
            return $a->getRevealedToPlayerIdentifier()->toString() <=> $b->getRevealedToPlayerIdentifier()->toString();
        });

        $this->assertEquals($expected, $actual);
    }
}
