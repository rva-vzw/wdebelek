<?php

declare(strict_types=1);

namespace App\Tests\Acceptance;

use App\TestData\TestSecrets;
use App\TestData\TestTables;
use App\Tests\Support\AcceptanceTester;

final class DealingCest
{
    public function checkIWaitForDealer(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/9b9b2e23-ae06-5bc2-92fa-8a957f53a71c/play/c99f0f58-7895-44c3-a10e-e8ac2154aaa1

        $i->amOnPage('/en/table/'.TestTables::table28()->toString().'/play/'.TestSecrets::dpb()->toString());
        $i->waitForElement('.non-playing-trump [data-card-number="0"]');
        $i->waitForElementVisible('#waiting-for-dealer-message');
        $i->dontSee('Dealing strategy');
    }

    public function checkDealerCanChooseDealingStrategy(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/9b9b2e23-ae06-5bc2-92fa-8a957f53a71c/play/e0fd006c-becb-4344-96c7-7426caafafdc

        $i->amOnPage('/en/table/'.TestTables::table28()->toString().'/play/'.TestSecrets::dtl()->toString());
        $i->waitForElement('.non-playing-trump [data-card-number="0"]');
        $i->waitForElementVisible('#dealing-strategy-modal');
    }

    public function checkItDealsDefaultStrategyForFourPlayersAnd52Cards(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/5185d695-ea89-5b10-a9b4-ed4cc53cb8ad/play/e0fd006c-becb-4344-96c7-7426caafafdc

        $i->amOnPage('/en/table/'.TestTables::table15()->toString().'/play/'.TestSecrets::dtl()->toString());
        $i->waitForElementVisible('#waiting-for-dealer-message');
        $i->see('Waiting for dtl to deal...', '#waiting-for-dealer-message');
        $i->waitForElementVisible('#dealing-strategy-modal');
        $i->waitForElement('option[value="4,4,5"]');
        // 4,4,4,5 doesn't make sense in this constellation:
        $i->dontSeeElement('option[value="4,4,4,5"]');
        $i->click('OK');
        $i->waitForElementNotVisible('#waiting-for-dealder-message');
    }

    public function checkItShowsDealingStrategyDialogToNewDealerWhenDealerKicked(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/6da99528-6798-57f2-ba08-4d4fa60ebb7c/play/0ae2962c-9cf9-4e6b-a29b-231885207ebe
        $i->amOnPage('/en/table/'.TestTables::table29()->toString().'/play/'.TestSecrets::diu()->toString());
        $i->waitForElementVisible('#waiting-for-dealer-message');
        $i->see('Waiting for dtl to deal...', '#waiting-for-dealer-message');
        $i->dontSeeElement('#dealing-strategy-modal');

        $i->openNewTab();
        // http://localhost:8080/en/admin/seats/89f08032-193b-4026-aa44-02a0ee853b5b/manage/e0fd006c-becb-4344-96c7-7426caafafdc
        $i->amOnPage(
            '/en/admin/seats/'.TestSecrets::table29()->toString().'/manage/'.TestSecrets::secretaris()->toString()
        );
        // dtl will kick himself.
        $i->waitForElement('.card[data-seat="0"] .kick-button');
        $i->click('.card[data-seat="0"] .kick-button');

        // It's not really clear who should be the new dealer; for the moment it's just the
        // previous person at the table. Important thing is that the new dealer can select
        // the dealing strategy.
        $i->switchToPreviousTab();

        $i->waitForElementVisible('#dealing-strategy-modal');
    }

    public function checkICanChooseDealingStrategyWhenRedealing(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/d0ac11cb-51f5-5c6a-bd78-ade67fc57b5d/play/5e33f0b0-4081-414f-bd6f-b13ec2cf8311
        $i->amOnPage('/en/table/'.TestTables::table30()->toString().'/play/'.TestSecrets::ddb()->toString());
        $i->waitForElement('.conclusion-request');
        $i->click('.conclusion-request');
        $i->waitForText('same dealer');
        $i->click('same dealer');

        $i->waitForElementVisible('#dealing-strategy-modal');
    }
}
