<?php

declare(strict_types=1);

namespace App\Tests\Acceptance;

use App\TestData\TestSecrets;
use App\TestData\TestTables;
use App\Tests\Support\AcceptanceTester;
use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverKeys;
use Webmozart\Assert\Assert;

final class OrganizerCest
{
    public function checkClaimingTableDoesntWorkWhenIDontEnterMyName(AcceptanceTester $i): void
    {
        $i->amOnPage('/en');
        $i->click('Go to empty table');
        $i->see('Enter your name');
        $i->dontSee('Your table');
        $i->see('English', '.btn.active');
    }

    public function checkInvitationsPage(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/admin/seats/0a93f053-2c1f-4778-8911-d6f7192e426f/manage/5e33f0b0-4081-414f-bd6f-b13ec2cf8311

        $page = '/en/admin/seats/'.TestSecrets::table7()->toString().'/manage/'
            .TestSecrets::ddb()->toString();
        $i->amOnPage(
            $page
        );
        $i->waitForElement('input.form-control[name="invitation_1"]');
        // In fact https is shown, but the relevant thing is that the url starts with a scheme.
        $invitationUrl = strval($i->grabValueFrom('invitation_1'));
        // I am not aware of a better way to do this check:
        Assert::contains($invitationUrl, 'http');

        // Should not see invite player, since 5 players have been invited.
        $i->dontSee('Invite player');
    }

    public function checkIPrepareTheGame(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/admin/seats/b7c32ccf-91a3-46f5-94f3-c3c334c5dcb5/manage/e0fd006c-becb-4344-96c7-7426caafafdc
        $i->amOnPage(
            '/en/admin/seats/'.TestSecrets::table8()->toString().'/manage/'
            .TestSecrets::dtl()->toString()
        );

        $i->openNewTab();
        // http://localhost:8080/en/table/b0cdd4c6-80be-5bc0-8d7f-7da4e5fc5e49/play/46d2015e-2f98-48aa-bf05-0b27f6707b20
        $i->amOnPage(
            '/en/table/'.TestTables::table8()->toString().'/play/'.TestSecrets::secretaris()->toString()
        );
        $i->waitForElement('.alert');
        $i->see('Waiting for the game to start...');
        $i->switchToPreviousTab();

        $i->waitForText('Start game', 10, 'button.disabled');
        $i->see('Start game', 'button.disabled');
        $i->selectOption('#dealer-select', 'dtl');
        $i->dontSee('Start game', 'button.disabled');
        $i->see('Start game', 'button');
        $i->click('Start game');
        $i->waitForText('To your table!');
        $i->see('To your table!');

        $i->switchToNextTab();
        $i->waitForElementVisible('#waiting-for-dealer-message');
        $i->waitForElementNotVisible('#waiting-message');
    }

    public function checkIStartPlayingWithMyLanguageOfChoice(AcceptanceTester $i): void
    {
        // http://localhost:8080/nl/admin/seats/e585e413-a2e0-4722-a7ff-212744f45a40/manage/e0fd006c-becb-4344-96c7-7426caafafdc
        $i->amOnPage(
            '/nl/admin/seats/'.TestSecrets::table14()->toString().'/manage/'
            .TestSecrets::dtl()->toString()
        );
        $i->waitForText('Naar uw tafel');
        $i->click('Naar uw tafel');
        // You'd think that opening a link with target="_blank" will automatically switch to a next tab. Not so
        // with webdriver, as it seems.

        // I used to $i->switchToNextTab(), but for some reason that doesn't always work on gitlab :-(
        $i->executeInSelenium(function (WebDriver $webDriver): void {
            $handles = $webDriver->getWindowHandles();
            $lastWindow = end($handles);
            $webDriver->switchTo()->window($lastWindow);
        });

        // Check for Dutch text
        $i->waitForText('secr');
        $i->waitForElement('//span[contains(@class,"conclusion-request")]');
        $i->click('.conclusion-request');
        $i->see('opnieuw delen');
        $i->dontSeeElement('same dealer');
    }

    public function checkTableIsSuspendedWhenInvolvedPlayerLeaves(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/admin/seats/538952f4-adbf-4559-b8e5-f665f5d50e52/manage/e0fd006c-becb-4344-96c7-7426caafafdc
        $i->amOnPage(
            '/en/admin/seats/'.TestSecrets::table20()->toString().'/manage/'
            .TestSecrets::dtl()->toString()
        );

        $i->waitForText('ddb');

        // Let's kick a pisser first (ddb):
        $i->waitForElement('.card[data-seat="2"] .kick-button');
        $i->waitForElementClickable('.card[data-seat="2"] .kick-button');
        $i->click('.card[data-seat="2"] .kick-button');
        $i->waitForElementNotVisible('.card[data-seat="2"] .kick-button');
        $i->waitForElement('#invitation_2');

        // Game continues, because ddb was not involved.
        $i->dontSee('Start game');

        // Let's check DTL page: Is game still going on?
        // http://localhost:8080/en/table/8220967c-f156-5d8d-8a43-96293ee1633f/play/e0fd006c-becb-4344-96c7-7426caafafdc
        $i->openNewTab();
        $i->amOnPage(
            '/en/table/'.TestTables::table20()->toString().'/play/'.TestSecrets::dtl()->toString()
        );
        // See my ace of hearts.
        $i->waitForElement('//*[@data-card-number="14"]');
        $i->dontSee('Waiting for the game to start...');
        $i->switchToPreviousTab();

        // Now kick dpb
        $i->click('.card[data-seat="1"] .kick-button');
        $i->waitForElementNotVisible('.card[data-seat="1"] .kick-button');
        $i->waitForElement('#invitation_1');

        $i->waitForText('Start game');

        // Game can be restarted, because there are 4 players left.
        $i->waitForText('Start game');
        $i->see('Start game');

        // Check for waiting screen on DTL page:
        $i->switchToNextTab();
        $i->waitForElement('.alert');
        $i->see('Waiting for the game to start...');
        $i->dontSeeElement('//*[@data-card-number="14"]');
    }

    public function checkICantSetPrivacyForAcceptedInvitations(AcceptanceTester $i): void
    {
        // http://localhost:8080/nl/admin/seats/e585e413-a2e0-4722-a7ff-212744f45a40/manage/e0fd006c-becb-4344-96c7-7426caafafdc
        $i->amOnPage(
            '/nl/admin/seats/'.TestSecrets::table14()->toString().'/manage/'
            .TestSecrets::dtl()->toString()
        );

        $i->waitForText('ddb');
        $i->dontSeeElement('tr td select');
    }

    public function checkIPublishInvitation(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/admin/seats/0a93f053-2c1f-4778-8911-d6f7192e426f/manage/5e33f0b0-4081-414f-bd6f-b13ec2cf8311

        $page = '/en/admin/seats/'.TestSecrets::table7()->toString().'/manage/'
            .TestSecrets::ddb()->toString();
        $i->amOnPage($page);

        $i->waitForElement('.card[data-seat="1"] input[value="public"]');
        // those bootstrap options are tricky to test.
        // I hope clicking the label will work...
        $i->click('.public-radio_1 label');

        // Refresh the page, see if the invitation is still public.
        $i->reloadPage();
        $i->waitForElement('.card input');
        $i->waitForElement('.card[data-seat="1"] input[value="public"]');
        $i->seeOptionIsSelected('.card[data-seat="1"]', 'public');
        $i->seeCheckboxIsChecked('.card[data-seat="1"] input[value="public"]');
    }

    public function checkIDontSeeWasteBasketForEmptyChair(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/admin/seats/0a93f053-2c1f-4778-8911-d6f7192e426f/manage/5e33f0b0-4081-414f-bd6f-b13ec2cf8311

        $page = '/en/admin/seats/'.TestSecrets::table7()->toString().'/manage/'
            .TestSecrets::ddb()->toString();
        $i->amOnPage($page);

        $i->waitForElement('.card[data-seat="1"]');
        $i->see('(empty chair)', '.card[data-seat="1"]');
        $i->dontSee('.kick-button', '.card[data-seat="1"]');
    }

    public function checkNewInvitationIsCreatedWhenIKickAPlayer(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/admin/seats/16a4359b-bcb2-40b5-92bf-5339e4a9b4d4/manage/ce29fba8-a25e-4ed6-abdf-3128daf90542

        $i->amOnPage('/en/admin/seats/'.TestSecrets::table25()->toString().'/manage/'
            .TestSecrets::penningmeester()->toString());

        // Kick dpb

        $i->waitForElement('[data-seat="1"] .kick-button');
        $i->wait(1); // Wait even longer :-(
        $i->click('[data-seat="1"] .kick-button');

        // An invitation should appear. Publish it.

        $i->waitForElement('[data-seat="1"] .public-radio_1 label');
        $i->click('[data-seat="1"] .public-radio_1 label');

        $i->fillField('#input-game-name', 'test for #149');

        // Enter a game name, so that we can look for the invitation on the front page.
        // Game name is passed when field loses focus, but that doesn't seem to work with webdriver.
        // Let's explicitly press tab.
        $i->pressKey('#input-game-name', [WebDriverKeys::TAB]);

        // Now go to front page, and check for the invitation.

        $i->amOnPage('/en/');
        $i->waitForText('test for #149');
    }
}
