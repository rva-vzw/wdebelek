<?php

declare(strict_types=1);

namespace App\Tests\Acceptance;

use App\TestData\TestPlayers;
use App\TestData\TestSecrets;
use App\TestData\TestTables;
use App\Tests\Support\AcceptanceTester;

final class GameCest
{
    public function checkNoWaitingMessageIfGameStarted(AcceptanceTester $i): void
    {
        // http://localhost:8080/nl/table/a4e394bd-d3a0-55fa-95f9-b2f93e77c0df/play/c99f0f58-7895-44c3-a10e-e8ac2154aaa1
        $i->amOnPage('/en/table/'.TestTables::table1()->toString().'/play/'.TestSecrets::dpb()->toString());

        $i->waitForElementNotVisible('#waiting-message');
        $i->dontSee('Waiting for the game to start...');

        // See player names on the screen (title bar).
        $i->waitForElement('//span[contains(text(),"dpb")]');
        $i->see('dpb');
        $i->see('ddb');
        $i->see('secr');
        $i->see('dtl');

        // See short player names on the actual card mat.
        $i->dontSee('secr', '.player-hand');
        $i->see('sec', '.player-hand');
    }

    public function checkPisserMode(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/242a33d2-8b44-5276-8f57-c299f231b154/play/5e33f0b0-4081-414f-bd6f-b13ec2cf8311
        $i->amOnPage('/en/table/'.TestTables::table11()->toString().'/play/'.TestSecrets::ddb()->toString());
        $i->waitForElement('.playing-card');
        $i->dontSee('.alert-danger');

        $i->waitForElement('.player-hand .h3');

        // See all hands except mine
        $i->see('sec', '.player-hand');
        $i->see('pen', '.player-hand');
        $i->see('dtl', '.player-hand');
        $i->see('dpb', '.player-hand');
        $i->dontSee('ddb', '.player-hand');

        // Although 4 cards are played, I am not allowed to vote.

        $i->dontSee('Please tick the winning card.');
        $i->dontSee('.conclusion-request');
    }

    public function checkICanVoteAWinnerOnATableOf5(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/242a33d2-8b44-5276-8f57-c299f231b154/play/ce29fba8-a25e-4ed6-abdf-3128daf90542
        $i->amOnPage('/en/table/'.TestTables::table11()->toString().'/play/'.TestSecrets::secretaris()->toString());
        $i->waitForElement('.playing-card');
        $i->waitForElement('.who-wins');

        $i->see('Please tick the winning card.');
    }

    public function checkIVoteAndUnvoteGameDone(AcceptanceTester $i): void
    {
        // Let's test this in Dutch.
        // http://localhost:8080/nl/table/55375067-3fa3-52db-98ef-433194371c69/play/46d2015e-2f98-48aa-bf05-0b27f6707b20
        $i->amOnPage('/nl/table/'.TestTables::table4()->toString().'/play/'.TestSecrets::ddb()->toString());
        $i->waitForElement('.conclusion-request');
        $i->click('.conclusion-request');
        $i->waitForText('volgende deler');
        $i->click('volgende deler');
        // The conclusion request icon should start blinking.
        $i->waitForElement('.conclusion-request.blink');
        $i->click('.conclusion-request');
        // There should be a check mark next to 'volgende deler'.
        $i->waitForElement('.voted-conclusion');
        $i->waitForText('volgende deler');
        $i->click('volgende deler');
        // It should stop blinking
        $i->waitForElementNotVisible('.conclusion-request.blink');
    }

    public function checkConclusionCheckMarkIsGoneWhenNewGameStarted(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/b1871aef-37bc-5596-ba80-d96e3b13abf3/play/c99f0f58-7895-44c3-a10e-e8ac2154aaa1
        $i->amOnPage('/en/table/'.TestTables::table14()->toString().'/play/'.TestSecrets::dpb()->toString());
        $i->waitForElement('.conclusion-request.blink');
        $i->click('.conclusion-request');
        $i->waitForText('next dealer');
        $i->click('next dealer');
        // Conclusion request will stop blinking because the next game started.
        $i->waitForElementNotVisible('.conclusion-request.blink');
        $i->click('.conclusion-request');
        $i->dontSeeElement('.voted-conclusion');
    }

    public function checkVotingRedeal(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/b34f4f83-1fda-527d-b799-8c34fedc2852/play/c99f0f58-7895-44c3-a10e-e8ac2154aaa1
        $i->amOnPage('/en/table/'.TestTables::table16()->toString().'/play/'.TestSecrets::dpb()->toString());
        $i->waitForElement('.conclusion-request');
        $i->click('.conclusion-request');
        $i->waitForText('same dealer');
        $i->click('same dealer');
        // The conclusion request icon should start blinking.
        $i->waitForElement('.conclusion-request.blink');
    }

    public function checkTrumpIsShown(AcceptanceTester $i): void
    {
        // If this test fails: did you run yarn encore dev?

        // http://localhost:8080/en/table/b34f4f83-1fda-527d-b799-8c34fedc2852/play/c99f0f58-7895-44c3-a10e-e8ac2154aaa1
        $i->amOnPage('/en/table/'.TestTables::table16()->toString().'/play/'.TestSecrets::dpb()->toString());

        // Let's first wait for the cards of the fellow players
        $i->waitForElement('.playing-card.reversed-card');
        // Card 4, is trump, because we didn't shuffle, and we cut after 4 cards.
        $i->waitForElement('//*[@data-card-number="4"]');
        $i->seeElement('//*[@data-card-number="4"]');
    }

    /**
     * See #72, https://gitlab.com/rva-vzw/wdebelek/-/issues/72.
     */
    public function checkSingleCardInHandIsShown(AcceptanceTester $i): void
    {
        // http://localhost:8080/nl/table/c89ef743-7b5d-52d8-a03f-2d50f3c48972/play/46d2015e-2f98-48aa-bf05-0b27f6707b20
        $i->amOnPage('/en/table/'.TestTables::table19()->toString().'/play/'.TestSecrets::secretaris()->toString());

        $i->waitForElement('.player-hand .reversed-card');
        $i->seeElement('.player-hand .reversed-card');
    }

    public function checkItRevealsTrump(AcceptanceTester $i): void
    {
        // http://localhost:8080/nl/table/55375067-3fa3-52db-98ef-433194371c69/play/5e33f0b0-4081-414f-bd6f-b13ec2cf8311
        $i->amOnPage('/en/table/'.TestTables::table4()->toString().'/play/'.TestSecrets::ddb()->toString());

        $i->waitForElement('.reveal-trump');
        $i->click('.reveal-trump');
        // When trump is revealed, the link should disappear.
        $i->waitForElementNotVisible('.reveal-trump');
    }

    public function checkItShowsWhoGotTheTrumpCard(AcceptanceTester $i): void
    {
        // http://localhost:8080/nl/table/a4e394bd-d3a0-55fa-95f9-b2f93e77c0df/play/c99f0f58-7895-44c3-a10e-e8ac2154aaa1
        $i->amOnPage('/en/table/'.TestTables::table1()->toString().'/play/'.TestSecrets::dpb()->toString());

        $i->waitForElement('.playing-card.trump');
        $i->see('ddb', 'span.font-weight-bold');
    }

    public function checkItStillShowsCardsPickedUpAgain(AcceptanceTester $i): void
    {
        // http://localhost:8080/nl/table/a4e394bd-d3a0-55fa-95f9-b2f93e77c0df/play/c99f0f58-7895-44c3-a10e-e8ac2154aaa1
        $i->amOnPage('/en/table/'.TestTables::table1()->toString().'/play/'.TestSecrets::dpb()->toString());

        // The 4 of diamonds was thrown and picked up again by secretaris.
        $i->waitForElement('//*[@data-card-number="30"]');
        $i->waitForElement('//div[@data-player="'.TestPlayers::secretaris()->toString().'"]/span/*[@data-card-number="30"]');
        // I should also see at least a card that's still reversed.
        $i->waitForElement('//div[@data-player="'.TestPlayers::secretaris()->toString().'"]/span/*[@data-card-number="0"]');
    }

    public function checkItShowsThePisser(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/242a33d2-8b44-5276-8f57-c299f231b154/play/ce29fba8-a25e-4ed6-abdf-3128daf90542
        $i->amOnPage('/en/table/'.TestTables::table11()->toString().'/play/'.TestSecrets::penningmeester()->toString());
        $i->waitForElement('.pisser');
        $i->see('ddb', '.pisser');
    }

    public function checkItRemovesBigCardAfterPickingUpTrick(AcceptanceTester $i): void
    {
        // http://localhost:8080/nl/table/b55faeae-c43a-5868-909f-5cd4337ecba1/play/ce29fba8-a25e-4ed6-abdf-3128daf90542
        $i->amOnPage('/en/table/'.TestTables::table21()->toString().'/play/'.TestSecrets::penningmeester()->toString());

        // Wait for cards of dpb
        $i->waitForElement('//div[@data-player="'.TestPlayers::dpb()->toString().'"]/span/*[@data-card-number="0"]');
        $i->dontSeeElement('.reversed-card.trump');
    }

    public function checkItUnrevealsCardsWhenCardsCollected(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/d6a83d8f-58d5-50fb-a247-8fa34dc2d177/play/c99f0f58-7895-44c3-a10e-e8ac2154aaa1
        $i->amOnPage('/en/table/'.TestTables::table26()->toString().'/play/'.TestSecrets::dpb()->toString());

        $i->waitForElement('.conclusion-request');
        $i->waitForElement('.hand-side [data-card-number="17"]');
        $i->click('.conclusion-request');
        $i->waitForText('same dealer');
        $i->click('same dealer');

        // 4 of hearts (in hand of other player, ddb)
        $i->waitForElementNotVisible('.hand-side [data-card-number="17"]');
        // ace of hearts (in my hand)
        $i->waitForElementNotVisible('.hand-side [data-card-number="14"]');
    }

    public function checkItDisclosesCardsWhenAllHandsEmpty(AcceptanceTester $i): void
    {
        // http://localhost:8080/nl/table/4873a86d-1dbd-58ff-be14-efa90ab4e9ce/play/5e33f0b0-4081-414f-bd6f-b13ec2cf8311
        $i->amOnPage('/en/table/'.TestTables::table33()->toString().'/play/'.TestSecrets::ddb()->toString());

        $i->waitForElement('.team-score[data-team="2"] a');
        $i->click('.team-score[data-team="2"] a');
        $i->waitForText('Team dpb');
        $i->waitForElement('[data-card-number="5"]');
    }
}
