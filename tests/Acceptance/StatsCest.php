<?php

declare(strict_types=1);

namespace App\Tests\Acceptance;

use App\Tests\Support\AcceptanceTester;

final class StatsCest
{
    public function checkICanClickToTheStats(AcceptanceTester $i): void
    {
        $i->amOnPage('/en');
        // The alert with the stats link only appears after the ajax-call that
        // gets the number of active tables.
        $i->waitForElementVisible('.alert');
        $i->click('More stats...');
        $i->see('Statistics');
    }
}
