<?php

declare(strict_types=1);

namespace App\Tests\Acceptance;

use App\TestData\TestSecrets;
use App\TestData\TestTables;
use App\Tests\Support\AcceptanceTester;

final class SpectatorCest
{
    public function checkSpectatorSeesShownCards(AcceptanceTester $i): void
    {
        // http://localhost:8080/nl/table/120c0f4f-f6d3-5640-aa02-958544c8d645/play/5e33f0b0-4081-414f-bd6f-b13ec2cf8311
        $i->amOnPage('/en/table/'.TestTables::table22()->toString().'/play/'.TestSecrets::ddb()->toString());

        // Wait for any card to be shown.
        $i->waitForElement('.playing-card.revealed');
        // Wait for 8 of hearts.
        $i->waitForElement('//*[@data-card-number=21]');
    }
}
