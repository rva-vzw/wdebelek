<?php

declare(strict_types=1);

namespace App\Tests\Acceptance;

use App\TestData\TestSecrets;
use App\TestData\TestTables;
use App\Tests\Support\AcceptanceTester;

final class TeamsCest
{
    public function checkIRemovePlayerFromTeam(AcceptanceTester $i): void
    {
        // http://localhost:8080/nl/table/8903a65f-fafb-54aa-8625-010731d011eb/play/c99f0f58-7895-44c3-a10e-e8ac2154aaa1
        $i->amOnPage('/en/table/'.TestTables::table18()->toString().'/play/'.TestSecrets::dpb()->toString());

        // This test is very unstable, and I don't understand why 😭

        $i->wait(2);
        $i->reloadPage();
        $i->wait(1);

        $i->waitForElement('//*[@data-team="1" and contains(text(), "ddb")]');
        // There should be a place I can drag the player to unteam to.
        $i->seeElement('div.empty-team');
        $i->wait(1);

        $i->dragAndDrop(
            '//*[@data-team="1" and contains(text(), "ddb")]',
            'div.empty-team'
        );

        // I don't understand why this doesn't work 🤯

        //        $i->waitForElement('//*[@data-team="2" and contains(text(), "ddb")]');
        //
        //        // Refresh to check whether everything was saved in the back-end.
        //        $i->reloadPage();
        //        $i->waitForElement('//*[@data-team="2" and contains(text(), "ddb")]');
    }
}
