<?php

declare(strict_types=1);

namespace App\Tests\Acceptance;

use App\TestData\TestInvitations;
use App\TestData\TestTables;
use App\Tests\Support\AcceptanceTester;

final class InvitationCest
{
    public function checkIDontGetAnErrorWhenAcceptingAnInvitation(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/56b35eaf-9bd6-5e5b-9df8-781521b7a3d8/invitation/aa4afa5b-bddd-4452-a0e5-1dd7cda3b6ac
        $i->amOnPage(
            '/en/table/'.TestTables::table9()->toString()
            .'/invitation/'.TestInvitations::invitation1()->toString());

        $i->fillField('Enter your name', 'freddy');
        $i->click('Accept invitation');

        $i->waitForElement('.alert');
        $i->see('Waiting for the game to start...');
    }

    public function checkISeeTableInDutch(AcceptanceTester $i): void
    {
        // http://localhost:8080/nl/table/56b35eaf-9bd6-5e5b-9df8-781521b7a3d8/invitation/e06897ab-db19-4e2e-86d8-d34de392fc37
        $i->amOnPage(
            '/nl/table/'.TestTables::table9()->toString()
            .'/invitation/'.TestInvitations::invitation2()->toString());

        $i->fillField('Uw naam a.u.b.', 'freddy');
        $i->click('Uitnodiging accepteren');

        $i->waitForElement('.alert');
        $i->see('Wachten op de start van het spel...');
    }

    public function checkISeeDutchPageWhenAcceptingADutchInvitationFromFrontPage(AcceptanceTester $i): void
    {
        $i->amOnPage('/');
        $i->waitForElement('//tr[@data-pk="'.TestTables::table24()->toString().'"]/td/a[contains(@class, "join-btn")]');
        $i->click('//tr[@data-pk="'.TestTables::table24()->toString().'"]/td/a[contains(@class, "join-btn")]');

        $i->waitForText('Uitnodiging accepteren');
    }

    public function checkIGetAnInformationalMessageWhenAcceptingANonExistingInvitation(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/56b35eaf-9bd6-5e5b-9df8-781521b7a3d8/invitation/163522b1-1287-4613-bf80-2673778655fb
        $i->amOnPage(
            '/en/table/'.TestTables::table9()->toString()
            .'/invitation/163522b1-1287-4613-bf80-2673778655fb');

        $i->see('No such invitation');
    }

    public function checkIGetAnInformationalMessageWhenSomeoneAcceptedTheInvitationBeforeMe(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/7c98b39e-7f9b-57ce-9f0c-16a62d6b2a5e/invitation/aefe17db-d295-45b4-ab76-87861b9336e9
        $i->amOnPage(
            '/en/table/'.TestTables::table24()->toString()
            .'/invitation/'.TestInvitations::invitation24_2()->toString());

        // Let's first accept the invitation in another tab...
        $i->openNewTab();
        $i->amOnPage(
            '/en/table/'.TestTables::table24()->toString()
            .'/invitation/'.TestInvitations::invitation24_2()->toString());

        $i->waitForElement('//input[@name="name"]');
        $i->fillField('Enter your name', 'freddy');
        $i->click('Accept invitation');

        // Now try the same in the previous tab.
        $i->switchToPreviousTab();
        $i->fillField('Enter your name', 'paul');
        $i->click('Accept invitation');

        $i->see('No such invitation');
    }
}
