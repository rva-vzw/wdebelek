<?php

declare(strict_types=1);

namespace App\Tests\Acceptance;

use App\TestData\TestGames;
use App\TestData\TestPlayers;
use App\TestData\TestSecrets;
use App\TestData\TestTables;
use App\Tests\Support\AcceptanceTester;

final class HandCest
{
    public function checkISeeMyCards(AcceptanceTester $i): void
    {
        // http://localhost:8080/nl/play/d8976bef-2166-55cf-9e87-bd85859dddfd/46d2015e-2f98-48aa-bf05-0b27f6707b20
        $i->amOnPage('/en/play/'.TestGames::game1()->toString().'/'.TestSecrets::secretaris()->toString());
        $i->waitForElement('//*[@data-card-number="14"]');
        $i->seeElement('//*[@data-card-number="14"]');
    }

    public function checkAutosortHand(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/b34f4f83-1fda-527d-b799-8c34fedc2852/play/c99f0f58-7895-44c3-a10e-e8ac2154aaa1
        $i->amOnPage('/en/table/'.TestTables::table16()->toString().'/play/'.TestSecrets::dpb()->toString());

        // Wait for some card in my hand.
        $i->waitForElement('//*[@data-card-number="47"]');
        $i->click('A-Z');

        // Now I have to check the card order. I suspect the easiest way is retrieving it via an
        // api call?
        // Let's wait for the read models to get updated.
        $i->wait(1);
        // http://localhost:8080/api/table/b34f4f83-1fda-527d-b799-8c34fedc2852/player/c99f0f58-7895-44c3-a10e-e8ac2154aaa1
        $i->amOnPage('/api/table/'.TestTables::table16()->toString().'/player/'.TestSecrets::dpb()->toString());
        $i->seeInPageSource('[15,16,14,47,48,49,50,51,29,30,31,32,13]');
    }

    public function checkISeeMyOwnTrumpCard(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/b34f4f83-1fda-527d-b799-8c34fedc2852/play/5e33f0b0-4081-414f-bd6f-b13ec2cf8311
        $i->amOnPage('/en/table/'.TestTables::table16()->toString().'/play/'.TestSecrets::ddb()->toString());

        $i->waitForElement('.my-trump');
        $i->waitForElement('//*[@data-card-number="4"]');
        $i->seeElement('.my-trump[data-card-number="4"]');
    }

    public function checkMyInitialHandIsShown(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/7b93871f-bba2-5ccf-a2d8-f1991e3849aa/play/5e33f0b0-4081-414f-bd6f-b13ec2cf8311
        $i->amOnPage('/en/table/'.TestTables::table23()->toString().'/play/'.TestSecrets::ddb()->toString());

        // although card 39 has been played by penningmeester, it should be visible because he
        // shared his initial cards.
        $i->waitForElement('//*[@data-card-number="39"]');
        $i->seeElement('//*[@data-card-number="39"]');
    }

    public function checkISeeMyOwnInitialHand(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/7b93871f-bba2-5ccf-a2d8-f1991e3849aa/play/e0fd006c-becb-4344-96c7-7426caafafdc
        $i->amOnPage('/en/table/'.TestTables::table23()->toString().'/play/'.TestSecrets::dtl()->toString());

        $i->waitForElement('svg.fa-eye');
        $i->click('svg.fa-eye');
        $i->waitForText('show hand to everybody');
        $i->dontSee('show initial hand to everybody');
        $i->click('show hand to everybody');

        $i->click('svg.fa-eye');
        $i->waitForText('show initial hand to everybody');
        $i->dontSee('show hand to everybody');
        $i->dontSee('//*[@data-card-number="27"]');
        $i->click('show initial hand to everybody');

        // Wait for a card I've already played.
        $i->waitForElement('//*[@data-card-number="27"]');
        $i->seeElement('//*[@data-card-number="27"]');
    }

    public function checkIUnrevealMyCards(AcceptanceTester $i): void
    {
        // Player ddb has cards revealed on table 27.
        // http://localhost:8080/en/table/3276b3fb-45b5-5bac-924b-6d4ccdb621c3/play/5e33f0b0-4081-414f-bd6f-b13ec2cf8311
        $i->amOnPage('/en/table/'.TestTables::table27()->toString().'/play/'.TestSecrets::ddb()->toString());

        $i->waitForElement('svg.fa-eye');
        $i->click('svg.fa-eye');
        $i->waitForText('unreveal cards');
        // Unreveal cards for player ddb
        $i->click('unreveal cards');

        // Check as player dtl
        // http://localhost:8080/en/table/3276b3fb-45b5-5bac-924b-6d4ccdb621c3/play/e0fd006c-becb-4344-96c7-7426caafafdc
        $i->amOnPage('/en/table/'.TestTables::table27()->toString().'/play/'.TestSecrets::dtl()->toString());
        // Wait for an unrevealed card in the hand of player ddb.
        $i->waitForElement("//div[@class='player-hand' and @data-player='".TestPlayers::ddb()->toString()."']//span[@data-card-number='0']");
        // Check I don't see ace of spades (which is in hand of ddb)
        $i->dontSee('//*[@data-card-number="1"]');
    }
}
