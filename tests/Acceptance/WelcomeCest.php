<?php

declare(strict_types=1);

namespace App\Tests\Acceptance;

use App\TestData\TestTables;
use App\Tests\Support\AcceptanceTester;

final class WelcomeCest
{
    public function checkISeeNumberOfActiveTables(AcceptanceTester $i): void
    {
        $i->amOnPage('/en');
        $i->waitForElementVisible('.alert');
        // There should be some active tables if test data loaded.
        // If not, check if the time zone for the php cli is the same as the time zone for php web.
        $i->waitForText('Currently, there are');
        $i->dontSee('Currently, there are 0 active table(s).');
    }

    public function checkIChooseDutchAsLanguage(AcceptanceTester $i): void
    {
        $i->amOnPage('/');
        $i->waitForText('Nederlands');
        $i->seeCurrentUrlMatches('/nl/');
        $i->click('Nederlands');
        // The test data contains tables with open invites. Wait
        // until I see this vue component; let's hope the app has stabilized
        // by then.
        $i->waitForText('Aansluiten bij een bestaande tafel?');
        $i->fillField('name', 'jos');
        $i->click('Naar een lege tafel');
        $i->waitForText('Uw tafel');
        $i->see('Uw tafel');
        $i->waitForText('jos');
        $i->see('jos');
    }

    public function checkIGetAboutPageInDutch(AcceptanceTester $i): void
    {
        $i->amOnPage('/nl');
        $i->click('Meer info');
        $i->see('Event sourced kaarttafel');
        $i->dontSee('Event sourced card playing');
    }

    public function checkItUpdatesWhenInvitationUnpublished(AcceptanceTester $i): void
    {
        $i->amOnPage('/en');
        $tableId = TestTables::table32();
        // Invitation should be visible on front page.
        $i->waitForElement("tr[data-pk='{$tableId->toString()}']");

        // Accept invitation in new tab
        $i->openNewTab();
        $i->amOnPage("/en/table/{$tableId->toString()}/join");
        $i->fillField('Enter your name', 'freddy');
        $i->click('Accept invitation');

        // Invitation should be gone in previous tab
        $i->switchToPreviousTab();
        $i->waitForElementNotVisible("tr[data-pk='{$tableId->toString()}']");
    }
}
