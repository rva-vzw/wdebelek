<?php

declare(strict_types=1);

namespace App\Tests\Acceptance;

use App\Domain\ValueObject\Card\Card;
use App\TestData\TestGames;
use App\TestData\TestPlayers;
use App\TestData\TestSecrets;
use App\TestData\TestTables;
use App\Tests\Support\AcceptanceTester;

final class TrickCest
{
    public function checkIThrowACard(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/play/628ff94e-d23d-5fa9-870a-72d5cf5308a1/46d2015e-2f98-48aa-bf05-0b27f6707b20

        $i->amOnPage('/en/play/'.TestGames::game2()->toString().'/'.TestSecrets::secretaris()->toString());
        $i->waitForElement('//*[@data-card-number="14"]');
        $i->seeElement('//*[@data-card-number="14"]');

        // What I wanted to do, is the following:
        // $i->dragAndDrop('//*[@data-card-number="14"]', 'div.throwing-area');
        // This doesn't work, because selenium seems to have problems with native html5 drag and drop
        // See https://gitlab.com/rva-vzw/wdebelek/-/issues/22

        // So let's work around the issue:

        // http://localhost:8080/hack/628ff94e-d23d-5fa9-870a-72d5cf5308a1/play/1/46d2015e-2f98-48aa-bf05-0b27f6707b20
        $i->playCard(
            TestGames::game2(),
            1,
            TestSecrets::secretaris(),
            new Card(1, Card::HEARTS)
        );

        $i->waitForElement('//div[contains(@class, "throwing-area")]/span[@data-card-number="14"]');

        // The below doesn't work because I didn't actually drag, I used the '$i->playCard()'-workaround.
        // When a card is played, your own cards aren't updated, which is fine.

        // $i->waitForElementNotVisible('.cards-to-be-played [data-card-number="14"]');
    }

    public function checkICantVoteWhoWinsWhenTheTrickIsNotComplete(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/play/6fae9b47-9487-479b-963e-1d89c731c170/46d2015e-2f98-48aa-bf05-0b27f6707b20

        $i->amOnPage('/en/play/'.TestGames::game4()->toString().'/'.TestSecrets::secretaris()->toString());
        $i->waitForElement('//*[@data-card-number="37"]');
        $i->clickWithLeftButton('//*[@data-card-number="37"]');
        $i->dontSeeElement('.winner');
    }

    public function checkCardsArePickedUpWhenTrickIsWon(AcceptanceTester $i): void
    {
        // http://localhost:8080/nl/play/97a2706d-47f1-585c-9d1b-d656027d2f6f/46d2015e-2f98-48aa-bf05-0b27f6707b20

        $i->amOnPage('/en/play/'.TestGames::game5()->toString().'/'.TestSecrets::secretaris()->toString());
        // Ace of diamonds
        $i->waitForElement('//*[@data-card-number="27"]');
        $i->clickWithLeftButton('//*[@data-card-number="27"]');
        // All played cards should disappear
        $i->waitForElementNotVisible('//*[@data-card-number="27"]');
        $i->waitForElementNotVisible('//*[@data-card-number="30"]');
        $i->waitForElementNotVisible('//*[@data-card-number="37"]');
        $i->waitForElementNotVisible('//*[@data-card-number="39"]');

        // While we're at it, check whether trick count for winning player increases.
        // $i->waitForText('ddb: 1');
        $i->waitForElement('//*[@data-team="3" and text()="1" and contains(@class ,"team-score")]');
    }

    public function checkISeeCardsOfFellowPlayersChanging(AcceptanceTester $i): void
    {
        // Note that every player has only one card in the hand for this test!
        // Open ddb view in one tab ...

        // http://localhost:8080/nl/table/c89ef743-7b5d-52d8-a03f-2d50f3c48972/play/5e33f0b0-4081-414f-bd6f-b13ec2cf8311
        $i->amOnPage('/en/table/'.TestTables::table19()->toString().'/play/'.TestSecrets::ddb()->toString());

        // See reversed card in hand of secretaris
        $i->waitForElement('div[data-player="'.TestPlayers::secretaris()->toString().'"].player-hand .reversed-card');

        // http://localhost:8080/hack/290d185f-fcf0-5a1e-a8d5-058ef14cadff/play/1/46d2015e-2f98-48aa-bf05-0b27f6707b20
        $i->playCard(
            TestGames::game19(),
            1,
            TestSecrets::secretaris(),
            new Card(5, Card::SPADES)
        );

        // Wait until I see the card thrown by secretaris.
        $i->waitForElement('.other-player-card [data-card-number="5"]');
        // Reversed card in hand of secretaris should be gone now.
        $i->dontSeeElement('div[data-player="'.TestPlayers::secretaris()->toString().'"].player-hand .reversed-card');

        $i->pickUpCard(
            TestGames::game19(),
            1,
            TestSecrets::secretaris()
        );

        // Wait for thrown card to disappear
        $i->waitForElementNotVisible('.other-player-card [data-card-number="5"]');
        // Card still visible in hand of secretaris
        $i->waitForElement('div[data-player="'.TestPlayers::secretaris()->toString().'"].player-hand [data-card-number="5"]');
    }

    public function checkIReviewPreviousTrick(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/b55faeae-c43a-5868-909f-5cd4337ecba1/play/ce29fba8-a25e-4ed6-abdf-3128daf90542
        $i->amOnPage('/en/table/'.TestTables::table21()->toString().'/play/'.TestSecrets::penningmeester()->toString());

        $i->waitForElement('.review-request');
        $i->click('.review-request');

        $i->waitForText('Review previous trick');
        $i->click('Review previous trick');

        $i->waitForText('Reviewed by penn');
        // ace of diamonds
        $i->waitForElement("span[data-card-number='27']");
    }

    public function checkIReopenPreviousTrick(AcceptanceTester $i): void
    {
        // http://localhost:8080/en/table/e2add500-241e-5e93-843d-99a837276195/play/ce29fba8-a25e-4ed6-abdf-3128daf90542
        $i->amOnPage('/en/table/'.TestTables::table31()->toString().'/play/'.TestSecrets::penningmeester()->toString());

        $i->waitForElement('.review-request');
        $i->click('.review-request');

        $i->waitForText('Reopen previous trick');
        $i->click('Reopen previous trick');

        // Waiting for ace of diamonds to reappear
        $i->waitForElement(".other-player-card span[data-card-number='27']");

        // I should be able to click it again as winner (check whether card mat is functional again)
        $i->waitForElementClickable("span[data-card-number='27']");
        $i->click("span[data-card-number='27']");
        $i->waitForElement("span[data-card-number='27'].winner");

        // Now let's do this for 2 other players as well, and see whether trick is picked up again.
        $i->amOnPage('/en/table/'.TestTables::table31()->toString().'/play/'.TestSecrets::dtl()->toString());
        $i->waitForElement("span[data-card-number='27']");
        $i->click("span[data-card-number='27']");

        $i->amOnPage('/en/table/'.TestTables::table31()->toString().'/play/'.TestSecrets::dpb()->toString());
        $i->waitForElement("span[data-card-number='27']");
        $i->click("span[data-card-number='27']");

        $i->waitForElementNotVisible("span[data-card-number='27']");
    }
}
