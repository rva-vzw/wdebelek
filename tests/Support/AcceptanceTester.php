<?php

namespace App\Tests\Support;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\GameIdentifier;
use Codeception\Actor;

/**
 * Inherited Methods.
 *
 * @method void                    wantToTest($text)
 * @method void                    wantTo($text)
 * @method void                    execute($callable)
 * @method void                    expectTo($prediction)
 * @method void                    expect($prediction)
 * @method void                    amGoingTo($argumentation)
 * @method void                    am($role)
 * @method void                    lookForwardTo($achieveValue)
 * @method void                    comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
 */
class AcceptanceTester extends Actor
{
    use _generated\AcceptanceTesterActions;

    /*
     * Define custom actions here
     */

    public function playCard(
        GameIdentifier $gameIdentifier,
        int $trickNumber,
        Secret $playerSecret,
        Card $card
    ): void {
        $this->openNewTab();
        $page = "/hack/{$gameIdentifier->toString()}/play/{$trickNumber}/{$playerSecret->toString()}";
        $this->amOnPage($page);

        $this->submitForm(
            'form',
            [
                'cardNumber' => $card->getCardNumber(),
            ]
        );
        $this->closeTab();
    }

    public function pickUpCard(
        GameIdentifier $gameIdentifier,
        int $trickNumber,
        Secret $playerSecret
    ): void {
        $this->openNewTab();
        $page = "/hack/{$gameIdentifier->toString()}/pick-up/{$trickNumber}/{$playerSecret->toString()}";
        $this->amOnPage($page);

        $this->submitForm(
            'form',
            []
        );
        $this->closeTab();
    }
}
