<?php

declare(strict_types=1);

namespace App\Tests\Api;

use App\TestData\TestPlayers;
use App\TestData\TestTables;
use App\Tests\Support\ApiTester;

final class PlayerCest
{
    /** @test */
    public function itRetrievesPlayerNames(ApiTester $i): void
    {
        $tableIdentifier = TestTables::table10();

        // http://localhost:8080/api/table/5588e3cc-5e78-584f-8b88-e5b9e128041d/players
        $i->sendGet("table/{$tableIdentifier->toString()}/players");
        $i->seeResponseIsJson();
        $i->seeResponseContainsJson([
            ['id' => TestPlayers::dtl()->toString(), 'name' => 'dtl'],
            ['id' => TestPlayers::dpb()->toString(), 'name' => 'dpb'],
            ['id' => TestPlayers::ddb()->toString(), 'name' => 'ddb'],
            ['id' => TestPlayers::secretaris()->toString(), 'name' => 'secr'],
        ]);
    }
}
