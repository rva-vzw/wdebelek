<?php

declare(strict_types=1);

namespace App\Tests\Api;

use App\TestData\TestGames;
use App\TestData\TestSecrets;
use App\TestData\TestTables;
use App\Tests\Support\ApiTester;

final class HandCest
{
    public function itReturnsPersonalPlayerInfo(ApiTester $i): void
    {
        $tableIdentifier = TestTables::table1();
        $playerSecret = TestSecrets::dpb();

        // http://localhost:8080/api/table/a4e394bd-d3a0-55fa-95f9-b2f93e77c0df/player/c99f0f58-7895-44c3-a10e-e8ac2154aaa1

        $i->sendGet('table/'.$tableIdentifier->toString().'/player/'.$playerSecret->toString());
        $i->seeResponseIsJson();
        $i->seeResponseContainsJson(
            [38, 39, 40, 41, 2, 3, 4, 5, 20, 21, 22, 23, 24],
        );
    }

    public function itSortsHand(ApiTester $i): void
    {
        $gameIdentifier = TestGames::game1();
        $playerSecret = TestSecrets::dpb();

        // curl 'http://localhost:8080/api/game/18d48ae6-dcad-4551-8abc-2c1622036229/hand/c99f0f58-7895-44c3-a10e-e8ac2154aaa1' -X PUT --data '{"cards":[30,31,32,33,46,47,48,49,14,10,11,12,13]}'

        // This doesn't make sense, but it fixes a phpstan warning.
        /** @var array<mixed> $params */
        $params = json_encode([
            'cards' => [38, 39, 40, 41, 2, 3, 4, 5, 20, 21, 22, 23, 24],
        ]);

        $i->sendPUT(
            'game/'.$gameIdentifier->toString().'/hand/'.$playerSecret->toString(),
            // It seems that axios sends requests like this.
            $params
        );

        $i->seeResponseEquals('');
    }

    public function itRetrievesShownHands(ApiTester $i): void
    {
        $gameIdentifier = TestGames::game1();
        $playerSecret = TestSecrets::secretaris();

        // http://localhost:8080/api/game/18d48ae6-dcad-4551-8abc-2c1622036229/cards/a4e394bd-d3a0-55fa-95f9-b2f93e77c0df/player/46d2015e-2f98-48aa-bf05-0b27f6707b20

        $i->sendGet('game/'.$gameIdentifier->toString().'/cards/'.$playerSecret->toString());
        $i->seeResponseIsJson();
    }
}
