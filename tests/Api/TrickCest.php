<?php

declare(strict_types=1);

namespace App\Tests\Api;

use App\TestData\TestGames;
use App\TestData\TestSecrets;
use App\Tests\Support\ApiTester;

final class TrickCest
{
    public function itPlaysCard(ApiTester $i): void
    {
        $gameIdentifier = TestGames::game1();
        $playerSecret = TestSecrets::secretaris();

        // curl 'http://localhost:8080/api/game/18d48ae6-dcad-4551-8abc-2c1622036229/trick/1/46d2015e-2f98-48aa-bf05-0b27f6707b20' -X POST --data '{"card":13}'

        // This doesn't make sense, but it fixes a phpstan warning.
        /** @var array<mixed> $params */
        $params = json_encode(['card' => 13]);
        $i->sendPOST(
            'game/'.$gameIdentifier->toString().'/trick/1/'.$playerSecret->toString(),
            $params
        );

        $i->seeResponseEquals('');
    }
}
