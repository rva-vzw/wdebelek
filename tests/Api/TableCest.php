<?php

declare(strict_types=1);

namespace App\Tests\Api;

use App\TestData\TestTables;
use App\Tests\Support\ApiTester;

final class TableCest
{
    public function itReturnsEmptyHandArrayWhenRequestingTableStateWithNoStartedGame(ApiTester $i): void
    {
        $tableIdentifier = TestTables::table9();

        $i->sendGet('table/'.$tableIdentifier->toString());
        $i->seeResponseIsJson();
        $i->seeResponseContainsJson([]);
    }

    public function itReturnsPlayersInCorrectOrder(ApiTester $i): void
    {
        $tableIdentifier = TestTables::table1();

        // http://localhost:8080/api/table/a4e394bd-d3a0-55fa-95f9-b2f93e77c0df/players/playing
        $i->sendGet('table/'.$tableIdentifier->toString().'/players/playing');

        // Note that I removed the 'cardsInHand' property from the expected result.
        // This is not a problem, because I use 'response *contains* json'.
        // I left the cardsInHand property out, because depending on whether the other
        // tests have run or not, player 'secr' might have thrown a card.
        $i->seeResponseContainsJson(
            [
                0 => [
                        'tricksWon' => 0,
                        'lastPlayer' => false,
                        'playerName' => 'dtl',
                        'playerIdentifier' => '1f41d1bc-0426-56a4-b59e-3c2f97b8883c',
                        'shownTrump' => null,
                        'tableIdentifier' => 'a4e394bd-d3a0-55fa-95f9-b2f93e77c0df',
                        'seat' => 0,
                    ],
                1 => [
                        'tricksWon' => 0,
                        'lastPlayer' => false,
                        'playerName' => 'dpb',
                        'playerIdentifier' => 'd08dfaf8-aafe-589c-8e6c-63c9af68e086',
                        'shownTrump' => null,
                        'tableIdentifier' => 'a4e394bd-d3a0-55fa-95f9-b2f93e77c0df',
                        'seat' => 1,
                    ],
                2 => [
                        'tricksWon' => 0,
                        'lastPlayer' => true,
                        'playerName' => 'ddb',
                        'playerIdentifier' => '4513d272-d90a-589f-8a24-de611f071f51',
                        'shownTrump' => null,
                        'tableIdentifier' => 'a4e394bd-d3a0-55fa-95f9-b2f93e77c0df',
                        'seat' => 2,
                    ],
                3 => [
                        'tricksWon' => 0,
                        'lastPlayer' => false,
                        'playerName' => 'secr',
                        'playerIdentifier' => '54cfb752-c723-55d0-9e6f-d5790a6b82af',
                        'shownTrump' => null,
                        'tableIdentifier' => 'a4e394bd-d3a0-55fa-95f9-b2f93e77c0df',
                        'seat' => 3,
                    ],
            ]
        );
    }
}
