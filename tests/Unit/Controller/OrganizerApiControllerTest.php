<?php

declare(strict_types=1);

namespace App\Tests\Unit\Controller;

use App\Controller\OrganizerApiController;
use App\Domain\ReadModel\PlayerProfile\PlayerProfiles;
use App\Domain\ReadModel\TableState\SuspendedTables;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Table\Command\KickPlayer;
use App\Domain\WriteModel\Table\Command\PrepareGame;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestPlayers;
use App\TestData\TestProfiles;
use App\TestData\TestSecrets;
use PHPUnit\Framework\TestCase;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class OrganizerApiControllerTest extends TestCase
{
    /** @test */
    public function itShufflesTheCardsWhenPreparingFirstGame(): void
    {
        $dummyNormalizer = $this->createMock(NormalizerInterface::class);
        /** @var NormalizerInterface $normalizer */
        $normalizer = $dummyNormalizer;

        $commandBusMock = $this->createMock(CommandBus::class);
        $commandBusMock->expects($this->once())
            ->method('dispatch')
            ->willReturnCallback(
                function (PrepareGame $command): void {
                    // theoretically it could be that the shuffled cards
                    // are identical to the unshuffled cards, but let's
                    // suppose this chance is neglectible.
                    $this->assertNotEquals(
                        SimplePileOfCards::complete(),
                        $command->getNumberOfCards()
                    );
                }
            );

        /** @var CommandBus $commandBus */
        $commandBus = $commandBusMock;

        $requestStub = $this->createMock(Request::class);
        $requestStub->method('getContent')
            ->willReturn(json_encode([
                'dealer' => TestPlayers::dpb()->toString(),
                'hostSecret' => '00a737bc-ac6a-470c-8c8c-bb9d5841e9d4',
                'cards' => 52,
            ]));

        /** @var Request $request */
        $request = $requestStub;

        $suspendedTablesStub = $this->createMock(SuspendedTables::class);
        $suspendedTablesStub->method('hasSuspendedTable')->willReturn(false);

        /** @var SuspendedTables $suspendedTables */
        $suspendedTables = $suspendedTablesStub;

        $controller = new OrganizerApiController($commandBus, $normalizer);
        $controller->prepareGame($requestStub, 'ea8ebcfc-f6b9-4428-9385-a81f8d752f8d', $suspendedTables);
    }

    /** @test */
    public function itKicksTheCorrectPlayer(): void
    {
        $dummyNormalizer = $this->createMock(NormalizerInterface::class);
        /** @var NormalizerInterface $normalizer */
        $normalizer = $dummyNormalizer;

        $playerProfilesStub = $this->createMock(PlayerProfiles::class);
        $playerProfilesStub->method('getPlayerProfile')
            ->willReturn(TestProfiles::dtl());
        /** @var PlayerProfiles $playerProfiles */
        $playerProfiles = $playerProfilesStub;

        $tableSecret = Secret::fromString('22e4e746-1400-412d-b683-633f2603ec33');
        $tableIdentifier = TableIdentifier::withSecret($tableSecret);
        $expectedCommand = new KickPlayer(
            $tableIdentifier,
            TestPlayers::dpb(),
            TestPlayers::dtl()
        );

        $commandBusMock = $this->createMock(CommandBus::class);
        $commandBusMock->expects($this->once())
            ->method('dispatch')
            ->with($expectedCommand);

        /** @var CommandBus $commandBus */
        $commandBus = $commandBusMock;

        $controller = new OrganizerApiController($commandBus, $normalizer);
        $controller->kickPlayer(
            $tableSecret->toString(),
            TestPlayers::dpb()->toString(),
            TestSecrets::dtl()->toString(),
            $playerProfiles
        );
    }
}
