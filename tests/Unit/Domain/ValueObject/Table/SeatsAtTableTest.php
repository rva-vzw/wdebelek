<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\ValueObject\Table;

use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\ValueObject\Table\SeatsAtTable;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;

final class SeatsAtTableTest extends TestCase
{
    /** @test */
    public function itAssignsPlayerToSeat(): void
    {
        $seats = SeatsAtTable::empty(6)
            ->withPlayerOnSeat(Seat::fromInteger(0), TestPlayers::ddb());

        $this->assertTrue(
            $seats->hasPlayerOnSeat(Seat::fromInteger(0), TestPlayers::ddb())
        );
    }

    /** @test */
    public function itFindsReservedSeat(): void
    {
        $invitation = Invitation::fromString('BC9F9ED5-4DBB-4C9E-9FC1-22ED9AC8C969');
        $seats = SeatsAtTable::empty(6)
            ->withInvitation(
                Seat::fromInteger(0),
                $invitation
            );

        $reservedSeat = $seats->getReservedSeat($invitation);

        $this->assertEquals(Seat::fromInteger(0), $reservedSeat);
    }
}
