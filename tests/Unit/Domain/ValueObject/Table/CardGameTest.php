<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\ValueObject\Table;

use App\Domain\ValueObject\StringObject\Exception\StringTooLong;
use App\Domain\ValueObject\Table\CardGame;
use PHPUnit\Framework\TestCase;

final class CardGameTest extends TestCase
{
    /** @test */
    public function itRefusesLongGameNames(): void
    {
        $this->expectException(StringTooLong::class);

        $cardGame = CardGame::fromString("This is a very long game name, too long to be precise, which we should'nt allow because we don't want to create a messaging system with game names.");
    }

    /** @test */
    public function itReturnsUnspecifiedWhenNoGameNameSpecified(): void
    {
        $cardGame = CardGame::fromString('');

        $this->assertEquals(CardGame::unspecified(), $cardGame);
    }
}
