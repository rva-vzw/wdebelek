<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\ValueObject\Invitation;

use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Invitation\Invitations;
use PHPUnit\Framework\TestCase;

final class InvitationsTest extends TestCase
{
    /** @test */
    public function itCreatesAnEmptyInvitationCollection(): void
    {
        $invitations = Invitations::none();

        $this->assertFalse(
            $invitations->has(Invitation::fromString('fa147b6f-b55d-4977-82f0-860399313ac0'))
        );
    }

    /** @test */
    public function itCreatesNonEmptyInvitationCollection(): void
    {
        $invitation = Invitation::fromString('928bf3b1-340c-44af-944b-e1e3e16c4d0d');
        $invitations = new Invitations($invitation);

        $this->assertTrue($invitations->has($invitation));
        $this->assertFalse($invitations->isEmpty());
    }

    /** @test */
    public function itReturnsRandomInvitation(): void
    {
        $invitation = Invitation::fromString('928bf3b1-340c-44af-944b-e1e3e16c4d0d');
        $invitations = new Invitations($invitation);

        // With single invitation, choosing a random one is not really random, but
        // it makes an easy test :-)

        $actual = $invitations->getRandomInvitation();
        $this->assertEquals($invitation, $actual);
    }
}
