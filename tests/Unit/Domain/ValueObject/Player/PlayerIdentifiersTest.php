<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\ValueObject\Player;

use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;

final class PlayerIdentifiersTest extends TestCase
{
    /** @test */
    public function itGetsPreviousPlayer(): void
    {
        $playerIdentifiers = new SimplePlayerIdentifiers(
            TestPlayers::secretaris(),
            TestPlayers::dtl(),
            TestPlayers::penningmeester(),
            TestPlayers::ddb()
        );

        $actual = $playerIdentifiers->getPreviousPlayer(TestPlayers::secretaris());

        $this->assertEquals(TestPlayers::ddb(), $actual);
    }

    /** @test */
    public function itDoesntComplainWhenRemovingNonExistingPlayer(): void
    {
        $this->assertEquals(
            SimplePlayerIdentifiers::none(),
            SimplePlayerIdentifiers::none()->without(TestPlayers::secretaris())
        );
    }
}
