<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\ValueObject\Player;

use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Player\TableMates;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;

final class TableMatesTest extends TestCase
{
    /** @test */
    public function itReturnsLastPlayerForGroupOfThree(): void
    {
        $playerIdentifiers = new SimplePlayerIdentifiers(
            TestPlayers::secretaris(),
            TestPlayers::dtl(),
            TestPlayers::penningmeester(),
        );
        $tableMates = TableMates::fromPlayers($playerIdentifiers);

        $actual = $tableMates->getLastPlayerForDealer(TestPlayers::secretaris());
        $this->assertEquals(TestPlayers::secretaris(), $actual);
    }

    /** @test */
    public function itCorrectlyOrdersTheFourPlayersOnTableOfFive(): void
    {
        $tableMates = TableMates::fromPlayers(new SimplePlayerIdentifiers(
            TestPlayers::dtl(),
            TestPlayers::dpb(),
            TestPlayers::ddb(),
            TestPlayers::secretaris(),
            TestPlayers::penningmeester(),
        ));

        $actual = $tableMates->getFourPlayers(TestPlayers::secretaris());
        $expected = new SimplePlayerIdentifiers(
            TestPlayers::penningmeester(),
            TestPlayers::dtl(),
            TestPlayers::dpb(),
            TestPlayers::ddb()
        );

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itCorrectlyOrdersTheFourPlayersOnTableOfSix(): void
    {
        $tableMates = TableMates::fromPlayers(new SimplePlayerIdentifiers(
            TestPlayers::dtl(),
            TestPlayers::dpb(),
            TestPlayers::ddb(),
            TestPlayers::secretaris(),
            TestPlayers::penningmeester(),
            TestPlayers::diu()
        ));

        $actual = $tableMates->getFourPlayers(TestPlayers::secretaris());
        $expected = new SimplePlayerIdentifiers(
            TestPlayers::penningmeester(),
            TestPlayers::diu(),
            TestPlayers::dpb(),
            TestPlayers::ddb()
        );

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itCorrectlyOrdersThePlayersOnTableOfThree(): void
    {
        $tableMates = TableMates::fromPlayers(new SimplePlayerIdentifiers(
            TestPlayers::ddb(),
            TestPlayers::secretaris(),
            TestPlayers::penningmeester(),
        ));

        $actual = $tableMates->getAtMostFourPlayers(TestPlayers::secretaris());
        $expected = new SimplePlayerIdentifiers(
            TestPlayers::penningmeester(),
            TestPlayers::ddb(),
            TestPlayers::secretaris()
        );

        $this->assertEquals($expected, $actual);
    }
}
