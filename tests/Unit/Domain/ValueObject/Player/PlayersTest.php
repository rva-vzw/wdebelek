<?php

namespace App\Tests\Unit\Domain\ValueObject\Player;

use App\Domain\ValueObject\Player\Exception\PlayerNotFound;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;

final class PlayersTest extends TestCase
{
    /** @test */
    public function itCreatesParticipatingPlayers(): void
    {
        $players = new SimplePlayerIdentifiers(TestPlayers::penningmeester());

        $this->assertInstanceOf(SimplePlayerIdentifiers::class, $players);
        $this->assertEquals(1, $players->count());
    }

    /** @test */
    public function itCantPutNonExistingPlayerAtLastPlace(): void
    {
        $players = new SimplePlayerIdentifiers(
            TestPlayers::dtl(),
            TestPlayers::secretaris(),
            TestPlayers::ddb(),
            TestPlayers::dpb()
        );

        $this->expectException(PlayerNotFound::class);

        $players->withLastPlayer(TestPlayers::penningmeester());
    }

    /** @test */
    public function itCorrectlyOrdersPlayer(): void
    {
        $players = new SimplePlayerIdentifiers(
            TestPlayers::dtl(),
            TestPlayers::secretaris(),
            TestPlayers::ddb(),
            TestPlayers::dpb()
        );

        $withDdbLast = $players->withLastPlayer(TestPlayers::dtl());

        $expected = new SimplePlayerIdentifiers(
            TestPlayers::secretaris(),
            TestPlayers::ddb(),
            TestPlayers::dpb(),
            TestPlayers::dtl()
        );

        $this->assertEquals($expected, $withDdbLast);
    }
}
