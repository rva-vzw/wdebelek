<?php

namespace App\Tests\Unit\Domain\ValueObject\Player;

use App\Domain\ValueObject\Player\Team;
use App\Domain\ValueObject\Player\Teams;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;

final class TeamsTest extends TestCase
{
    /** @test */
    public function itAddsTeamToEmptyTeams(): void
    {
        $team = Team::withPlayers(TestPlayers::dtl(), TestPlayers::penningmeester());
        $teams = Teams::empty()->withAdditionalTeam($team);

        $expected = [$team];
        $actual = iterator_to_array($teams);

        $this->assertEquals($expected, $actual);
    }
}
