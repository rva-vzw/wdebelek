<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\ValueObject\Card;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\Exception\CardNotFound;
use App\Domain\ValueObject\Card\Hands;
use PHPUnit\Framework\TestCase;

class HandsTest extends TestCase
{
    /** @test */
    public function itThrowsWhenCardHolderNotFound(): void
    {
        $hands = Hands::empty();

        $this->expectException(CardNotFound::class);
        $hands->getCardHolder(new Card(1, Card::DIAMONDS));
    }
}
