<?php

namespace App\Tests\Unit\Domain\ValueObject\Card;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Card\SimplePiles;
use PHPUnit\Framework\TestCase;

final class SimplePilesTest extends TestCase
{
    /** @test */
    public function itMerges(): void
    {
        $pile1 = SimplePileOfCards::fromCards(new \ArrayIterator([
            new Card(1, Card::HEARTS),
            new Card(2, Card::HEARTS),
        ]));

        $pile2 = SimplePileOfCards::fromCards(new \ArrayIterator([
            new Card(1, Card::SPADES),
            new Card(2, Card::SPADES),
        ]));

        $piles = new SimplePiles($pile1, $pile2);
        $merged = $piles->merged();

        $expected = SimplePileOfCards::fromCards(new \ArrayIterator([
            new Card(1, Card::HEARTS),
            new Card(2, Card::HEARTS),
            new Card(1, Card::SPADES),
            new Card(2, Card::SPADES),
        ]));

        $this->assertEquals($expected, $merged);
    }
}
