<?php

namespace App\Tests\Unit\Domain\ValueObject\Card;

use App\Domain\ValueObject\Card\Card;
use PHPUnit\Framework\TestCase;

final class CardTest extends TestCase
{
    /** @test */
    public function itCreatesCardFromCardNumber(): void
    {
        $kingOfClubs = new Card(13, 'clubs');
        $aceOfDiamonds = new Card(1, 'diamonds');

        $card1 = Card::fromCardNumber($kingOfClubs->getCardNumber());
        $card2 = Card::fromCardNumber($aceOfDiamonds->getCardNumber());

        $this->assertEquals($kingOfClubs, $card1);
        $this->assertEquals($aceOfDiamonds, $card2);
    }
}
