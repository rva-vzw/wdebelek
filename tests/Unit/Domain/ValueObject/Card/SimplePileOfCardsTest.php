<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\ValueObject\Card;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use PHPUnit\Framework\TestCase;

final class SimplePileOfCardsTest extends TestCase
{
    /** @test */
    public function itCreatesCompletePile(): void
    {
        $cards = SimplePileOfCards::complete();

        $this->assertInstanceOf(SimplePileOfCards::class, $cards);
        $this->assertEquals(52, $cards->count());
    }

    /** @test */
    public function itCutsTheCards(): void
    {
        $cards = SimplePileOfCards::complete()->cutAfter(29);

        $this->assertEquals(new Card(4, Card::DIAMONDS), $cards->getFirst());
        $this->assertEquals(new Card(3, Card::DIAMONDS), $cards->getLast());
    }

    /** @test */
    public function itRecognizesPilesWithSameCards(): void
    {
        $pile1 = SimplePileOfCards::fromCards(new \ArrayIterator([
            new Card(1, Card::HEARTS),
            new Card(12, Card::HEARTS),
        ]));

        $pile2 = SimplePileOfCards::fromCards(new \ArrayIterator([
            new Card(12, Card::HEARTS),
            new Card(1, Card::HEARTS),
        ]));

        $this->assertTrue($pile1->hasSameCardsAs($pile2));
    }

    /** @test */
    public function itRemovesACard(): void
    {
        $pile = SimplePileOfCards::fromCards(new \ArrayIterator([
            new Card(1, Card::HEARTS),
            new Card(2, Card::HEARTS),
            new Card(3, Card::HEARTS),
        ]));

        $actual = $pile->without(new Card(2, Card::HEARTS));

        $expected = SimplePileOfCards::fromCards(new \ArrayIterator([
            new Card(1, Card::HEARTS),
            new Card(3, Card::HEARTS),
        ]));

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function itDoesntMindRemovingACardThatIsNotThere(): void
    {
        $pile = SimplePileOfCards::fromCards(new \ArrayIterator([
            new Card(1, Card::HEARTS),
            new Card(2, Card::HEARTS),
            new Card(3, Card::HEARTS),
        ]));

        $actual = $pile->without(new Card(4, Card::HEARTS));

        $this->assertEquals($pile, $actual);
    }

    /** @test */
    public function itCantHaveTheSameCardTwice(): void
    {
        $card = new Card(10, Card::DIAMONDS);

        $pile1 = SimplePileOfCards::empty()->with($card);
        $pile2 = $pile1->with($card);

        $this->assertEquals($pile1, $pile2);
    }
}
