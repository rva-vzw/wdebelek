<?php

namespace App\Tests\Unit\Domain\ReadModel\PersonalPlayerInfo;

use App\Domain\ReadModel\PersonalPlayerInfo\PersonalPlayerInfo;
use App\Domain\ReadModel\PersonalPlayerInfo\PersonalPlayerInfoProjector;
use App\Domain\ReadModel\PersonalPlayerInfo\PersonalPlayerInfoRepository;
use App\Domain\ReadModel\PersonalPlayerInfo\PlayerHandUpdater;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\Hand;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Game\Event\CardPickedUp;
use App\Domain\WriteModel\Game\Event\GameStarted;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\Event\CardsCollected;
use App\Domain\WriteModel\Table\Event\TableClaimed;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;

final class PersonalPlayerInfoProjectorTest extends TestCase
{
    /** @var PlayerHandUpdater */
    private $dummyPlayerHandUpdater;

    protected function setUp(): void
    {
        parent::setUp();

        $dummyPlayerHandUpdater = $this->createMock(PlayerHandUpdater::class);
        $this->dummyPlayerHandUpdater = $dummyPlayerHandUpdater;
    }

    /** @test */
    public function itPicksUpACard(): void
    {
        $playerHandUpdaterMock = $this->createMock(PlayerHandUpdater::class);
        $playerHandUpdaterMock->expects($this->once())
            ->method('receiveCards');

        /** @var PlayerHandUpdater $playerHandUpdater */
        $playerHandUpdater = $playerHandUpdaterMock;

        $dummyPersonalPlayerInfoRepository = $this->createMock(PersonalPlayerInfoRepository::class);
        /** @var PersonalPlayerInfoRepository $dummyPersonalPlayerInfoRepository */
        $personalPlayerInfoRepository = $dummyPersonalPlayerInfoRepository;

        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('5b2de6c9-8b01-4993-982a-9e4234ab8560'));
        $gameIdentifier = GameIdentifier::fromNumber($tableIdentifier, GameNumber::first());

        $projector = new PersonalPlayerInfoProjector($playerHandUpdater, $personalPlayerInfoRepository);
        $projector->__invoke(new CardPickedUp(
            $gameIdentifier,
            PlayerIdentifier::withSecret(Secret::fromString('da3ef4c4-ceff-4bf2-b279-7b996a09adcf')),
            new Card(1, Card::SPADES),
            1,
            $tableIdentifier
        ));
    }

    /** @test */
    public function itCreatesPersonalInfoWhenTableClaimed(): void
    {
        $dummyPlayerHandUpdater = $this->createMock(PlayerHandUpdater::class);
        /** @var PlayerHandUpdater $playerHandUpdater */
        $playerHandUpdater = $dummyPlayerHandUpdater;

        $personalPlayerInfoRepositoryMock = $this->createMock(PersonalPlayerInfoRepository::class);
        $personalPlayerInfoRepositoryMock->expects($this->atLeastOnce())
            ->method('savePersonalPlayerInfo');
        /** @var PersonalPlayerInfoRepository $personalPlayerInfoRepository */
        $personalPlayerInfoRepository = $personalPlayerInfoRepositoryMock;

        $projector = new PersonalPlayerInfoProjector($playerHandUpdater, $personalPlayerInfoRepositoryMock);
        $projector->__invoke(new TableClaimed(
            TableIdentifier::withSecret(Secret::fromString('30c61e78-2792-4030-9651-0a06a650ec56')),
            TestPlayers::penningmeester(),
            PlayerName::fromString('penn'),
            Seat::zero(),
            Language::default()
        ));
    }

    /** @test */
    public function itClearsTrumpWhenCardsCollected(): void
    {
        $dummyPlayerHandUpdater = $this->createMock(PlayerHandUpdater::class);
        /** @var PlayerHandUpdater $playerHandUpdater */
        $playerHandUpdater = $dummyPlayerHandUpdater;

        $gameIdentifier = GameIdentifier::fromString('f8341a4b-b8b1-412d-a164-d81d8535b3f1');

        $repository = new class() implements PersonalPlayerInfoRepository {
            public function getPersonalPlayerInfo(
                PlayerIdentifier $playerIdentifier,
                TableIdentifier $tableIdentifier
            ): PersonalPlayerInfo {
                return new PersonalPlayerInfo(
                    Hand::fromCards(new \ArrayIterator([
                            new Card(1, Card::HEARTS),
                    ])),
                    new Card(1, Card::HEARTS),
                    GameIdentifier::fromString('f8341a4b-b8b1-412d-a164-d81d8535b3f1'),
                    $playerIdentifier,
                    $tableIdentifier,
                    false
                );
            }

            public function savePersonalPlayerInfo(PersonalPlayerInfo $playerInfo): void
            {
                if ($playerInfo->hasTrumpCard()) {
                    throw new \Exception('There is no trump as long as no cards were dealt.');
                }
            }

            public function getPlayerInfosByTable(TableIdentifier $tableIdentifier): \Traversable
            {
                return new \ArrayIterator([
                    $this->getPersonalPlayerInfo(TestPlayers::penningmeester(), $tableIdentifier),
                ]);
            }
        };

        $projector = new PersonalPlayerInfoProjector($dummyPlayerHandUpdater, $repository);
        $projector->__invoke(new CardsCollected(
            TableIdentifier::withSecret(Secret::fromString('1d0b197b-c461-4759-b0c7-9c74065e6a91')),
            SimplePileOfCards::complete()
        ));

        // If no exception occurred, it's fine, see #100 and #169.
        $this->assertTrue(true);
    }

    /** @test */
    public function itUpdatesGameNumberWhenNewGameStarted(): void
    {
        $playerIdentifier = TestPlayers::diu();
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('950ffd91-d75a-493e-95f9-4ebc3dd72873'));

        $personalPlayerInfo = PersonalPlayerInfo::forPlayerAtTable(
            Hand::empty(),
            $playerIdentifier,
            $tableIdentifier
        )->playingGame(GameIdentifier::fromNumber($tableIdentifier, GameNumber::first()));

        $expectedPersonalPlayerInfo = PersonalPlayerInfo::forPlayerAtTable(
            Hand::empty(),
            $playerIdentifier,
            $tableIdentifier
        )->playingGame(GameIdentifier::fromNumber($tableIdentifier, GameNumber::fromInteger(2)));

        $personalPlayerInfoRepositoryMock = $this->createMock(PersonalPlayerInfoRepository::class);
        $personalPlayerInfoRepositoryMock->method('getPersonalPlayerInfo')
            ->with($playerIdentifier)->willReturn($personalPlayerInfo);
        $personalPlayerInfoRepositoryMock->expects($this->atLeastOnce())
            ->method('savePersonalPlayerInfo')->with($expectedPersonalPlayerInfo);

        /** @var PersonalPlayerInfoRepository $personalPlayerInfoRepository */
        $personalPlayerInfoRepository = $personalPlayerInfoRepositoryMock;

        $projector = new PersonalPlayerInfoProjector(
            $this->dummyPlayerHandUpdater,
            $personalPlayerInfoRepository
        );

        $projector->__invoke(new GameStarted(
            GameIdentifier::fromNumber($tableIdentifier, GameNumber::fromInteger(2)),
            new SimplePlayerIdentifiers($playerIdentifier),
            SimplePileOfCards::complete(),
            $tableIdentifier,
            $playerIdentifier,
            $playerIdentifier,
            new \DateTimeImmutable('2021-01-07')
        ));
    }

    /** @test */
    public function itEmptiesHandWhenCardsCollected(): void
    {
        $playerIdentifier = TestPlayers::diu();
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('950ffd91-d75a-493e-95f9-4ebc3dd72873'));

        $personalPlayerInfo = PersonalPlayerInfo::forPlayerAtTable(
            Hand::fromCards(new \ArrayIterator([
                new Card(3, Card::CLUBS),
            ])),
            $playerIdentifier,
            $tableIdentifier
        )->playingGame(GameIdentifier::fromNumber($tableIdentifier, GameNumber::first()))->withCardsOpen();

        $expectedPersonalPlayerInfo = PersonalPlayerInfo::forPlayerAtTable(
            Hand::empty(),
            $playerIdentifier,
            $tableIdentifier
            // the game number is not very relevant for this test.
        )->playingGame(GameIdentifier::fromNumber($tableIdentifier, GameNumber::first()));

        $personalPlayerInfoRepositoryMock = $this->createMock(PersonalPlayerInfoRepository::class);
        $personalPlayerInfoRepositoryMock->method('getPlayerInfosByTable')
            ->with($tableIdentifier)->willReturn(new \ArrayIterator([$personalPlayerInfo]));
        $personalPlayerInfoRepositoryMock->expects($this->atLeastOnce())
            ->method('savePersonalPlayerInfo')->with($expectedPersonalPlayerInfo);

        /** @var PersonalPlayerInfoRepository $personalPlayerInfoRepository */
        $personalPlayerInfoRepository = $personalPlayerInfoRepositoryMock;

        $projector = new PersonalPlayerInfoProjector(
            $this->dummyPlayerHandUpdater,
            $personalPlayerInfoRepository
        );

        $projector->__invoke(new CardsCollected(
            $tableIdentifier,
            SimplePileOfCards::complete()
        ));
    }
}
