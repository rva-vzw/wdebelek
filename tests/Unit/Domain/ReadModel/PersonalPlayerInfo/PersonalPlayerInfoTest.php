<?php

namespace App\Tests\Unit\Domain\ReadModel\PersonalPlayerInfo;

use App\Domain\ReadModel\PersonalPlayerInfo\PersonalPlayerInfo;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\Hand;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;

final class PersonalPlayerInfoTest extends TestCase
{
    /** @test */
    public function itAllowsTrumpNotInHand(): void
    {
        // If the trump card is already thrown, it can still be stored in the personal player info.

        $info = PersonalPlayerInfo::forPlayerAtTable(
            Hand::empty()->with(new Card(4, Card::SPADES)),
            TestPlayers::diu(),
            TableIdentifier::withSecret(Secret::fromString('805b3907-f52e-476e-a75d-c539b3f7ad52'))
        )->withTrumpCard(new Card(7, Card::DIAMONDS));

        $this->assertInstanceOf(PersonalPlayerInfo::class, $info);
    }
}
