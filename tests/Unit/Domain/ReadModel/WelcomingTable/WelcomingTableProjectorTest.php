<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\ReadModel\WelcomingTable;

use App\Domain\ReadModel\CardGames\TableCardGames;
use App\Domain\ReadModel\PlayerProfile\PlayerProfiles;
use App\Domain\ReadModel\PlayingPlayer\PlayerCounts;
use App\Domain\ReadModel\WelcomingTable\WelcomingTable;
use App\Domain\ReadModel\WelcomingTable\WelcomingTableProjector;
use App\Domain\ReadModel\WelcomingTable\WelcomingTableRepository;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Invitation\Invitations;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\Event\InvitationAccepted;
use App\Domain\WriteModel\Table\TableIdentifier;
use PHPUnit\Framework\TestCase;

final class WelcomingTableProjectorTest extends TestCase
{
    /** @var PlayerCounts */
    private $dummyPlayerCounts;
    /** @var PlayerProfiles */
    private $dummyPlayerProfiles;
    /** @var TableCardGames */
    private $dummyTableCardGames;

    protected function setUp(): void
    {
        parent::setUp();

        $this->dummyPlayerCounts = $this->createMock(PlayerCounts::class);
        $this->dummyPlayerProfiles = $this->createMock(PlayerProfiles::class);
        $this->dummyTableCardGames = $this->createMock(TableCardGames::class);
    }

    /** @test */
    public function itJustIncreasesPlayerCountWhenInvitationAccepted(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('04154774-5004-4b14-b827-e7a317d87891'));
        $invitation = Invitation::fromString('7bb92925-6e07-42e0-9a15-9d687efdc80c');
        $welcomingTable = new WelcomingTable(
            $tableIdentifier,
            PlayerName::fromString('Frits'),
            new \DateTimeImmutable('2020-10-27'),
            new Invitations($invitation),
            5,
            CardGame::fromString('Wiezen'),
            Language::default()
        );

        $repositoryMock = $this->createMock(WelcomingTableRepository::class);
        $repositoryMock->method('hasWelcomingTable')->willReturn(true);
        $repositoryMock->method('getWelcomingTable')->willReturn($welcomingTable);
        $repositoryMock->expects($this->once())
            ->method('saveWelcomingTable')
            ->with($welcomingTable->withAdditionalPlayer());

        $projector = new WelcomingTableProjector(
            $repositoryMock,
            $this->dummyPlayerCounts,
            $this->dummyPlayerProfiles,
            $this->dummyTableCardGames
        );

        $projector->__invoke(new InvitationAccepted(
            $tableIdentifier,
            $invitation,
            PlayerIdentifier::withSecret(Secret::fromString('49ff89d1-8653-41fd-88aa-547b94deee22')),
            PlayerName::fromString('Frats'),
            Seat::fromInteger(5),
            Language::default()
        ));
    }
}
