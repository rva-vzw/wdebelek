<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\ReadModel\TrickAtTable;

use App\Domain\ReadModel\TrickAtTable\TrickAtTableProjector;
use App\Domain\ReadModel\TrickAtTable\TrickAtTableRepository;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Game\Trick;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\Event\CardPickedUp;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;

final class TrickAtTableProjectorTest extends TestCase
{
    /** @test */
    public function itPicksUpACard(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('eb5fbd7f-1153-4492-9057-d6b40ad2834e')
        );

        $initialTrickAtTable = Trick::empty()->withPlayedCard(
            TestPlayers::penningmeester(),
            new Card(12, Card::HEARTS)
        );

        $trickAtTableRepositoryMock = $this->createMock(TrickAtTableRepository::class);
        $trickAtTableRepositoryMock->method('getTrickAtTable')
            ->willReturn($initialTrickAtTable);
        $trickAtTableRepositoryMock->expects($this->once())
            ->method('replaceTrickAtTable')
            ->with($tableIdentifier, Trick::empty());

        $projector = new TrickAtTableProjector($trickAtTableRepositoryMock);
        $projector->__invoke(new CardPickedUp(
            GameIdentifier::fromNumber($tableIdentifier, GameNumber::first()),
            TestPlayers::penningmeester(),
            new Card(12, Card::HEARTS),
            1,
            $tableIdentifier
        ));
    }
}
