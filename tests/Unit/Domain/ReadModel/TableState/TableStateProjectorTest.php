<?php

namespace App\Tests\Unit\Domain\ReadModel\TableState;

use App\Domain\ReadModel\TableState\TableState;
use App\Domain\ReadModel\TableState\TableStateProjector;
use App\Domain\ReadModel\TableState\TableStateRepository;
use App\Domain\ReadModel\TableState\TableStateUpdater;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\Event\GameStarted;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;

final class TableStateProjectorTest extends TestCase
{
    /** @test */
    public function itSetsCurrentTrickWhenGameStarted(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('dcbf8fd4-dcee-4689-9673-c68285856fac'));
        $gameIdentifier = GameIdentifier::fromNumber($tableIdentifier, GameNumber::first());

        $tableState = TableState::forNewTable($tableIdentifier, TestPlayers::secretaris());

        $tableStateRepositoryMock = $this->createMock(TableStateRepository::class);
        $tableStateRepositoryMock->method('getTableState')->willReturn($tableState);

        $tableStateRepositoryMock->expects($this->once())->method('saveTableState');
        /** @var TableStateRepository $tableStateRepository */
        $tableStateRepository = $tableStateRepositoryMock;

        // FIXME: get rid of the TableUpdater dependency (#51)
        $dummyTableStateUpdater = $this->createMock(TableStateUpdater::class);
        /** @var TableStateUpdater $tableStateUpdater */
        $tableStateUpdater = $dummyTableStateUpdater;

        $projector = new TableStateProjector($tableStateUpdater, $tableStateRepository);
        $projector->__invoke(new GameStarted(
            $gameIdentifier,
            new SimplePlayerIdentifiers(
                TestPlayers::penningmeester(),
                TestPlayers::ddb(),
                TestPlayers::dtl(),
                TestPlayers::secretaris()
            ),
            SimplePileOfCards::complete(),
            $tableIdentifier,
            TestPlayers::secretaris(),
            TestPlayers::secretaris(),
            new \DateTimeImmutable('2021-01-07')
        ));
    }
}
