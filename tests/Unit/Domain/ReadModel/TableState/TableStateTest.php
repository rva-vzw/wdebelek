<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\ReadModel\TableState;

use App\Domain\ReadModel\TableState\TableState;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Player\Teams;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;

final class TableStateTest extends TestCase
{
    /** @test */
    public function itAssignsAGameNumberToNewTable(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('b733d085-9b29-410a-aa86-7dcd34826cc4'));
        $tableState = TableState::forNewTable(
            $tableIdentifier,
            TestPlayers::penningmeester()
        );

        $this->assertEquals(
            GameIdentifier::fromNumber($tableIdentifier, GameNumber::first()),
            $tableState->getGameIdentifier()
        );
    }

    /** @test */
    public function itDoesntTouchFirstGameWhenSettingNewGameNumber(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('aa8dca94-7c15-4f4b-b26d-6dae1b9eabb9'));
        $tableState = TableState::forNewTable(
            $tableIdentifier,
            TestPlayers::dtl()
        );

        $this->assertTrue($tableState->isSuspended());

        $tableState = $tableState->forNewGame(
            GameIdentifier::fromNumber($tableIdentifier, GameNumber::first()),
            new SimplePlayerIdentifiers(TestPlayers::dpb(), TestPlayers::dtl()),
            52,
            TestPlayers::dtl()
        );

        $this->assertFalse($tableState->isSuspended());
        $this->assertTrue($tableState->isFirstGame());

        $tableState = $tableState->forNewGame(
            GameIdentifier::fromNumber($tableIdentifier, GameNumber::fromInteger(2)),
            new SimplePlayerIdentifiers(TestPlayers::dpb(), TestPlayers::dtl()),
            52,
            TestPlayers::dtl()
        );

        $this->assertFalse($tableState->isSuspended());
        $this->assertFalse($tableState->isFirstGame());
    }

    /** @test */
    public function itClearsRevealedTrumpOnNewGame(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('2195e14d-1842-42e5-8ab2-e03c7bbf53f7'));
        $tableState = TableState::forNewTable($tableIdentifier, TestPlayers::penningmeester())->withRevealedTrump(new Card(2, Card::SPADES));

        $gameIdentifier = GameIdentifier::fromNumber($tableIdentifier, GameNumber::second());
        $players = new SimplePlayerIdentifiers(TestPlayers::dtl(), TestPlayers::secretaris(), TestPlayers::penningmeester());
        $actual = $tableState->forNewGame(
            $gameIdentifier,
            $players,
            52,
            TestPlayers::penningmeester()
        );

        $expected = new TableState(
            $gameIdentifier,
            1,
            $tableIdentifier,
            0,
            0,
            Teams::allIndividual($players),
            CardGame::unspecified(),
            false,
            // forNewGame doesn't change 'firstGame', this is by design. Bad design, but by design.
            null,
            false,
            TestPlayers::penningmeester(),
            TestPlayers::penningmeester(),
            52,
            false
        );

        $this->assertEquals($expected, $actual);
    }
}
