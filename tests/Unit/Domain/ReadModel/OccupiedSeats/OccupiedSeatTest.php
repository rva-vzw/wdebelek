<?php

namespace App\Tests\Unit\Domain\ReadModel\OccupiedSeats;

use App\Domain\ReadModel\OccupiedSeats\OccupiedSeat;
use App\Domain\ReadModel\PlayerProfile\SimplePlayerProfile;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Invitation\InvitationPrivacy;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;

final class OccupiedSeatTest extends TestCase
{
    /** @test */
    public function itCreatesInitialOccupiedSeat(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('bbd2de64-f0a9-4cef-8556-5e79aaaf7630'));
        $playerProfile = new SimplePlayerProfile(
            TestPlayers::dpb(),
            PlayerName::fromString('dpb'),
            Language::nl()
        );

        $occupiedSeat = OccupiedSeat::initial($tableIdentifier, $playerProfile);

        $expected = new OccupiedSeat(
            $tableIdentifier,
            TestPlayers::dpb(),
            PlayerName::fromString('dpb'),
            '',
            Seat::zero(),
            null,
            // The initial player was invited by himself.
            PlayerName::fromString('dpb'),
            Language::nl(),
            InvitationPrivacy::expired()
        );

        $this->assertEquals($expected, $occupiedSeat);
    }

    /** @test */
    public function itInvalidatesInvitationWhenAccepting(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('73634b8a-3ef1-4325-a1e3-9085badc24cd'));
        $invitation = Invitation::fromString('f877c5ab-d172-471b-9790-7e421d2e6f67');
        $occupiedSeat = OccupiedSeat::invited(
            $tableIdentifier,
            'https://kaart.rijkvanafdronk.be/foo/bar',
            Seat::fromInteger(1),
            $invitation,
            PlayerName::fromString('penningmeester'),
            Language::default()
        );

        $actual = $occupiedSeat->acceptedBy(
            TestPlayers::secretaris(),
            PlayerName::fromString('secretaris')
        );

        $expected = new OccupiedSeat(
            $tableIdentifier,
            TestPlayers::secretaris(),
            PlayerName::fromString('secretaris'),
            '',
            Seat::fromInteger(1),
            $invitation,
            PlayerName::fromString('penningmeester'),
            Language::default(),
            InvitationPrivacy::expired()
        );

        $this->assertEquals($expected, $actual);
    }
}
