<?php

namespace App\Tests\Unit\Domain\ReadModel\SeatedPlayer;

use App\Domain\ReadModel\PlayerProfile\SimplePlayerProfile;
use App\Domain\ReadModel\SeatedPlayer\SeatedPlayerProjector;
use App\Domain\ReadModel\SeatedPlayer\SeatedPlayerRepository;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\Event\TableClaimed;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;

final class SeatedPlayerProjectorTest extends TestCase
{
    /** @test */
    public function itSavesPlayerWhoClaimsTable(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('f7ecb55a-7067-47b3-9f31-be811b1defbe'));

        $playerProfile = new SimplePlayerProfile(
            TestPlayers::penningmeester(),
            PlayerName::fromString('penningmeester'),
            Language::default()
        );

        $seatedPlayerRepositoryMock = $this->createMock(SeatedPlayerRepository::class);
        $seatedPlayerRepositoryMock->expects($this->atLeastOnce())
            ->method('putPlayerAtTable')
            ->with(
                $playerProfile,
                $tableIdentifier
            );

        /** @var SeatedPlayerRepository $seatedPlayerRepository */
        $seatedPlayerRepository = $seatedPlayerRepositoryMock;

        $projector = new SeatedPlayerProjector($seatedPlayerRepository);

        $projector->__invoke(
            new TableClaimed(
                $tableIdentifier,
                TestPlayers::penningmeester(),
                PlayerName::fromString('penningmeester'),
                Seat::zero(),
                Language::default()
            )
        );
    }
}
