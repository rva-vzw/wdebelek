<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\ReadModel\GameCounter;

use App\Domain\ReadModel\GameCounter\Month;
use PHPUnit\Framework\TestCase;

final class MonthTest extends TestCase
{
    /** @test */
    public function itGetsStartDate(): void
    {
        $month = Month::fromYearAndDay(2021, 2);
        $this->assertEquals(
            new \DateTimeImmutable('2021-02-01'),
            $month->getStartDate()
        );
    }
}
