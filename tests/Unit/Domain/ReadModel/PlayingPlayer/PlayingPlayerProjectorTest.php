<?php

namespace App\Tests\Unit\Domain\ReadModel\PlayingPlayer;

use App\Domain\ReadModel\PlayingPlayer\PlayingPlayer;
use App\Domain\ReadModel\PlayingPlayer\PlayingPlayerProjector;
use App\Domain\ReadModel\PlayingPlayer\PlayingPlayerRepository;
use App\Domain\ReadModel\SeatedPlayer\SeatedPlayers;
use App\Domain\ReadModel\SeatedPlayer\SimpleSeatedPlayer;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Game\Trick;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Game\Event\CardPickedUp;
use App\Domain\WriteModel\Game\Event\CardPlayed;
use App\Domain\WriteModel\Game\Event\TrickWon;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestPlayers;
use App\TestData\TestProfiles;
use PHPUnit\Framework\TestCase;

final class PlayingPlayerProjectorTest extends TestCase
{
    /** @var SeatedPlayers */
    private $dummySeatedPlayers;

    protected function setUp(): void
    {
        parent::setUp();

        $dummySeatedPlayers = $this->createMock(SeatedPlayers::class);
        $this->dummySeatedPlayers = $dummySeatedPlayers;
    }

    /**
     * @test
     */
    public function itUpdatesCardCountWhenCardPlayed(): void
    {
        $repositoryMock = $this->createMock(PlayingPlayerRepository::class);
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('3f7688d0-cd5f-43a9-b8e6-d7daf936cf70'));
        $playingPlayer = PlayingPlayer::create(
            $tableIdentifier,
            SimpleSeatedPlayer::fromProfile(
                TestProfiles::penningmeester(),
                Seat::zero()
            )
        )->withAdditionalCards(3);

        $expectedPlayingPlayer = PlayingPlayer::create(
            $tableIdentifier,
            SimpleSeatedPlayer::fromProfile(
                TestProfiles::penningmeester(),
                Seat::zero()
            )
        )->withAdditionalCards(2);

        $repositoryMock->method('getPlayingPlayer')
            ->willReturn($playingPlayer);
        $repositoryMock->expects($this->atLeastOnce())
            ->method('savePlayingPlayer')
            ->with($expectedPlayingPlayer);

        /** @var PlayingPlayerRepository $repository */
        $repository = $repositoryMock;

        $projector = new PlayingPlayerProjector($repository, $this->dummySeatedPlayers);
        $projector->__invoke(new CardPlayed(
            GameIdentifier::fromNumber($tableIdentifier, GameNumber::first()),
            TestPlayers::penningmeester(),
            new Card(2, Card::CLUBS),
            1,
            $tableIdentifier
        ));
    }

    /**
     * @test
     */
    public function itUpdatesCardCountWhenCardPickedUp(): void
    {
        $repositoryMock = $this->createMock(PlayingPlayerRepository::class);
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('3f7688d0-cd5f-43a9-b8e6-d7daf936cf70'));
        $playingPlayer = PlayingPlayer::create(
            $tableIdentifier,
            SimpleSeatedPlayer::fromProfile(
                TestProfiles::penningmeester(),
                Seat::zero()
            )
        )->withAdditionalCards(3);

        $repositoryMock->method('getPlayingPlayer')
            ->willReturn($playingPlayer);
        $repositoryMock->expects($this->atLeastOnce())
            ->method('savePlayingPlayer')
            ->with($playingPlayer->withAdditionalCard());

        /** @var PlayingPlayerRepository $repository */
        $repository = $repositoryMock;

        $projector = new PlayingPlayerProjector($repository, $this->dummySeatedPlayers);
        $projector->__invoke(new CardPickedUp(
            GameIdentifier::fromNumber($tableIdentifier, GameNumber::first()),
            TestPlayers::penningmeester(),
            new Card(2, Card::CLUBS),
            1,
            $tableIdentifier
        ));
    }

    /**
     * @test
     */
    public function itHidesTrumpCardAfterCollectingFirstTrick(): void
    {
        $repositoryMock = $this->createMock(PlayingPlayerRepository::class);
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('3f7688d0-cd5f-43a9-b8e6-d7daf936cf70'));
        $playingPlayer = PlayingPlayer::create(
            $tableIdentifier,
            SimpleSeatedPlayer::fromProfile(
                TestProfiles::ddb(),
                Seat::zero()
            )
        )->withAdditionalCards(3)->withTrumpRevealed(
            new Card(4, Card::CLUBS)
        );

        // Normally you'd expect 4 players, but for this test, 1 will suffice.
        $repositoryMock->method('getOrderedPlayingPlayers')
            ->willReturn(new \ArrayIterator([
                $playingPlayer,
            ]));
        $repositoryMock->expects($this->atLeastOnce())
            ->method('savePlayingPlayer')
            ->with($playingPlayer->withTrumpHidden());

        /** @var PlayingPlayerRepository $repository */
        $repository = $repositoryMock;

        $projector = new PlayingPlayerProjector($repository, $this->dummySeatedPlayers);
        $projector->__invoke(new TrickWon(
            GameIdentifier::fromNumber($tableIdentifier, GameNumber::first()),
            TestPlayers::penningmeester(),
            1,
            Trick::empty()
                ->withPlayedCard(TestPlayers::dtl(), new Card(4, Card::HEARTS))
                ->withPlayedCard(TestPlayers::penningmeester(), new Card(12, Card::HEARTS))
                ->withPlayedCard(TestPlayers::ddb(), new Card(5, Card::HEARTS))
                ->withPlayedCard(TestPlayers::secretaris(), new Card(3, Card::SPADES)),
            $tableIdentifier
        ));
    }
}
