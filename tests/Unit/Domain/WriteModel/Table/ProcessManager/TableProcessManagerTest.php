<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\WriteModel\Table\ProcessManager;

use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Player\TableMates;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\Command\CutCardsAfter;
use App\Domain\WriteModel\Game\Command\StartGame;
use App\Domain\WriteModel\Game\Event\GameEnded;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\Command\CollectCards;
use App\Domain\WriteModel\Table\Command\PrepareNextGame;
use App\Domain\WriteModel\Table\Event\GamePrepared;
use App\Domain\WriteModel\Table\ProcessManager\TableProcessManager;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\NonDeterministic\DateTimeProvider;
use App\NonDeterministic\RandomProvider;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;

final class TableProcessManagerTest extends TestCase
{
    private RandomProvider $randomNumbersStub;
    private DateTimeProvider $dateTimeProviderStub;

    protected function setUp(): void
    {
        parent::setUp();

        // produce reproducible 'random' number for easier testing.
        $this->randomNumbersStub = new class() implements RandomProvider {
            public function getRandomNumber(int $min, int $max): int
            {
                return $min;
            }
        };

        $this->dateTimeProviderStub = new class() implements DateTimeProvider {
            public function getNow(): \DateTimeImmutable
            {
                return new \DateTimeImmutable('2021-01-07');
            }
        };
    }

    /** @test */
    public function itStartsAGameWithTheCorrectPlayers(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('8167240d-ad20-4fd1-8121-1da6b69457b5'));
        $gameIdentifier = GameIdentifier::fromNumber($tableIdentifier, GameNumber::first());

        $players = new SimplePlayerIdentifiers(
            TestPlayers::secretaris(),
            TestPlayers::penningmeester(),
            TestPlayers::dtl(),
            TestPlayers::ddb(),
            TestPlayers::dpb()
        );
        $tableMates = TableMates::fromPlayers($players);

        $expectedCommand = new StartGame(
            $gameIdentifier,
            $players->without(TestPlayers::ddb())->withLastPlayer(TestPlayers::dtl()),
            SimplePileOfCards::complete(),
            $tableIdentifier,
            TestPlayers::ddb(),
            new \DateTimeImmutable('2021-01-07')
        );

        $commandBus = $this->getCommandBusWithExpectedCommands($expectedCommand);

        // 5 players at the table, ddb deals. Expecting started game without ddb.
        $processManager = new TableProcessManager(
            $commandBus,
            $this->randomNumbersStub,
            $this->dateTimeProviderStub
        );
        $processManager->__invoke(new GamePrepared(
            $tableIdentifier,
            TestPlayers::ddb(),
            TestPlayers::secretaris(),
            GameNumber::first(),
            SimplePileOfCards::complete(),
            $tableMates,
            $tableMates->getFourPlayers(TestPlayers::ddb())
        ));
    }

    /** @test */
    public function itPreparesNextGameWhenTheCurrentOneEnds(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('8167240d-ad20-4fd1-8121-1da6b69457b5'));
        $expectedCommands = [
            new CollectCards($tableIdentifier, SimplePileOfCards::complete()),
            new PrepareNextGame($tableIdentifier),
        ];

        $processManager = new TableProcessManager(
            $this->getCommandBusWithExpectedCommands(...$expectedCommands),
            $this->randomNumbersStub,
            $this->dateTimeProviderStub
        );

        $processManager->__invoke(new GameEnded(
            GameIdentifier::fromNumber($tableIdentifier, GameNumber::first()),
            $tableIdentifier,
            SimplePileOfCards::complete()
        ));
    }

    /** @test */
    public function itStartsGameAndCutsTheDeckWhenGameIsPrepared(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('8167240d-ad20-4fd1-8121-1da6b69457b5'));
        $gameIdentifier = GameIdentifier::fromNumber($tableIdentifier, GameNumber::first());
        $currentPlayers = new SimplePlayerIdentifiers(
            TestPlayers::penningmeester(),
            TestPlayers::secretaris(),
            TestPlayers::dtl(),
            TestPlayers::dpb(),
        );

        $expectedCommands = [
            new StartGame(
                $gameIdentifier,
                $currentPlayers,
                SimplePileOfCards::complete(),
                $tableIdentifier,
                TestPlayers::secretaris(),
                new \DateTimeImmutable('2021-01-07')
            ),
            new CutCardsAfter(
                $gameIdentifier,
                4
            ),
        ];

        $processManager = new TableProcessManager(
            $this->getCommandBusWithExpectedCommands(...$expectedCommands),
            $this->randomNumbersStub,
            $this->dateTimeProviderStub
        );

        $processManager->__invoke(new GamePrepared(
            $tableIdentifier,
            TestPlayers::secretaris(),
            TestPlayers::penningmeester(),
            GameNumber::first(),
            SimplePileOfCards::complete(),
            TableMates::fromPlayers($currentPlayers),
            $currentPlayers
        ));
    }

    protected function getCommandBusWithExpectedCommands(Command ...$expectedCommands): CommandBus
    {
        $commandBusMock = $this->createMock(CommandBus::class);
        $commandBusMock->expects($this->once())
            ->method('dispatch')
            ->with(...$expectedCommands);

        /** @var CommandBus $commandBus */
        $commandBus = $commandBusMock;

        return $commandBus;
    }
}
