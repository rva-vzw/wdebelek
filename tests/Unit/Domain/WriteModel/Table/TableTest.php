<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\WriteModel\Table;

use App\Domain\ReadModel\PlayerProfile\SimplePlayerProfile;
use App\Domain\ValueObject\Card\PileOfCards;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\Exception\PlayerNotFound;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Player\TableMates;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\Event\DealerChanged;
use App\Domain\WriteModel\Table\Event\GamePrepared;
use App\Domain\WriteModel\Table\Event\InvitationAccepted;
use App\Domain\WriteModel\Table\Event\InvitationUnpublished;
use App\Domain\WriteModel\Table\Event\PlayerInvited;
use App\Domain\WriteModel\Table\Event\PlayerKicked;
use App\Domain\WriteModel\Table\Table;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\NonDeterministic\Shuffler;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;

final class TableTest extends TestCase
{
    /** @var Shuffler */
    private $shufflerMock;

    protected function setUp(): void
    {
        $this->shufflerMock = new class() implements Shuffler {
            public function shuffle(PileOfCards $pileOfCards): SimplePileOfCards
            {
                return SimplePileOfCards::fromCards($pileOfCards);
            }
        };
    }

    /** @test */
    public function itInvitesAPlayer(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('4E1F496F-2B38-432E-9E36-0CE04794E303')
        );

        $table = Table::claim(
            $tableIdentifier,
            new SimplePlayerProfile(
                TestPlayers::dtl(),
                PlayerName::fromString('dtl'),
                Language::default()
            )
        );

        $table->clearUncommittedEvents();

        $invitation = Invitation::fromString('697DF60B-D636-435C-95F7-FD43F73B3315');
        $table->invitePlayer($invitation, TestPlayers::dtl(), Language::default());

        $expectedEvents = [
            new PlayerInvited(
                $tableIdentifier,
                $invitation,
                Seat::fromInteger(1),
                TestPlayers::dtl(),
                Language::default()
            ),
        ];
        $actualEvents = array_values($table->getUncommittedEvents());
        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test */
    public function itDoesNotIssueTheSameInvitationTwice(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('4E1F496F-2B38-432E-9E36-0CE04794E303')
        );

        $table = Table::claim(
            $tableIdentifier,
            new SimplePlayerProfile(
                TestPlayers::dtl(),
                PlayerName::fromString('dtl'),
                Language::default()
            )
        );

        $table->clearUncommittedEvents();

        $invitation = Invitation::fromString('697DF60B-D636-435C-95F7-FD43F73B3315');
        $table->invitePlayer($invitation, TestPlayers::dtl(), Language::default());
        $table->invitePlayer($invitation, TestPlayers::dtl(), Language::default());

        $expectedEvents = [
            new PlayerInvited(
                $tableIdentifier, $invitation, Seat::fromInteger(1), TestPlayers::dtl(), Language::default()
            ),
        ];
        $actualEvents = array_values($table->getUncommittedEvents());
        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test */
    public function itDoesNotSendOutInvitationsFromUnknownPlayer(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('2ca33a57-5a01-46f6-9181-caa9ebf15d24')
        );

        $table = Table::claim(
            $tableIdentifier,
            new SimplePlayerProfile(
                TestPlayers::dtl(),
                PlayerName::fromString('dtl'),
                Language::default()
            )
        );

        $this->expectException(PlayerNotFound::class);
        $table->invitePlayer(
            Invitation::fromString('f135b9cd-7731-4506-8698-8075dbbecf45'), TestPlayers::penningmeester(), Language::default()
        );
    }

    /** @test */
    public function itDoesntPrepareGamesIfHostUnknown(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(
            Secret::fromString('2ca33a57-5a01-46f6-9181-caa9ebf15d24')
        );

        $table = $this->createTableWithPlayers($tableIdentifier, 4);
        $this->expectException(PlayerNotFound::class);
        $table->prepareGameWithSpecificDealer(
            TestPlayers::penningmeester(),
            TestPlayers::dpb(),
            52,
            $this->shufflerMock
        );
    }

    private function createTableWithPlayers(TableIdentifier $tableIdentifier, int $numberOfPlayers): Table
    {
        if ($numberOfPlayers > 6) {
            throw new \InvalidArgumentException('Too many players');
        }

        $additionalPlayers = [
            'penn' => TestPlayers::penningmeester(),
            'ddb' => TestPlayers::ddb(),
            'dtl' => TestPlayers::dtl(),
            'dpb' => TestPlayers::dpb(),
            'diu' => TestPlayers::diu(),
        ];

        $table = Table::claim(
            $tableIdentifier,
            new SimplePlayerProfile(
                TestPlayers::secretaris(),
                PlayerName::fromString('secr'),
                Language::default()
            )
        );

        $count = 1; // the organizer is already present.
        foreach ($additionalPlayers as $name => $playerIdentifier) {
            if (++$count > $numberOfPlayers) {
                break;
            }
            $invitation = Invitation::create();
            $table->invitePlayer($invitation, TestPlayers::secretaris(), Language::default());
            $table->acceptInvitation(
                $invitation,
                new \DateTimeImmutable(),
                new SimplePlayerProfile(
                    $playerIdentifier,
                    PlayerName::fromString('name'),
                    Language::default()
                )
            );
        }

        return $table;
    }

    /** @test */
    public function itUnpublishesPublishedInvitationBeforeAccepting(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('a3083cee-8963-493c-a0f9-2e7f2c668371'));
        $table = Table::claim(
            $tableIdentifier,
            new SimplePlayerProfile(
                TestPlayers::dtl(),
                PlayerName::fromString('dtl'),
                Language::default()
            )
        );

        $invitation = Invitation::fromString('821b50b4-52f7-474c-9f0b-ef3b19925e2b');
        $table->invitePlayer($invitation, TestPlayers::dtl(), Language::default());
        $table->publishInvitation($invitation, TestPlayers::dtl(), new \DateTimeImmutable('2020-09-08 20:00'));

        $table->clearUncommittedEvents();

        $acceptingPlayerProfile = new SimplePlayerProfile(
            TestPlayers::penningmeester(),
            PlayerName::fromString('penn'),
            Language::default()
        );

        $table->acceptInvitation(
            $invitation,
            new \DateTimeImmutable('2020-09-08 20:10'),
            $acceptingPlayerProfile
        );

        $expectedEvents = [
            new InvitationUnpublished(
                $tableIdentifier,
                $invitation,
                TestPlayers::penningmeester(),
                new \DateTimeImmutable('2020-09-08 20:10')
            ),
            InvitationAccepted::byPlayer(
                $tableIdentifier,
                $invitation,
                Seat::fromInteger(1),
                $acceptingPlayerProfile
            ),
        ];
        $actualEvents = array_values($table->getUncommittedEvents());
        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test */
    public function itAllowsLeftPlayersToCreateInvitations(): void
    {
        // See https://gitlab.com/rva-vzw/wdebelek/-/issues/114

        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('acb739ca-6a1f-4c7a-a1e6-8edd6d7caae2'));
        $table = Table::claim(
            $tableIdentifier,
            new SimplePlayerProfile(
                TestPlayers::dtl(),
                PlayerName::fromString('dtl'),
                Language::default()
            )
        );

        // Yes, a player can kick himself. This is useful if you want to organize a table
        // for someone else.
        $table->kickPlayer(TestPlayers::dtl(), TestPlayers::dtl());
        $table->clearUncommittedEvents();

        // I should still be able to create an invitation.
        $invitation = Invitation::fromString('8e88469f-d0a1-4d23-a8ec-ab515575e22a');
        $table->invitePlayer(
            $invitation,
            // Remember: this player is the one that creates the invitation:
            TestPlayers::dtl(),
            Language::default()
        );

        $expectedEvents = [
            new PlayerInvited(
                $tableIdentifier,
                $invitation,
                Seat::zero(),
                TestPlayers::dtl(),
                Language::default()
            ),
        ];
        $actualEvents = array_values($table->getUncommittedEvents());

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test */
    public function itDealsFourFourFourFiveOnTableWithThreePlayers(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('395c388a-78b0-438e-b5fd-efadb3844b90'));
        $table = $this->createTableWithPlayers($tableIdentifier, 3);
        $table->clearUncommittedEvents();

        $table->prepareGameWithSpecificDealer(
            TestPlayers::penningmeester(),
            TestPlayers::ddb(),
            52,
            $this->shufflerMock
        );

        $actualEvents = array_values($table->getUncommittedEvents());

        // table mates as seated on the table
        $tableMates = TableMates::fromPlayers(new SimplePlayerIdentifiers(
            TestPlayers::secretaris(),
            TestPlayers::penningmeester(),
            TestPlayers::ddb(),
        ));

        $orderForGame = new SimplePlayerIdentifiers(
            TestPlayers::ddb(),
            TestPlayers::secretaris(),
            // penningmeester is last player
            TestPlayers::penningmeester(),
        );

        $expectedEvents = [
            new GamePrepared(
                $tableIdentifier,
                TestPlayers::penningmeester(),
                TestPlayers::secretaris(),
                GameNumber::first(),
                SimplePileOfCards::complete(),
                $tableMates,
                $orderForGame
            ),
        ];

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test */
    public function itDealsThreeThreeFourOnTableWithThreePlayersAnd32Cards(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('395c388a-78b0-438e-b5fd-efadb3844b90'));
        $table = $this->createTableWithPlayers($tableIdentifier, 3);
        $table->clearUncommittedEvents();

        $table->prepareGameWithSpecificDealer(
            TestPlayers::penningmeester(),
            TestPlayers::ddb(),
            32,
            $this->shufflerMock
        );

        $actualEvents = array_values($table->getUncommittedEvents());

        // table mates as seated on the table
        $tableMates = TableMates::fromPlayers(new SimplePlayerIdentifiers(
            TestPlayers::secretaris(),
            TestPlayers::penningmeester(),
            TestPlayers::ddb(),
        ));

        $orderForGame = new SimplePlayerIdentifiers(
            TestPlayers::ddb(),
            TestPlayers::secretaris(),
            // penningmeester is last player
            TestPlayers::penningmeester(),
        );

        $expectedEvents = [
            new GamePrepared(
                $tableIdentifier,
                TestPlayers::penningmeester(),
                TestPlayers::secretaris(),
                GameNumber::first(),
                SimplePileOfCards::thirtyTwo(),
                $tableMates,
                $orderForGame
            ),
        ];

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test */
    public function itPreparesNext3PlayerGame(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('395c388a-78b0-438e-b5fd-efadb3844b90'));
        $table = $this->createTableWithPlayers($tableIdentifier, 3);

        $table->prepareGameWithSpecificDealer(
            TestPlayers::penningmeester(),
            TestPlayers::ddb(),
            52,
            $this->shufflerMock
        );
        $table->collectCards(
            SimplePileOfCards::complete()
        );
        $table->clearUncommittedEvents();
        $table->prepareNextGame();

        $actualEvents = array_values($table->getUncommittedEvents());
        $playerIdentifiers = new SimplePlayerIdentifiers(
            TestPlayers::secretaris(),
            TestPlayers::penningmeester(),
            TestPlayers::ddb()
        );

        $expectedEvents = [
            new GamePrepared(
                $tableIdentifier,
                TestPlayers::ddb(),
                TestPlayers::penningmeester(),
                GameNumber::second(),
                SimplePileOfCards::complete(),
                TableMates::fromPlayers($playerIdentifiers),
                $playerIdentifiers
            ),
        ];

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test */
    public function itPreparesRedealFor3PlayerGame(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('395c388a-78b0-438e-b5fd-efadb3844b90'));
        $table = $this->createTableWithPlayers($tableIdentifier, 3);

        $table->prepareGameWithSpecificDealer(
            TestPlayers::penningmeester(),
            TestPlayers::ddb(),
            52,
            $this->shufflerMock
        );
        $table->collectCards(
            SimplePileOfCards::complete()
        );
        $table->clearUncommittedEvents();

        $table->prepareRedeal();

        $actualEvents = array_values($table->getUncommittedEvents());

        // table mates as seated on the table
        $tableMates = TableMates::fromPlayers(new SimplePlayerIdentifiers(
            TestPlayers::secretaris(),
            TestPlayers::penningmeester(),
            TestPlayers::ddb(),
        ));

        $orderForGame = new SimplePlayerIdentifiers(
            TestPlayers::ddb(),
            TestPlayers::secretaris(),
            // penningmeester is last player
            TestPlayers::penningmeester(),
        );

        $expectedEvents = [
            new GamePrepared(
                $tableIdentifier,
                TestPlayers::penningmeester(),
                TestPlayers::secretaris(),
                GameNumber::second(),
                SimplePileOfCards::complete(),
                $tableMates,
                $orderForGame
            ),
        ];

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test */
    public function itChangesDealerWhenDealerIsKickedFromTableOf5(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('1e058621-9f9c-4dab-bb0b-45bef0678209'));
        $table = $this->createTableWithPlayers($tableIdentifier, 5);
        $table->prepareGameWithSpecificDealer(
            TestPlayers::ddb(),
            TestPlayers::penningmeester(),
            52,
            $this->shufflerMock
        );
        $table->clearUncommittedEvents();

        // Penningmeester kicks the dealer (ddb)
        $table->kickPlayer(TestPlayers::ddb(), TestPlayers::penningmeester());

        $actualEvents = array_values($table->getUncommittedEvents());
        $expectedEvents = [
            new PlayerKicked(
                $tableIdentifier,
                TestPlayers::ddb(),
                TestPlayers::penningmeester()
            ),
            new DealerChanged(
                $tableIdentifier,
                TestPlayers::penningmeester()
            ),
        ];

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test */
    public function itPreparesNextGameWithCardsReplaced(): void
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('013f9d25-cf6c-4dfe-baf8-b7ba013e1aa8'));
        $table = $this->createTableWithPlayers($tableIdentifier, 4);
        $table->prepareGameWithSpecificDealer(
            TestPlayers::penningmeester(),
            TestPlayers::ddb(),
            52,
            $this->shufflerMock
        );
        $table->collectCards(
            SimplePileOfCards::complete()
        );
        $table->clearUncommittedEvents();
        $table->prepareGameWithCardsReplaced(
            TestPlayers::ddb(),
            TestPlayers::ddb(),
            SimplePileOfCards::thirtyTwo()
        );

        $actualEvents = array_values($table->getUncommittedEvents());

        $playerIdentifiers = new SimplePlayerIdentifiers(
            TestPlayers::secretaris(),
            TestPlayers::penningmeester(),
            TestPlayers::ddb(),
            TestPlayers::dtl(),
        );

        $expectedEvents = [
            new GamePrepared(
                $tableIdentifier,
                TestPlayers::ddb(),
                TestPlayers::penningmeester(),
                GameNumber::second(),
                SimplePileOfCards::thirtyTwo(),
                TableMates::fromPlayers($playerIdentifiers),
                $playerIdentifiers->withLastPlayer(TestPlayers::ddb())
            ),
        ];

        $this->assertEquals($expectedEvents, $actualEvents);
    }
}
