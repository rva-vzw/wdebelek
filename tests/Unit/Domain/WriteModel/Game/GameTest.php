<?php

namespace App\Tests\Unit\Domain\WriteModel\Game;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\Hand;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\DealingStrategy;
use App\Domain\ValueObject\Game\GameConclusion;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Game\Trick;
use App\Domain\ValueObject\Player\Exception\PlayerNotFound;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Player\Teams;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\Event\CardPickedUp;
use App\Domain\WriteModel\Game\Event\CardsCutAfter;
use App\Domain\WriteModel\Game\Event\CardsReceived;
use App\Domain\WriteModel\Game\Event\Dealt;
use App\Domain\WriteModel\Game\Event\GameEnded;
use App\Domain\WriteModel\Game\Event\GameStarted;
use App\Domain\WriteModel\Game\Event\HandRevealed;
use App\Domain\WriteModel\Game\Event\HandSorted;
use App\Domain\WriteModel\Game\Event\NonPlayingTrumpRevealed;
use App\Domain\WriteModel\Game\Event\TrickReopened;
use App\Domain\WriteModel\Game\Event\TrickWon;
use App\Domain\WriteModel\Game\Event\VotedAsWinner;
use App\Domain\WriteModel\Game\Event\VotedConclusion;
use App\Domain\WriteModel\Game\Event\WinnerVotesInvalidated;
use App\Domain\WriteModel\Game\Exception\HandNotRevealed;
use App\Domain\WriteModel\Game\Exception\IncorrectTrickNumber;
use App\Domain\WriteModel\Game\Exception\StatusInvalid;
use App\Domain\WriteModel\Game\Game;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;

final class GameTest extends TestCase
{
    private GameIdentifier $gameIdentifier;
    private TableIdentifier $tableIdentifier;

    public function setUp(): void
    {
        parent::setUp();
        $this->tableIdentifier = TableIdentifier::withSecret(Secret::fromString('1c4846a2-e767-4bc1-9411-e0bcb27e6e44'));
        $this->gameIdentifier = GameIdentifier::fromNumber($this->tableIdentifier, GameNumber::first());
    }

    /** @test */
    public function itStartsAGame(): void
    {
        $players = new SimplePlayerIdentifiers(
            TestPlayers::penningmeester(),
            TestPlayers::secretaris(),
            TestPlayers::dtl(),
            TestPlayers::ddb()
        );
        $cards = SimplePileOfCards::complete();

        $game = Game::start(
            $this->gameIdentifier,
            $players,
            $cards,
            $this->tableIdentifier,
            TestPlayers::ddb(),
            new \DateTimeImmutable('2021-01-07')
        );

        $actualEvents = array_values($game->getUncommittedEvents());
        $expectedEvents = [
            new GameStarted(
                $this->gameIdentifier,
                $players,
                $cards,
                $this->tableIdentifier,
                TestPlayers::ddb(),
                TestPlayers::ddb(),
                new \DateTimeImmutable('2021-01-07')
            ),
        ];

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test */
    public function itDoesNotDealWithoutCutting(): void
    {
        $game = $this->getStartedGame();
        $this->expectException(StatusInvalid::class);
        $game->deal(DealingStrategy::fourFourFive());
    }

    /** @test */
    public function itCuts(): void
    {
        // players are penn, secr, dtl, ddb
        $game = $this->getStartedGame();
        $game->clearUncommittedEvents();

        $game->cutCards(29);

        $expectedEvents = [
            new CardsCutAfter($this->gameIdentifier, 29),
        ];
        $actualEvents = array_values($game->getUncommittedEvents());

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test */
    public function itDeals(): void
    {
        // players are penn, secr, dtl, ddb.
        // dtl (1f41d...) deals
        $game = $this->getStartedGameAfterCutting();
        $game->clearUncommittedEvents();

        $game->deal(DealingStrategy::fourFourFive());

        $expectedEvents = [
            new CardsReceived($this->gameIdentifier, TestPlayers::ddb(), SimplePileOfCards::fromCards(new \ArrayIterator([
                new Card(4, Card::DIAMONDS),
                new Card(5, Card::DIAMONDS),
                new Card(6, Card::DIAMONDS),
                new Card(7, Card::DIAMONDS),
            ])), $this->tableIdentifier),
            new CardsReceived($this->gameIdentifier, TestPlayers::penningmeester(), SimplePileOfCards::fromCards(new \ArrayIterator([
                new Card(8, Card::DIAMONDS),
                new Card(9, Card::DIAMONDS),
                new Card(10, Card::DIAMONDS),
                new Card(11, Card::DIAMONDS),
            ])), $this->tableIdentifier),
            new CardsReceived($this->gameIdentifier, TestPlayers::secretaris(), SimplePileOfCards::fromCards(new \ArrayIterator([
                new Card(12, Card::DIAMONDS),
                new Card(13, Card::DIAMONDS),
                new Card(1, Card::CLUBS),
                new Card(2, Card::CLUBS),
            ])), $this->tableIdentifier),
            new CardsReceived($this->gameIdentifier, TestPlayers::dtl(), SimplePileOfCards::fromCards(new \ArrayIterator([
                new Card(3, Card::CLUBS),
                new Card(4, Card::CLUBS),
                new Card(5, Card::CLUBS),
                new Card(6, Card::CLUBS),
            ])), $this->tableIdentifier),
            new CardsReceived($this->gameIdentifier, TestPlayers::ddb(), SimplePileOfCards::fromCards(new \ArrayIterator([
                new Card(7, Card::CLUBS),
                new Card(8, Card::CLUBS),
                new Card(9, Card::CLUBS),
                new Card(10, Card::CLUBS),
            ])), $this->tableIdentifier),
            new CardsReceived($this->gameIdentifier, TestPlayers::penningmeester(), SimplePileOfCards::fromCards(new \ArrayIterator([
                new Card(11, Card::CLUBS),
                new Card(12, Card::CLUBS),
                new Card(13, Card::CLUBS),
                new Card(1, Card::SPADES),
            ])), $this->tableIdentifier),
            new CardsReceived($this->gameIdentifier, TestPlayers::secretaris(), SimplePileOfCards::fromCards(new \ArrayIterator([
                new Card(2, Card::SPADES),
                new Card(3, Card::SPADES),
                new Card(4, Card::SPADES),
                new Card(5, Card::SPADES),
            ])), $this->tableIdentifier),
            new CardsReceived($this->gameIdentifier, TestPlayers::dtl(), SimplePileOfCards::fromCards(new \ArrayIterator([
                new Card(6, Card::SPADES),
                new Card(7, Card::SPADES),
                new Card(8, Card::SPADES),
                new Card(9, Card::SPADES),
            ])), $this->tableIdentifier),
            new CardsReceived($this->gameIdentifier, TestPlayers::ddb(), SimplePileOfCards::fromCards(new \ArrayIterator([
                new Card(10, Card::SPADES),
                new Card(11, Card::SPADES),
                new Card(12, Card::SPADES),
                new Card(13, Card::SPADES),
                new Card(1, Card::HEARTS),
            ])), $this->tableIdentifier),
            new CardsReceived($this->gameIdentifier, TestPlayers::penningmeester(), SimplePileOfCards::fromCards(new \ArrayIterator([
                new Card(2, Card::HEARTS),
                new Card(3, Card::HEARTS),
                new Card(4, Card::HEARTS),
                new Card(5, Card::HEARTS),
                new Card(6, Card::HEARTS),
            ])), $this->tableIdentifier),
            new CardsReceived($this->gameIdentifier, TestPlayers::secretaris(), SimplePileOfCards::fromCards(new \ArrayIterator([
                new Card(7, Card::HEARTS),
                new Card(8, Card::HEARTS),
                new Card(9, Card::HEARTS),
                new Card(10, Card::HEARTS),
                new Card(11, Card::HEARTS),
            ])), $this->tableIdentifier),
            new CardsReceived($this->gameIdentifier, TestPlayers::dtl(), SimplePileOfCards::fromCards(new \ArrayIterator([
                new Card(12, Card::HEARTS),
                new Card(13, Card::HEARTS),
                new Card(1, Card::DIAMONDS),
                new Card(2, Card::DIAMONDS),
                new Card(3, Card::DIAMONDS),
            ])), $this->tableIdentifier),
            new Dealt(
                $this->gameIdentifier,
                TestPlayers::dtl(),
                new Card(3, Card::DIAMONDS),
                $this->tableIdentifier
            ),
        ];
        $actualEvents = array_values($game->getUncommittedEvents());

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    private function getStartedGameAfterCutting(): Game
    {
        $game = $this->getStartedGame();
        $game->cutCards(29);

        return $game;
    }

    /** @test */
    public function itSortsAHand(): void
    {
        $game = $this->getStartedGameAfterDealing();
        $game->clearUncommittedEvents();

        $sortedCards = Hand::fromCards(new \ArrayIterator([
            new Card(1, Card::HEARTS),
            new Card(7, Card::CLUBS),
            new Card(8, Card::CLUBS),
            new Card(9, Card::CLUBS),
            new Card(10, Card::CLUBS),
            new Card(4, Card::DIAMONDS),
            new Card(5, Card::DIAMONDS),
            new Card(6, Card::DIAMONDS),
            new Card(7, Card::DIAMONDS),
            new Card(10, Card::SPADES),
            new Card(11, Card::SPADES),
            new Card(12, Card::SPADES),
            new Card(13, Card::SPADES),
        ]));
        $game->sortHand(
            TestPlayers::ddb(),
            $sortedCards
        );

        $expectedEvents = [
            new HandSorted(
                $this->gameIdentifier,
                TestPlayers::ddb(),
                $sortedCards
            ),
        ];
        $actualEvents = array_values($game->getUncommittedEvents());

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test */
    public function itOnlyVotesAWinnerForTheCurrentTrick(): void
    {
        $game = $this->getGameWithFirstTrick();

        $this->expectException(IncorrectTrickNumber::class);
        $game->voteWinner(TestPlayers::penningmeester(), TestPlayers::penningmeester(), 2);
    }

    private function getGameWithFirstTrick(): Game
    {
        $game = $this->getStartedGameAfterDealing();
        $game->play(TestPlayers::ddb(), new Card(13, Card::SPADES), 1);
        $game->play(TestPlayers::penningmeester(), new Card(1, Card::SPADES), 1);
        $game->play(TestPlayers::secretaris(), new Card(2, Card::SPADES), 1);
        $game->play(TestPlayers::dtl(), new Card(6, Card::SPADES), 1);

        return $game;
    }

    /** @test */
    public function itVotesAWinner(): void
    {
        $game = $this->getGameWithFirstTrick();
        $game->clearUncommittedEvents();

        $game->voteWinner(TestPlayers::ddb(), TestPlayers::penningmeester(), 1);

        $expectedEvents = [
            new VotedAsWinner(
                $this->gameIdentifier,
                TestPlayers::ddb(),
                TestPlayers::penningmeester(),
                1
            ),
        ];
        $actualEvents = array_values($game->getUncommittedEvents());

        $this->assertEquals($actualEvents, $expectedEvents);
    }

    /** @test */
    public function itInvalidatesVotesWhenCardPickedUp(): void
    {
        $game = $this->getGameWithFirstTrick();
        $game->voteWinner(TestPlayers::ddb(), TestPlayers::penningmeester(), 1);
        $game->clearUncommittedEvents();

        $game->pickUpCard(TestPlayers::dtl(), 1);

        $expectedEvents = [
            new WinnerVotesInvalidated($this->gameIdentifier, 1),
            new CardPickedUp(
                $this->gameIdentifier,
                TestPlayers::dtl(),
                new Card(6, Card::SPADES),
                1,
                $this->tableIdentifier
            ),
        ];

        $actualEvents = array_values($game->getUncommittedEvents());

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test **/
    public function itRestartsVotingWhenGameWon(): void
    {
        $game = $this->getGameWithFirstTrick();
        $game->voteWinner(TestPlayers::ddb(), TestPlayers::penningmeester(), 1);
        $game->voteWinner(TestPlayers::dtl(), TestPlayers::penningmeester(), 1);
        $game->voteWinner(TestPlayers::secretaris(), TestPlayers::penningmeester(), 1);
        // Three votes should be enough to win the trick.
        $game->play(TestPlayers::penningmeester(), new Card(13, Card::CLUBS), 2);
        $game->play(TestPlayers::secretaris(), new Card(2, Card::CLUBS), 2);
        $game->play(TestPlayers::dtl(), new Card(3, Card::CLUBS), 2);
        $game->play(TestPlayers::ddb(), new Card(7, Card::CLUBS), 2);
        $game->clearUncommittedEvents();

        // Should be able to vote again for penningmeester.
        $game->voteWinner(TestPlayers::penningmeester(), TestPlayers::penningmeester(), 2);
        $game->voteWinner(TestPlayers::ddb(), TestPlayers::penningmeester(), 2);
        $game->voteWinner(TestPlayers::dtl(), TestPlayers::penningmeester(), 2);

        $expectedEvents = [
            new VotedAsWinner($this->gameIdentifier, TestPlayers::penningmeester(), TestPlayers::penningmeester(), 2),
            new VotedAsWinner($this->gameIdentifier, TestPlayers::ddb(), TestPlayers::penningmeester(), 2),
            new VotedAsWinner($this->gameIdentifier, TestPlayers::dtl(), TestPlayers::penningmeester(), 2),
            new TrickWon(
                $this->gameIdentifier, TestPlayers::penningmeester(),
                2,
                Trick::empty()->withPlayedCard(TestPlayers::penningmeester(), new Card(13, Card::CLUBS))
                    ->withPlayedCard(TestPlayers::secretaris(), new Card(2, Card::CLUBS))
                    ->withPlayedCard(TestPlayers::dtl(), new Card(3, Card::CLUBS))
                    ->withPlayedCard(TestPlayers::ddb(), new Card(7, Card::CLUBS)),
                $this->tableIdentifier
            ),
        ];
        $actualEvents = array_values($game->getUncommittedEvents());

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test */
    public function itDoesNotVoteTwiceForTheSamePlayer(): void
    {
        $game = $this->getGameWithFirstTrick();
        $game->clearUncommittedEvents();

        $game->voteWinner(TestPlayers::ddb(), TestPlayers::penningmeester(), 1);
        $game->voteWinner(TestPlayers::ddb(), TestPlayers::penningmeester(), 1);

        $expectedEvents = [
            new VotedAsWinner(
                $this->gameIdentifier,
                TestPlayers::ddb(),
                TestPlayers::penningmeester(),
                1
            ),
        ];
        $actualEvents = array_values($game->getUncommittedEvents());

        $this->assertEquals($actualEvents, $expectedEvents);
    }

    /** @test */
    public function itCanOnlyEndTheGameOnce(): void
    {
        $game = $this->getStartedGameAfterCutting();
        $game->deal(DealingStrategy::fourFourFive());
        $game->endGame();

        $this->expectException(StatusInvalid::class);
        $game->endGame();
    }

    /** @test */
    public function itConcludesGameWhen3PlayersVoted(): void
    {
        // penn, secr, dtl, ddb
        $game = $this->getStartedGameAfterCutting();
        $game->deal(DealingStrategy::fourFourFive());
        $game->voteGameConclusion(TestPlayers::penningmeester(), GameConclusion::DONE());
        $game->voteGameConclusion(TestPlayers::secretaris(), GameConclusion::DONE());
        $game->clearUncommittedEvents();

        $game->voteGameConclusion(TestPlayers::dtl(), GameConclusion::DONE());

        $events = array_values($game->getUncommittedEvents());

        $this->assertCount(2, $events);
        $this->assertEquals(
            new VotedConclusion($this->gameIdentifier, TestPlayers::dtl(), GameConclusion::DONE(), $this->tableIdentifier),
            $events[0]
        );

        // Can't really predict the exact 2nd event, since there's some randomness in picking
        // up the cards, that I can't really control, I think.

        $this->assertInstanceOf(GameEnded::class, $events[1]);

        /** @var GameEnded $gameEnded */
        $gameEnded = $events[1];
        $this->assertEquals($this->gameIdentifier, $gameEnded->getGameIdentifier());
        $this->assertEquals($this->tableIdentifier, $gameEnded->getTableIdentifier());
        $this->assertCount(52, iterator_to_array($gameEnded->getCards()));
    }

    /** @test */
    public function itBlocksConclusionVotesIfGameAlreadyDone(): void
    {
        $game = $this->getStartedGameAfterCutting();
        $game->deal(DealingStrategy::fourFourFive());
        $game->endGame();

        $this->expectException(StatusInvalid::class);
        $game->voteGameConclusion(TestPlayers::penningmeester(), GameConclusion::DONE());
    }

    /** @test */
    public function itDoesNotInvalidateVotesThatWereNotCast(): void
    {
        $game = $this->getStartedGameAfterCutting();
        $game->deal(DealingStrategy::fourFourFive());
        $game->clearUncommittedEvents();

        $game->invalidateConclusionVote(TestPlayers::penningmeester());

        $this->assertEmpty($game->getUncommittedEvents());
    }

    /** @test */
    public function itEndsGameWithTricksPlayed(): void
    {
        // trick consists of king of spades, ace of spades, 2 of spades and 6 of spades.
        $game = $this->getGameWithFirstTrick();
        $game->voteWinner(TestPlayers::ddb(), TestPlayers::penningmeester(), 1);
        $game->voteWinner(TestPlayers::dtl(), TestPlayers::penningmeester(), 1);
        $game->voteWinner(TestPlayers::secretaris(), TestPlayers::penningmeester(), 1);
        $game->clearUncommittedEvents();

        $game->endGame();

        $events = array_values($game->getUncommittedEvents());
        $this->assertCount(1, $events);

        /** @var GameEnded $gameEnded */
        $gameEnded = $events[0];
        $this->assertInstanceOf(GameEnded::class, $gameEnded);

        // FIXME: I need a clean way to quickly create a pile of cards.
        $expectedSequence = SimplePileOfCards::fromCards(new \ArrayIterator([
            new Card(13, Card::SPADES),
            new Card(1, Card::SPADES),
            new Card(2, Card::SPADES),
            new Card(6, Card::SPADES),
        ]));

        $cards = $gameEnded->getCards();

        $this->assertTrue($cards->containsSequence($expectedSequence));
    }

    /** @test */
    public function itCantCancelAGameThatsAlreadyDone(): void
    {
        $game = $this->getStartedGameAfterCutting();
        $game->deal(DealingStrategy::fourFourFive());
        $game->endGame();

        $this->expectException(StatusInvalid::class);
        $game->cancelGame();
    }

    /** @test */
    public function itRevealsAHand(): void
    {
        $game = $this->getStartedGameAfterCutting();
        $game->deal(DealingStrategy::fourFourFive());
        $game->clearUncommittedEvents();

        $game->revealHand(TestPlayers::penningmeester());

        $actualEvents = array_values($game->getUncommittedEvents());
        $expectedEvents = [
            new HandRevealed(
                $this->gameIdentifier,
                TestPlayers::penningmeester(),
                Hand::fromCards(new \ArrayIterator([
                    new Card(8, Card::DIAMONDS),
                    new Card(9, Card::DIAMONDS),
                    new Card(10, Card::DIAMONDS),
                    new Card(11, Card::DIAMONDS),
                    new Card(11, Card::CLUBS),
                    new Card(12, Card::CLUBS),
                    new Card(13, Card::CLUBS),
                    new Card(1, Card::SPADES),
                    new Card(2, Card::HEARTS),
                    new Card(3, Card::HEARTS),
                    new Card(4, Card::HEARTS),
                    new Card(5, Card::HEARTS),
                    new Card(6, Card::HEARTS),
                ])),
                $this->tableIdentifier
            ),
        ];

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test */
    public function itDoesNotRevealHandsTwice(): void
    {
        $game = $this->getStartedGameAfterCutting();
        $game->deal(DealingStrategy::fourFourFive());
        $game->clearUncommittedEvents();

        $game->revealHand(TestPlayers::penningmeester());
        $game->revealHand(TestPlayers::penningmeester());

        $actualEvents = array_values($game->getUncommittedEvents());
        $expectedEvents = [
            new HandRevealed(
                $this->gameIdentifier,
                TestPlayers::penningmeester(),
                Hand::fromCards(new \ArrayIterator([
                    new Card(8, Card::DIAMONDS),
                    new Card(9, Card::DIAMONDS),
                    new Card(10, Card::DIAMONDS),
                    new Card(11, Card::DIAMONDS),
                    new Card(11, Card::CLUBS),
                    new Card(12, Card::CLUBS),
                    new Card(13, Card::CLUBS),
                    new Card(1, Card::SPADES),
                    new Card(2, Card::HEARTS),
                    new Card(3, Card::HEARTS),
                    new Card(4, Card::HEARTS),
                    new Card(5, Card::HEARTS),
                    new Card(6, Card::HEARTS),
                ])),
                $this->tableIdentifier
            ),
        ];

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    /** @test */
    public function itRejectsTeamsWithUnknownPlayers(): void
    {
        $game = $this->getStartedGameAfterCutting();
        $game->deal(DealingStrategy::fourFourFive());
        $this->expectException(PlayerNotFound::class);
        $game->teamUpPlayers(Teams::allIndividual(
            new SimplePlayerIdentifiers(
                TestPlayers::penningmeester(),
                TestPlayers::secretaris(),
                TestPlayers::dtl(),
                // diu doesn't play.
                TestPlayers::diu()
            )),
            TestPlayers::penningmeester()
        );
    }

    private function getStartedGame(): Game
    {
        $players = new SimplePlayerIdentifiers(
            TestPlayers::ddb(),
            TestPlayers::penningmeester(),
            TestPlayers::secretaris(),
            // Last player to get cards should be last player in this list.
            TestPlayers::dtl(),
        );
        $cards = SimplePileOfCards::complete();

        return Game::start(
            $this->gameIdentifier,
            $players,
            $cards,
            $this->tableIdentifier,
            TestPlayers::dtl(),
            new \DateTimeImmutable('2021-01-07')
        );
    }

    private function getStartedGameAfterDealing(): Game
    {
        $game = $this->getStartedGameAfterCutting();
        $game->deal(DealingStrategy::fourFourFive());

        return $game;
    }

    /** @test */
    public function itDoesNotShowInitialCardsIfHandWasNotRevealed(): void
    {
        $game = $this->getStartedGameAfterDealing();
        $this->expectException(HandNotRevealed::class);
        $game->showInitialHand(TestPlayers::penningmeester());
    }

    /** @test */
    public function itCollectsUndealtCardsAsWellWhenGameEnds(): void
    {
        $game = $this->getDealt3PlayerGame();
        $game->clearUncommittedEvents();

        $game->endGame();
        $events = array_values($game->getUncommittedEvents());
        $this->assertEquals(1, count($events));

        /** @var GameEnded $event */
        $event = array_values($events)[0];

        $this->assertInstanceOf(GameEnded::class, $event);
        $this->assertEquals(52, $event->getCards()->count());
    }

    /** @test */
    public function itRevealsNonPlayingTrumpIfNotAllCardsWereDealt(): void
    {
        $game = $this->getDealt3PlayerGame();
        $game->clearUncommittedEvents();

        $game->revealTrump(TestPlayers::secretaris());

        $actualEvents = array_values($game->getUncommittedEvents());
        $expectedEvents = [
            new NonPlayingTrumpRevealed(
                $this->gameIdentifier,
                TestPlayers::secretaris(),
                new Card(3, Card::DIAMONDS),
                $this->tableIdentifier
            ),
        ];

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    private function getDealt3PlayerGame(): Game
    {
        $players = new SimplePlayerIdentifiers(
            TestPlayers::dtl(),
            TestPlayers::penningmeester(),
            // Last player will be dealer
            TestPlayers::secretaris(),
        );
        $cards = SimplePileOfCards::complete();

        $game = Game::start(
            $this->gameIdentifier,
            $players,
            $cards,
            $this->tableIdentifier,
            TestPlayers::secretaris(),
            new \DateTimeImmutable('2021-01-07')
        );
        $game->cutCards(29);
        $game->deal(DealingStrategy::fourFourFourFive());

        return $game;
    }

    /** @test */
    public function itReopensTrick(): void
    {
        $game = $this->getGameWithFirstTrickWon();
        $game->clearUncommittedEvents();
        $game->reopenTrick(
            TestPlayers::ddb(),
            1
        );
        $expectedEvents = [
            new WinnerVotesInvalidated(
                $this->gameIdentifier,
                1
            ),
            new TrickReopened(
                $this->gameIdentifier,
                TestPlayers::ddb(),
                TestPlayers::penningmeester(),
                1,
                Trick::empty()
                    ->withPlayedCard(TestPlayers::ddb(), new Card(13, Card::SPADES))
                    ->withPlayedCard(TestPlayers::penningmeester(), new Card(1, Card::SPADES))
                    ->withPlayedCard(TestPlayers::secretaris(), new Card(2, Card::SPADES))
                    ->withPlayedCard(TestPlayers::dtl(), new Card(6, Card::SPADES)),
                $this->tableIdentifier
            ),
        ];
        $actualEvents = array_values($game->getUncommittedEvents());

        $this->assertEquals($expectedEvents, $actualEvents);
    }

    private function getGameWithFirstTrickWon(): Game
    {
        $game = $this->getGameWithFirstTrick();
        $game->voteWinner(
            TestPlayers::ddb(),
            TestPlayers::penningmeester(),
            1
        );
        $game->voteWinner(
            TestPlayers::penningmeester(),
            TestPlayers::penningmeester(),
            1
        );
        $game->voteWinner(
            TestPlayers::secretaris(),
            TestPlayers::penningmeester(),
            1
        );

        return $game;
    }

    /** @test */
    public function itRevotesWinnerWhenTrickReopened(): void
    {
        $game = $this->getGameWithFirstTrickWon();
        $game->reopenTrick(
            TestPlayers::ddb(),
            1
        );
        $game->clearUncommittedEvents();
        $game->voteWinner(
            TestPlayers::ddb(),
            TestPlayers::penningmeester(),
            1
        );

        $expectedEvents = [
            new VotedAsWinner(
                $this->gameIdentifier,
                TestPlayers::ddb(),
                TestPlayers::penningmeester(),
                1
            ),
        ];
        $actualEvents = array_values($game->getUncommittedEvents());

        $this->assertEquals($expectedEvents, $actualEvents);
    }
}
