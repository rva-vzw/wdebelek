<?php

declare(strict_types=1);

namespace App\Tests\Unit\Publishing;

use App\Domain\ReadModel\TableState\GamesAtTable;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\Hand;
use App\Domain\ValueObject\Game\GameConclusion;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\Event\ConclusionVoteInvalidated;
use App\Domain\WriteModel\Game\Event\Dealt;
use App\Domain\WriteModel\Game\Event\HandRevealed;
use App\Domain\WriteModel\Game\Event\NonPlayingTrumpRevealed;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\Event\DealerChanged;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use App\Publishing\PublishingProcessManager;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class PublishingProcessManagerTest extends TestCase
{
    private NormalizerInterface $normalizerStub;
    private GamesAtTable $dummyGamesAtTable;

    /**
     * @return ClientRelevantTableEvent[][]
     */
    public function eventDataProvider(): array
    {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString('3a964830-003a-47a7-80f1-9eb5e9ad4601'));
        $gameIdentifier = GameIdentifier::fromNumber($tableIdentifier, GameNumber::first());

        return [
            ConclusionVoteInvalidated::class => [new ConclusionVoteInvalidated(
                $gameIdentifier,
                TestPlayers::secretaris(),
                GameConclusion::DONE(),
                $tableIdentifier
            )],
            Dealt::class => [new Dealt(
                $gameIdentifier,
                TestPlayers::secretaris(),
                new Card(8, Card::CLUBS),
                $tableIdentifier
            )],
            HandRevealed::class => [new HandRevealed(
                $gameIdentifier,
                TestPlayers::secretaris(),
                Hand::fromCards(new \ArrayIterator([
                    new Card(4, Card::DIAMONDS),
                ])),
                $tableIdentifier
            )],
            NonPlayingTrumpRevealed::class => [new NonPlayingTrumpRevealed(
                $gameIdentifier,
                TestPlayers::secretaris(),
                new Card(10, Card::CLUBS),
                $tableIdentifier
            )],
            DealerChanged::class => [new DealerChanged(
                $tableIdentifier,
                TestPlayers::ddb(),
            )],
        ];
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->dummyGamesAtTable = $this->createMock(GamesAtTable::class);

        $normalizerStub = $this->createMock(NormalizerInterface::class);
        $normalizerStub->method('normalize')->willReturn([
            'someData' => 'Dummy data',
        ]);
        /** @var NormalizerInterface $normalizer */
        $normalizer = $normalizerStub;
        $this->normalizerStub = $normalizer;
    }

    /**
     * @test
     *
     * @dataProvider eventDataProvider
     */
    public function itPublishesEvents(ClientRelevantTableEvent $event): void
    {
        $publisherMock = $this->createMock(PublisherInterface::class);
        $publisherMock->expects($this->once())->method('__invoke');
        /** @var PublisherInterface $publisher */
        $publisher = $publisherMock;

        $processManager = new PublishingProcessManager($publisher, $this->normalizerStub, $this->dummyGamesAtTable);
        $processManager->__invoke($event);
    }
}
