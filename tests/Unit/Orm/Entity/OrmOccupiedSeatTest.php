<?php

declare(strict_types=1);

namespace App\Tests\Unit\Orm\Entity;

use App\Domain\ReadModel\OccupiedSeats\OccupiedSeat;
use App\Domain\ReadModel\PlayerProfile\SimplePlayerProfile;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Entity\OrmOccupiedSeat;
use App\TestData\TestPlayers;
use PHPUnit\Framework\TestCase;

final class OrmOccupiedSeatTest extends TestCase
{
    /** @test */
    public function itMapsInitialSeatFromAndToOrm(): void
    {
        $playerAtTable = OccupiedSeat::initial(
            TableIdentifier::withSecret(
                Secret::fromString('73EB470F-ECE4-4CA5-A657-724268C7F2C4')
            ),
            new SimplePlayerProfile(
                PlayerIdentifier::withSecret(Secret::fromString('76e92e33-274c-4048-838c-3e7b873859b1')),
                PlayerName::fromString('jos'),
                Language::default()
            )
        );

        $ormPlayerAtTable = OrmOccupiedSeat::fromOccupiedSeat($playerAtTable);
        $playerAtTable2 = $ormPlayerAtTable->toOccupiedSeat();

        $this->assertEquals($playerAtTable, $playerAtTable2);
    }

    /** @test */
    public function itMapsAcceptedInvitationFromAndToOrm(): void
    {
        $playerAtTable = OccupiedSeat::initial(
            TableIdentifier::withSecret(
                Secret::fromString('73EB470F-ECE4-4CA5-A657-724268C7F2C4')
            ),
            new SimplePlayerProfile(
                PlayerIdentifier::withSecret(Secret::fromString('ac943ff1-a0dd-41f0-9e55-9dfb402e61a5')),
                PlayerName::fromString('jos'),
                Language::default()
            )
        )->acceptedBy(
            TestPlayers::diu(),
            PlayerName::fromString('diu')
        );

        $ormPlayerAtTable = OrmOccupiedSeat::fromOccupiedSeat($playerAtTable);
        $playerAtTable2 = $ormPlayerAtTable->toOccupiedSeat();

        $this->assertEquals($playerAtTable, $playerAtTable2);
    }
}
