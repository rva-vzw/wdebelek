# Upgrade instructions

## Resetting the database

If you don't care about the existing data, you can also reset the database after upgrading the source code.
The following command clears and re-initializes the database:
```
# Let's backup the event store, in case you'll regret your decision
# (adapt the command below to your needs)
mysqldump wdebelek event_store | gzip > ~/event-store.sql.gz
# Warning: you will loose all data if you execute this command!
make reset
```
If you don't want to loose the past events, don't reset the database, but check out the instructions below:

## Upgrading minor versions (e.g. from 1.1 to 1.2)

It is recommended to replay the events from the event store, so that all read models are up to date.

```
make replay
```

## Upgrading major versions

### v1 -> v2

To keep your event stream intact when moving to version 2, you'll have to perform
an event migration:

```
# put the app in maintenance mode
echo MAINTENANCE=true >> .env.local
# Let's backup the event store, in case something goes wrong.
# (adapt the command below to your needs)
mysqldump wdebelek event_store | gzip > ~/event-store.sql.gz
# perform the migration; this can take several minutes
./bin/console wdebelek:migrate:toV2
# replay, as always
make replay
# disable maintenance mode (may be not needed)
sed -i '/^MAINTENANCE=true$$/d' .env.local
```