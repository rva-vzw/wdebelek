# This is a copy of the makefile of [dikdikdik](https://gitlab.com/rva-vzw/dikdikdik).
# Still needs to be adapted.

.PHONY: qa fix php-cs-fixer test reset testdata security-checker permissions

qa: php-cs-fixer psalm phpstan

php-cs-fixer:
	PHP_CS_FIXER_IGNORE_ENV=1 ./vendor/bin/php-cs-fixer --dry-run fix .

phpstan:
	php -d memory_limit=4G ./vendor/bin/phpstan analyse --no-progress

psalm:
	./vendor/bin/psalm --no-progress

deptrac:
	./vendor/bin/deptrac

fix:
	PHP_CS_FIXER_IGNORE_ENV=1 ./vendor/bin/php-cs-fixer fix .

test: tests/Support/_generated/IntegrationTesterActions.php testdata
	./vendor/bin/codecept run Unit,Integration,Api,Acceptance

test-ff: tests/Support/_generated/IntegrationTesterActions.php testdata
	./vendor/bin/codecept run Unit,Integration,Api,Acceptance --fail-fast

tests/Support/_generated/IntegrationTesterActions.php:
	./vendor/bin/codecept build

reset-read-model:
	./bin/console doctrine:schema:drop --em=read_model --force
	./bin/console doctrine:schema:create --em=read_model

# Prevent duplicate deployment with a mkdir statement.

replay: reset-read-model
	mkdir replay_lock
	echo MAINTENANCE=true >> .env.local
	./bin/console cache:clear
	./bin/console eventsourcing:readmodel:replay
	sed -i '/^MAINTENANCE=true$$/d' .env.local
	./bin/console cache:clear
	rmdir replay_lock

reset: reset-read-model
	./bin/console doctrine:schema:drop --force --em=event_store
	./bin/console doctrine:schema:create --em=event_store

testdata: reset
	php -d memory_limit=4G ./bin/console wdebelek:testdata

warmup:
	./bin/console cache:clear
	./bin/console cache:warmup
	chmod -R ug+rwX var/log var/cache
