# Manually deploying WDEBELEK on a production server

*Please note that I'm not a sever guy. The instructions below workerd when I tried it,
but they're probably suboptimal.*
*Please submit a merge request if you can improve this documentation.*

## Requirements

You need a server with php8, and mercure. For mercure installation instructions, see below.
For php, make sure you install the required extensions: php-mysql php-xml, php-curl, php-dom, 
php-mbstring, php-zip. On Debian, with the 
[sury repositories](https://www.linuxtechi.com/install-php-8-on-debian-10/) enabled:

```
apt install php8.0-mysql php8.0-xml php8.0-curl php8.0-dom php8.0-mbstring php8.0-zip
```

You'll need make as well:

```
apt install make
```

## Configure a php8 site for symfony

WDEBELEK is a Symfony 5 application. You need to configure
a web site for Symfony, if you want to do this for nginx, you can
find instructions
[on the nginx webiste](https://www.nginx.com/resources/wiki/start/topics/recipes/symfony/).
(The instructions for Symfony 4 also work for Symfony 5).

If you set up your webserver as documented in that guide, you can test it by putting
a file `index.php` in the directory `/var/www/project/public`, with these contents:

```
<?php
echo 'Hello world';
```

You should see 'Hello world' at www.domain.tld. (Of course, you'll have to use your own
domain name everywhere www.domain.tld is mentioned.)

## Enable https

The easiests way is using certbot. You can do that as follows on Debian 10:

```
apt install certbot
apt install python3-certbot-nginx # only if you run the nginx server
```

Then:

```
certbot --nginx www.domain.tld
```

Note that this only works if your dns is set up so that your domain name points to your server.
(For apache, I think you just can omit `--nginx`)

## Install mercure

I use mercure-legacy, but of course I should migrate to the non-legacy version.
I haven't tried that yet, so here are the instructions on how to install the legacy
version:

```
cd ~
wget https://github.com/dunglas/mercure/releases/download/v0.11.0/mercure-legacy_0.11.0_Linux_x86_64.tar.gz
mkdir /opt/mercure
cd /opt/mercure
tar xzvf ~/mercure-legacy_0.11.0_Linux_x86_64.tar.gz
```

I will run mercure as www-data, which is suboptimal, because I will use the
certificate generated by certbot for Mercure as well, so I'll have to
grant read-access to the tls private key.

```
chgrp -R www-data /etc/letsencrypt/archive
chmod -R ug+rX /etc/letsencrypt/archive
chgrp -R www-data /etc/letsencrypt/live
chmod -R ug+rX /etc/letsencrypt/live

ln -s /etc/letsencrypt/live/www.domain.tld/fullchain.pem /opt/mercure/ssl/cert.pem
ln -s /etc/letsencrypt/live/www.domain.tld/privkey.pem /opt/mercure/ssl/key.pem
```

I'll use supervisor to keep mercure up and running.

```
apt install supervisor

mkdir /opt/mercure/log
chown www-data:www-data /opt/mercure/log
mkdir /opt/mercure/ssl

cat > /etc/supervisor/conf.d/mercure.conf << EOF
[program:mercure]
command=/opt/mercure/mercure
process_name=%(program_name)s_%(process_num)s
numprocs=1
environment=JWT_KEY="make up your own key",ADDR=":3000",CERT_FILE="/opt/mercure/ssl/cert.pem",KEY_FILE="/opt/mercure/ssl/key.pem",ALLOW_ANONYMOUS=1,CORS_ALLOWED_ORIGINS="*"
directory=/tmp
autostart=true
autorestart=true
startsecs=5
startretries=10
user=www-data
redirect_stderr=false
stdout_capture_maxbytes=1MB
stderr_capture_maxbytes=1MB
stdout_logfile=/opt/mercure/log/mercure.out.log
stderr_logfile=/opt/mercure/log/mercure.error.log
EOF

supervisorctl reload
supervisorctl restart all
```

You can check if it works using curl:
```
curl -N 'https://www.domain.tld:3000/.well-known/mercure?topic=https://table.rijkvanafdronk.be'
```
If you get a column (`:`), it's probably all right.

## Deploying wdebelek

If you download a tgz from gitlab, you only get the source for wdebelek, and not the
source of the used packages (php and javascript). In this howto, I describe how you
get those dependencies by running `composer` and `yarn` on your production server, but
you can as well run those tools on another machine, and rsync the result.

```
groupadd wdebelek
useradd -g wdebelek -m wdebelek

cd ~
wget https://gitlab.com/rva-vzw/wdebelek/-/archive/master/wdebelek-master.tar.gz

cd /var/www/project
tar xzvf ~/wdebelek-master.tar.gz

apt install composer
# yarn isn't in the standard package repositories. We'll add a custom source.
apt install curl gnupg
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
apt update
apt install yarn

composer install
yarn install --frozen-lockfile
yarn encore production
```


## The configuration file

```
cat > /var/www/project/.env.local << EOF
APP_ENV=prod
# you can generate a secret at http://nux.net/secret
APP_SECRET=748358306e33ee306450ca207fc5bea52a388f6c
# use another password; this is just a random one
DATABASE_URL=mysql://wdebelek:Meet3ruchoocenu@localhost:3306/wdebelek
MAINTENANCE=false

MERCURE_PUBLISH_URL=https://www.domain.tld:3000/.well-known/mercure
MERCURE_JWT_TOKEN=the-jwt-token
EOF
```
The tricky part is the JWT-token. I'll have to look into this again.
[This thread on stack overflow](https://stackoverflow.com/questions/60396780/jwt-key-for-mercure)
might help.

## Initialize the database

```
make reset
```

Now your application should be up and running

## Optional: use .gitlab-ci

If you set up everything properly, you can also use gitlab ci/cd for deployment; see
[.gitlab-ci.yml](.gitlab-ci.yml) to find out which secrets you should provide.
