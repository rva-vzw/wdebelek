# Changelog

## [3.0] - 2024-02-03

### Added

- Use rva-vzw/krakboem-bundle, #206

### Changed

- Php 8.3, #207
- Symfony 5.4, #209
- Symfony 6.4, #208

### Removed

- Event migrations, #206

## [2.5.1] - 2022-12-20

- Test on php 8.2, #205

## [2.5] - 2022-02-21

### Changed

- Updated to php 8.1, #204
- Use Symfony 5.3, #202

### [2.4] - 2021-11-05

### Changed

- Use ObjectContext::detach() anyway, #201
- (Partially) refactor test data creation, #200
- Update composer recipes, #204
- Update all Symfony packages (still for Symfony 5.2), #203
- Use Mercure 0.11, #162

## [2.3.1] - 2020-03-29

### Changed
- Undisclose tricks when new game starts, #197
- Fix issues on mobile, #195

## [2.3]  - 2020-03-25

### Added
- Disclose tricks when all hands are empty, #84

## [2.2.1] - 2020-02-24

### Changed
- Update front page when invitation is unpublished, #191
- On failure when picking your card back up, re-update hand and played cards, #192

## [2.2] - 2021-02-21

### Added
- Add some stats, #185
- Reopen last trick, #184
  
### Changed
- Some workarounds for slow replay, #186
- Use new sensiolabs/security-checker, #183

## [2.1] - 2021-02-01

### Changed
- Use php 8.0, #182

## [2.0] - 2021-01-27

### Changed

- Added dealer, last player and time to `GameStarted` event, #171
- Deprecated `LastPlayerAnnounced` event, #171
- `HandShownToSpectator` is now a game event, #170 
- Dedicated `TrickAtTable` read model, #177
- DealerIdentifier and LastPlayer not nullable anymore in `TableState`, #178
- No more nullable game identifier in `TableState`, #179
- No underlined links, #181

## [1.2] - 2021-01-04

### Added

- Choosing a dealing strategy, #166, #169, #167

### Changed

- Fixed `Deal` command, #168
- Layout improvements, #91, #173

## [1.1] - 2020-12-18

### Added

- Unreveal cards, #159
- Playing with three players, #160

## [1.0] - 2020-11-01

### Changed

- Automatically create invitations for an empty table, #148.
- Publish invitation with single click, #143.
- Bugfix changing card game on suspended table, #152.
- Moved ORM-related classes to dedicated namespace, #136.
- Bugfix phantom cards, #151.
- Show public invitations grouped by table, #129.
- Switch menu items 'next dealer' and 'same dealer', #131.
- Show hint 'drag card to play' on first trick, #156.
- Use php 7.4, #150

## [0.7] - 2020-10-04

### Added

- Log uncaught exceptions, #10.
- Work around selenium problem with drag and drop, #22.
- Review previous tricks, #126.

### Changed

- Reload table state when playing a card fails, #128
- Extracted PlayingPlayers read model from TableState, #132, #106.
- Make this work with php 7.4.10? #141
- Use wodby/php for php containers, #142

## [0.6] - 2020-09-15

### Added

- Indicating the card game you'll be playing at your table, #110.
- Provide links to a video chat per table, #92.
- Publishing an invitation, #108.
- Accept published invitations from front page, #109.
- (Translated) about page, #123.

### Changed

- Show player name and card game on invitation page, #38.
- Use Symfony 5.1, #115
- If you know the table secret, you can still manage the table after you left, #114.
- Nice error message when your invitation is already gone, #87.

## [0.5.1] - 2020-08-15

### Changed

- Bugfix (workaround) voting own card as winner on phone/tablet, #82.
- Bugfix for problem can only reveal cards once, #113.

## [0.5] - 2020-08-07

### Added

- Showing cards to spectators, #68.
- Showing initial hand, #112.

### Changed

- Trying to fix the bug that caused thrown cards not to get at the backend, #111.

## [0.4] - 2020-07-25

### Added

- Indication for own trump card, #62.
- Staging environment, #98.
- Only reveal trump when te person with the trump card decides so, #75.
- If you pick up a card again, the card keeps being visible to the other players, #26.

### Changed

- Fix issue with wrong shown trump after dealing a new game, #97.
- Renamed GameStateReadModel as TableStateReadModel, #61

## [0.3] - 2020-06-30

### Added

- Removing players from the table, #76
- CardsCut event, #86, #94

## Changed

- Clean up hand read model when game ends, #52
- Bugfix: showing last card in hands of other players, #72

## [0.2.1] - 2020-06-23

### Changed

- Fix security issue, #93

## [0.2] - 2020-06-14

### Added

- Teaming up players, #49

## [0.1] - 2020-06-07

- Initial release 🎉
