<?php

declare(strict_types=1);

namespace App\Publishing;

use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;

interface ClientRelevantTableEvent extends Event
{
    public function getTableIdentifier(): TableIdentifier;
}
