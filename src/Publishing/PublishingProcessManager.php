<?php

declare(strict_types=1);

namespace App\Publishing;

use App\Domain\ReadModel\TableState\GamesAtTable;
use App\Domain\WriteModel\Game\Event\CardPickedUp;
use App\Domain\WriteModel\Game\Event\CardPlayed;
use App\Domain\WriteModel\Game\Event\CardsCutAfter;
use App\Domain\WriteModel\Game\Event\CardsUnrevealed;
use App\Domain\WriteModel\Game\Event\ConclusionVoteInvalidated;
use App\Domain\WriteModel\Game\Event\Dealt;
use App\Domain\WriteModel\Game\Event\GameStarted;
use App\Domain\WriteModel\Game\Event\HandRevealed;
use App\Domain\WriteModel\Game\Event\HandShownToSpectator;
use App\Domain\WriteModel\Game\Event\InitialHandShown;
use App\Domain\WriteModel\Game\Event\NonPlayingTrumpRevealed;
use App\Domain\WriteModel\Game\Event\PlayersTeamedUp;
use App\Domain\WriteModel\Game\Event\TrickReopened;
use App\Domain\WriteModel\Game\Event\TrickReviewed;
use App\Domain\WriteModel\Game\Event\TrickWon;
use App\Domain\WriteModel\Game\Event\TrumpRevealed;
use App\Domain\WriteModel\Game\Event\VotedConclusion;
use App\Domain\WriteModel\Table\Event\CardsCollected;
use App\Domain\WriteModel\Table\Event\DealerChanged;
use App\Domain\WriteModel\Table\Event\InvitationAccepted;
use App\Domain\WriteModel\Table\Event\InvitationPublished;
use App\Domain\WriteModel\Table\Event\InvitationUnpublished;
use App\Domain\WriteModel\Table\Event\PlayerInvited;
use App\Domain\WriteModel\Table\Event\PlayerKicked;
use App\Domain\WriteModel\Table\Event\TableSuspended;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\EventSourcing\EventListener\AbstractEventListener;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final readonly class PublishingProcessManager extends AbstractEventListener
{
    private PublisherInterface $publisher;
    private NormalizerInterface $normalizer;
    private GamesAtTable $gamesAtTable;

    public function __construct(
        PublisherInterface $publisher,
        NormalizerInterface $normalizer,
        GamesAtTable $gamesAtTable
    ) {
        $this->publisher = $publisher;
        $this->normalizer = $normalizer;
        $this->gamesAtTable = $gamesAtTable;
    }

    public function applyCardPlayed(CardPlayed $event): void
    {
        $this->publishTableEvent($event);
    }

    private function publishTableEvent(ClientRelevantTableEvent $event): void
    {
        $topic = $this->getTopic($event->getTableIdentifier());

        /** @var string $data */
        $data = json_encode(['eventType' => get_class($event)]);

        $this->publisher->__invoke(new Update(
            $topic,
            $data
        ));
    }

    public function applyCardPickedUp(CardPickedUp $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyTrickWon(TrickWon $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyInvitationAccepted(InvitationAccepted $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyVotedConclusion(VotedConclusion $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyConclusionVoteInvalidated(ConclusionVoteInvalidated $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyDealt(Dealt $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyHandRevealed(HandRevealed $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyPlayersTeamedUp(PlayersTeamedUp $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyTableSuspended(TableSuspended $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyTrumpRevealed(TrumpRevealed $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyHandShownToSpectator(HandShownToSpectator $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyInitialHandShown(InitialHandShown $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyPlayerKicked(PlayerKicked $event): void
    {
        $this->publishTableEvent($event);
    }

    // InvitationPublished and invitationUnpublished aren't published with a table-id in the topic,
    // people who are interested typically have not joined a table yet.
    // Maybe I should move handling these events to a dedicated process manager.

    public function applyInvitationPublished(InvitationPublished $event): void
    {
        $this->publishGeneralEvent($event);
    }

    public function applyInvitationUnpublished(InvitationUnpublished $event): void
    {
        $this->publishGeneralEvent($event);
    }

    public function applyGameStarted(GameStarted $event): void
    {
        $this->publishTableEvent($event);
        $this->publishGeneralEvent($event);
    }

    public function applyTrickReviewed(TrickReviewed $event): void
    {
        $topic = $this->getTopic($event->getTableIdentifier());

        /** @var mixed[] $data */
        $data = $this->normalizer->normalize($event);
        $data['eventType'] = get_class($event);

        /** @var string $encoded */
        $encoded = json_encode($data);

        $this->publisher->__invoke(new Update(
            $topic,
            $encoded
        ));
    }

    public function applyPlayerInvited(PlayerInvited $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyCardsCollected(CardsCollected $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyCardsUnrevealed(CardsUnrevealed $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyNonPlayingTrumpRevealed(NonPlayingTrumpRevealed $event): void
    {
        $this->publishTableEvent($event);
    }

    public function applyCardsCutAfter(CardsCutAfter $event): void
    {
        $tableIdentifier = $this->gamesAtTable->getTableByCurrentGame($event->getGameIdentifier());
        $topic = $this->getTopic($tableIdentifier);

        /** @var string $data */
        $data = json_encode(['eventType' => get_class($event)]);

        $this->publisher->__invoke(new Update(
            $topic,
            $data
        ));
    }

    public function applyDealerChanged(DealerChanged $event): void
    {
        $this->publishTableEvent($event);
    }

    private function getTopic(TableIdentifier $tableIdentifier): string
    {
        return 'https://table.rijkvanafdronk.be/'.$tableIdentifier->toString();
    }

    private function publishGeneralEvent(Event $event): void
    {
        $topic = 'https://table.rijkvanafdronk.be';

        /** @var string $data */
        $data = json_encode(['eventType' => get_class($event)]);

        $this->publisher->__invoke(new Update($topic, $data));
    }

    public function applyTrickReopened(TrickReopened $event): void
    {
        $this->publishTableEvent($event);
    }
}
