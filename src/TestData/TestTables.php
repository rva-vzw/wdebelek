<?php

declare(strict_types=1);

namespace App\TestData;

use App\Domain\WriteModel\Table\TableIdentifier;

final class TestTables
{
    public static function table1(): TableIdentifier
    {
        // a4e394bd-d3a0-55fa-95f9-b2f93e77c0df
        return TableIdentifier::withSecret(TestSecrets::table1());
    }

    public static function table2(): TableIdentifier
    {
        return TableIdentifier::withSecret(TestSecrets::table2());
    }

    public static function table3(): TableIdentifier
    {
        return TableIdentifier::withSecret(TestSecrets::table3());
    }

    public static function table4(): TableIdentifier
    {
        return TableIdentifier::withSecret(TestSecrets::table4());
    }

    public static function table5(): TableIdentifier
    {
        return TableIdentifier::withSecret(TestSecrets::table5());
    }

    public static function table6(): TableIdentifier
    {
        return TableIdentifier::withSecret(TestSecrets::table6());
    }

    public static function table7(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table7()
        );
    }

    public static function table8(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table8()
        );
    }

    public static function table9(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table9()
        );
    }

    public static function table10(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table10()
        );
    }

    public static function table11(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table11()
        );
    }

    public static function table12(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table12()
        );
    }

    public static function table13(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table13()
        );
    }

    public static function table14(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table14()
        );
    }

    public static function table15(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table15()
        );
    }

    public static function table16(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table16()
        );
    }

    /**
     * The test table with 6 players from the welcome screen.
     */
    public static function table17(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table17()
        );
    }

    public static function table18(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table18()
        );
    }

    public static function table19(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table19()
        );
    }

    public static function table20(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table20()
        );
    }

    public static function table21(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table21()
        );
    }

    public static function table22(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table22()
        );
    }

    public static function table23(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table23()
        );
    }

    public static function table24(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table24()
        );
    }

    public static function table25(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table25()
        );
    }

    public static function table26(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table26()
        );
    }

    public static function table27(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table27()
        );
    }

    public static function table28(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table28()
        );
    }

    public static function table29(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table29()
        );
    }

    public static function table30(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table30()
        );
    }

    public static function table31(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table31()
        );
    }

    public static function table32(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table32()
        );
    }

    public static function table33(): TableIdentifier
    {
        return TableIdentifier::withSecret(
            TestSecrets::table33()
        );
    }
}
