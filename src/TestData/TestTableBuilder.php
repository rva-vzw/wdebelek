<?php

declare(strict_types=1);

namespace App\TestData;

use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Invitation\Invitations;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\Command\Deal;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\Command\AcceptInvitation;
use App\Domain\WriteModel\Table\Command\ClaimTable;
use App\Domain\WriteModel\Table\Command\InvitePlayer;
use App\Domain\WriteModel\Table\Command\PrepareGameWithManipulatedCards;
use App\Domain\WriteModel\Table\TableIdentifier;
use Psr\Log\LoggerInterface;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;

final class TestTableBuilder
{
    public function __construct(
        private CommandBus $commandBus,
        private LoggerInterface $logger,
    ) {
    }

    public function buildTableReadyToPlay(
        TestTableConfiguration $configuration
    ): void {
        $this->buildTableWithPlayers($configuration);
        $this->logger->info(
            "Added players; preparing first game for table {$configuration->getTableIdentifier()}.",
        );

        $this->commandBus->dispatch(
            new PrepareGameWithManipulatedCards(
                $configuration->getTableIdentifier(),
                $configuration->getDealer(),
                $configuration->getDealer(),
                SimplePileOfCards::fromCards($configuration->getCards()),
            ),
        );
        $this->commandBus->dispatch(
            new Deal(
                GameIdentifier::first($configuration->getTableIdentifier()),
                $configuration->getDealingStrategy()
            )
        );
        $this->logger->info(
            "Started dealing on table {$configuration->getTableIdentifier()}.",
        );
    }

    public function buildTableWithPlayers(TestTableConfiguration $configuration): void
    {
        $this->logger->info(
            "Building table {$configuration->getTableIdentifier()} with {$configuration->getNumberOfPlayers()} players.",
        );
        $this->commandBus->dispatch(
            new ClaimTable(
                $configuration->getTableIdentifier(),
                $configuration->getHostProfile(),
            )
        );

        $invitations = $this->createInvitations(
            $configuration->getTableIdentifier(),
            $configuration->getNumberOfPlayers() - 1,
            $configuration->getHost()
        );

        /** @var PlayerProfile $playerProfile */
        foreach ($configuration->getPlayerProfiles() as $playerProfile) {
            if ($playerProfile == $configuration->getHostProfile()) {
                // Dealer is already at the table
                continue;
            }
            if ($invitations->isEmpty()) {
                // No more invitations. In fact this should not happen
                break;
            }
            $invitation = $invitations->getFirstInvitation();
            $this->commandBus->dispatch(
                AcceptInvitation::byPlayer(
                    $configuration->getTableIdentifier(),
                    $invitation,
                    $playerProfile,
                    new \DateTimeImmutable()
                )
            );
            $invitations = $invitations->without($invitation);
        }
        $this->logger->info(
            "Commands issued for building table {$configuration->getTableIdentifier()} with {$configuration->getNumberOfPlayers()} players.",
        );
    }

    private function createInvitations(
        TableIdentifier $tableIdentifier,
        int $numberOfInvitations,
        PlayerIdentifier $host
    ): Invitations {
        $invitations = Invitations::none();

        for ($i = 0; $i < $numberOfInvitations; ++$i) {
            $invitation = Invitation::create();
            $this->commandBus->dispatch(new InvitePlayer(
                $tableIdentifier,
                $invitation,
                $host,
                Language::default()
            ));
            $invitations = $invitations->with($invitation);
        }

        return $invitations;
    }
}
