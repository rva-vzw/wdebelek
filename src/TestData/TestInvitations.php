<?php

declare(strict_types=1);

namespace App\TestData;

use App\Domain\ValueObject\Invitation\Invitation;

final class TestInvitations
{
    public static function invitation1(): Invitation
    {
        return Invitation::fromString('aa4afa5b-bddd-4452-a0e5-1dd7cda3b6ac');
    }

    public static function invitation2(): Invitation
    {
        return Invitation::fromString('e06897ab-db19-4e2e-86d8-d34de392fc37');
    }

    public static function invitation24_1(): Invitation
    {
        return Invitation::fromString('b845122c-c618-49a7-bf2f-af73a4830c94');
    }

    public static function invitation24_2(): Invitation
    {
        return Invitation::fromString('aefe17db-d295-45b4-ab76-87861b9336e9');
    }

    public static function invitation32(): Invitation
    {
        return Invitation::fromString('a4691e24-7c09-47b4-9ae5-b1fd80a311ed');
    }
}
