<?php

declare(strict_types=1);

namespace App\TestData;

use App\Domain\ValueObject\Player\PlayerIdentifier;

final class TestPlayers
{
    public static function penningmeester(): PlayerIdentifier
    {
        // ce2678e3-ef66-56a4-b037-6f3609b58aa2
        return PlayerIdentifier::withSecret(TestSecrets::penningmeester());
    }

    public static function secretaris(): PlayerIdentifier
    {
        // ? 54cfb752-c723-55d0-9e6f-d5790a6b82af
        return PlayerIdentifier::withSecret(TestSecrets::secretaris());
    }

    public static function dtl(): PlayerIdentifier
    {
        // 1f41d1bc-0426-56a4-b59e-3c2f97b8883c
        return PlayerIdentifier::withSecret(TestSecrets::dtl());
    }

    public static function ddb(): PlayerIdentifier
    {
        // 4513d272-d90a-589f-8a24-de611f071f51
        return PlayerIdentifier::withSecret(TestSecrets::ddb());
    }

    public static function dpb(): PlayerIdentifier
    {
        // d08dfaf8-aafe-589c-8e6c-63c9af68e086
        return PlayerIdentifier::withSecret(TestSecrets::dpb());
    }

    public static function diu(): PlayerIdentifier
    {
        return PlayerIdentifier::withSecret(TestSecrets::diu());
    }

    public static function ddl(): PlayerIdentifier
    {
        return PlayerIdentifier::withSecret(TestSecrets::ddl());
    }
}
