<?php

declare(strict_types=1);

namespace App\TestData;

use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\ReadModel\PlayerProfile\SimplePlayerProfile;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerName;

final class TestProfiles
{
    public static function ddb(): PlayerProfile
    {
        return new SimplePlayerProfile(
            TestPlayers::ddb(),
            PlayerName::fromString('ddb'),
            Language::default()
        );
    }

    public static function penningmeester(): PlayerProfile
    {
        return new SimplePlayerProfile(
            TestPlayers::penningmeester(),
            PlayerName::fromString('penningmeester'),
            Language::default()
        );
    }

    public static function secretaris(): PlayerProfile
    {
        return new SimplePlayerProfile(
            TestPlayers::secretaris(),
            PlayerName::fromString('secretaris'),
            Language::default()
        );
    }

    public static function dpb(): PlayerProfile
    {
        return new SimplePlayerProfile(
            TestPlayers::dpb(),
            PlayerName::fromString('dpb'),
            Language::default()
        );
    }

    public static function dtl(): PlayerProfile
    {
        return new SimplePlayerProfile(
            TestPlayers::dtl(),
            PlayerName::fromString('dtl'),
            Language::default()
        );
    }

    public static function ddl(): PlayerProfile
    {
        return new SimplePlayerProfile(
            TestPlayers::ddl(),
            PlayerName::fromString('ddl'),
            Language::default()
        );
    }
}
