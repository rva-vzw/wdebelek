<?php

declare(strict_types=1);

namespace App\TestData;

use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\ReadModel\PlayerProfile\SimplePlayerProfile;
use App\Domain\ValueObject\Card\PileOfCards;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\DealingStrategy;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\WriteModel\Table\TableIdentifier;

final class TestTableConfiguration
{
    public const DEFAULT_NUMBER_OF_CARDS = 52;
    public const DEFAULT_NUMBER_OF_PLAYERS = 4;
    public const DEFAULT_DEALER = 'dtl';
    public const DEFAULT_HOST = 'dtl';

    /**
     * @param PlayerIdentifier[] $moreThanEnoughPlayers
     */
    private function __construct(
        private TableIdentifier $tableIdentifier,
        private int $numberOfPlayers,
        private array $moreThanEnoughPlayers,
        private PlayerIdentifier $dealer,
        private PileOfCards $cards,
        private DealingStrategy $dealingStrategy,
        private PlayerIdentifier $host
    ) {
    }

    public static function create(TableIdentifier $tableIdentifier): self
    {
        $players = [
            'dpb' => TestPlayers::dpb(),
            'ddb' => TestPlayers::ddb(),
            'secr' => TestPlayers::secretaris(),
            'penn' => TestPlayers::penningmeester(),
            'diu' => TestPlayers::diu(),
            'dtl' => TestPlayers::dtl(),
        ];

        return new self(
            $tableIdentifier,
            self::DEFAULT_NUMBER_OF_PLAYERS,
            $players,
            $players[self::DEFAULT_DEALER],
            SimplePileOfCards::complete(),
            DealingStrategy::getDefault(intdiv(52, self::DEFAULT_NUMBER_OF_PLAYERS)),
            $players[self::DEFAULT_HOST],
        );
    }

    public function withCards(PileOfCards $cards): self
    {
        $result = clone $this;
        $result->cards = $cards;

        return $result;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getNumberOfPlayers(): int
    {
        return $this->numberOfPlayers;
    }

    public function getDealer(): PlayerIdentifier
    {
        return $this->dealer;
    }

    public function getHostProfile(): PlayerProfile
    {
        /** @var string $name */
        $name = array_search($this->host, $this->moreThanEnoughPlayers, false);

        return new SimplePlayerProfile(
            $this->host,
            PlayerName::fromString($name),
            Language::default()
        );
    }

    /**
     * @return \Generator<PlayerProfile>
     */
    public function getPlayerProfiles(): \Generator
    {
        $playersToAdd = $this->numberOfPlayers;

        foreach ($this->moreThanEnoughPlayers as $name => $playerIdentifier) {
            if (1 === $playersToAdd) {
                break;
            }
            if ($playerIdentifier == $this->host) {
                // We'll add the host as last player
                continue;
            }
            yield new SimplePlayerProfile(
                $playerIdentifier,
                PlayerName::fromString($name),
                Language::default()
            );
        }

        yield $this->getHostProfile();
    }

    public function getCards(): PileOfCards
    {
        return $this->cards;
    }

    public function getDealingStrategy(): DealingStrategy
    {
        return $this->dealingStrategy;
    }

    public function withDealer(PlayerIdentifier $dealer): self
    {
        $result = clone $this;
        $result->dealer = $dealer;

        return $result;
    }

    public function getHost(): PlayerIdentifier
    {
        return $this->host;
    }

    public function hostedBy(PlayerIdentifier $host): self
    {
        $result = clone $this;
        $result->host = $host;

        return $result;
    }

    public function withNumberOfPlayers(int $numberOfPlayers): self
    {
        $result = clone $this;
        $result->numberOfPlayers = $numberOfPlayers;

        return $result;
    }
}
