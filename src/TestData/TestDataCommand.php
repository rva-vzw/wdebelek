<?php

declare(strict_types=1);

namespace App\TestData;

use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\ReadModel\PlayerProfile\SimplePlayerProfile;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\PileOfCards;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\DealingStrategy;
use App\Domain\ValueObject\Game\GameConclusion;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Invitation\Invitations;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Player\Team;
use App\Domain\ValueObject\Player\Teams;
use App\Domain\WriteModel\Game\Command\Deal;
use App\Domain\WriteModel\Game\Command\PickUpCard;
use App\Domain\WriteModel\Game\Command\PlayCard;
use App\Domain\WriteModel\Game\Command\RevealHand;
use App\Domain\WriteModel\Game\Command\RevealTrump;
use App\Domain\WriteModel\Game\Command\ShowHandToSpectator;
use App\Domain\WriteModel\Game\Command\ShowInitialHand;
use App\Domain\WriteModel\Game\Command\TeamUpPlayers;
use App\Domain\WriteModel\Game\Command\VoteAsWinner;
use App\Domain\WriteModel\Game\Command\VoteGameConclusion;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\Command\ClaimTable;
use App\Domain\WriteModel\Table\Command\InvitePlayer;
use App\Domain\WriteModel\Table\Command\PrepareGame;
use App\Domain\WriteModel\Table\Command\PrepareGameHandler;
use App\Domain\WriteModel\Table\Command\PublishInvitation;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\NonDeterministic\Shuffler;
use App\NonDeterministic\SimpleRandomProvider;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class TestDataCommand extends Command
{
    protected static $defaultName = 'wdebelek:testdata';

    public function __construct(
        private PrepareGameHandler $prepareGameHandler,
        private TestTableBuilder $testTableBuilder,
        private CommandBus $commandBus,
        private SimpleRandomProvider $randomProvider,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->arrangeReproducibleData();

        // Tables 1-5, with 4 players and 52 cards.
        $tableIds = [
            TestTables::table1(),
            TestTables::table2(),
            TestTables::table3(),
            TestTables::table4(),
            TestTables::table5(),
        ];

        // Players: dtl, dpb, ddb, secr. ddb deals.
        // (I don't understand why I didn't take the same players as in the unit test...)
        foreach ($tableIds as $tableId) {
            // cut deck before starting to deal to keep old tests working.
            $this->testTableBuilder->buildTableReadyToPlay(
                TestTableConfiguration::create($tableId)
                    ->withCards(SimplePileOfCards::complete()->cutAfter(25))
                    ->withDealer(TestPlayers::ddb())
            );
        }

        $this->commandBus->dispatch(
            new PlayCard(
                TestGames::game1(),
                TestPlayers::secretaris(),
                new Card(4, Card::DIAMONDS),
                1
            ),
        );
        $this->commandBus->dispatch(
            new PickUpCard(
                TestGames::game1(),
                TestPlayers::secretaris(),
                1
            )
        );

        $this->commandBus->dispatch(
            new PlayCard(
                TestGames::game4(),
                TestPlayers::secretaris(),
                new Card(4, Card::DIAMONDS),
                1
            ),
            new PlayCard(
                TestGames::game4(),
                TestPlayers::dtl(),
                new Card(11, Card::DIAMONDS),
                1
            )
        );

        $this->commandBus->dispatch(
            new PlayCard(
                TestGames::game5(),
                TestPlayers::secretaris(),
                new Card(4, Card::DIAMONDS),
                1
            ),
            new PlayCard(
                TestGames::game5(),
                TestPlayers::dtl(),
                new Card(11, Card::DIAMONDS),
                1
            ),
            new PlayCard(
                TestGames::game5(),
                TestPlayers::dpb(),
                new Card(13, Card::DIAMONDS),
                1
            ),
            new PlayCard(
                TestGames::game5(),
                TestPlayers::ddb(),
                new Card(1, Card::DIAMONDS),
                1
            ),
        );
        $this->commandBus->dispatch(
            new VoteAsWinner(
                TestGames::game5(),
                TestPlayers::dtl(),
                1,
                TestPlayers::ddb()
            ),
            new VoteAsWinner(
                TestGames::game5(),
                TestPlayers::dpb(),
                1,
                TestPlayers::ddb()
            )
        );

        // Table 6, 32 cards.
        $this->testTableBuilder->buildTableReadyToPlay(
            TestTableConfiguration::create(TestTables::table6())->withCards(
                SimplePileOfCards::thirtyTwo()->cutAfter(4)
            )
        );

        // Table 7, only invites.
        $this->claimTableAndPublishInvitations(
            TestTables::table7(),
            new SimplePlayerProfile(
                TestPlayers::ddb(),
                PlayerName::fromString('ddb'),
                Language::default()
            ),
            new Invitations(
                Invitation::fromString('a39aa0ec-dc9f-4f03-8dda-4c1f59a19105'),
                Invitation::fromString('017f68ce-dda7-41d8-a92b-fc58f558b858'),
                Invitation::fromString('cf4621d2-421e-44ee-8956-c977eccee276'),
                Invitation::fromString('5af2a063-b780-48ef-a870-4b3e6bf5dc64'),
                Invitation::fromString('00cd4554-cbd8-4d3b-9c9f-9b921ad91002'),
            )
        );

        // 4 players, 52 cards. No game started.
        $tableIds = [
            TestTables::table8(),
            TestTables::table10(),
            TestTables::table19(),
        ];
        foreach ($tableIds as $tableId) {
            $this->testTableBuilder->buildTableWithPlayers(
                TestTableConfiguration::create($tableId)
            );
        }

        // Table 9: table with 2 invites only.
        // (This will not happen the way the application works today, but theoretically
        // there shouldn't be a problem.)
        $this->commandBus->dispatch(
            new ClaimTable(
                TestTables::table9(),
                new SimplePlayerProfile(
                    TestPlayers::dpb(),
                    PlayerName::fromString('dpb'),
                    Language::default()
                )
            ),
            new InvitePlayer(
                TestTables::table9(),
                TestInvitations::invitation1(),
                TestPlayers::dpb(),
                Language::default()
            ),
            new InvitePlayer(
                TestTables::table9(),
                TestInvitations::invitation2(),
                TestPlayers::dpb(),
                Language::default()
            )
        );

        $this->createTableOf5WithFirstTrickOnTheTable(TestTables::table11());

        // Some default tables, ready to play. 4 players: dtl, dpb, ddb, secr
        $tableIds = [
            TestTables::table12(),
            TestTables::table14(),
            TestTables::table16(),
            TestTables::table18(),
            TestTables::table26(),
            TestTables::table27(),
            TestTables::table30(),
        ];

        foreach ($tableIds as $tableId) {
            $this->testTableBuilder->buildTableReadyToPlay(
                TestTableConfiguration::create($tableId)
                ->withDealer(TestPlayers::ddb())
            );
        }

        // dtl, dpb, ddb, secr, penn; ddb deals first game
        $this->createTableOf5WithFirstTrickOnTheTable(TestTables::table13());
        $game1 = GameIdentifier::fromNumber(TestTables::table13(), GameNumber::first());
        $this->commandBus->dispatch(
            // collect a trick
            new VoteAsWinner($game1, TestPlayers::dtl(), 1, TestPlayers::dtl()),
            new VoteAsWinner($game1, TestPlayers::secretaris(), 1, TestPlayers::dtl()),
            new VoteAsWinner($game1, TestPlayers::penningmeester(), 1, TestPlayers::dtl()),
            // dpb cards at this time: king of spades, ace of hearts, 2 of hearts, 3 of hearts, and some diamonds and clubs
            // next dealer (remember this is a test game)
            new VoteGameConclusion($game1, TestPlayers::dtl(), GameConclusion::DONE()),
            new VoteGameConclusion($game1, TestPlayers::penningmeester(), GameConclusion::DONE()),
            new VoteGameConclusion($game1, TestPlayers::secretaris(), GameConclusion::DONE())
        );

        $this->commandBus->dispatch(
            new VoteGameConclusion(TestGames::game14(), TestPlayers::dtl(), GameConclusion::DONE()),
            new VoteGameConclusion(TestGames::game14(), TestPlayers::secretaris(), GameConclusion::DONE())
        );

        // Table 15: table with 5 players, waiting for dealer to deal
        $this->testTableBuilder->buildTableWithPlayers(
            TestTableConfiguration::create(TestTables::table15())
                ->withNumberOfPlayers(5)
        );
        $this->commandBus->dispatch(new PrepareGame(
            TestTables::table15(),
            TestPlayers::dtl(),
            TestPlayers::dtl(),
            52
        ));

        // Reveal trump on table 16
        $this->commandBus->dispatch(
            new RevealTrump(TestGames::game16(), TestPlayers::ddb())
        );

        // Some six player tables
        $tableIds = [
            TestTables::table17(),
            TestTables::table20(),
            TestTables::table25(),
        ];

        foreach ($tableIds as $tableId) {
            $this->testTableBuilder->buildTableReadyToPlay(
                TestTableConfiguration::create($tableId)
                    ->withNumberOfPlayers(6)
                    ->withDealer(TestPlayers::ddb())
            );
        }

        $this->commandBus->dispatch(
            new TeamUpPlayers(
                TestGames::game18(),
                Teams::empty()->withAdditionalTeam(
                    Team::withPlayers(TestPlayers::secretaris(), TestPlayers::dtl())
                )->withAdditionalTeam(
                    Team::withPlayers(TestPlayers::ddb(), TestPlayers::dpb())
                ),
                TestPlayers::dpb()
            )
        );

        $this->commandBus->dispatch(
            new PrepareGame(
                TestTables::table19(),
                TestPlayers::ddb(),
                TestPlayers::ddb(),
                52
            ),
            new Deal(
                TestGames::game19(),
                DealingStrategy::singleCard()
            )
        );

        $this->createTableOf5WithFirstTrickCollected(TestTables::table21());

        $this->testTableBuilder->buildTableReadyToPlay(
            TestTableConfiguration::create(TestTables::table22())
                ->withNumberOfPlayers(5)
                ->withDealer(TestPlayers::ddb())
        );
        $this->commandBus->dispatch(
            new ShowHandToSpectator(
                TestPlayers::secretaris(),
                TestPlayers::ddb(),
                TestGames::game22()
            )
        );

        $this->createTableOf5WithFirstTrickCollected(TestTables::table23());
        $this->commandBus->dispatch(
            new RevealHand(
                TestGames::game23(),
                TestPlayers::penningmeester()
            ),
            new ShowInitialHand(
                TestGames::game23(),
                TestPlayers::penningmeester()
            )
        );

        $this->claimTableAndPublishInvitations(
            TestTables::table24(),
            new SimplePlayerProfile(
                TestPlayers::diu(),
                PlayerName::fromString('diu'),
                Language::nl()
            ),
            new Invitations(
                TestInvitations::invitation24_1(),
                TestInvitations::invitation24_2(),
            )
        );

        $this->commandBus->dispatch(
            new PlayCard(
                TestGames::game26(),
                TestPlayers::ddb(),
                new Card(1, Card::SPADES),
                1
            ),
            // 🂴🂵🂶🂷🃇🃈🃉🃊🃞🂡🂢🂣🂤
            new RevealHand(
                TestGames::game26(),
                TestPlayers::ddb()
            ),
            new VoteGameConclusion(
                TestGames::game26(),
                TestPlayers::ddb(),
                GameConclusion::REDEAL()
            ),
            new VoteGameConclusion(
                TestGames::game26(),
                TestPlayers::secretaris(),
                GameConclusion::REDEAL()
            )
        );

        $this->commandBus->dispatch(
            new RevealHand(
                TestGames::game27(),
                TestPlayers::ddb()
            )
        );

        // Table 28 and 33: 3 playes, no game started.
        $tableIds = [
            TestTables::table28(),
            TestTables::table33(),
        ];
        foreach ($tableIds as $tableId) {
            $this->testTableBuilder->buildTableWithPlayers(
                TestTableConfiguration::create($tableId)
                    ->withNumberOfPlayers(3)
                    ->withDealer(TestPlayers::ddb())
            );
        }

        $this->commandBus->dispatch(
            new PrepareGame(
                TestTables::table28(),
                TestPlayers::dtl(),
                TestPlayers::dtl(),
                52
            )
        );

        // Table of six, no game started
        $this->testTableBuilder->buildTableWithPlayers(
            TestTableConfiguration::create(TestTables::table29())
                ->withNumberOfPlayers(6)
        );
        $this->commandBus->dispatch(
            new PrepareGame(
                TestTables::table29(),
                TestPlayers::dtl(),
                TestPlayers::dtl(),
                52
            )
        );

        $this->commandBus->dispatch(
            new VoteGameConclusion(TestGames::game30(), TestPlayers::dtl(), GameConclusion::REDEAL()),
            new VoteGameConclusion(TestGames::game30(), TestPlayers::dpb(), GameConclusion::REDEAL()),
        );

        $this->createTableOf5WithFirstTrickCollected(TestTables::table31());

        $this->claimTableAndPublishInvitations(
            TestTables::table32(),
            TestProfiles::ddl(),
            new Invitations(
                TestInvitations::invitation32()
            )
        );

        $this->commandBus->dispatch(
            new PrepareGame(
                TestTables::table33(),
                TestPlayers::dpb(),
                TestPlayers::dpb(),
                52
            ),
            new Deal(
                TestGames::game33(),
                DealingStrategy::singleCard()
            ),
            new PlayCard(
                TestGames::game33(),
                TestPlayers::ddb(),
                new Card(5, Card::SPADES),
                1
            ),
            new PlayCard(
                TestGames::game33(),
                TestPlayers::dtl(),
                new Card(6, Card::SPADES),
                1
            ),
            new PlayCard(
                TestGames::game33(),
                TestPlayers::dpb(),
                new Card(7, Card::SPADES),
                1
            ),
            new VoteAsWinner(
                TestGames::game33(),
                TestPlayers::dpb(),
                1,
                TestPlayers::dpb()
            ),
            new VoteAsWinner(
                TestGames::game33(),
                TestPlayers::dtl(),
                1,
                TestPlayers::dpb()
            ),
        );

        $output->writeln('Test data loaded.');

        return 0;
    }

    private function arrangeReproducibleData(): void
    {
        // The service container always returns the same instance of a requested service.
        // So if I fiddle with the random provider here, I have control on how the cards are cut further on.
        $this->randomProvider->testMode = true;

        // The same for 'prepareGameHandler'; replace the shuffler by a dummy one, so that we know what happens.
        $this->prepareGameHandler->changeShuffler(new class() implements Shuffler {
            public function shuffle(PileOfCards $pileOfCards): SimplePileOfCards
            {
                return SimplePileOfCards::fromCards($pileOfCards);
            }
        });
    }

    private function createTableOf5WithFirstTrickOnTheTable(TableIdentifier $tableIdentifier): void
    {
        $this->testTableBuilder->buildTableReadyToPlay(
            TestTableConfiguration::create($tableIdentifier)
                ->withNumberOfPlayers(5)
                ->withDealer(TestPlayers::ddb())
                ->withCards(SimplePileOfCards::complete()->cutAfter(48))
        );
        $gameIdentifier = GameIdentifier::fromNumber($tableIdentifier, GameNumber::first());
        $this->commandBus->dispatch(
            new PlayCard($gameIdentifier, TestPlayers::secretaris(), new Card(7, Card::DIAMONDS), 1),
            new PlayCard($gameIdentifier, TestPlayers::penningmeester(), new Card(13, Card::DIAMONDS), 1),
            new PlayCard($gameIdentifier, TestPlayers::dtl(), new Card(1, Card::DIAMONDS), 1),
            new PlayCard($gameIdentifier, TestPlayers::dpb(), new Card(3, Card::DIAMONDS), 1)
        );
    }

    private function createTableOf5WithFirstTrickCollected(TableIdentifier $tableIdentifier): void
    {
        $this->createTableOf5WithFirstTrickOnTheTable($tableIdentifier);
        $gameIdentifier = GameIdentifier::fromNumber($tableIdentifier, GameNumber::first());

        $this->commandBus->dispatch(
            new VoteAsWinner($gameIdentifier, TestPlayers::dtl(), 1, TestPlayers::dtl()),
            new VoteAsWinner($gameIdentifier, TestPlayers::secretaris(), 1, TestPlayers::dtl()),
            new VoteAsWinner($gameIdentifier, TestPlayers::penningmeester(), 1, TestPlayers::dtl())
        );
    }

    private function claimTableAndPublishInvitations(
        TableIdentifier $tableIdentifier,
        PlayerProfile $playerProfile,
        Invitations $invitations
    ): void {
        $this->commandBus->dispatch(
            new ClaimTable(
                $tableIdentifier,
                $playerProfile
            ),
        );

        foreach ($invitations as $invitation) {
            $this->commandBus->dispatch(
                new InvitePlayer(
                    $tableIdentifier,
                    $invitation,
                    $playerProfile->getPlayerIdentifier(),
                    Language::nl()
                ),
                new PublishInvitation(
                    $tableIdentifier,
                    $invitation,
                    $playerProfile->getPlayerIdentifier(),
                    new \DateTimeImmutable()
                )
            );
        }
    }
}
