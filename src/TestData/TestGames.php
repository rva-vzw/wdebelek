<?php

declare(strict_types=1);

namespace App\TestData;

use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\WriteModel\Game\GameIdentifier;

/**
 * Admittedly, this is a dumb class. But hey, it's just testdata.
 */
final class TestGames
{
    public static function game1(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table1(), GameNumber::first());
    }

    public static function game2(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table2(), GameNumber::first());
    }

    public static function game3(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table3(), GameNumber::first());
    }

    public static function game4(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table4(), GameNumber::first());
    }

    public static function game5(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table5(), GameNumber::first());
    }

    public static function game6(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table6(), GameNumber::first());
    }

    public static function game11(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table11(), GameNumber::first());
    }

    public static function game14(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table14(), GameNumber::first());
    }

    public static function game16(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table16(), GameNumber::first());
    }

    public static function game18(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table18(), GameNumber::first());
    }

    public static function game19(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table19(), GameNumber::first());
    }

    public static function game21_1(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table21(), GameNumber::first());
    }

    public static function game23(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table23(), GameNumber::first());
    }

    public static function game26(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table26(), GameNumber::first());
    }

    public static function game27(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table27(), GameNumber::first());
    }

    public static function game30(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table30(), GameNumber::first());
    }

    public static function game22(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table22(), GameNumber::first());
    }

    public static function game31(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table31(), GameNumber::first());
    }

    public static function game33(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table33(), GameNumber::first());
    }

    public static function game20(): GameIdentifier
    {
        return GameIdentifier::fromNumber(TestTables::table20(), GameNumber::first());
    }
}
