<?php

declare(strict_types=1);

namespace App\NonDeterministic;

use App\Domain\ValueObject\Card\PileOfCards;
use App\Domain\ValueObject\Card\SimplePileOfCards;

interface Shuffler
{
    public function shuffle(PileOfCards $pileOfCards): SimplePileOfCards;
}
