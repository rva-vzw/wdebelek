<?php

declare(strict_types=1);

namespace App\NonDeterministic;

/**
 * Interface for random number generation. Just to make things testable.
 */
interface RandomProvider
{
    public function getRandomNumber(int $min, int $max): int;
}
