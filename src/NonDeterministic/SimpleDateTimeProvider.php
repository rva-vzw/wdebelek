<?php

declare(strict_types=1);

namespace App\NonDeterministic;

final class SimpleDateTimeProvider implements DateTimeProvider
{
    public function getNow(): \DateTimeImmutable
    {
        return new \DateTimeImmutable();
    }
}
