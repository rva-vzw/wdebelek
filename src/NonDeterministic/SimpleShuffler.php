<?php

declare(strict_types=1);

namespace App\NonDeterministic;

use App\Domain\ValueObject\Card\PileOfCards;
use App\Domain\ValueObject\Card\SimplePileOfCards;

final class SimpleShuffler implements Shuffler
{
    public function shuffle(PileOfCards $pileOfCards): SimplePileOfCards
    {
        $cards = iterator_to_array($pileOfCards);
        shuffle($cards);

        return SimplePileOfCards::fromCards(new \ArrayIterator($cards));
    }
}
