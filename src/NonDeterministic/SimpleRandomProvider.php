<?php

declare(strict_types=1);

namespace App\NonDeterministic;

/**
 * Service to generate random numbers.
 *
 * The $testMode is a hack for making automatic testing easier.
 * I'm not very happy about how this is done, but it will do for now, I guess.
 */
final class SimpleRandomProvider implements RandomProvider
{
    /**
     * @param bool $testMode if set to true, 4 is returned instead of random numbers
     */
    public function __construct(
        public bool $testMode = false,
    ) {
    }

    public function getRandomNumber(int $min, int $max): int
    {
        return $this->testMode ? 4 : rand($min, $max);
    }
}
