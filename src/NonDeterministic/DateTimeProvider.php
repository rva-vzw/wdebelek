<?php

declare(strict_types=1);

namespace App\NonDeterministic;

interface DateTimeProvider
{
    public function getNow(): \DateTimeImmutable;
}
