<?php

declare(strict_types=1);

namespace App\Orm\Entity;

use App\Domain\ReadModel\GameCounter\GameCounter;
use App\Domain\WriteModel\Table\TableIdentifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="readmodel_game_counter",
 *     indexes={
 *
 *      @ORM\Index(name="game_counter_started", columns={"first_game_started_at"}),
 *      @ORM\Index(name="game_counter_ended", columns={"latest_game_started_at"}),
 *      @ORM\Index(name="game_counter_date_parts", columns={"year", "month"})
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="App\Orm\Repository\GameCounterOrmRepository")
 */
class OrmGameCounter
{
    private function __construct(
        /**
         * @var non-empty-string
         *
         * @ORM\Column(type="guid")
         *
         * @ORM\Id()
         */
        private string $tableId,
        /**
         * @ORM\Column(type="integer")
         */
        private int $gameCount,
        /**
         * @ORM\Column(type="datetime_immutable")
         */
        private \DateTimeImmutable $firstGameStartedAt,
        /**
         * @ORM\Column(type="datetime_immutable")
         */
        private \DateTimeImmutable $latestGameStartedAt,
        /**
         * @ORM\Column(type="integer")
         */
        private int $year,
        /**
         * @ORM\Column(type="integer")
         */
        private int $month,
    ) {
    }

    public static function createNew(TableIdentifier $tableIdentifier, \DateTimeImmutable $dateTime): self
    {
        return new self(
            $tableIdentifier->toString(),
            0,
            $dateTime,
            $dateTime,
            (int) $dateTime->format('Y'),
            (int) $dateTime->format('m'),
        );
    }

    public function getTableId(): string
    {
        return $this->tableId;
    }

    public function getGameCount(): int
    {
        return $this->gameCount;
    }

    public function setGameCount(int $gameCount): void
    {
        $this->gameCount = $gameCount;
    }

    public function getFirstGameStartedAt(): \DateTimeImmutable
    {
        return $this->firstGameStartedAt;
    }

    public function getLatestGameStartedAt(): \DateTimeImmutable
    {
        return $this->latestGameStartedAt;
    }

    public function setLatestGameStartedAt(\DateTimeImmutable $latestGameStartedAt): void
    {
        $this->latestGameStartedAt = $latestGameStartedAt;
        $this->year = (int) $latestGameStartedAt->format('Y');
        $this->month = (int) $latestGameStartedAt->format('m');
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function getMonth(): int
    {
        return $this->month;
    }

    public function toGameCounter(): GameCounter
    {
        return GameCounter::createNew(
            TableIdentifier::fromString($this->tableId),
            $this->firstGameStartedAt
        )->withGamesCounted(
            $this->gameCount,
            $this->latestGameStartedAt
        );
    }
}
