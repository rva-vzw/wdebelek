<?php

declare(strict_types=1);

namespace App\Orm\Entity;

use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\ReadModel\SeatedPlayer\SeatedPlayer;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\TableIdentifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="readmodel_seated_player")
 *
 * @ORM\Entity(repositoryClass="App\Orm\Repository\SeatedPlayerOrmRepository")
 */
class OrmSeatedPlayer implements SeatedPlayer
{
    /**
     * @var non-empty-string
     *
     * @ORM\Column(type="guid")
     *
     * @ORM\Id()
     */
    private $tableId;

    /**
     * @var non-empty-string
     *
     * @ORM\Column(type="guid")
     *
     * @ORM\Id()
     */
    private $playerId;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $playerName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $lang;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $seat;

    /**
     * @param non-empty-string $tableId
     * @param non-empty-string $playerId
     */
    private function __construct(
        string $tableId,
        string $playerId,
        string $playerName,
        string $lang,
        int $seat
    ) {
        $this->tableId = $tableId;
        $this->playerId = $playerId;
        $this->playerName = $playerName;
        $this->lang = $lang;
        $this->seat = $seat;
    }

    public static function fromSeatedPlayer(
        PlayerProfile $playerProfile,
        TableIdentifier $tableIdentifier,
        Seat $seat
    ): self {
        return new self(
            $tableIdentifier->toString(),
            $playerProfile->getPlayerIdentifier()->toString(),
            $playerProfile->getPlayerName()->toString(),
            $playerProfile->getLanguage()->toString(),
            $seat->toInteger()
        );
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString($this->playerId);
    }

    public function getPlayerName(): PlayerName
    {
        return PlayerName::fromString($this->playerName);
    }

    public function getLanguage(): Language
    {
        return Language::fromString($this->lang);
    }

    public function getSeat(): Seat
    {
        return Seat::fromInteger($this->seat);
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return TableIdentifier::fromString($this->tableId);
    }
}
