<?php

declare(strict_types=1);

namespace App\Orm\Entity;

use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * State of the *current* games at the tables.
 *
 * @ORM\Table(
 *     name="readmodel_table_state",
 *     indexes={@ORM\Index(name="table_state_game_id", columns={"game_id"})}
 *     )
 *
 * @ORM\Entity(repositoryClass="App\Orm\Repository\TableStateOrmRepository")
 */
class OrmTableState
{
    private function __construct(
        /**
         * Current game ID, even if table is suspended.
         *
         * @var non-empty-string
         *
         * @ORM\Column(type="guid")
         */
        private string $gameId,
        /**
         * @ORM\Column(type="integer")
         */
        private int $trickNumber,
        /**
         * @var non-empty-string
         *
         * @ORM\Column(type="guid")
         *
         * @ORM\Id()
         */
        private string $tableId,
        /**
         * @ORM\Column(type="integer")
         */
        private int $doneVotes,
        /**
         * @ORM\Column(type="integer")
         */
        private int $redealVotes,
        /**
         * @var non-empty-string
         *
         * @ORM\Column(type="guid")
         */
        private string $lastPlayer,
        /**
         * @var mixed[]
         *
         * @ORM\Column(type="json")
         */
        private array $normalizedTeams,
        /**
         * @ORM\Column(type="boolean")
         */
        private bool $suspended,
        /**
         * @ORM\Column(type="string")
         */
        private string $cardGame,
        /**
         * @ORM\Column(type="integer")
         */
        private int $trumpCardNumber,
        /**
         * @ORM\Column(type="boolean")
         */
        private bool $waitingForDealer,
        /**
         * @var non-empty-string
         *
         * @ORM\Column(type="guid")
         */
        private string $dealer,
        /**
         * @ORM\Column(type="guid")
         */
        private int $totalNumberOfCards,
        /**
         * @ORM\Column(type="boolean")
         */
        private bool $collectedTricksDisclosed,
    ) {
    }

    public function setTrickNumber(int $trickNumber): void
    {
        $this->trickNumber = $trickNumber;
    }

    public function getTrickNumber(): int
    {
        return $this->trickNumber;
    }

    public static function forNewTable(
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $lastPayer,
        PlayerIdentifier $dealer
    ): self {
        return new self(
            GameIdentifier::fromNumber($tableIdentifier, GameNumber::first())->toString(),
            0,
            $tableIdentifier->toString(),
            0,
            0,
            $lastPayer->toString(),
            [],
            false,
            CardGame::unspecified()->toString(),
            0,
            false,
            $dealer->toString(),
            0,
            false
        );
    }

    public function setGameIdentifier(GameIdentifier $gameIdentifier): void
    {
        $this->gameId = $gameIdentifier->toString();
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return GameIdentifier::fromString($this->gameId);
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return TableIdentifier::fromString($this->tableId);
    }

    public function getDoneVotes(): int
    {
        return $this->doneVotes;
    }

    public function getRedealVotes(): int
    {
        return $this->redealVotes;
    }

    public function setDoneVotes(int $doneVotes): void
    {
        $this->doneVotes = $doneVotes;
    }

    public function setRedealVotes(int $redealVotes): void
    {
        $this->redealVotes = $redealVotes;
    }

    /**
     * The last player is the player that has the trump card, in case of 4 players.
     */
    public function getLastPlayer(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString($this->lastPlayer);
    }

    public function setLastPlayer(PlayerIdentifier $playerIdentifier): void
    {
        $this->lastPlayer = $playerIdentifier->toString();
    }

    /**
     * @return mixed[]
     */
    public function getNormalizedTeams(): array
    {
        return $this->normalizedTeams;
    }

    /**
     * @param mixed[] $normalizedTeams
     */
    public function setNormalizedTeams(array $normalizedTeams): void
    {
        $this->normalizedTeams = $normalizedTeams;
    }

    public function suspend(): void
    {
        $this->trickNumber = 0;
        $this->doneVotes = 0;
        $this->redealVotes = 0;
        $this->normalizedTeams = [];
        $this->suspended = true;
    }

    public function isSuspended(): bool
    {
        return $this->suspended;
    }

    public function getCardGame(): CardGame
    {
        return CardGame::fromString($this->cardGame);
    }

    public function setCardGame(CardGame $cardGame): void
    {
        $this->cardGame = $cardGame->toString();
    }

    public function resume(): void
    {
        $this->suspended = false;
    }

    public function getTrumpCardNumber(): int
    {
        return $this->trumpCardNumber;
    }

    public function setTrumpCard(int $cardNumber): void
    {
        $this->trumpCardNumber = $cardNumber;
    }

    public function isWaitingForDealer(): bool
    {
        return $this->waitingForDealer;
    }

    public function setWaitingForDealer(bool $waitingForDealer): void
    {
        $this->waitingForDealer = $waitingForDealer;
    }

    public function getDealer(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString($this->dealer);
    }

    public function setDealer(PlayerIdentifier $dealer): void
    {
        $this->dealer = $dealer->toString();
    }

    public function getTotalNumberOfCards(): int
    {
        return $this->totalNumberOfCards;
    }

    public function setTotalNumberOfCards(int $totalNumberOfCards): void
    {
        $this->totalNumberOfCards = $totalNumberOfCards;
    }

    public function hasCollectedTricksDisclosed(): bool
    {
        return $this->collectedTricksDisclosed;
    }

    public function discloseCollectedTricks(): void
    {
        $this->collectedTricksDisclosed = true;
    }

    public function undiscloseCollectedTricks(): void
    {
        $this->collectedTricksDisclosed = false;
    }
}
