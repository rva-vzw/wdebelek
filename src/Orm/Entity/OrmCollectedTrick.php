<?php

declare(strict_types=1);

namespace App\Orm\Entity;

use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="readmodel_collected_trick",
 *     indexes={@ORM\Index(name="played_trick_table_number", columns={"table_id","trick_number"})}
 *     )
 *
 * @ORM\Entity(repositoryClass="App\Orm\Repository\CollectedTrickOrmRepository")
 */
class OrmCollectedTrick
{
    public function __construct(
        /**
         * @var non-empty-string
         *
         * @ORM\Column(type="guid")
         */
        private string $tableId,
        /**
         * @var non-empty-string
         *
         * @ORM\Column(type="guid")
         *
         * @ORM\Id()
         */
        private string $gameId,
        /**
         * @ORM\Column(type="integer")
         *
         * @ORM\Id()
         */
        private int $trickNumber,
        /**
         * @var int[]
         *
         * @ORM\Column(type="json")
         */
        private array $normalizedCards,
        /**
         * @var non-empty-string
         *
         * @ORM\Column(type="guid")
         */
        private string $winner,
        /**
         * @ORM\Column(type="boolean")
         */
        private bool $disclosed
    ) {
    }

    /**
     * @return mixed[]
     */
    public function getNormalizedCards(): array
    {
        return $this->normalizedCards;
    }

    public function getTrickNumber(): int
    {
        return $this->trickNumber;
    }

    /**
     * @return non-empty-string
     */
    public function getWinner(): string
    {
        return $this->winner;
    }

    public function disclose(): void
    {
        $this->disclosed = true;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return TableIdentifier::fromString($this->tableId);
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return GameIdentifier::fromString($this->gameId);
    }

    public function isDisclosed(): bool
    {
        return $this->disclosed;
    }
}
