<?php

declare(strict_types=1);

namespace App\Orm\Entity;

use App\Domain\ReadModel\OccupiedSeats\OccupiedSeat;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Invitation\InvitationPrivacy;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Player\SimpleNamedPlayer;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\TableIdentifier;
use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Table(
 *     name="readmodel_occupied_seat",
 *     indexes={@ORM\Index(name="occupied_seat_player_id", columns={"player_id"})}
 *     )
 *
 * @ORM\Entity(repositoryClass="App\Orm\Repository\OccupiedSeatOrmRepository")
 */
class OrmOccupiedSeat
{
    /**
     * @var non-empty-string
     *
     * @ORM\Column(type="guid")
     *
     * @ORM\Id()
     */
    private $tableId;

    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     */
    private $playerId;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $playerName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $invitationLink;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @ORM\Id()
     */
    private $seat;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $invitation;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $hostName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $lang;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $invitationPrivacy;

    /**
     * @param non-empty-string $tableId
     */
    private function __construct(
        string $tableId,
        string $playerId,
        string $playerName,
        string $invitationLink,
        int $seat,
        string $invitation,
        string $hostName,
        string $lang,
        string $invitationPrivacy
    ) {
        $this->tableId = $tableId;
        $this->playerId = $playerId;
        $this->playerName = $playerName;
        $this->invitationLink = $invitationLink;
        $this->seat = $seat;
        $this->invitation = $invitation;
        $this->hostName = $hostName;
        $this->lang = $lang;
        $this->invitationPrivacy = $invitationPrivacy;
    }

    public static function fromOccupiedSeat(OccupiedSeat $occupiedSeat): self
    {
        return new self(
            $occupiedSeat->getTableIdentifier()->toString(),
            $occupiedSeat->hasAccepted() ? $occupiedSeat->getPlayerIdentifier()->toString() : '',
            $occupiedSeat->getPlayerName()->toString(),
            $occupiedSeat->getInvitationUrl(),
            $occupiedSeat->getSeat()->toInteger(),
            $occupiedSeat->invitationPending() ? $occupiedSeat->getInvitation()->toString() : '',
            $occupiedSeat->getHostName()->toString(),
            $occupiedSeat->getLanguage()->toString(),
            $occupiedSeat->getInvitationPrivacy()->toString()
        );
    }

    public function accept(PlayerIdentifier $playerIdentifier, PlayerName $playerName): void
    {
        $this->playerId = $playerIdentifier->toString();
        $this->invitationLink = '';
        $this->playerName = $playerName->toString();
        // The invitation itself can stay where it is, for later lookup. The link is gone, since it won't work anymore.
    }

    public function toOccupiedSeat(): OccupiedSeat
    {
        $tableIdentifier = TableIdentifier::fromString($this->tableId);

        return new OccupiedSeat(
            $tableIdentifier,
            empty($this->playerId) ? null : PlayerIdentifier::fromString($this->playerId),
            PlayerName::fromString($this->playerName),
            $this->invitationLink,
            Seat::fromInteger($this->seat),
            empty($this->invitation) ? null : Invitation::fromString($this->invitation),
            PlayerName::fromString($this->hostName),
            Language::fromString($this->lang),
            InvitationPrivacy::fromString($this->invitationPrivacy)
        );
    }

    public function hasActivePlayer(): bool
    {
        return !empty($this->playerId);
    }

    public function getPlayer(): SimpleNamedPlayer
    {
        Assert::stringNotEmpty($this->playerId);

        return new SimpleNamedPlayer(
            PlayerIdentifier::fromString($this->playerId),
            PlayerName::fromString($this->playerName)
        );
    }

    public function setInvitationPrivacy(InvitationPrivacy $invitationPrivacy): void
    {
        $this->invitationPrivacy = $invitationPrivacy->toString();
    }
}
