<?php

declare(strict_types=1);

namespace App\Orm\Entity;

use App\Domain\ReadModel\WelcomingTable\WelcomingTable;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Invitation\Invitations;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\WriteModel\Table\TableIdentifier;
use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Table(
 *     name="readmodel_welcoming_table",
 *     indexes={
 *
 *      @ORM\Index(name="latest_invitation_date", columns={"latest_invitation_published_at"})
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="App\Orm\Repository\WelcomingTableOrmRepository")
 */
class OrmWelcomingTable
{
    /**
     * @var non-empty-string
     *
     * @ORM\Column(type="guid")
     *
     * @ORM\Id()
     */
    private $tableId;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $host;

    /**
     * @var \DateTimeImmutable
     *
     * @ORM\Column(type="datetime_immutable")
     */
    private $latestInvitationPublishedAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $publishedInvitations;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $currentPlayers;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $cardGame;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $language;

    /**
     * @param non-empty-string $tableId
     */
    private function __construct(
        string $tableId,
        string $host,
        \DateTimeImmutable $latestInvitationPublishedAt,
        string $publishedInvitations,
        int $currentPlayers,
        string $cardGame,
        string $language
    ) {
        $this->tableId = $tableId;
        $this->host = $host;
        $this->latestInvitationPublishedAt = $latestInvitationPublishedAt;
        $this->publishedInvitations = $publishedInvitations;
        $this->currentPlayers = $currentPlayers;
        $this->cardGame = $cardGame;
        $this->language = $language;
    }

    public static function fromWelcomingTable(WelcomingTable $table): self
    {
        return new self(
            $table->getTableIdentifier()->toString(),
            $table->getHost()->toString(),
            $table->getLatestInvitationPublishedAt(),
            self::invitationsToScalarList($table->getPublishedInvitations()),
            $table->getPlayerCount(),
            $table->getCardGame()->toString(),
            $table->getLanguage()->toString()
        );
    }

    public static function invitationsToScalarList(Invitations $invitations): string
    {
        return implode(
            ',',
            array_map(
                function (Invitation $invitation): string {
                    return $invitation->toString();
                },
                iterator_to_array($invitations)
            )
        );
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return TableIdentifier::fromString($this->tableId);
    }

    public function getHost(): PlayerName
    {
        return PlayerName::fromString($this->host);
    }

    public function getLatestInvitationPublishedAt(): \DateTimeImmutable
    {
        return $this->latestInvitationPublishedAt;
    }

    public function getPublishedInvitations(): Invitations
    {
        if (empty($this->publishedInvitations)) {
            return Invitations::none();
        }

        return new Invitations(
            ...array_map(
                function (string $invitationString): Invitation {
                    Assert::stringNotEmpty($invitationString);

                    return Invitation::fromString($invitationString);
                },
                explode(',', $this->publishedInvitations)
            )
        );
    }

    public function getCurrentPlayers(): int
    {
        return $this->currentPlayers;
    }

    public function toWelcomingTable(): WelcomingTable
    {
        return new WelcomingTable(
            TableIdentifier::fromString($this->tableId),
            PlayerName::fromString($this->host),
            $this->latestInvitationPublishedAt,
            $this->getPublishedInvitations(),
            $this->currentPlayers,
            CardGame::fromString($this->cardGame),
            Language::fromString($this->language)
        );
    }

    public function setHost(PlayerName $name): void
    {
        $this->host = $name->toString();
    }

    public function setLatestInvitationPublishedAt(\DateTimeImmutable $latestInvitationPublishedAt): void
    {
        $this->latestInvitationPublishedAt = $latestInvitationPublishedAt;
    }

    public function setPublishedInvitations(Invitations $invitations): void
    {
        $this->publishedInvitations = self::invitationsToScalarList($invitations);
    }

    public function setPlayerCount(int $playerCount): void
    {
        $this->currentPlayers = $playerCount;
    }

    public function setCardGame(CardGame $cardGame): void
    {
        $this->cardGame = $cardGame->toString();
    }
}
