<?php

declare(strict_types=1);

namespace App\Orm\Entity;

use App\Domain\ReadModel\PlayingPlayer\PlayingPlayer;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\TableIdentifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="readmodel_playing_player"
 * )
 *
 * @ORM\Entity(repositoryClass="PlayingPlayerOrmRepository")
 */
class OrmPlayingPlayer
{
    /**
     * @var non-empty-string
     *
     * @ORM\Column(type="guid")
     *
     * @ORM\Id()
     */
    private $tableId;

    /**
     * @var non-empty-string
     *
     * @ORM\Column(type="guid")
     *
     * @ORM\Id()
     */
    private $playerId;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $playerName;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $cardsInHand;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $tricksWon;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isLastPlayer;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $shownTrump;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $seat;

    public function __construct(
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $playerIdentifier,
        PlayerName $playerName,
        int $cardsInHand,
        int $tricksWon,
        bool $isLastPlayer,
        ?Card $shownTrump,
        int $seat
    ) {
        $this->tableId = $tableIdentifier->toString();
        $this->playerId = $playerIdentifier->toString();
        $this->playerName = $playerName->toString();
        $this->cardsInHand = $cardsInHand;
        $this->tricksWon = $tricksWon;
        $this->isLastPlayer = $isLastPlayer;
        $this->shownTrump = $shownTrump instanceof Card
            ? $shownTrump->getCardNumber() : 0;
        $this->seat = $seat;
    }

    public static function fromPlayingPlayer(PlayingPlayer $playingPlayer): self
    {
        return new self(
            $playingPlayer->getTableIdentifier(),
            $playingPlayer->getPlayerIdentifier(),
            $playingPlayer->getPlayerName(),
            $playingPlayer->getCardsInHand(),
            $playingPlayer->getTricksWon(),
            $playingPlayer->isLastPlayer(),
            $playingPlayer->getShownTrump(),
            $playingPlayer->getSeat()->toInteger()
        );
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return TableIdentifier::fromString($this->tableId);
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString($this->playerId);
    }

    public function getPlayerName(): PlayerName
    {
        return PlayerName::fromString($this->playerName);
    }

    public function getCardsInHand(): int
    {
        return $this->cardsInHand;
    }

    public function getTricksWon(): int
    {
        return $this->tricksWon;
    }

    public function isLastPlayer(): bool
    {
        return $this->isLastPlayer;
    }

    public function getShownTrump(): ?Card
    {
        return 0 === $this->shownTrump ? null : Card::fromCardNumber($this->shownTrump);
    }

    public function asPlayingPlayer(): PlayingPlayer
    {
        return new PlayingPlayer(
            $this->cardsInHand,
            $this->tricksWon,
            $this->isLastPlayer,
            $this->getPlayerName(),
            $this->getPlayerIdentifier(),
            $this->getShownTrump(),
            $this->getTableIdentifier(),
            $this->getSeat()
        );
    }

    public function updateFrom(PlayingPlayer $fellowPlayer): void
    {
        $this->playerName = $fellowPlayer->getPlayerName()->toString();
        $this->cardsInHand = $fellowPlayer->getCardsInHand();
        $this->tricksWon = $fellowPlayer->getTricksWon();
        $this->isLastPlayer = $fellowPlayer->isLastPlayer();
        $this->shownTrump = $fellowPlayer->getShownTrump() instanceof Card
            ? $fellowPlayer->getShownTrump()->getCardNumber() : 0;
    }

    private function getSeat(): Seat
    {
        return Seat::fromInteger($this->seat);
    }
}
