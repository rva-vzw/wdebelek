<?php

declare(strict_types=1);

namespace App\Orm\Entity;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * Personal player information.
 *
 * I've had problems with doctrine and immutable doctrine entities in the past, so this class is mutable.
 *
 * @ORM\Table(
 *     name="readmodel_personal_player_info",
 *     indexes={@ORM\Index(name="personal_info_game_id", columns={"game_id","player_id"})}
 *     )
 *
 * @ORM\Entity(repositoryClass="App\Orm\Repository\PersonalPlayerInfoOrmRepository")
 */
class OrmPersonalPlayerInfo
{
    /**
     * @var non-empty-string
     *
     * @ORM\Id()
     *
     * @ORM\Column(type="guid")
     */
    private $playerId;

    /**
     * @var mixed[]
     *
     * @ORM\Column(type="json")
     */
    private $normalizedCards;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $trumpCardNumber;

    /**
     * @var non-empty-string
     *
     * @ORM\Id()
     *
     * @ORM\Column(type="guid")
     */
    private $tableId;

    /**
     * @var non-empty-string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $gameId;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $cardsOpen;

    /**
     * @param int[]            $normalizedCards
     * @param non-empty-string $playerId
     * @param non-empty-string $tableId
     * @param non-empty-string $gameId
     */
    private function __construct(
        string $playerId,
        array $normalizedCards,
        int $trumpCardNumber,
        string $tableId,
        ?string $gameId,
        bool $cardsOpen
    ) {
        $this->playerId = $playerId;
        $this->normalizedCards = $normalizedCards;
        $this->trumpCardNumber = $trumpCardNumber;
        $this->tableId = $tableId;
        $this->gameId = $gameId;
        $this->cardsOpen = $cardsOpen;
    }

    public static function createEmpty(
        PlayerIdentifier $playerIdentifier,
        TableIdentifier $tableIdentifier
    ): self {
        return new self(
            $playerIdentifier->toString(),
            [],
            0,
            $tableIdentifier->toString(),
            null,
            false
        );
    }

    public function clear(): void
    {
        $this->normalizedCards = [];
    }

    /**
     * @return mixed[]
     */
    public function getNormalizedCards(): array
    {
        return $this->normalizedCards;
    }

    /**
     * @param mixed[] $normalizedCards
     */
    public function replaceNormalizedCards(array $normalizedCards): void
    {
        $this->normalizedCards = $normalizedCards;
    }

    public function getTrumpCardNumber(): int
    {
        return $this->trumpCardNumber;
    }

    public function setTrumpCard(Card $trumpCard): void
    {
        $this->trumpCardNumber = $trumpCard->getCardNumber();
    }

    public function getGameIdentifier(): ?GameIdentifier
    {
        if (is_string($this->gameId)) {
            return GameIdentifier::fromString($this->gameId);
        }

        return null;
    }

    public function setGameIdentifier(GameIdentifier $gameIdentifier): void
    {
        $this->gameId = $gameIdentifier->toString();
    }

    public function clearTrumpCard(): void
    {
        $this->trumpCardNumber = 0;
    }

    public function showCards(): void
    {
        $this->cardsOpen = true;
    }

    public function hasCardsOpen(): bool
    {
        return $this->cardsOpen;
    }

    public function hideCards(): void
    {
        $this->cardsOpen = false;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString($this->playerId);
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return TableIdentifier::fromString($this->tableId);
    }
}
