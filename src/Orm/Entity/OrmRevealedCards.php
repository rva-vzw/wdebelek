<?php

declare(strict_types=1);

namespace App\Orm\Entity;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="readmodel_revealed_cards",
 *     indexes={
 *
 *      @ORM\Index(name="revealed_cards_viewer", columns={"game_id","viewer_id"}),
 *      @ORM\Index(name="revealed_hands_table", columns={"table_id"})
 *     }
 * )
 *
 * @ORM\Entity(
 *     repositoryClass="App\Orm\Repository\RevealedCardsOrmRepository"
 * )
 */
class OrmRevealedCards
{
    /**
     * @var non-empty-string
     *
     * @ORM\Column(type="guid")
     */
    private $gameId;

    /**
     * @var non-empty-string
     *
     * @ORM\Id()
     *
     * @ORM\Column(type="guid")
     */
    private $viewerId;

    /**
     * @var non-empty-string
     *
     * @ORM\Id()
     *
     * @ORM\Column(type="guid")
     */
    private $playerId;

    /**
     * @var int[]
     *
     * @ORM\Column(type="json")
     */
    private $normalizedCards;

    /**
     * @var non-empty-string
     *
     * @ORM\Id()
     *
     * @ORM\Column(type="guid")
     */
    private $tableId;

    /**
     * @param int[]            $normalizedCards
     * @param non-empty-string $gameId
     * @param non-empty-string $playerId
     * @param non-empty-string $spectatorId
     * @param non-empty-string $tableId
     */
    private function __construct(
        string $gameId,
        string $playerId,
        string $spectatorId,
        array $normalizedCards,
        string $tableId
    ) {
        $this->gameId = $gameId;
        $this->playerId = $playerId;
        $this->viewerId = $spectatorId;
        $this->normalizedCards = $normalizedCards;
        $this->tableId = $tableId;
    }

    /**
     * @param int[] $normalizedCards
     */
    public static function showingCards(
        PlayerIdentifier $playerIdentifier,
        PlayerIdentifier $showedToPlayerIdentifier,
        GameIdentifier $gameIdentifier,
        array $normalizedCards,
        TableIdentifier $tableIdentifier
    ): self {
        return new self(
            $gameIdentifier->toString(),
            $playerIdentifier->toString(),
            $showedToPlayerIdentifier->toString(),
            $normalizedCards,
            $tableIdentifier->toString()
        );
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return GameIdentifier::fromString($this->gameId);
    }

    public function getViewer(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString($this->viewerId);
    }

    public function getPlayer(): PlayerIdentifier
    {
        return PlayerIdentifier::fromString($this->playerId);
    }

    /**
     * @return int[]
     */
    public function getNormalizedCards(): array
    {
        return $this->normalizedCards;
    }

    public function setGameIdentifier(GameIdentifier $gameIdentifier): void
    {
        $this->gameId = $gameIdentifier->toString();
    }

    public function setViewerIdentifier(PlayerIdentifier $playerIdentifier): void
    {
        $this->viewerId = $playerIdentifier->toString();
    }

    public function setPlayerIdentifier(PlayerIdentifier $playerIdentifier): void
    {
        $this->playerId = $playerIdentifier->toString();
    }

    /**
     * @param int[] $normalizedCards
     */
    public function setNormalizedCards(array $normalizedCards): void
    {
        $this->normalizedCards = $normalizedCards;
    }

    public function setTableIdentifier(TableIdentifier $tableIdentifier): void
    {
        $this->tableId = $tableIdentifier->toString();
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return TableIdentifier::fromString($this->tableId);
    }
}
