<?php

declare(strict_types=1);

namespace App\Orm\Entity;

use App\Domain\WriteModel\Table\TableIdentifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="readmodel_trick_at_table"
 * )
 *
 * @ORM\Entity(repositoryClass="App\Orm\Repository\TrickAtTableOrmRepository")
 */
class OrmTrickAtTable
{
    /**
     * @var int[]
     *
     * @ORM\Column(type="json")
     */
    private array $normalizedTrick;

    /**
     * @var non-empty-string
     *
     * @ORM\Column(type="guid")
     *
     * @ORM\Id()
     */
    private string $tableId;

    /**
     * @param int[] $normalizedTrick
     */
    public function __construct(TableIdentifier $tableIdentifier, array $normalizedTrick)
    {
        $this->tableId = $tableIdentifier->toString();
        $this->normalizedTrick = $normalizedTrick;
    }

    /**
     * @return int[]
     */
    public function getNormalizedTrick(): array
    {
        return $this->normalizedTrick;
    }

    /**
     * @param int[] $normalizedTrick
     */
    public function setNormalizedTrick(array $normalizedTrick): void
    {
        $this->normalizedTrick = $normalizedTrick;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return TableIdentifier::fromString($this->tableId);
    }

    public function setTableIdentifier(TableIdentifier $tableIdentifier): void
    {
        $this->tableId = $tableIdentifier->toString();
    }
}
