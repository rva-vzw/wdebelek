<?php

declare(strict_types=1);

namespace App\Orm\ReadModel;

use App\Domain\ReadModel\Exception\NotFoundInReadModel;
use App\Domain\ReadModel\WelcomingTable\WelcomingTable;
use App\Domain\ReadModel\WelcomingTable\WelcomingTableRepository;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Entity\OrmWelcomingTable;
use App\Orm\Repository\WelcomingTableOrmRepository;

final class WelcomingTableReadModel implements WelcomingTableRepository
{
    /** @var WelcomingTableOrmRepository */
    private $ormRepository;

    public function __construct(WelcomingTableOrmRepository $ormRepository)
    {
        $this->ormRepository = $ormRepository;
    }

    public function hasWelcomingTable(TableIdentifier $tableIdentifier): bool
    {
        $table = $this->ormRepository->find($tableIdentifier->toString());

        return $table instanceof OrmWelcomingTable;
    }

    public function getWelcomingTable(TableIdentifier $tableIdentifier): WelcomingTable
    {
        $table = $this->ormRepository->find($tableIdentifier->toString());

        if ($table instanceof OrmWelcomingTable) {
            return $table->toWelcomingTable();
        }

        throw NotFoundInReadModel::create(WelcomingTable::class, $tableIdentifier);
    }

    public function saveWelcomingTable(WelcomingTable $table): void
    {
        $ormWelcomingTable = $this->ormRepository->find($table->getTableIdentifier()->toString());

        if ($ormWelcomingTable instanceof OrmWelcomingTable) {
            $ormWelcomingTable->setHost($table->getHost());
            $ormWelcomingTable->setLatestInvitationPublishedAt($table->getLatestInvitationPublishedAt());
            $ormWelcomingTable->setPublishedInvitations($table->getPublishedInvitations());
            $ormWelcomingTable->setPlayerCount($table->getPlayerCount());
            $ormWelcomingTable->setCardGame($table->getCardGame());
        } else {
            $ormWelcomingTable = OrmWelcomingTable::fromWelcomingTable($table);
        }
        $this->ormRepository->save($ormWelcomingTable);
    }

    public function removeWelcomingTable(TableIdentifier $tableIdentifier): void
    {
        $this->ormRepository->remove($tableIdentifier);
    }
}
