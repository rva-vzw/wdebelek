<?php

declare(strict_types=1);

namespace App\Orm\ReadModel;

use App\Domain\ReadModel\ActiveTables\ActiveTables;
use App\Domain\ReadModel\CardGames\TableCardGames;
use App\Domain\ReadModel\Exception\NotFoundInReadModel;
use App\Domain\ReadModel\LastPlayers\LastPlayers;
use App\Domain\ReadModel\TableState\GamesAtTable;
use App\Domain\ReadModel\TableState\SuspendedTables;
use App\Domain\ReadModel\TableState\TableState;
use App\Domain\ReadModel\TableState\TableStateRepository;
use App\Domain\ReadModel\TableState\TableStates;
use App\Domain\ReadModel\TableState\TableStateUpdater;
use App\Domain\ReadModel\TableState\TeamsByGame;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\Teams;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Entity\OrmTableState;
use App\Orm\Repository\TableStateOrmRepository;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Table state read model.
 *
 * This table contains all kind of table-related info. I have thought about splitting it up, but since
 * replay is so slow already, maybe I shouldn't add too many tables to keep up to dated.
 */
final class TableStateReadModel implements TableStateUpdater, TableStates, ActiveTables, TableStateRepository, SuspendedTables, GamesAtTable, TeamsByGame, TableCardGames, LastPlayers
{
    public function __construct(
        private TableStateOrmRepository $repository,
        private NormalizerInterface $normalizer,
        private DenormalizerInterface $denormalizer
    ) {
    }

    public function logTrick(
        GameIdentifier $gameIdentifier,
        int $trickNumber,
        PlayerIdentifier $winner
    ): void {
        $entity = $this->getOrmTableState($gameIdentifier);
        $entity->setTrickNumber($trickNumber + 1);
        $this->repository->save($entity);
    }

    private function getOrmTableState(GameIdentifier $gameIdentifier): OrmTableState
    {
        $entity = $this->repository->findOneBy([
            'gameId' => $gameIdentifier->toString(),
        ]);

        if ($entity instanceof OrmTableState) {
            return $entity;
        }

        // You know, an entry should be created as the game is started.
        throw NotFoundInReadModel::create(TableStateReadModel::class, $gameIdentifier);
    }

    public function deleteTableState(TableIdentifier $tableIdentifier): void
    {
        $this->repository->deleteByTableIdentifier($tableIdentifier);
    }

    public function getTableStateByGame(GameIdentifier $gameIdentifier): TableState
    {
        $entity = $this->getOrmTableState($gameIdentifier);

        return $this->mapFromOrm($entity);
    }

    public function prepareGame(
        TableIdentifier $tableIdentifier,
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $dealerIdentifier,
        PlayerIdentifier $lastPlayer
    ): void {
        $entity = $this->repository->find($tableIdentifier->toString());

        if ($entity instanceof OrmTableState) {
            $entity->setGameIdentifier($gameIdentifier);
            $entity->resume();
            $entity->setDealer($dealerIdentifier);
            $entity->setLastPlayer($lastPlayer);

            $this->repository->save($entity);

            return;
        }

        throw NotFoundInReadModel::create(TableState::class, $gameIdentifier);
    }

    public function getTableState(TableIdentifier $tableIdentifier): TableState
    {
        $ormTableState = $this->repository->find($tableIdentifier->toString());

        if ($ormTableState instanceof OrmTableState) {
            return $this->mapFromOrm($ormTableState);
        }

        throw NotFoundInReadModel::create(TableState::class, $tableIdentifier);
    }

    public function hasActiveTable(TableIdentifier $tableIdentifier): bool
    {
        $table = $this->repository->find($tableIdentifier->toString());

        return $table instanceof OrmTableState;
    }

    public function saveTableState(TableState $tableState): void
    {
        $ormTableState = $this->repository->find($tableState->getTableIdentifier()->toString());
        if (!$ormTableState instanceof OrmTableState) {
            $ormTableState = OrmTableState::forNewTable(
                $tableState->getTableIdentifier(),
                $tableState->getLastPlayer(),
                $tableState->getDealer(),
            );
        }

        $this->mapToOrm($tableState, $ormTableState);
        $this->repository->save($ormTableState);
    }

    // FIXME (#81): Move this functionality to some kind of mapper class.
    private function mapToOrm(TableState $tableState, OrmTableState $ormTableState): void
    {
        $ormTableState->setGameIdentifier($tableState->getGameIdentifier());
        $ormTableState->setTrickNumber($tableState->getTrickNumber());
        $ormTableState->setDoneVotes($tableState->getDoneVotes());
        $ormTableState->setRedealVotes($tableState->getRedealVotes());
        /** @var string[][] $normalizedTeams */
        $normalizedTeams = $this->normalizer->normalize($tableState->getPlayerTeams());
        $ormTableState->setNormalizedTeams($normalizedTeams);
        $ormTableState->setCardGame($tableState->getCardGame());

        if ($tableState->isSuspended()) {
            $ormTableState->suspend();
        } else {
            $ormTableState->resume();
        }

        $ormTableState->setTrumpCard(
            $tableState->hasRevealedTrump() ? $tableState->getRevealedTrump()->getCardNumber() : 0
        );

        $ormTableState->setWaitingForDealer($tableState->isWaitingForDealer());
        $ormTableState->setDealer($tableState->getDealer());
        $ormTableState->setTotalNumberOfCards($tableState->getTotalNumberOfCards());
        $ormTableState->setLastPlayer($tableState->getLastPlayer());

        if ($tableState->isCollectedTricksDisclosed()) {
            $ormTableState->discloseCollectedTricks();
        } else {
            $ormTableState->undiscloseCollectedTricks();
        }
    }

    private function mapFromOrm(OrmTableState $entity): TableState
    {
        $gameIdentifier = $entity->getGameIdentifier();
        $trumpCard = 0 === $entity->getTrumpCardNumber() ? null : Card::fromCardNumber($entity->getTrumpCardNumber());

        return new TableState(
            $gameIdentifier,
            $entity->getTrickNumber(),
            $entity->getTableIdentifier(),
            $entity->getDoneVotes(),
            $entity->getRedealVotes(),
            $this->getTeamsFromEntity($entity),
            $entity->getCardGame(),
            $entity->isSuspended(),
            $trumpCard,
            $entity->isWaitingForDealer(),
            $entity->getDealer(),
            $entity->getLastPlayer(),
            $entity->getTotalNumberOfCards(),
            $entity->hasCollectedTricksDisclosed(),
        );
    }

    private function getTeamsFromEntity(OrmTableState $entity): Teams
    {
        /** @var Teams $teams */
        $teams = $this->denormalizer->denormalize(
            $entity->getNormalizedTeams(),
            Teams::class
        );

        return $teams;
    }

    public function hasSuspendedTable(TableIdentifier $tableIdentifier): bool
    {
        $ormTableState = $this->repository->find($tableIdentifier->toString());

        return $ormTableState instanceof OrmTableState && $ormTableState->isSuspended();
    }

    public function getTableByCurrentGame(GameIdentifier $gameIdentifier): TableIdentifier
    {
        $ormTableState = $this->getOrmTableState($gameIdentifier);

        return $ormTableState->getTableIdentifier();
    }

    public function getTeamsByGame(GameIdentifier $gameIdentifier): Teams
    {
        // FIXME: This could use a dedicated read model.
        return $this->getTableStateByGame($gameIdentifier)->getPlayerTeams();
    }

    public function getCardGameByTable(TableIdentifier $tableIdentifier): CardGame
    {
        $ormTableState = $this->repository->find($tableIdentifier->toString());

        if ($ormTableState instanceof OrmTableState) {
            return $ormTableState->getCardGame();
        }

        throw NotFoundInReadModel::create(TableState::class, $tableIdentifier);
    }

    public function getLastPlayerByGame(GameIdentifier $gameIdentifier): PlayerIdentifier
    {
        return $this->getTableStateByGame($gameIdentifier)->getLastPlayer();
    }
}
