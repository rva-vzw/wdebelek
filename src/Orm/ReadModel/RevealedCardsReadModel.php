<?php

declare(strict_types=1);

namespace App\Orm\ReadModel;

use App\Domain\ReadModel\RevealedCards\CardsRevealed;
use App\Domain\ReadModel\RevealedCards\RevealedCards;
use App\Domain\ReadModel\RevealedCards\RevealedCardsRepository;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Entity\OrmRevealedCards;
use App\Orm\Repository\RevealedCardsOrmRepository;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class RevealedCardsReadModel implements RevealedCardsRepository, CardsRevealed
{
    /** @var RevealedCardsOrmRepository */
    private $ormRepository;
    /** @var NormalizerInterface */
    private $normalizer;
    /** @var DenormalizerInterface */
    private $denormalizer;

    public function __construct(
        RevealedCardsOrmRepository $ormRepository,
        NormalizerInterface $normalizer,
        DenormalizerInterface $denormalizer
    ) {
        $this->ormRepository = $ormRepository;
        $this->normalizer = $normalizer;
        $this->denormalizer = $denormalizer;
    }

    public function saveRevealedCards(RevealedCards $revealedCards): void
    {
        /** @var int[] $normalizedCards */
        $normalizedCards = $this->normalizer->normalize($revealedCards->getCards());

        $ormRevealedCards = $this->findOrmRevealedCards($revealedCards);

        if ($ormRevealedCards instanceof OrmRevealedCards && $revealedCards->isEmpty()) {
            // Delete revealed cards if no cards, to avoid useless processing of revealed
            // card records.
            $this->ormRepository->deleteRevealedCards($ormRevealedCards);

            return;
        }

        if ($ormRevealedCards instanceof OrmRevealedCards) {
            $ormRevealedCards->setGameIdentifier($revealedCards->getGameIdentifier());
            $ormRevealedCards->setNormalizedCards(
                $normalizedCards
            );
            $ormRevealedCards->setPlayerIdentifier($revealedCards->getPlayerIdentifier());
            $ormRevealedCards->setViewerIdentifier($revealedCards->getRevealedToPlayerIdentifier());
        } else {
            $ormRevealedCards = OrmRevealedCards::showingCards(
                $revealedCards->getPlayerIdentifier(),
                $revealedCards->getRevealedToPlayerIdentifier(),
                $revealedCards->getGameIdentifier(),
                $normalizedCards,
                $revealedCards->getTableIdentifier()
            );
        }

        $this->ormRepository->save($ormRevealedCards);
    }

    /**
     * @return \Traversable<RevealedCards>
     */
    public function getRevealedByPlayer(
        PlayerIdentifier $playerIdentifier,
        GameIdentifier $gameIdentifier
    ): \Traversable {
        return $this->getShownHandsBy([
            'playerId' => $playerIdentifier->toString(),
            'gameId' => $gameIdentifier->toString(),
        ]);
    }

    /**
     * @param array<string,mixed> $criteria
     *
     * @return \Traversable<RevealedCards>
     */
    private function getShownHandsBy(
        array $criteria
    ): \Traversable {
        /** @var OrmRevealedCards[] $ormShownHands */
        $ormShownHands = $this->ormRepository->findBy($criteria);

        foreach ($ormShownHands as $ormShownHand) {
            yield $this->getRevealedCardsFromOrmEntity($ormShownHand);
        }
    }

    /**
     * @return \Traversable<RevealedCards>
     */
    public function getRevealedCardsAtTable(TableIdentifier $tableIdentifier): \Traversable
    {
        return $this->getShownHandsBy([
            'tableId' => $tableIdentifier->toString(),
        ]);
    }

    private function findOrmRevealedCards(RevealedCards $shownHand): ?OrmRevealedCards
    {
        return $this->ormRepository->findOneBy([
            'tableId' => $shownHand->getTableIdentifier()->toString(),
            'playerId' => $shownHand->getPlayerIdentifier()->toString(),
            'viewerId' => $shownHand->getRevealedToPlayerIdentifier()->toString(),
        ]);
    }

    public function deleteRevealedCards(RevealedCards $shownHand): void
    {
        $ormShownHand = $this->findOrmRevealedCards($shownHand);

        if ($ormShownHand instanceof OrmRevealedCards) {
            $this->ormRepository->deleteRevealedCards($ormShownHand);
        }
    }

    /**
     * @return \Traversable<RevealedCards>
     */
    public function getCardsRevealedTo(PlayerIdentifier $playerIdentifier, GameIdentifier $gameIdentifier): \Traversable
    {
        return $this->getShownHandsBy([
            'viewerId' => $playerIdentifier->toString(),
            'gameId' => $gameIdentifier->toString(),
        ]);
    }

    public function getOrCreateRevealedCards(
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $playerIdentifier,
        PlayerIdentifier $revealedToPlayerIdentifier,
        TableIdentifier $tableIdentifier
    ): RevealedCards {
        $ormRevealedCards = $this->ormRepository->findOneBy([
            'gameId' => $gameIdentifier->toString(),
            'playerId' => $playerIdentifier->toString(),
            'viewerId' => $revealedToPlayerIdentifier->toString(),
            'tableId' => $tableIdentifier->toString(),
        ]);

        if ($ormRevealedCards instanceof OrmRevealedCards) {
            return $this->getRevealedCardsFromOrmEntity($ormRevealedCards);
        }

        return RevealedCards::none(
            $tableIdentifier,
            $gameIdentifier,
            $playerIdentifier,
            $revealedToPlayerIdentifier
        );
    }

    private function getRevealedCardsFromOrmEntity(OrmRevealedCards $ormShownHand): RevealedCards
    {
        /** @var SimplePileOfCards $cards */
        $cards = $this->denormalizer->denormalize($ormShownHand->getNormalizedCards(), SimplePileOfCards::class);

        $revealedCards = new RevealedCards(
            $ormShownHand->getGameIdentifier(),
            $ormShownHand->getPlayer(),
            $ormShownHand->getViewer(),
            $ormShownHand->getTableIdentifier(),
            $cards
        );

        return $revealedCards;
    }
}
