<?php

declare(strict_types=1);

namespace App\Orm\ReadModel;

use App\Domain\ReadModel\Exception\NotFoundInReadModel;
use App\Domain\ReadModel\PersonalPlayerInfo\PersonalPlayerInfo;
use App\Domain\ReadModel\PersonalPlayerInfo\PersonalPlayerInfoRepository;
use App\Domain\ReadModel\PersonalPlayerInfo\PlayerHands;
use App\Domain\ReadModel\PersonalPlayerInfo\PlayerHandUpdater;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\Hand;
use App\Domain\ValueObject\Card\PileOfCards;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Entity\OrmPersonalPlayerInfo;
use App\Orm\Repository\PersonalPlayerInfoOrmRepository;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class PersonalPlayerInfoReadModel implements PlayerHandUpdater, PersonalPlayerInfoRepository, PlayerHands
{
    private PersonalPlayerInfoOrmRepository $ormRepository;
    private NormalizerInterface $normalizer;
    private DenormalizerInterface $denormalizer;

    public function __construct(
        PersonalPlayerInfoOrmRepository $repository,
        NormalizerInterface $normalizer,
        DenormalizerInterface $denormalizer
    ) {
        $this->ormRepository = $repository;
        $this->normalizer = $normalizer;
        $this->denormalizer = $denormalizer;
    }

    public function emptyHand(
        PlayerIdentifier $playerIdentifier,
        TableIdentifier $tableIdentifier
    ): void {
        $hand = $this->getOrmPersonalInfo($playerIdentifier, $tableIdentifier);
        $hand->clear();
        $this->ormRepository->save($hand);
    }

    public function receiveCards(
        PlayerIdentifier $playerIdentifier,
        PileOfCards $cards,
        GameIdentifier $gameIdentifier
    ): void {
        $ormHand = $this->getOrmPersonalInfoByGame($playerIdentifier, $gameIdentifier);
        /** @var Hand $hand */
        $hand = $this->denormalizer->denormalize($ormHand->getNormalizedCards(), Hand::class);

        $this->updateHand($ormHand, $hand->mergedWith($cards));
        /** @var mixed[] $normalizedCards */
        $normalizedCards = $this->normalizer->normalize($hand->mergedWith($cards));
        $ormHand->replaceNormalizedCards($normalizedCards);
        $this->ormRepository->save($ormHand);
    }

    public function getPlayerHand(
        PlayerIdentifier $playerIdentifier,
        TableIdentifier $tableIdentifier,
        GameIdentifier $gameIdentifier
    ): Hand {
        $ormHand = $this->getOrmPersonalInfo($playerIdentifier, $tableIdentifier);

        if ($ormHand->getGameIdentifier() == $gameIdentifier) {
            /** @var Hand $hand */
            $hand = $this->denormalizer->denormalize($ormHand->getNormalizedCards(), Hand::class);

            return $hand;
        }

        // hands of past games are always empty.
        return Hand::empty();
    }

    public function replaceCards(
        PlayerIdentifier $playerIdentifier,
        PileOfCards $pileOfCards,
        GameIdentifier $gameIdentifier
    ): void {
        $ormHand = $this->getOrmPersonalInfoByGame($playerIdentifier, $gameIdentifier);
        /** @var mixed[] $normalizedCards */
        $normalizedCards = $this->normalizer->normalize($pileOfCards);
        $ormHand->replaceNormalizedCards($normalizedCards);
        $this->ormRepository->save($ormHand);
    }

    public function removeCard(
        PlayerIdentifier $playerIdentifier,
        Card $card,
        TableIdentifier $tableIdentifier
    ): void {
        $ormHand = $this->getOrmPersonalInfo($playerIdentifier, $tableIdentifier);
        /** @var Hand $hand */
        $hand = $this->denormalizer->denormalize($ormHand->getNormalizedCards(), Hand::class);

        /** @var mixed[] $normalizedCards */
        $normalizedCards = $this->normalizer->normalize($hand->without($card));
        $ormHand->replaceNormalizedCards($normalizedCards);
        $this->ormRepository->save($ormHand);
    }

    private function getOrmPersonalInfo(PlayerIdentifier $playerIdentifier, TableIdentifier $tableIdentifier): OrmPersonalPlayerInfo
    {
        $existing = $this->ormRepository->findOneBy([
            'tableId' => $tableIdentifier->toString(),
            'playerId' => $playerIdentifier->toString(),
        ]);

        if ($existing instanceof OrmPersonalPlayerInfo) {
            return $existing;
        }

        // FIXME: This exception message will be ambiguous.
        throw NotFoundInReadModel::create(OrmPersonalPlayerInfo::class, $playerIdentifier);
    }

    public function getPersonalPlayerInfo(
        PlayerIdentifier $playerIdentifier,
        TableIdentifier $tableIdentifier
    ): PersonalPlayerInfo {
        $ormPersonalInfo = $this->getOrmPersonalInfo($playerIdentifier, $tableIdentifier);

        return $this->mapDataToObject($ormPersonalInfo);
    }

    private function getOrmPersonalInfoByGame(PlayerIdentifier $playerIdentifier, GameIdentifier $gameIdentifier): OrmPersonalPlayerInfo
    {
        $existing = $this->ormRepository->findOneBy([
            'gameId' => $gameIdentifier->toString(),
            'playerId' => $playerIdentifier->toString(),
        ]);

        if ($existing instanceof OrmPersonalPlayerInfo) {
            return $existing;
        }

        // FIXME: This exception message will be ambiguous.
        throw NotFoundInReadModel::create(OrmPersonalPlayerInfo::class, $playerIdentifier);
    }

    public function savePersonalPlayerInfo(PersonalPlayerInfo $playerInfo): void
    {
        $ormPersonalPlayerInfo = $this->ormRepository->findOneBy([
            'playerId' => $playerInfo->getPlayerIdentifier()->toString(),
            'tableId' => $playerInfo->getTableIdentifier()->toString(),
        ]);

        if (null === $ormPersonalPlayerInfo) {
            $ormPersonalPlayerInfo = OrmPersonalPlayerInfo::createEmpty(
                $playerInfo->getPlayerIdentifier(),
                $playerInfo->getTableIdentifier()
            );
        }
        $this->updateHand($ormPersonalPlayerInfo, $playerInfo->getHand());

        $ormPersonalPlayerInfo->clearTrumpCard();
        $ormPersonalPlayerInfo->hideCards();

        if ($playerInfo->hasTrumpCard()) {
            $ormPersonalPlayerInfo->setTrumpCard($playerInfo->getTrumpCard());
        }

        if ($playerInfo->isPlaying()) {
            $ormPersonalPlayerInfo->setGameIdentifier($playerInfo->getGameIdentifier());
        }

        if ($playerInfo->hasCardsOpen()) {
            $ormPersonalPlayerInfo->showCards();
        }

        $this->ormRepository->save($ormPersonalPlayerInfo);
    }

    private function updateHand(OrmPersonalPlayerInfo $ormPersonalPlayerInfo, Hand $hand): void
    {
        /** @var mixed[] $normalizedCards */
        $normalizedCards = $this->normalizer->normalize($hand);
        $ormPersonalPlayerInfo->replaceNormalizedCards($normalizedCards);
    }

    public function forgetTable(TableIdentifier $tableIdentifier): void
    {
        /** @var OrmPersonalPlayerInfo[] $playerInfosForTable */
        $playerInfosForTable = $this->ormRepository->findBy([
            'tableId' => $tableIdentifier->toString(),
        ]);

        foreach ($playerInfosForTable as $playerInfo) {
            $this->ormRepository->delete($playerInfo);
        }
    }

    private function mapDataToObject(OrmPersonalPlayerInfo $ormPersonalInfo): PersonalPlayerInfo
    {
        /** @var Hand $hand */
        $hand = $this->denormalizer->denormalize($ormPersonalInfo->getNormalizedCards(), Hand::class);

        return new PersonalPlayerInfo(
            $hand,
            $ormPersonalInfo->getTrumpCardNumber() > 0
                ? Card::fromCardNumber($ormPersonalInfo->getTrumpCardNumber()) : null,
            $ormPersonalInfo->getGameIdentifier(),
            $ormPersonalInfo->getPlayerIdentifier(),
            $ormPersonalInfo->getTableIdentifier(),
            $ormPersonalInfo->hasCardsOpen()
        );
    }

    /**
     * @return \Traversable<PersonalPlayerInfo>
     */
    public function getPlayerInfosByTable(TableIdentifier $tableIdentifier): \Traversable
    {
        /** @var OrmPersonalPlayerInfo[] $resultData */
        $resultData = $this->ormRepository->findBy([
            'tableId' => $tableIdentifier->toString(),
        ]);

        foreach ($resultData as $row) {
            yield $this->mapDataToObject($row);
        }
    }
}
