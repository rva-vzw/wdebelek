<?php

declare(strict_types=1);

namespace App\Orm\ReadModel;

use App\Domain\ReadModel\Exception\NotFoundInReadModel;
use App\Domain\ReadModel\OccupiedSeats\OccupiedSeat;
use App\Domain\ReadModel\OccupiedSeats\OccupiedSeatRepository;
use App\Domain\ReadModel\OccupiedSeats\OccupiedSeatUpdater;
use App\Domain\ReadModel\OpenInvitations\OpenInvitations;
use App\Domain\ReadModel\PlayersAtTable\PlayersAtTable;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\Exception\PlayerNotFound;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\SimpleNamedPlayer;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Entity\OrmOccupiedSeat;
use App\Orm\Repository\OccupiedSeatOrmRepository;

final class OccupiedSeatReadModel implements OccupiedSeatUpdater, OccupiedSeatRepository, PlayersAtTable, OpenInvitations
{
    /** @var OccupiedSeatOrmRepository */
    private $repository;

    public function __construct(
        OccupiedSeatOrmRepository $repository,
    ) {
        $this->repository = $repository;
    }

    public function saveOccupiedSeat(
        OccupiedSeat $occupiedSeat
    ): void {
        $ormOccupiedSeat = $this->repository->findOneBy([
            'tableId' => $occupiedSeat->getTableIdentifier()->toString(),
            'seat' => $occupiedSeat->getSeat()->toInteger(),
        ]);

        if (!$ormOccupiedSeat instanceof OrmOccupiedSeat) {
            $ormOccupiedSeat = OrmOccupiedSeat::fromOccupiedSeat($occupiedSeat);
        }

        // FIXME: create a mapper or something...

        if ($occupiedSeat->hasAccepted()) {
            $ormOccupiedSeat->accept($occupiedSeat->getPlayerIdentifier(), $occupiedSeat->getPlayerName());
        }
        $ormOccupiedSeat->setInvitationPrivacy($occupiedSeat->getInvitationPrivacy());

        $this->repository->save(
            $ormOccupiedSeat
        );
    }

    /**
     * @return \Traversable<OccupiedSeat>
     */
    public function getOccupiedSeats(TableIdentifier $tableIdentifier): \Traversable
    {
        /** @var OrmOccupiedSeat[] $ormOccupiedSeat */
        $ormOccupiedSeat = $this->repository->findBy([
            'tableId' => $tableIdentifier->toString(),
        ]);

        foreach ($ormOccupiedSeat as $ormPlayer) {
            yield $ormPlayer->toOccupiedSeat();
        }
    }

    public function getSeatByOpenInvitation(
        TableIdentifier $tableIdentifier,
        Invitation $invitation
    ): OccupiedSeat {
        $ormOccupiedSeat = $this->repository->findOneBy([
            'tableId' => $tableIdentifier->toString(),
            'invitation' => $invitation->toString(),
            'playerId' => '',
        ]);

        if ($ormOccupiedSeat instanceof OrmOccupiedSeat) {
            return $ormOccupiedSeat->toOccupiedSeat();
        }

        throw NotFoundInReadModel::create(OccupiedSeat::class, $invitation);
    }

    public function deleteByTable(TableIdentifier $tableIdentifier): void
    {
        $ormSeats = $this->repository->findBy([
            'tableId' => $tableIdentifier->toString(),
        ]);

        foreach ($ormSeats as $seat) {
            $this->repository->delete($seat);
        }
    }

    /**
     * @return \Traversable<SimpleNamedPlayer>
     */
    public function getPlayersAtTable(TableIdentifier $tableIdentifier): \Traversable
    {
        /** @var OrmOccupiedSeat[] $ormOccupiedSeats */
        $ormOccupiedSeats = $this->repository->findBy(
            ['tableId' => $tableIdentifier->toString()],
            ['seat' => 'ASC']
        );

        foreach ($ormOccupiedSeats as $occupiedSeat) {
            if ($occupiedSeat->hasActivePlayer()) {
                yield $occupiedSeat->getPlayer();
            }
        }
    }

    public function getSeatByPlayer(TableIdentifier $tableIdentifier, PlayerIdentifier $playerIdentifier): OccupiedSeat
    {
        $ormOccupiedSeat = $this->repository->findOneBy([
            'tableId' => $tableIdentifier->toString(),
            'playerId' => $playerIdentifier->toString(),
        ]);

        if ($ormOccupiedSeat instanceof OrmOccupiedSeat) {
            return $ormOccupiedSeat->toOccupiedSeat();
        }

        throw PlayerNotFound::create($playerIdentifier);
    }

    public function delete(OccupiedSeat $occupiedSeat): void
    {
        $ormOccupiedSeat = $this->repository->findOneBy([
            'tableId' => $occupiedSeat->getTableIdentifier()->toString(),
            'seat' => $occupiedSeat->getSeat()->toInteger(),
        ]);

        if ($ormOccupiedSeat instanceof OrmOccupiedSeat) {
            $this->repository->delete($ormOccupiedSeat);
        }
    }

    public function hasOpenInvitation(TableIdentifier $tableIdentifier, Invitation $invitation): bool
    {
        $ormOccupiedSeat = $this->repository->findOneBy([
            'tableId' => $tableIdentifier->toString(),
            'invitation' => $invitation->toString(),
            'playerId' => '',
        ]);

        return $ormOccupiedSeat instanceof OrmOccupiedSeat;
    }
}
