<?php

declare(strict_types=1);

namespace App\Orm\ReadModel;

use App\Domain\ReadModel\CollectedTricks\CollectedTricks;
use App\Domain\ValueObject\Game\PlayedTrick;
use App\Domain\ValueObject\Game\PlayedTricks;
use App\Domain\ValueObject\Game\Trick;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\Team;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Entity\OrmCollectedTrick;
use App\Orm\Repository\CollectedTrickOrmRepository;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class CollectedTricksReadModel implements CollectedTricks
{
    public function __construct(
        private CollectedTrickOrmRepository $repository,
        private NormalizerInterface $normalizer,
        private DenormalizerInterface $denormalizer,
    ) {
    }

    public function collectTrick(TableIdentifier $tableIdentifier,
        GameIdentifier $gameIdentifier,
        PlayedTrick $playedTrick
    ): void {
        $this->repository->save(
            $this->trickToOrm($tableIdentifier, $gameIdentifier, $playedTrick)
        );
    }

    public function discloseAllTricks(GameIdentifier $gameIdentifier): void
    {
        $tricks = $this->repository->findBy([
            'gameId' => $gameIdentifier->toString(),
        ]);

        /** @var OrmCollectedTrick $trick */
        foreach ($tricks as $trick) {
            $trick->disclose();
            $this->repository->save($trick);
        }
    }

    public function getDisclosedTricks(GameIdentifier $gameIdentifier, Team $team): PlayedTricks
    {
        $tricks = $this->repository->findBy([
            'gameId' => $gameIdentifier->toString(),
            'disclosed' => true,
        ]);

        $allTricks = PlayedTricks::fromArray(
            array_map(
                fn (OrmCollectedTrick $ormCollectedTrick) => $this->ormToTrick($ormCollectedTrick),
                $tricks
            )
        );

        return $allTricks->wonByTeam($team);
    }

    public function discardTricks(TableIdentifier $tableIdentifier): void
    {
        $this->repository->deleteAllByTable($tableIdentifier);
    }

    private function trickToOrm(TableIdentifier $tableIdentifier,
        GameIdentifier $gameIdentifier,
        PlayedTrick $playedTrick
    ): OrmCollectedTrick {
        /** @var int[] $normalizedCards */
        $normalizedCards = $this->normalizer->normalize($playedTrick->getTrick());

        return new OrmCollectedTrick(
            $tableIdentifier->toString(),
            $gameIdentifier->toString(),
            $playedTrick->getTrickNumber(),
            $normalizedCards,
            $playedTrick->getWinner()->toString(), false
        );
    }

    private function ormToTrick(OrmCollectedTrick $ormCollectedTrick): PlayedTrick
    {
        return new PlayedTrick(
            $this->denormalizer->denormalize($ormCollectedTrick->getNormalizedCards(), Trick::class),
            $ormCollectedTrick->getTrickNumber(),
            PlayerIdentifier::fromString($ormCollectedTrick->getWinner())
        );
    }

    public function uncollectTrick(GameIdentifier $gameIdentifier, int $trickNumber): void
    {
        $this->repository->deleteOneByGame($gameIdentifier, $trickNumber);
    }
}
