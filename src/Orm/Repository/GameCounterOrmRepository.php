<?php

declare(strict_types=1);

namespace App\Orm\Repository;

use App\Domain\ReadModel\GameCounter\ActiveTablesCounter;
use App\Domain\ReadModel\GameCounter\GameCounter;
use App\Domain\ReadModel\GameCounter\GameCounterRepository;
use App\Domain\ReadModel\GameCounter\Month;
use App\Domain\ReadModel\GameCounter\MonthlyTotal;
use App\Domain\ReadModel\GameCounter\MonthlyTotals;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Entity\OrmGameCounter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OrmGameCounter>
 */
final class GameCounterOrmRepository extends ServiceEntityRepository implements GameCounterRepository, ActiveTablesCounter, MonthlyTotals
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrmGameCounter::class);
    }

    public function save(OrmGameCounter $ormGameCounter): void
    {
        $this->_em->persist($ormGameCounter);
        $this->_em->flush();
    }

    public function findCounterForTable(TableIdentifier $tableIdentifier): ?GameCounter
    {
        return $this->find($tableIdentifier->toString())?->toGameCounter();
    }

    public function saveGameCounter(GameCounter $counter): void
    {
        $ormCounter = $this->find($counter->getTableIdentifier()->toString());
        if (null === $ormCounter) {
            $ormCounter = OrmGameCounter::createNew(
                $counter->getTableIdentifier(),
                $counter->getFirstGameStartedAt()
            );
        }
        $this->mapToOrm($counter, $ormCounter);
        $this->save($ormCounter);
    }

    private function mapToOrm(GameCounter $counter, OrmGameCounter $ormCounter): void
    {
        $ormCounter->setGameCount($counter->getGameCount());
        $ormCounter->setLatestGameStartedAt($counter->getLatestGameStartedAt());
    }

    public function removeForTable(TableIdentifier $tableIdentifier): void
    {
        $ormGameCounter = $this->find($tableIdentifier->toString());

        if ($ormGameCounter instanceof OrmGameCounter) {
            $this->_em->remove($ormGameCounter);
            $this->_em->flush();
        }
    }

    public function getActiveTablesCount(
        \DateTimeImmutable $startedBefore,
        \DateTimeImmutable $stillBusyAt,
        int $minimumGameCount
    ): int {
        $query = $this->_em->createQueryBuilder()
            ->select('COUNT(q.tableId)')
            ->from('ReadModel:OrmGameCounter', 'q')
            ->where('q.latestGameStartedAt >= :stillBusyAt')
            ->andWhere('q.firstGameStartedAt <= :startedBefore')
            ->andWhere('q.gameCount >= :minimumGameCount')
            ->setParameter('stillBusyAt', $stillBusyAt)
            ->setParameter('startedBefore', $startedBefore)
            ->setParameter('minimumGameCount', $minimumGameCount)
            ->getQuery();

        $result = $query->getSingleScalarResult();

        return (int) $result;
    }

    /**
     * @return \Traversable<MonthlyTotal>
     */
    public function getByMonth(Month $from, Month $to, int $minimumGameCount): \Traversable
    {
        // We could also create a dedicated read model that just keeps track of one record each month,
        // and increases the counters when a new game starts/the first game on a table starts,
        // reducing the impact on the database.

        $startDate = $from->getStartDate();
        $endDate = $to->next()->getStartDate();

        $query = $this->_em->createQueryBuilder()
            ->select('q.year, q.month, COUNT(q.tableId) AS totalTables, SUM(q.gameCount) AS totalGames')
            ->from('ReadModel:OrmGameCounter', 'q')
            ->where('q.firstGameStartedAt >= :startDate')
            ->andWhere('q.firstGameStartedAt < :endDate')
            ->andWhere('q.gameCount >= :minimumGameCount')
            ->groupBy('q.year, q.month')
            ->orderBy('q.year, q.month')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('minimumGameCount', $minimumGameCount)
            ->getQuery();

        foreach ($query->iterate() as $row) {
            // It's always a little strange, the way a query builder from the
            // entity manager returns its results.
            $data = array_values($row)[0];
            yield new MonthlyTotal(
                Month::fromYearAndDay((int) $data['year'], (int) $data['month']),
                (int) $data['totalTables'],
                (int) $data['totalGames'],
            );
        }
    }
}
