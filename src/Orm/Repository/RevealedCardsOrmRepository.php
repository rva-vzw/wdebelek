<?php

declare(strict_types=1);

namespace App\Orm\Repository;

use App\Orm\Entity\OrmRevealedCards;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OrmRevealedCards>
 */
final class RevealedCardsOrmRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrmRevealedCards::class);
    }

    public function save(OrmRevealedCards $ormShownHand): void
    {
        $this->_em->persist($ormShownHand);
        $this->_em->flush();
    }

    public function deleteRevealedCards(OrmRevealedCards $ormShownHand): void
    {
        $this->_em->remove($ormShownHand);
        $this->_em->flush();
    }
}
