<?php

declare(strict_types=1);

namespace App\Orm\Repository;

use App\Domain\ReadModel\Exception\NotFoundInReadModel;
use App\Domain\ReadModel\PlayingPlayer\PlayerCounts;
use App\Domain\ReadModel\PlayingPlayer\PlayingPlayer;
use App\Domain\ReadModel\PlayingPlayer\PlayingPlayerRepository;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Entity\OrmPlayingPlayer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OrmPlayingPlayer>
 */
final class PlayingPlayerOrmRepository extends ServiceEntityRepository implements PlayingPlayerRepository, PlayerCounts
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrmPlayingPlayer::class);
    }

    public function save(OrmPlayingPlayer $ormFellowPlayer): void
    {
        $this->_em->persist($ormFellowPlayer);
        $this->_em->flush();
    }

    public function delete(OrmPlayingPlayer $ormFellowPlayer): void
    {
        $this->_em->remove($ormFellowPlayer);
        $this->_em->flush();
    }

    public function getPlayingPlayer(
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $playerIdentifier
    ): PlayingPlayer {
        $result = $this->findOneBy(
            [
                'tableId' => $tableIdentifier->toString(),
                'playerId' => $playerIdentifier->toString(),
            ]
        );

        if ($result instanceof OrmPlayingPlayer) {
            return $result->asPlayingPlayer();
        }

        throw NotFoundInReadModel::create(PlayingPlayer::class, $playerIdentifier);
    }

    public function savePlayingPlayer(PlayingPlayer $fellowPlayer): void
    {
        $ormFellowPlayer = $this->findOneBy([
            'tableId' => $fellowPlayer->getTableIdentifier()->toString(),
            'playerId' => $fellowPlayer->getPlayerIdentifier()->toString(),
        ]);

        if ($ormFellowPlayer instanceof OrmPlayingPlayer) {
            $ormFellowPlayer->updateFrom($fellowPlayer);
        } else {
            $ormFellowPlayer = OrmPlayingPlayer::fromPlayingPlayer($fellowPlayer);
        }

        $this->save($ormFellowPlayer);
    }

    public function deleteForTable(TableIdentifier $tableIdentifier): void
    {
        /** @var OrmPlayingPlayer[] $fellowPlayers */
        $fellowPlayers = $this->findBy([
            'tableId' => $tableIdentifier->toString(),
        ]);

        foreach ($fellowPlayers as $fellowPlayer) {
            $this->delete($fellowPlayer);
        }
    }

    /**
     * @return \Traversable<PlayingPlayer>
     */
    public function getOrderedPlayingPlayers(TableIdentifier $tableIdentifier): \Traversable
    {
        /** @var OrmPlayingPlayer[] $ormPlayingPlayers */
        $ormPlayingPlayers = $this->findBy(
            ['tableId' => $tableIdentifier->toString()],
            ['seat' => 'ASC']
        );

        foreach ($ormPlayingPlayers as $ormPlayingPlayer) {
            yield $ormPlayingPlayer->asPlayingPlayer();
        }
    }

    public function getPlayerCount(TableIdentifier $tableIdentifier): int
    {
        /** @var OrmPlayingPlayer[] $ormPlayingPlayers */
        $ormPlayingPlayers = $this->findBy(
            ['tableId' => $tableIdentifier->toString()],
        );

        return count($ormPlayingPlayers);
    }
}
