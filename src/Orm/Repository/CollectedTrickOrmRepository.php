<?php

declare(strict_types=1);

namespace App\Orm\Repository;

use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Entity\OrmCollectedTrick;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OrmCollectedTrick>
 */
final class CollectedTrickOrmRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrmCollectedTrick::class);
    }

    public function save(OrmCollectedTrick $trick): void
    {
        $this->_em->persist($trick);
        $this->_em->flush();
    }

    public function deleteAllByTable(TableIdentifier $tableIdentifier): void
    {
        $tricks = $this->findBy([
            'tableId' => $tableIdentifier->toString(),
        ]);

        foreach ($tricks as $trick) {
            $this->_em->remove($trick);
        }

        $this->_em->flush();
    }

    public function deleteOneByGame(GameIdentifier $gameIdentifier, int $trickNumber): void
    {
        $trick = $this->findOneBy([
            'gameId' => $gameIdentifier->toString(),
            'trickNumber' => $trickNumber,
        ]);
        if ($trick instanceof OrmCollectedTrick) {
            $this->_em->remove($trick);
            $this->_em->flush();
        }
    }
}
