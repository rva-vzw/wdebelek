<?php

declare(strict_types=1);

namespace App\Orm\Repository;

use App\Orm\Entity\OrmPersonalPlayerInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OrmPersonalPlayerInfo>
 */
final class PersonalPlayerInfoOrmRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrmPersonalPlayerInfo::class);
    }

    public function save(OrmPersonalPlayerInfo $ormPersonalPlayerInfo): void
    {
        $this->_em->persist($ormPersonalPlayerInfo);
        $this->_em->flush();
    }

    public function delete(OrmPersonalPlayerInfo $ormPersonalPlayerInfo): void
    {
        $this->_em->remove($ormPersonalPlayerInfo);
        $this->_em->flush();
    }
}
