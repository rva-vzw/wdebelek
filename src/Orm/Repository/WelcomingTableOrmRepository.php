<?php

declare(strict_types=1);

namespace App\Orm\Repository;

use App\Domain\ReadModel\WelcomingTable\RecentWelcomingTables;
use App\Domain\ReadModel\WelcomingTable\WelcomingTable;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Entity\OrmWelcomingTable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OrmWelcomingTable>
 */
final class WelcomingTableOrmRepository extends ServiceEntityRepository implements RecentWelcomingTables
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrmWelcomingTable::class);
    }

    public function save(OrmWelcomingTable $fromWelcomingTable): void
    {
        $this->_em->persist($fromWelcomingTable);
        $this->_em->flush();
    }

    public function remove(TableIdentifier $tableIdentifier): void
    {
        $object = $this->find($tableIdentifier->toString());

        if ($object instanceof OrmWelcomingTable) {
            $this->_em->remove($object);
            $this->_em->flush();
        }
    }

    /**
     * @return \Traversable<WelcomingTable>
     */
    public function getRecentWelcomingTables(\DateTimeImmutable $startingDate): \Traversable
    {
        $criteria = new Criteria();
        $criteria->where(
            new Comparison('latestInvitationPublishedAt', Comparison::GTE, $startingDate)
        )->orderBy(['latestInvitationPublishedAt' => Criteria::ASC]);

        $results = $this->matching($criteria);

        foreach ($results as $result) {
            yield $result->toWelcomingTable();
        }
    }
}
