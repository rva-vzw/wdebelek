<?php

declare(strict_types=1);

namespace App\Orm\Repository;

use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Entity\OrmTableState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OrmTableState>
 */
final class TableStateOrmRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrmTableState::class);
    }

    public function save(OrmTableState $ormTableState): void
    {
        $this->_em->persist($ormTableState);
        $this->_em->flush();
    }

    public function deleteByTableIdentifier(TableIdentifier $tableIdentifier): void
    {
        $toBeDeleted = $this->find($tableIdentifier->toString());

        if ($toBeDeleted instanceof OrmTableState) {
            $this->_em->remove($toBeDeleted);
            $this->_em->flush();
        }
    }
}
