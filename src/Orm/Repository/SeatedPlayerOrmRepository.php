<?php

declare(strict_types=1);

namespace App\Orm\Repository;

use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\ReadModel\PlayerProfile\PlayerProfiles;
use App\Domain\ReadModel\SeatedPlayer\SeatedPlayer;
use App\Domain\ReadModel\SeatedPlayer\SeatedPlayerRepository;
use App\Domain\ValueObject\Player\Exception\PlayerNotFound;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Entity\OrmSeatedPlayer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OrmSeatedPlayer>
 */
final class SeatedPlayerOrmRepository extends ServiceEntityRepository implements PlayerProfiles, SeatedPlayerRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrmSeatedPlayer::class);
    }

    public function save(OrmSeatedPlayer $ormPlayerProfile): void
    {
        $this->_em->persist($ormPlayerProfile);
        $this->_em->flush();
    }

    public function getSeatedPlayer(
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $playerIdentifier
    ): SeatedPlayer {
        $profile = $this->findOneBy([
            'tableId' => $tableIdentifier->toString(),
            'playerId' => $playerIdentifier->toString(),
        ]);

        if ($profile instanceof SeatedPlayer) {
            return $profile;
        }

        throw PlayerNotFound::create($playerIdentifier);
    }

    public function putPlayerAtTable(
        PlayerProfile $playerProfile,
        TableIdentifier $tableIdentifier,
        Seat $seat
    ): void {
        $this->save(
            OrmSeatedPlayer::fromSeatedPlayer($playerProfile, $tableIdentifier, $seat)
        );
    }

    public function getPlayerProfile(
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $playerIdentifier
    ): PlayerProfile {
        return $this->getSeatedPlayer($tableIdentifier, $playerIdentifier);
    }
}
