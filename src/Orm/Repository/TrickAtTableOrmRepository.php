<?php

declare(strict_types=1);

namespace App\Orm\Repository;

use App\Domain\ReadModel\TrickAtTable\TrickAtTableRepository;
use App\Domain\ValueObject\Game\Trick;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Orm\Entity\OrmTrickAtTable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @extends ServiceEntityRepository<OrmTrickAtTable>
 */
final class TrickAtTableOrmRepository extends ServiceEntityRepository implements TrickAtTableRepository
{
    private NormalizerInterface $normalizer;
    private DenormalizerInterface $denormalizer;

    public function __construct(
        ManagerRegistry $registry,
        NormalizerInterface $normalizer,
        DenormalizerInterface $denormalizer
    ) {
        parent::__construct($registry, OrmTrickAtTable::class);
        $this->normalizer = $normalizer;
        $this->denormalizer = $denormalizer;
    }

    public function getTrickAtTable(TableIdentifier $tableIdentifier): Trick
    {
        $ormTrickAtTable = $this->find($tableIdentifier->toString());
        if ($ormTrickAtTable instanceof OrmTrickAtTable) {
            /** @var Trick $trick */
            $trick = $this->denormalizer->denormalize(
                $ormTrickAtTable->getNormalizedTrick(),
                Trick::class
            );

            return $trick;
        }

        return Trick::empty();
    }

    public function replaceTrickAtTable(TableIdentifier $tableIdentifier, Trick $trick): void
    {
        $ormTrickAtTable = $this->find($tableIdentifier->toString());

        if (null === $ormTrickAtTable) {
            $ormTrickAtTable = new OrmTrickAtTable($tableIdentifier, []);
        }

        /** @var int[] $normalizedTrick */
        $normalizedTrick = $this->normalizer->normalize($trick);
        $ormTrickAtTable->setNormalizedTrick(
            $normalizedTrick
        );

        $this->_em->persist($ormTrickAtTable);
        $this->_em->flush();
    }
}
