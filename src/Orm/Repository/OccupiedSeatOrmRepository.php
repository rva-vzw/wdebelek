<?php

declare(strict_types=1);

namespace App\Orm\Repository;

use App\Orm\Entity\OrmOccupiedSeat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OrmOccupiedSeat>
 */
final class OccupiedSeatOrmRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrmOccupiedSeat::class);
    }

    public function save(OrmOccupiedSeat $seat): void
    {
        $this->_em->persist($seat);
        $this->_em->flush();
    }

    public function delete(OrmOccupiedSeat $seat): void
    {
        $this->_em->remove($seat);
        $this->_em->flush();
    }
}
