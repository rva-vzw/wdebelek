<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Card;

/**
 * @extends \IteratorAggregate<Card>
 */
interface PileOfCards extends \IteratorAggregate
{
}
