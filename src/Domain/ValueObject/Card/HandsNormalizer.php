<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Card;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

final class HandsNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /** @var PileOfCardsNormalizer */
    private $pileOfCardsNormalizer;

    public function __construct(PileOfCardsNormalizer $pileOfCardsNormalizer)
    {
        $this->pileOfCardsNormalizer = $pileOfCardsNormalizer;
    }

    /**
     * @param mixed[] $context
     *
     * @return Hands
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = [])
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException('Array expected');
        }
        $hands = Hands::empty();

        foreach ($data as $playerId => $normalizedHand) {
            Assert::stringNotEmpty($playerId);
            /** @var Hand $hand */
            $hand = $this->pileOfCardsNormalizer->denormalize($normalizedHand, Hand::class);

            $hands = $hands->withHand(
                PlayerIdentifier::fromString($playerId),
                $hand
            );
        }

        return $hands;
    }

    public function supportsDenormalization($data, string $type, ?string $format = null)
    {
        return Hands::class === $type;
    }

    /**
     * @param mixed[] $context
     *
     * @return int[][]
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        if (!$object instanceof Hands) {
            throw new \InvalidArgumentException('Hands expected');
        }
        $result = [];
        foreach ($object->getPlayers() as $playerIdentifier) {
            /** @var int[] $normalizedHand */
            $normalizedHand = $this->pileOfCardsNormalizer->normalize(
                $object->getHand($playerIdentifier)
            );
            $result[$playerIdentifier->toString()] = $normalizedHand;
        }

        return $result;
    }

    public function supportsNormalization($data, ?string $format = null)
    {
        return $data instanceof Hands;
    }
}
