<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Card;

final class SimplePileOfCards extends AbstractPileOfCards
{
    public static function complete(): self
    {
        return new self(self::allCards());
    }

    /**
     * @return \Generator<Card>
     */
    private static function allCards(): \Generator
    {
        foreach (Card::getSuits() as $suit) {
            for ($rank = 1; $rank <= 13; ++$rank) {
                yield new Card($rank, $suit);
            }
        }
    }

    public static function singleCard(Card $card): self
    {
        return new self(self::generateSingleCard($card));
    }

    /**
     * @return \Iterator<Card>
     */
    private static function generateSingleCard(Card $card): \Iterator
    {
        yield $card;
    }

    public static function thirtyTwo(): self
    {
        return new self(self::thirtyTwoCards());
    }

    /**
     * @return \Generator<Card>
     */
    private static function thirtyTwoCards(): \Generator
    {
        foreach (Card::getSuits() as $suit) {
            yield new Card(1, $suit);
            for ($rank = 7; $rank <= 13; ++$rank) {
                yield new Card($rank, $suit);
            }
        }
    }

    public function count(): int
    {
        return count($this->orderedCards);
    }

    public function cutAfter(int $numberOfCards): self
    {
        $piles = $this->splitAfter($numberOfCards);

        return self::fromCards($piles->backwards()->merged());
    }

    public function splitAfter(int $numberOfCards): SimplePiles
    {
        $firstPart = new self(new \ArrayIterator(array_slice($this->orderedCards, 0, $numberOfCards)));
        $lastPart = new self(new \ArrayIterator(array_slice($this->orderedCards, $numberOfCards)));

        return new SimplePiles($firstPart, $lastPart);
    }

    public function getLast(): Card
    {
        return array_values(array_slice($this->orderedCards, -1))[0];
    }

    public function getFirst(): Card
    {
        return array_values($this->orderedCards)[0];
    }

    public function withCardsRemoved(PileOfCards $cards): self
    {
        // FIXME: This is a rather messy implementation.

        $result = clone $this;

        $otherPile = self::fromCards($cards);

        $result->orderedCards = array_values(array_diff(
            $this->orderedCards,
            $otherPile->orderedCards
        ));

        $result->cardsByNumber = array_diff(
            $this->cardsByNumber,
            $otherPile->cardsByNumber
        );

        return $result;
    }

    public function containsSequence(SimplePileOfCards $expectedSequence): bool
    {
        $firstCard = $expectedSequence->getFirst();
        $cardNumber = array_search($firstCard, $this->orderedCards);

        if (!is_int($cardNumber)) {
            return false;
        }

        // ! card number is zero based.
        $cutCards = $this->cutAfter($cardNumber);
        $firstCards = $cutCards->getFirstCards($expectedSequence->count());

        // Only compare ordered cards, because the items in cardsByNumber
        // can have a different order.
        return $firstCards->orderedCards == $expectedSequence->orderedCards;
    }

    public function getFirstCards(int $count): SimplePileOfCards
    {
        return self::fromCards($this->splitAfter($count)->first());
    }
}
