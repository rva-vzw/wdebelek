<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Card;

use App\Domain\ValueObject\Card\Exception\CardNotFound;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;

final class Hands implements PilesOfCards
{
    /** @var array<non-empty-string, Hand> */
    private $handsArray;

    /**
     * @param Hand[] $piles
     */
    private function __construct(array $piles)
    {
        $this->handsArray = $piles;
    }

    public static function empty(): self
    {
        return new self([]);
    }

    public function withHand(PlayerIdentifier $playerIdentifier, Hand $hand): self
    {
        $result = clone $this;

        $playerId = $playerIdentifier->toString();
        $result->handsArray[$playerId] = $hand;

        return $result;
    }

    public function withAdditionalCardsForPlayer(PlayerIdentifier $playerIdentifier, PileOfCards $cards): self
    {
        return $this->withHand(
            $playerIdentifier,
            $this->getHand($playerIdentifier)->mergedWith($cards)
        );
    }

    public function getHand(PlayerIdentifier $playerIdentifier): Hand
    {
        $playerId = $playerIdentifier->toString();
        if (array_key_exists($playerId, $this->handsArray)) {
            return $this->handsArray[$playerId];
        }

        return Hand::empty();
    }

    public function withoutPlayerCard(PlayerIdentifier $playerIdentifier, Card $card): self
    {
        $result = clone $this;

        $result = $result->withHand(
            $playerIdentifier,
            $this->getHand($playerIdentifier)->without($card)
        );

        return $result;
    }

    public function withAdditionalCardForPlayer(PlayerIdentifier $playerIdentifier, Card $card): self
    {
        $result = clone $this;

        $result = $result->withHand(
            $playerIdentifier,
            $this->getHand($playerIdentifier)->with($card)
        );

        return $result;
    }

    /**
     * @return \Traversable<PileOfCards>
     */
    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->handsArray);
    }

    public function getPlayers(): SimplePlayerIdentifiers
    {
        return new SimplePlayerIdentifiers(
            ...array_map(
                function (string $playerId): PlayerIdentifier {
                    return PlayerIdentifier::fromString($playerId);
                },
                array_keys($this->handsArray)
            )
        );
    }

    public function hasHandFor(PlayerIdentifier $playerIdentifier): bool
    {
        return array_key_exists($playerIdentifier->toString(), $this->handsArray);
    }

    public function getCardHolder(Card $card): PlayerIdentifier
    {
        $key = array_key_first(
            array_filter(
                $this->handsArray,
                fn (Hand $hand): bool => $hand->has($card)
            )
        );

        if (is_string($key) && !empty($key)) {
            return PlayerIdentifier::fromString($key);
        }

        throw CardNotFound::create($card);
    }

    public function allEmpty(): bool
    {
        foreach ($this->handsArray as $hand) {
            if (!$hand->isEmpty()) {
                return false;
            }
        }

        return true;
    }
}
