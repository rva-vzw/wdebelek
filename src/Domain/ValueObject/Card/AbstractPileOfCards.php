<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Card;

use App\Domain\ValueObject\Card\Exception\DuplicateCard;

abstract class AbstractPileOfCards implements PileOfCards
{
    /** @var Card[] */
    protected $orderedCards;

    /** @var Card[] */
    protected $cardsByNumber;

    /**
     * @param \Traversable<Card> $orderedCards
     */
    final protected function __construct(\Traversable $orderedCards)
    {
        $this->orderedCards = [];
        $this->cardsByNumber = [];

        foreach ($orderedCards as $card) {
            $cardNumber = $card->getCardNumber();
            if (array_key_exists($cardNumber, $this->cardsByNumber)) {
                throw DuplicateCard::fromCard($card);
            }

            $this->cardsByNumber[$cardNumber] = $card;
            $this->orderedCards[] = $card;
        }
    }

    /**
     * @param \Traversable<Card> $cards
     *
     * @return static
     */
    public static function fromCards(\Traversable $cards): self
    {
        return new static($cards);
    }

    /**
     * @return \Traversable<Card>
     */
    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->orderedCards);
    }

    /**
     * @return static
     */
    public static function empty(): self
    {
        return new static(self::noCards());
    }

    /**
     * @return \Traversable<Card>
     */
    private static function noCards(): \Traversable
    {
        return new \ArrayIterator([]);
    }

    /**
     * @return static
     */
    public function mergedWith(PileOfCards $other): self
    {
        return new static($this->generateCardsMergedWith($other));
    }

    /**
     * @return \Generator<Card>
     */
    private function generateCardsMergedWith(PileOfCards $other): \Generator
    {
        foreach ($this->orderedCards as $card) {
            yield $card;
        }

        foreach ($other as $card) {
            yield $card;
        }
    }

    public function hasSameCardsAs(PileOfCards $cards): bool
    {
        // FIXME: This is not very efficient.
        return $this->cardsByNumber == SimplePileOfCards::fromCards($cards)->cardsByNumber;
    }

    public function has(Card $card): bool
    {
        return array_key_exists($card->getCardNumber(), $this->cardsByNumber);
    }

    /**
     * @return static
     */
    public function without(Card $card): self
    {
        if (!$this->has($card)) {
            return $this;
        }

        $result = clone $this;

        unset($result->cardsByNumber[$card->getCardNumber()]);

        /** @var int $key */
        $key = array_search($card, $result->orderedCards);

        array_splice(
            $result->orderedCards,
            $key,
            1
        );

        return $result;
    }

    /**
     * @return static
     */
    public function with(Card $card): self
    {
        if ($this->has($card)) {
            return $this;
        }

        $result = clone $this;

        $result->cardsByNumber[$card->getCardNumber()] = $card;
        $result->orderedCards[] = $card;

        return $result;
    }

    public function isEmpty(): bool
    {
        return empty($this->orderedCards);
    }
}
