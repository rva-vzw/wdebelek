<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Card;

final class SimplePiles implements PilesOfCards
{
    /** @var PileOfCards[] */
    private $piles;

    public function __construct(PileOfCards ...$piles)
    {
        $this->piles = $piles;
    }

    public function count(): int
    {
        return count($this->piles);
    }

    /**
     * @param int $index zero-based index
     */
    public function getByIndex(int $index): PileOfCards
    {
        return $this->piles[$index];
    }

    public function merged(): PileOfCards
    {
        $result = SimplePileOfCards::empty();

        foreach ($this->piles as $pile) {
            $result = $result->mergedWith($pile);
        }

        return $result;
    }

    public function first(): PileOfCards
    {
        return $this->piles[0];
    }

    public function second(): PileOfCards
    {
        return $this->piles[1];
    }

    public function backwards(): self
    {
        $result = clone $this;
        $result->piles = array_reverse($result->piles);

        return $result;
    }

    public function withAdditionalPiles(PilesOfCards $other): self
    {
        $result = clone $this;

        foreach ($other as $pile) {
            $result->piles[] = $pile;
        }

        return $result;
    }

    /**
     * @return \Traversable<PileOfCards>
     */
    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->piles);
    }

    public function withAdditionalPile(PileOfCards $pile): self
    {
        $result = clone $this;
        $result->piles[] = $pile;

        return $result;
    }

    public function shuffled(): self
    {
        $result = clone $this;
        shuffle($result->piles);

        return $result;
    }
}
