<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Card\Exception;

use App\Domain\ValueObject\Card\Card;

final class DuplicateCard extends \Exception
{
    public static function fromCard(Card $card): self
    {
        $result = new self(
            sprintf(
                'Duplicate card: %d of %s.',
                $card->getRank(),
                $card->getSuit()
            )
        );

        return $result;
    }
}
