<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Card\Exception;

final class InvalidCard extends \Exception
{
}
