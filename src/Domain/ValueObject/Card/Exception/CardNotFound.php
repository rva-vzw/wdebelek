<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Card\Exception;

use App\Domain\ValueObject\Card\Card;

final class CardNotFound extends \Exception
{
    public static function create(Card $card): self
    {
        return new self("Card {$card} not found.");
    }
}
