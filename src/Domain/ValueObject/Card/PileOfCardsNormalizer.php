<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Card;

use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * This normalizer only normalizes subclasses of {@see AbstractPileOfCards}.
 */
final class PileOfCardsNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed[] $context
     *
     * @return PileOfCards
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = [])
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException('Array expected');
        }
        $reflectionMethod = new \ReflectionMethod($type, 'fromCards');

        return $reflectionMethod->invoke(null, $this->generateCards($data));
    }

    public function supportsDenormalization($data, string $type, ?string $format = null)
    {
        $parents = class_parents($type);

        return is_array($parents) && in_array(AbstractPileOfCards::class, $parents);
    }

    /**
     * @param mixed[] $context
     *
     * @return int[]
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        if (!$object instanceof AbstractPileOfCards) {
            throw new \InvalidArgumentException('AbstractPileOfCards expected');
        }
        $result = [];

        /** @var Card $card */
        foreach ($object as $card) {
            $result[] = $card->getCardNumber();
        }

        return $result;
    }

    public function supportsNormalization($data, ?string $format = null)
    {
        return $data instanceof AbstractPileOfCards;
    }

    /**
     * @param int[] $cardNumbers
     *
     * @return \Generator<Card>
     */
    private function generateCards(array $cardNumbers): \Generator
    {
        foreach ($cardNumbers as $cardNumber) {
            yield Card::fromCardNumber($cardNumber);
        }
    }
}
