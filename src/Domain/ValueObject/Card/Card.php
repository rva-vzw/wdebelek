<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Card;

use App\Domain\ValueObject\Card\Exception\InvalidCard;

final class Card
{
    public const CLUBS = 'clubs';
    public const DIAMONDS = 'diamonds';
    public const HEARTS = 'hearts';
    public const SPADES = 'spades';

    public const JACK = 11;
    public const QUEEN = 12;
    public const KING = 13;
    public const ACE = 1;

    /** @var int */
    private $rank;
    /** @var string */
    private $suit;

    public function __construct(int $rank, string $suit)
    {
        $this->guardRank($rank);
        $this->guardSuit($suit);

        $this->rank = $rank;
        $this->suit = $suit;
    }

    private function guardRank(int $rank): void
    {
        if ($rank >= 1 && $rank <= 13) {
            return;
        }

        throw new InvalidCard();
    }

    private function guardSuit(string $suit): void
    {
        switch ($suit) {
            case self::CLUBS:
            case self::DIAMONDS:
            case self::HEARTS:
            case self::SPADES:
                return;
            default:
                throw new InvalidCard();
        }
    }

    public static function fromCardNumber(int $cardNumber): self
    {
        $suitNumber = intdiv($cardNumber - 1, 13);
        $rank = (($cardNumber - 1) % 13) + 1;

        return new self($rank, self::getSuits()[$suitNumber]);
    }

    public function getCardNumber(): int
    {
        /** @var int $suitNumber */
        $suitNumber = array_search($this->suit, self::getSuits());

        return $suitNumber * 13 + $this->rank;
    }

    /**
     * @return string[]
     */
    public static function getSuits(): array
    {
        return [self::SPADES, self::HEARTS, self::DIAMONDS, self::CLUBS];
    }

    public function getRank(): int
    {
        return $this->rank;
    }

    public function getSuit(): string
    {
        return $this->suit;
    }

    public function __toString(): string
    {
        // this makes array_diff work 😉
        return "{$this->getRank()} of {$this->getSuit()}";
    }
}
