<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Card;

use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class CardNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed[] $context
     *
     * @return Card
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = [])
    {
        if (!is_int($data)) {
            throw new \InvalidArgumentException('Integer expected');
        }

        return Card::fromCardNumber($data);
    }

    public function supportsDenormalization($data, string $type, ?string $format = null)
    {
        return Card::class === $type;
    }

    /**
     * @param mixed[] $context
     *
     * @return int
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        if (!$object instanceof Card) {
            throw new \InvalidArgumentException('Card expected');
        }

        return $object->getCardNumber();
    }

    public function supportsNormalization($data, ?string $format = null)
    {
        return $data instanceof Card;
    }
}
