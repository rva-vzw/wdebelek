<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Card;

/**
 * @extends \IteratorAggregate<PileOfCards>
 */
interface PilesOfCards extends \IteratorAggregate
{
}
