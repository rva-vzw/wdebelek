<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\StringObject;

/**
 * Implement this interface for value objects that normalize to and denormalize from a string.
 */
interface StringObject
{
    public static function fromString(string $value): self;

    public function toString(): string;
}
