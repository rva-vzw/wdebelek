<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\StringObject\Exception;

final class StringTooLong extends \Exception
{
    public static function create(int $maxLength, int $actualLength): self
    {
        return new self("This string should not be longer than {$maxLength} characters; got {$actualLength}.");
    }
}
