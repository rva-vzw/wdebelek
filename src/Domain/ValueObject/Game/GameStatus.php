<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Game;

final class GameStatus
{
    /** @var string */
    private $code;

    private function __construct(string $code)
    {
        $this->code = $code;
    }

    public static function NEW(): self
    {
        return new self('new');
    }

    public static function CUT(): self
    {
        return new self('cut');
    }

    public static function PLAYING(): self
    {
        return new self('playing');
    }

    public static function DONE(): self
    {
        return new self('done');
    }

    public function __toString(): string
    {
        return $this->code;
    }
}
