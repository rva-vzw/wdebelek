<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Game;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\PileOfCards;
use App\Domain\ValueObject\Player\PlayerIdentifier;

final class PlayedTrick implements PileOfCards
{
    /** @var Trick */
    private $trick;
    /** @var int */
    private $trickNumber;
    /** @var PlayerIdentifier */
    private $winner;

    public function __construct(
        Trick $trick,
        int $trickNumber,
        PlayerIdentifier $winner
    ) {
        $this->trick = $trick;
        $this->trickNumber = $trickNumber;
        $this->winner = $winner;
    }

    /**
     * @return \Traversable<Card>
     */
    public function getIterator(): \Traversable
    {
        return $this->trick;
    }

    public function getTrick(): Trick
    {
        return $this->trick;
    }

    public function getTrickNumber(): int
    {
        return $this->trickNumber;
    }

    public function getWinner(): PlayerIdentifier
    {
        return $this->winner;
    }
}
