<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Game;

use App\Domain\ValueObject\Game\Exception\InvalidGameNumber;
use RvaVzw\KrakBoem\Id\IntWrapper;

final class GameNumber implements IntWrapper
{
    /** @var int */
    private $number;

    private function __construct(int $number)
    {
        if ($number <= 0) {
            throw InvalidGameNumber::create($number);
        }
        $this->number = $number;
    }

    /**
     * @return self
     */
    public static function fromInteger(int $value): IntWrapper
    {
        return new self($value);
    }

    public static function second(): self
    {
        return new self(2);
    }

    public function toInteger(): int
    {
        return $this->number;
    }

    public static function first(): self
    {
        return new self(1);
    }

    public function next(): self
    {
        return new self($this->number + 1);
    }
}
