<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Game;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\PileOfCards;
use App\Domain\ValueObject\Game\Exception\PlayerDidNotPlay;
use App\Domain\ValueObject\Game\Exception\PlayerPlayedTwice;
use App\Domain\ValueObject\Player\PlayerIdentifier;

final class Trick implements PileOfCards
{
    /** @var array<non-empty-string, Card> */
    private $playedCards;

    /**
     * @param Card[] $playedCards
     */
    private function __construct(
        array $playedCards
    ) {
        $this->playedCards = $playedCards;
    }

    public static function empty(): self
    {
        return new self([]);
    }

    /**
     * @return \Traversable<Card>
     */
    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->playedCards);
    }

    public function withPlayedCard(PlayerIdentifier $playerIdentifier, Card $card): self
    {
        if (array_key_exists(
            $playerIdentifier->toString(),
            $this->playedCards
        )) {
            throw PlayerPlayedTwice::create($playerIdentifier);
        }

        $result = clone $this;
        $result->playedCards[$playerIdentifier->toString()] = $card;

        return $result;
    }

    public function withCardWithdrawn(PlayerIdentifier $playerIdentifier): self
    {
        if (array_key_exists($playerIdentifier->toString(), $this->playedCards)) {
            $result = clone $this;
            unset($result->playedCards[$playerIdentifier->toString()]);

            return $result;
        }

        throw PlayerDidNotPlay::create($playerIdentifier);
    }

    public function hasCardFromPlayer(PlayerIdentifier $playerIdentifier): bool
    {
        return array_key_exists($playerIdentifier->toString(), $this->playedCards);
    }

    /**
     * @return \Iterator<PlayerIdentifier>
     */
    public function getPlayers(): \Iterator
    {
        foreach (array_keys($this->playedCards) as $playerId) {
            yield PlayerIdentifier::fromString($playerId);
        }
    }

    public function getCardPlayedBy(PlayerIdentifier $playerIdentifier): Card
    {
        return $this->playedCards[$playerIdentifier->toString()];
    }

    public function count(): int
    {
        return count($this->playedCards);
    }

    public function isEmpty(): bool
    {
        return empty($this->playedCards);
    }
}
