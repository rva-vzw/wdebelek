<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Game\Exception;

final class TrickNotFound extends \Exception
{
    public static function create(
        int $trickNumber
    ): self {
        return new self(
            "Trick {$trickNumber} not found."
        );
    }

    public static function noTricks(): self
    {
        return new self('Trick not found.');
    }
}
