<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Game\Exception;

final class InvalidGameNumber extends \Exception
{
    public static function create(int $number): self
    {
        return new self(
            "Game number should be strictly positive; $number given."
        );
    }
}
