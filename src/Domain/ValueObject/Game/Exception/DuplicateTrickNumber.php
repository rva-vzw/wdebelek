<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Game\Exception;

final class DuplicateTrickNumber extends \Exception
{
    public static function create(int $trickNumber): self
    {
        return new self(
            "Duplicate trick with number $trickNumber."
        );
    }
}
