<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Game\Exception;

use App\Domain\ValueObject\Game\Trick;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;

final class TrickIncomplete extends \Exception
{
    public static function create(
        Trick $trick,
        SimplePlayerIdentifiers $participatingPlayers
    ): self {
        $actualPlayerIds = implode(
            ', ',
            array_map(
                fn (PlayerIdentifier $playerIdentifier) => $playerIdentifier->toString(),
                iterator_to_array($trick->getPlayers())
            )
        );
        $expectedPlayerIds = implode(
            ', ',
            array_map(
                fn (PlayerIdentifier $playerIdentifier) => $playerIdentifier->toString(),
                iterator_to_array($participatingPlayers->getIterator())
            )
        );

        return new self("Trick incomplete. Only cards from $actualPlayerIds; expected cards from $expectedPlayerIds");
    }
}
