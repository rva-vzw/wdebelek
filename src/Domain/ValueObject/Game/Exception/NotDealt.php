<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Game\Exception;

use App\Domain\WriteModel\Game\GameIdentifier;

final class NotDealt extends \Exception
{
    public static function create(GameIdentifier $gameIdentifier): self
    {
        return new self(
            "The cards weren't dealt yet for game {$gameIdentifier->toString()}."
        );
    }
}
