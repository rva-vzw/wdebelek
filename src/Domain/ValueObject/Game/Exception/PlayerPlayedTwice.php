<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Game\Exception;

use App\Domain\ValueObject\Player\PlayerIdentifier;

final class PlayerPlayedTwice extends \Exception
{
    public static function create(PlayerIdentifier $playerIdentifier): self
    {
        return new self(
            "Player {$playerIdentifier->toString()} played more than one card."
        );
    }
}
