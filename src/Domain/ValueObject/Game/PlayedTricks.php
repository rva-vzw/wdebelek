<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Game;

use App\Domain\ValueObject\Card\PilesOfCards;
use App\Domain\ValueObject\Game\Exception\DuplicateTrickNumber;
use App\Domain\ValueObject\Game\Exception\TrickNotFound;
use App\Domain\ValueObject\Player\Team;

final class PlayedTricks implements PilesOfCards
{
    /** @var array<int, PlayedTrick> */
    private $playedTricks;

    /**
     * @param array<int, PlayedTrick> $playedTricks
     */
    private function __construct(array $playedTricks)
    {
        $this->playedTricks = $playedTricks;
    }

    public static function empty(): self
    {
        return new self([]);
    }

    /**
     * @param PlayedTrick[] $playedTricksArray
     */
    public static function fromArray(array $playedTricksArray): self
    {
        $indexedArray = [];
        foreach ($playedTricksArray as $playedTrick) {
            if (array_key_exists($playedTrick->getTrickNumber(), $indexedArray)) {
                throw DuplicateTrickNumber::create($playedTrick->getTrickNumber());
            }
            $indexedArray[$playedTrick->getTrickNumber()] = $playedTrick;
        }

        return new self($indexedArray);
    }

    public function withPlayedTrick(PlayedTrick $playedTrick): self
    {
        if (array_key_exists($playedTrick->getTrickNumber(), $this->playedTricks)) {
            if ($playedTrick != $this->playedTricks[$playedTrick->getTrickNumber()]) {
                throw DuplicateTrickNumber::create($playedTrick->getTrickNumber());
            }
        }
        $result = clone $this;
        $result->playedTricks[$playedTrick->getTrickNumber()] = $playedTrick;

        return $result;
    }

    /**
     * @return \Traversable<PlayedTrick>
     */
    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->playedTricks);
    }

    /**
     * @return \Traversable<PlayedTrick>
     */
    public function getReverseIterator(): \Traversable
    {
        return new \ArrayIterator(array_reverse($this->playedTricks, true));
    }

    public function getByTrickNumber(int $trickNumber): PlayedTrick
    {
        if (array_key_exists($trickNumber, $this->playedTricks)) {
            return $this->playedTricks[$trickNumber];
        }

        throw TrickNotFound::create($trickNumber);
    }

    public function withoutTrick(int $trickNumber): self
    {
        $result = clone $this;
        unset($result->playedTricks[$trickNumber]);

        return $result;
    }

    public function wonByTeam(Team $team): self
    {
        return new self(
            array_filter(
                $this->playedTricks,
                fn (PlayedTrick $t) => $team->has($t->getWinner())
            )
        );
    }
}
