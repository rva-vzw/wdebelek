<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Game;

final class GameConclusion
{
    /** @var non-empty-string */
    private $code;

    /**
     * @param non-empty-string $code
     */
    private function __construct(string $code)
    {
        $this->code = $code;
    }

    public static function DONE(): self
    {
        return new self('done');
    }

    public static function REDEAL(): self
    {
        return new self('redeal');
    }

    public static function fromString(string $conclusion): self
    {
        switch ($conclusion) {
            case (string) self::DONE():
                return self::DONE();
            case (string) self::REDEAL():
                return self::REDEAL();
            default:
                throw new \InvalidArgumentException('Invalid game conclusion');
        }
    }

    public function __toString(): string
    {
        return $this->code;
    }

    /**
     * @return non-empty-string
     */
    public function toString(): string
    {
        return $this->code;
    }
}
