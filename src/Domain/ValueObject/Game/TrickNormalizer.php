<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Game;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

final class TrickNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed[] $context
     *
     * @return Trick
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = [])
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException('Array expected');
        }
        $trick = Trick::empty();

        foreach ($data as $playerId => $cardNumber) {
            Assert::stringNotEmpty($playerId);
            $trick = $trick->withPlayedCard(
                PlayerIdentifier::fromString($playerId),
                Card::fromCardNumber($cardNumber)
            );
        }

        return $trick;
    }

    public function supportsDenormalization($data, string $type, ?string $format = null)
    {
        return Trick::class === $type;
    }

    /**
     * @param mixed[] $context
     *
     * @return array<string, int>
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        if (!$object instanceof Trick) {
            throw new \InvalidArgumentException('Trick expected');
        }
        $result = [];
        foreach ($object->getPlayers() as $playerIdentifier) {
            $result[$playerIdentifier->toString()] = $object->getCardPlayedBy($playerIdentifier)->getCardNumber();
        }

        return $result;
    }

    public function supportsNormalization($data, ?string $format = null)
    {
        return $data instanceof Trick;
    }
}
