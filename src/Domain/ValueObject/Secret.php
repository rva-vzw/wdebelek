<?php

declare(strict_types=1);

namespace App\Domain\ValueObject;

use RvaVzw\KrakBoem\Id\Uuid4Identifier;

/**
 * Secret - only used by the API to provide some basic authorization.
 */
final readonly class Secret extends Uuid4Identifier
{
}
