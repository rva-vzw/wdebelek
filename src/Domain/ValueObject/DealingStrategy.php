<?php

declare(strict_types=1);

namespace App\Domain\ValueObject;

use IteratorAggregate;

/**
 * @implements IteratorAggregate<int>
 */
final class DealingStrategy implements \IteratorAggregate
{
    /** @var int[] */
    private $cardCounts;

    public function __construct(int ...$cardCounts)
    {
        $this->cardCounts = $cardCounts;
    }

    public static function fourFourFive(): self
    {
        return new self(4, 4, 5);
    }

    public static function singleCard(): self
    {
        return new self(1);
    }

    public static function fourFourFourFive(): self
    {
        return new self(4, 4, 4, 5);
    }

    public static function threeThreeFour(): self
    {
        return new self(3, 3, 4);
    }

    public static function getDefault(int $cardsPerPlayer): DealingStrategy
    {
        switch ($cardsPerPlayer) {
            case 17:
                return self::fourFourFourFive();
            case 16:
                return new self(4, 4, 4, 4);
            case 15:
                return new self(5, 5, 5);
            case 14:
                return new self(4, 5, 5);
            case 13:
                return self::fourFourFive();
            case 12:
                return new self(4, 4, 4);
            case 11:
                return new self(3, 4, 4);
            case 10:
                return self::threeThreeFour();
            case 9:
                return new self(4, 5);
            case 8:
                return new self(4, 4);
            case 7:
                return new self(3, 4);
            case 6:
                return new self(3, 3);
            default:
                return new self($cardsPerPlayer);
        }
    }

    /**
     * @return \Iterator<int>
     */
    public function getIterator(): \Iterator
    {
        return new \ArrayIterator($this->cardCounts);
    }
}
