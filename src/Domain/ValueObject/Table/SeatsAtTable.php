<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Table;

use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\Exception\DuplicatePlayer;
use App\Domain\ValueObject\Player\Exception\PlayerNotFound;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Player\TableMates;
use App\Domain\ValueObject\Table\Exception\NoSuchSeat;
use App\Domain\ValueObject\Table\Exception\SeatTaken;
use App\Domain\WriteModel\Table\Exception\InvitationNotFound;
use App\Domain\WriteModel\Table\Exception\TableFull;

final class SeatsAtTable
{
    /** @var array<int,PlayerIdentifier> */
    private $players;

    /** @var array<int,Invitation> */
    private $invitations;

    /** @var int */
    private $capacity;

    /**
     * @param array<int,PlayerIdentifier> $players
     * @param array<int,Invitation>       $invitations
     */
    private function __construct(array $players, array $invitations, int $capacity)
    {
        $this->players = $players;
        ksort($this->players);
        $this->capacity = $capacity;
        $this->invitations = $invitations;
    }

    public static function empty(int $capacity): self
    {
        return new self([], [], $capacity);
    }

    public function hasPlayerOnSeat(Seat $seat, PlayerIdentifier $player): bool
    {
        return array_key_exists($seat->toInteger(), $this->players)
            && $this->players[$seat->toInteger()] == $player;
    }

    public function withPlayerOnSeat(Seat $seat, PlayerIdentifier $player): self
    {
        if ($this->hasPlayerOnSeat($seat, $player)) {
            return $this;
        }
        $this->guardSeatNumber($seat);

        if (false !== array_search($player, $this->players)) {
            throw DuplicatePlayer::fromPlayer($player);
        }

        $result = clone $this;
        $result->players[$seat->toInteger()] = $player;
        ksort($result->players);
        unset($result->invitations[$seat->toInteger()]);

        return $result;
    }

    private function guardSeatNumber(Seat $seat): void
    {
        if ($seat->toInteger() > $this->capacity) {
            throw NoSuchSeat::create($seat);
        }
    }

    public function hasInvitationForSeat(Seat $seat, Invitation $invitation): bool
    {
        return array_key_exists($seat->toInteger(), $this->invitations)
            && $this->invitations[$seat->toInteger()] == $invitation;
    }

    public function withInvitation(Seat $seat, Invitation $invitation): self
    {
        if ($this->hasInvitationForSeat($seat, $invitation)) {
            return $this;
        }
        $this->guardSeatNumber($seat);
        $this->guardFreeSeat($seat);

        $result = clone $this;
        $result->invitations[$seat->toInteger()] = $invitation;

        return $result;
    }

    private function guardFreeSeat(Seat $seat): void
    {
        if (array_key_exists($seat->toInteger(), $this->invitations)
            || array_key_exists($seat->toInteger(), $this->players)) {
            throw SeatTaken::create($seat);
        }
    }

    public function hasInvitation(Invitation $invitation): bool
    {
        return false !== array_search($invitation, $this->invitations);
    }

    public function getFreeSeat(): Seat
    {
        for ($i = 0; $i < $this->capacity; ++$i) {
            $seat = Seat::fromInteger($i);
            if ($this->isFree($seat)) {
                return $seat;
            }
        }

        throw TableFull::create($this->capacity);
    }

    private function isFree(Seat $seat): bool
    {
        return !array_key_exists($seat->toInteger(), $this->players)
            && !array_key_exists($seat->toInteger(), $this->invitations);
    }

    public function hasFreeSeat(): bool
    {
        return count($this->invitations) + count($this->players) < $this->capacity;
    }

    public function getReservedSeat(Invitation $invitation): Seat
    {
        $key = array_search($invitation, $this->invitations);

        if (!is_int($key)) {
            throw InvitationNotFound::create($invitation);
        }

        return Seat::fromInteger($key);
    }

    public function hasPlayer(PlayerIdentifier $playerIdentifier): bool
    {
        return false !== array_search($playerIdentifier, $this->players);
    }

    public function playerCount(): int
    {
        return count($this->players);
    }

    public function getPlayerNextTo(PlayerIdentifier $playerIdentifier): PlayerIdentifier
    {
        return $this->getTableMates()->getNextPlayer($playerIdentifier);
    }

    public function withPlayerGone(PlayerIdentifier $playerIdentifier): self
    {
        if ($this->hasPlayer($playerIdentifier)) {
            $seat = $this->getPlayerSeat($playerIdentifier);

            $result = clone $this;
            unset($result->players[$seat->toInteger()]);

            return $result;
        }

        return $this;
    }

    private function getPlayerSeat(PlayerIdentifier $playerIdentifier): Seat
    {
        $key = array_search($playerIdentifier, $this->players);

        if (!is_int($key)) {
            throw PlayerNotFound::create($playerIdentifier);
        }

        return Seat::fromInteger($key);
    }

    public function getPlayerBefore(PlayerIdentifier $playerIdentifier): PlayerIdentifier
    {
        return $this->getTableMates()->getPreviousPlayer($playerIdentifier);
    }

    public function getTableMates(): TableMates
    {
        return TableMates::fromPlayers(
            new SimplePlayerIdentifiers(...$this->players)
        );
    }
}
