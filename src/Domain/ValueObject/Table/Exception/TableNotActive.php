<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Table\Exception;

use App\Domain\WriteModel\Table\TableIdentifier;

final class TableNotActive extends \Exception
{
    public static function create(TableIdentifier $tableIdentifier): self
    {
        return new self(
            "Table {$tableIdentifier->toString()} not active."
        );
    }
}
