<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Table\Exception;

use App\Domain\ValueObject\Table\Seat;

final class SeatTaken extends \Exception
{
    public static function create(Seat $seat): self
    {
        return new self("Seat {$seat->toInteger()} is already taken.");
    }
}
