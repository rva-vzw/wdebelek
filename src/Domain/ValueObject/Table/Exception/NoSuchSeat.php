<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Table\Exception;

use App\Domain\ValueObject\Table\Seat;

final class NoSuchSeat extends \Exception
{
    public static function create(Seat $seat): self
    {
        return new self(
            "No such seat: {$seat->toInteger()}"
        );
    }
}
