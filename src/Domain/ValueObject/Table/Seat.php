<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Table;

use RvaVzw\KrakBoem\Id\IntWrapper;

/**
 * A seat.
 *
 * Seats are counted zero-based.
 */
final class Seat implements IntWrapper
{
    /** @var int */
    private $seatNumber;

    private function __construct(int $seatNumber)
    {
        $this->seatNumber = $seatNumber;
    }

    /**
     * @return self
     */
    public static function fromInteger(int $value): IntWrapper
    {
        return new self($value);
    }

    public static function zero(): self
    {
        return new self(0);
    }

    public function toInteger(): int
    {
        return $this->seatNumber;
    }
}
