<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Table;

use App\Domain\StringObject\ShortString\AbstractShortString;
use App\Domain\ValueObject\StringObject\StringObject;

final class CardGame extends AbstractShortString
{
    public const MAX_NAME_LENGTH = 30;

    public static function unspecified(): self
    {
        return new self('(unspecified)');
    }

    public static function soloWhist(): self
    {
        return new self('solo whist');
    }

    /**
     * @return self
     */
    public static function fromString(string $value): StringObject
    {
        if (empty($value)) {
            return self::unspecified();
        }

        return new self($value);
    }

    protected function getMaxLength(): int
    {
        return self::MAX_NAME_LENGTH;
    }

    public function isSpecified(): bool
    {
        return $this != self::unspecified();
    }
}
