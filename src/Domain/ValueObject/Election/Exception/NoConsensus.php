<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Election\Exception;

final class NoConsensus extends \Exception
{
    /**
     * @param array<string,int> $voteCount
     */
    public static function create(array $voteCount): self
    {
        $options = implode(', ', array_keys($voteCount));

        throw new self("No consensus. Options are $options.");
    }
}
