<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Election;

use App\Domain\ValueObject\Election\Exception\NoConsensus;
use App\Domain\ValueObject\Player\Exception\PlayerNotFound;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;

/**
 * Election class.
 *
 * The option tha gets more than half of the votes, wins.
 * Although this is only used to indicate the winner of a trick (for the moment),
 * I use just strings for voting options, because I'll probably need a voting
 * mechanism for other things as well, later on.
 */
final class Election
{
    /** @var non-empty-string[] */
    private $votesByPlayer;
    /** @var int[] */
    private $voteCount;
    /** @var int */
    private $votesRequired;
    /** @var SimplePlayerIdentifiers */
    private $participatingPlayers;
    /** @var ?non-empty-string */
    private $winningOption;

    /**
     * @param array<non-empty-string,non-empty-string> $votesByPlayer
     */
    public function __construct(
        array $votesByPlayer,
        SimplePlayerIdentifiers $participatingPlayers
    ) {
        $this->winningOption = null;
        $this->votesByPlayer = $votesByPlayer;
        $this->participatingPlayers = $participatingPlayers;
        $this->votesRequired = $participatingPlayers->count() / 2;

        $this->voteCount = [];
        foreach ($votesByPlayer as $voterId => $vote) {
            $this->guardValidPlayer(PlayerIdentifier::fromString($voterId));
            $votes = 0;
            if (array_key_exists($vote, $this->voteCount)) {
                $votes = $this->voteCount[$vote];
            }
            $this->voteCount[$vote] = ++$votes;

            if ($votes > $this->votesRequired) {
                $this->winningOption = $vote;
            }
        }
    }

    public static function start(SimplePlayerIdentifiers $players): self
    {
        return new self([], $players);
    }

    /**
     * @param non-empty-string $vote
     */
    public function withVote(PlayerIdentifier $voter, string $vote): self
    {
        $votes = $this->votesByPlayer;
        $votes[$voter->toString()] = $vote;

        return new self($votes, $this->participatingPlayers);
    }

    private function guardValidPlayer(PlayerIdentifier $playerIdentifier): void
    {
        if ($this->participatingPlayers->has($playerIdentifier)) {
            return;
        }

        throw PlayerNotFound::create($playerIdentifier);
    }

    /**
     * @return non-empty-string
     */
    public function getWinningOption(): string
    {
        if (is_string($this->winningOption)) {
            return $this->winningOption;
        }

        throw NoConsensus::create($this->voteCount);
    }

    public function hasWinner(): bool
    {
        return is_string($this->winningOption);
    }

    public function hasVote(): bool
    {
        return !empty($this->votesByPlayer);
    }

    public function playerHasVoted(PlayerIdentifier $voter): bool
    {
        return array_key_exists($voter->toString(), $this->votesByPlayer);
    }

    public function getVote(PlayerIdentifier $voter): string
    {
        return $this->votesByPlayer[$voter->toString()];
    }

    public function withVoteWithdrawn(PlayerIdentifier $voter): self
    {
        $votes = $this->votesByPlayer;
        unset($votes[$voter->toString()]);

        return new self($votes, $this->participatingPlayers);
    }
}
