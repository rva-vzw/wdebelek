<?php

declare(strict_types=1);

namespace App\Domain\ValueObject;

use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class DealingStrategyNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed[] $context
     *
     * @return DealingStrategy
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = [])
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException('Array expected');
        }

        return new DealingStrategy(...$data);
    }

    public function supportsDenormalization($data, string $type, ?string $format = null)
    {
        return DealingStrategy::class === $type;
    }

    /**
     * @param mixed[] $context
     *
     * @return int[]
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        if (!$object instanceof DealingStrategy) {
            throw new \InvalidArgumentException('DealingStrategy expected');
        }

        return iterator_to_array($object);
    }

    public function supportsNormalization($data, ?string $format = null)
    {
        return $data instanceof DealingStrategy;
    }
}
