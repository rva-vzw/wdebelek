<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player;

interface NamedPlayer
{
    public function getPlayerName(): PlayerName;

    public function getPlayerIdentifier(): PlayerIdentifier;
}
