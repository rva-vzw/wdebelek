<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player;

use App\Domain\ValueObject\Player\Exception\PlayerNotFound;
use App\Domain\WriteModel\Table\Exception\NotEnoughPlayers;

final class TableMates implements PlayerIdentifiers
{
    private SimplePlayerIdentifiers $players;

    private function __construct(SimplePlayerIdentifiers $players)
    {
        $this->players = $players;
    }

    public static function fromPlayers(SimplePlayerIdentifiers $players): self
    {
        return new self($players);
    }

    public function getFourPlayers(PlayerIdentifier $dealer): SimplePlayerIdentifiers
    {
        $count = $this->players->count();

        if ($count < 4) {
            throw NotEnoughPlayers::create(4, $count);
        }
        if (!$this->players->has($dealer)) {
            throw PlayerNotFound::create($dealer);
        }
        if (4 === $count) {
            return $this->players;
        }
        if (5 === $count) {
            return $this->players->withLastPlayer($dealer)->without($dealer);
        }
        if (6 !== $count) {
            throw new \LogicException('This should never happen');
        }

        $identifiers = iterator_to_array($this->players);

        /** @var int $dealerIndex */
        $dealerIndex = array_search($dealer, $identifiers);

        $oppositePlayer = $identifiers[($dealerIndex + 3) % 6];

        return $this->players->withLastPlayer($dealer)->without($oppositePlayer)->without($dealer);
    }

    public function getAtMostFourPlayers(PlayerIdentifier $dealerIdentifier): SimplePlayerIdentifiers
    {
        if ($this->players->count() <= 4) {
            return $this->players->withLastPlayer($dealerIdentifier);
        }

        return $this->getFourPlayers($dealerIdentifier);
    }

    public function getLastPlayerForDealer(PlayerIdentifier $dealer): PlayerIdentifier
    {
        $count = $this->players->count();

        if ($count <= 4) {
            return $dealer;
        }
        if (!$this->players->has($dealer)) {
            throw PlayerNotFound::create($dealer);
        }

        return $this->players->getPreviousPlayer($dealer);
    }

    public function getNextPlayer(PlayerIdentifier $playerIdentifier): PlayerIdentifier
    {
        return $this->players->getNextPlayer($playerIdentifier);
    }

    public function getPreviousPlayer(PlayerIdentifier $playerIdentifier): PlayerIdentifier
    {
        return $this->players->getPreviousPlayer($playerIdentifier);
    }

    /**
     * @param PlayerIdentifier[] $playerIdentifiersArray
     *
     * @return self
     */
    public static function fromArray(array $playerIdentifiersArray): PlayerIdentifiers
    {
        return new self(SimplePlayerIdentifiers::fromArray($playerIdentifiersArray));
    }

    /**
     * @return \Traversable<PlayerIdentifier>
     */
    public function getIterator(): \Traversable
    {
        return $this->players;
    }
}
