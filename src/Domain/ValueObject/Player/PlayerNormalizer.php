<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player;

use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class PlayerNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed[] $context
     *
     * @return SimpleNamedPlayer
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = [])
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException('Array expected');
        }

        return new SimpleNamedPlayer(
            PlayerIdentifier::fromString($data['id']),
            PlayerName::fromString($data['name'])
        );
    }

    public function supportsDenormalization($data, string $type, ?string $format = null)
    {
        return SimpleNamedPlayer::class === $type;
    }

    /**
     * @param mixed[] $context
     *
     * @return string[]
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        if (!$object instanceof SimpleNamedPlayer) {
            throw new \InvalidArgumentException('Player expected');
        }

        return [
            'id' => $object->getPlayerIdentifier()->toString(),
            'name' => $object->getPlayerName()->toString(),
        ];
    }

    public function supportsNormalization($data, ?string $format = null)
    {
        return $data instanceof SimpleNamedPlayer;
    }
}
