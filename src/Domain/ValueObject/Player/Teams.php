<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player;

use App\Domain\ValueObject\Player\Exception\DuplicatePlayer;
use App\Domain\ValueObject\Player\Exception\PlayerNotFound;
use Webmozart\Assert\Assert;

/**
 * @implements \IteratorAggregate<Team>
 */
final class Teams implements \IteratorAggregate
{
    /** @var array<non-empty-string,int> */
    private $teamNumbersByPlayer;

    /**
     * @param array<non-empty-string,int> $teamNumbersByPlayer
     */
    private function __construct(array $teamNumbersByPlayer)
    {
        $this->teamNumbersByPlayer = $teamNumbersByPlayer;
    }

    public static function allIndividual(SimplePlayerIdentifiers $players): self
    {
        $teamNumbersByPlayer = array_flip(
            array_map(
                function (PlayerIdentifier $playerIdentifier): string {
                    return $playerIdentifier->toString();
                },
                iterator_to_array($players)
            )
        );

        return new self($teamNumbersByPlayer);
    }

    public static function empty(): self
    {
        return new self([]);
    }

    /**
     * @param string[] $teamsAsStrings
     */
    public static function fromScalarArray(array $teamsAsStrings): self
    {
        $teamNumbersByPlayers = [];
        foreach (array_values($teamsAsStrings) as $teamNumber => $teamAsString) {
            foreach (explode('_', $teamAsString) as $playerId) {
                Assert::stringNotEmpty($playerId);
                $teamNumbersByPlayers[$playerId] = $teamNumber;
            }
        }

        return new self($teamNumbersByPlayers);
    }

    /**
     * @return \Traversable<Team>
     */
    public function getIterator(): \Traversable
    {
        $maxTeamNumber = empty($this->teamNumbersByPlayer) ? 0 : max($this->teamNumbersByPlayer);

        for ($teamNumber = 0; $teamNumber <= $maxTeamNumber; ++$teamNumber) {
            yield new Team(
                new SimplePlayerIdentifiers(...array_map(
                    function (string $playerId): PlayerIdentifier {
                        return PlayerIdentifier::fromString($playerId);
                    },
                    array_keys(array_filter(
                        $this->teamNumbersByPlayer,
                        function (int $value) use ($teamNumber) {
                            return $teamNumber === $value;
                        }
                    ))
                ))
            );
        }
    }

    public function getTeamNumber(PlayerIdentifier $playerIdentifier): int
    {
        $playerId = $playerIdentifier->toString();

        if (array_key_exists($playerId, $this->teamNumbersByPlayer)) {
            return $this->teamNumbersByPlayer[$playerId];
        }

        throw PlayerNotFound::create($playerIdentifier);
    }

    public function withAdditionalTeam(Team $team): self
    {
        $teamNumber = empty($this->teamNumbersByPlayer) ? 0 : max($this->teamNumbersByPlayer) + 1;
        $result = clone $this;

        /** @var PlayerIdentifier $playerIdentifier */
        foreach ($team as $playerIdentifier) {
            $playerId = $playerIdentifier->toString();
            if (array_key_exists($playerId, $result->teamNumbersByPlayer)) {
                throw DuplicatePlayer::fromPlayer($playerIdentifier);
            }
            $result->teamNumbersByPlayer[$playerId] = $teamNumber;
        }

        return $result;
    }

    /**
     * @return \Traversable<PlayerIdentifier>
     */
    public function getIndividualPlayers(): \Traversable
    {
        foreach ($this as $team) {
            foreach ($team as $player) {
                yield $player;
            }
        }
    }

    /**
     * @return string[]
     */
    public function toScalarArray(): array
    {
        return array_map(
            fn (int $teamNumber) => implode(
                '_',
                array_keys(
                    $this->teamNumbersByPlayer,
                    $teamNumber
                )
            ),
            array_unique($this->teamNumbersByPlayer)
        );
    }

    public function getTeamByPlayer(PlayerIdentifier $playerIdentifier): Team
    {
        $teamNumber = $this->getTeamNumber($playerIdentifier);

        return Team::withPlayers(
            ...array_map(
                /** @phpstan-ignore-next-line */
                fn (string $playerId) => PlayerIdentifier::fromString($playerId),
                array_filter(array_keys(
                    $this->teamNumbersByPlayer,
                    $teamNumber
                )),
            )
        );
    }
}
