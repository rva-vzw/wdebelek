<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player;

use App\Domain\ValueObject\Player\Exception\DuplicatePlayer;
use App\Domain\ValueObject\Player\Exception\PlayerNotFound;

final class SimplePlayerIdentifiers implements PlayerIdentifiers
{
    /** @var PlayerIdentifier[] */
    private array $orderedPlayers;

    /** @var PlayerIdentifier[] */
    private array $playersById;

    public function __construct(PlayerIdentifier ...$orderedPlayers)
    {
        $this->orderedPlayers = $orderedPlayers;
        $this->playersById = [];

        foreach ($this->orderedPlayers as $player) {
            if (array_key_exists($player->toString(), $this->playersById)) {
                throw DuplicatePlayer::fromPlayer($player);
            }
            $this->playersById[$player->toString()] = $player;
        }
    }

    /**
     * @param PlayerIdentifier[] $playerIdentifiersArray
     *
     * @return self
     */
    public static function fromArray(array $playerIdentifiersArray): PlayerIdentifiers
    {
        return new self(...$playerIdentifiersArray);
    }

    public static function none(): self
    {
        return new self();
    }

    /**
     * @return \Traversable<PlayerIdentifier>
     */
    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->orderedPlayers);
    }

    public function count(): int
    {
        return count($this->orderedPlayers);
    }

    public function withLastPlayer(PlayerIdentifier $lastPlayer): self
    {
        $key = array_search($lastPlayer, $this->orderedPlayers);

        if (!is_int($key)) {
            throw PlayerNotFound::create($lastPlayer);
        }

        if ($key === count($this->orderedPlayers) - 1) {
            return $this;
        }

        $firstPart = array_slice($this->orderedPlayers, 0, $key + 1);
        $lastPart = array_slice($this->orderedPlayers, $key + 1);

        $merged = array_merge($lastPart, $firstPart);

        return new self(...$merged);
    }

    public function has(PlayerIdentifier $playerIdentifier): bool
    {
        return array_key_exists($playerIdentifier->toString(), $this->playersById);
    }

    public function with(PlayerIdentifier $playerIdentifier): self
    {
        $result = clone $this;

        $result->playersById[$playerIdentifier->toString()] = $playerIdentifier;
        $result->orderedPlayers[] = $playerIdentifier;

        return $result;
    }

    public function getNextPlayer(PlayerIdentifier $playerIdentifier): PlayerIdentifier
    {
        $index = array_search($playerIdentifier, $this->orderedPlayers);
        if (is_int($index)) {
            return $this->orderedPlayers[($index + 1) % $this->count()];
        }

        throw PlayerNotFound::create($playerIdentifier);
    }

    public function without(PlayerIdentifier $playerIdentifier): self
    {
        $result = clone $this;

        unset($result->playersById[$playerIdentifier->toString()]);

        $key = array_search($playerIdentifier, $result->orderedPlayers);
        if (!is_int($key)) {
            return $this;
        }
        array_splice(
            $result->orderedPlayers,
            $key,
            1
        );

        return $result;
    }

    public function getPreviousPlayer(PlayerIdentifier $playerIdentifier): PlayerIdentifier
    {
        $count = $this->count();
        $index = array_search($playerIdentifier, $this->orderedPlayers);
        if (is_int($index)) {
            return $this->orderedPlayers[($index + $count - 1) % $this->count()];
        }

        throw PlayerNotFound::create($playerIdentifier);
    }

    public function getLast(): PlayerIdentifier
    {
        return $this->orderedPlayers[$this->count() - 1];
    }
}
