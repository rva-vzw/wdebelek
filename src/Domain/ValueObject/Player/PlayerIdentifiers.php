<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player;

/**
 * @extends  \IteratorAggregate<PlayerIdentifier>
 */
interface PlayerIdentifiers extends \IteratorAggregate
{
    /**
     * @param PlayerIdentifier[] $playerIdentifiersArray
     *
     * @return static
     */
    public static function fromArray(array $playerIdentifiersArray): PlayerIdentifiers;

    /**
     * @return \Traversable<PlayerIdentifier>
     */
    public function getIterator(): \Traversable;
}
