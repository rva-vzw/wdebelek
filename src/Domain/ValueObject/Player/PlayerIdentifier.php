<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player;

use App\Domain\ValueObject\Secret;
use RvaVzw\KrakBoem\Id\Uuid5Identifier;

final readonly class PlayerIdentifier extends Uuid5Identifier
{
    protected static function getNamespace(): string
    {
        return '3bac2901-0326-4210-bc45-bbff4e5bb485';
    }

    public static function withSecret(Secret $playerSecret): self
    {
        return self::fromName($playerSecret->toString());
    }
}
