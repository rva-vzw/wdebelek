<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player;

use IteratorAggregate;

/**
 * @implements IteratorAggregate<PlayerIdentifier>
 */
final class Team implements \IteratorAggregate
{
    /** @var SimplePlayerIdentifiers */
    private $playerIdentifiers;

    public function __construct(SimplePlayerIdentifiers $playerIdentifiers)
    {
        $this->playerIdentifiers = $playerIdentifiers;
    }

    public static function withPlayers(PlayerIdentifier ...$playerIdentifiers): self
    {
        return new self(new SimplePlayerIdentifiers(...$playerIdentifiers));
    }

    public function getPlayerIdentifiers(): SimplePlayerIdentifiers
    {
        return $this->playerIdentifiers;
    }

    /**
     * @return \Traversable<PlayerIdentifier>
     */
    public function getIterator(): \Traversable
    {
        return $this->playerIdentifiers;
    }

    public function has(PlayerIdentifier $playerIdentifier): bool
    {
        return $this->playerIdentifiers->has($playerIdentifier);
    }

    public function toString(): string
    {
        return implode(
            '_',
            array_map(
                fn (PlayerIdentifier $playerIdentifier) => $playerIdentifier->toString(),
                iterator_to_array($this)
            )
        );
    }
}
