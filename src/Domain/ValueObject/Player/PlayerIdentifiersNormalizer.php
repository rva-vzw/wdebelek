<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player;

use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

final class PlayerIdentifiersNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed[] $context
     *
     * @return PlayerIdentifiers
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = [])
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException('Array expected');
        }

        /** @var PlayerIdentifiers $result */
        $result = $type::fromArray(
            array_map(
                function (string $playerId): PlayerIdentifier {
                    Assert::stringNotEmpty($playerId);

                    return PlayerIdentifier::fromString($playerId);
                },
                $data
            )
        );

        return $result;
    }

    public function supportsDenormalization($data, string $type, ?string $format = null)
    {
        $interfaces = class_implements($type);

        return is_array($interfaces) && in_array(PlayerIdentifiers::class, $interfaces);
    }

    /**
     * @param mixed[] $context
     *
     * @return string[]
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        if (!$object instanceof PlayerIdentifiers) {
            throw new \InvalidArgumentException('PlayerIdentifiers expected');
        }
        $result = [];

        /** @var PlayerIdentifier $playerIdentifier */
        foreach ($object as $playerIdentifier) {
            $result[] = $playerIdentifier->toString();
        }

        return $result;
    }

    public function supportsNormalization($data, ?string $format = null)
    {
        return $data instanceof PlayerIdentifiers;
    }
}
