<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player;

use App\Domain\ValueObject\StringObject\StringObject;

final class Language implements StringObject
{
    public const DEFAULT = 'en';

    /** @var string */
    private $lang;

    private function __construct(string $lang)
    {
        $this->lang = $lang;
    }

    /**
     * @return self
     */
    public static function fromString(string $value): StringObject
    {
        if (in_array($value, self::getSupportedLanguages())) {
            return new self($value);
        }

        return self::default();
    }

    /**
     * @return string[]
     */
    private static function getSupportedLanguages(): array
    {
        return ['en', 'nl'];
    }

    public static function default(): self
    {
        return new self(self::DEFAULT);
    }

    public static function nl(): self
    {
        return new self('nl');
    }

    public function toString(): string
    {
        return $this->lang;
    }
}
