<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player;

final class SimpleNamedPlayer implements NamedPlayer
{
    /** @var PlayerIdentifier */
    private $playerIdentifier;
    /** @var PlayerName */
    private $playerName;

    public function __construct(PlayerIdentifier $playerIdentifier, PlayerName $playerName)
    {
        $this->playerIdentifier = $playerIdentifier;
        $this->playerName = $playerName;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getPlayerName(): PlayerName
    {
        return $this->playerName;
    }
}
