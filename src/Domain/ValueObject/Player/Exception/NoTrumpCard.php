<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player\Exception;

final class NoTrumpCard extends \Exception
{
    public static function create(): self
    {
        return new self(
            "Player didn't get the trump card."
        );
    }
}
