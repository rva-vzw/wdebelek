<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player\Exception;

use App\Domain\ValueObject\Player\PlayerIdentifier;

final class DuplicatePlayer extends \Exception
{
    public static function fromPlayer(PlayerIdentifier $playerIdentifier): self
    {
        return new self(
            sprintf('Duplicate player: %s.', $playerIdentifier->toString())
        );
    }
}
