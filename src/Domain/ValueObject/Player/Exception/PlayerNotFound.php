<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player\Exception;

use App\Domain\ValueObject\Player\PlayerIdentifier;

final class PlayerNotFound extends \Exception
{
    public static function create(PlayerIdentifier $playerIdentifier): self
    {
        return new self("Player with identifier {$playerIdentifier->toString()} not found.");
    }
}
