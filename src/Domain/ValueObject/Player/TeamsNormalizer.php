<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player;

use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class TeamsNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /** @var PlayerIdentifiersNormalizer */
    private $playerIdentifiersNormalizer;

    public function __construct(PlayerIdentifiersNormalizer $playerIdentifiersNormalizer)
    {
        $this->playerIdentifiersNormalizer = $playerIdentifiersNormalizer;
    }

    /**
     * @param mixed[] $context
     *
     * @return Teams
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = [])
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException('Array expected');
        }
        $teams = Teams::empty();

        foreach ($data as $normalizedPlayers) {
            /** @var SimplePlayerIdentifiers $playerIdentifiers */
            $playerIdentifiers = $this->playerIdentifiersNormalizer->denormalize($normalizedPlayers, SimplePlayerIdentifiers::class);

            $teams = $teams->withAdditionalTeam(new Team($playerIdentifiers));
        }

        return $teams;
    }

    public function supportsDenormalization($data, string $type, ?string $format = null)
    {
        return Teams::class === $type;
    }

    /**
     * @param mixed[] $context
     *
     * @return string[][]
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        if (!$object instanceof Teams) {
            throw new \InvalidArgumentException('Teams expected');
        }
        $result = [];
        /** @var Team $team */
        foreach ($object as $team) {
            $normalizedTeam = $this->playerIdentifiersNormalizer->normalize(
                $team->getPlayerIdentifiers()
            );
            $result[] = $normalizedTeam;
        }

        return $result;
    }

    public function supportsNormalization($data, ?string $format = null)
    {
        return $data instanceof Teams;
    }
}
