<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Player;

use App\Domain\StringObject\ShortString\AbstractShortString;
use App\Domain\ValueObject\StringObject\StringObject;

final class PlayerName extends AbstractShortString
{
    public const MAX_NAME_LENGTH = 35;

    /**
     * @return self
     */
    public static function fromString(string $value): StringObject
    {
        if (empty($value)) {
            return self::unknown();
        }

        return new self($value);
    }

    public static function unknown(): self
    {
        return new self('(unknown)');
    }

    protected function getMaxLength(): int
    {
        return self::MAX_NAME_LENGTH;
    }
}
