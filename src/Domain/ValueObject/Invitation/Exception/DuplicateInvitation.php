<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Invitation\Exception;

use App\Domain\ValueObject\Invitation\Invitation;

final class DuplicateInvitation extends \Exception
{
    public static function fromInvitation(Invitation $invitation): self
    {
        return new self("Duplicate invitation {$invitation->toString()}");
    }
}
