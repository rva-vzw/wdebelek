<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Invitation\Exception;

final class InvitationNotAccepted extends \Exception
{
    public static function create(): self
    {
        return new self('Invitation was not accepted');
    }
}
