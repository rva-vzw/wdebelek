<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Invitation\Exception;

final class NoInvitations extends \Exception
{
    public static function create(): self
    {
        return new self('No invitation found.');
    }
}
