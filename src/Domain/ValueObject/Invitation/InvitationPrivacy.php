<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Invitation;

use App\Domain\ValueObject\StringObject\StringObject;

final class InvitationPrivacy implements StringObject
{
    public const PRIVATE = 'private';
    public const PUBLIC = 'public';
    public const EXPIRED = 'expired';

    /** @var string */
    private $privacy;

    private function __construct(string $privacy)
    {
        $this->privacy = $privacy;
    }

    public static function privateInvitation(): self
    {
        return new self(self::PRIVATE);
    }

    public static function publicInvitation(): self
    {
        return new self(self::PUBLIC);
    }

    public static function expired(): self
    {
        return new self(self::EXPIRED);
    }

    /**
     * @return self
     */
    public static function fromString(string $value): StringObject
    {
        if (in_array(
            $value,
            [self::PRIVATE, self::PUBLIC, self::EXPIRED]
        )) {
            return new self($value);
        }

        throw new \InvalidArgumentException("Invalid invitation privacy: $value");
    }

    public function toString(): string
    {
        return $this->privacy;
    }
}
