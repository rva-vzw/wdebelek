<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Invitation;

use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

final class InvitationsNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed[] $context
     *
     * @return Invitations
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = [])
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException('Array expected');
        }

        return new Invitations(
            ...array_map(
                function (string $invitationCode): Invitation {
                    Assert::stringNotEmpty($invitationCode);

                    return Invitation::fromString($invitationCode);
                },
                $data
            )
        );
    }

    public function supportsDenormalization($data, string $type, ?string $format = null)
    {
        return Invitations::class === $type;
    }

    /**
     * @param mixed[] $context
     *
     * @return string[]
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        if (!$object instanceof Invitations) {
            throw new \InvalidArgumentException('Invitations expected');
        }

        return array_map(
            function (Invitation $invitation): string {
                return $invitation->toString();
            },
            array_values(iterator_to_array($object))
        );
    }

    public function supportsNormalization($data, ?string $format = null)
    {
        return $data instanceof Invitations;
    }
}
