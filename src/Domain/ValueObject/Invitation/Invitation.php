<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Invitation;

use RvaVzw\KrakBoem\Id\Uuid4Identifier;

/**
 * There are invitations for tables. A player can accept an invitation.
 *
 * Theoretically, an existing player can accept an invitation. But for now,
 * I will probably create a new player every time someone accepts an invitation.
 */
final readonly class Invitation extends Uuid4Identifier
{
    public function __toString(): string
    {
        return $this->toString();
    }
}
