<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Invitation;

use App\Domain\ValueObject\Invitation\Exception\DuplicateInvitation;
use App\Domain\ValueObject\Invitation\Exception\NoInvitations;
use IteratorAggregate;

/**
 * @implements IteratorAggregate<Invitation>
 */
final class Invitations implements \IteratorAggregate
{
    /** @var Invitation[] */
    private $invitations;

    public function __construct(Invitation ...$invitations)
    {
        $this->invitations = [];
        foreach ($invitations as $invitation) {
            $key = $invitation->toString();
            if (array_key_exists($key, $this->invitations)) {
                throw DuplicateInvitation::fromInvitation($invitation);
            }
            $this->invitations[$key] = $invitation;
        }
    }

    public static function none(): self
    {
        return new self();
    }

    /**
     * @return \Traversable<Invitation>
     */
    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->invitations);
    }

    public function has(Invitation $invitation): bool
    {
        return array_key_exists($invitation->toString(), $this->invitations);
    }

    public function with(Invitation $invitation): self
    {
        $result = clone $this;
        $result->invitations[$invitation->toString()] = $invitation;

        return $result;
    }

    public function without(Invitation $invitation): self
    {
        $result = clone $this;
        unset($result->invitations[$invitation->toString()]);

        return $result;
    }

    public function isEmpty(): bool
    {
        return 0 === count($this->invitations);
    }

    public function getRandomInvitation(): Invitation
    {
        if (empty($this->invitations)) {
            throw NoInvitations::create();
        }

        $index = mt_rand(0, count($this->invitations) - 1);

        return array_values($this->invitations)[$index];
    }

    public function getFirstInvitation(): Invitation
    {
        if (empty($this->invitations)) {
            throw NoInvitations::create();
        }

        return array_values($this->invitations)[0];
    }
}
