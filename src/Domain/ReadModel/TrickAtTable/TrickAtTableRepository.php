<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TrickAtTable;

use App\Domain\ValueObject\Game\Trick;
use App\Domain\WriteModel\Table\TableIdentifier;

interface TrickAtTableRepository extends TricksAtTables
{
    public function replaceTrickAtTable(TableIdentifier $tableIdentifier, Trick $trick): void;
}
