<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TrickAtTable;

use App\Domain\ValueObject\Game\Trick;
use App\Domain\WriteModel\Game\Event\CardPickedUp;
use App\Domain\WriteModel\Game\Event\CardPlayed;
use App\Domain\WriteModel\Game\Event\TrickReopened;
use App\Domain\WriteModel\Game\Event\TrickWon;
use App\Domain\WriteModel\Table\Event\CardsCollected;
use App\Domain\WriteModel\Table\Event\GamePrepared;
use RvaVzw\KrakBoem\EventSourcing\Projector\AbstractProjector;

final readonly class TrickAtTableProjector extends AbstractProjector
{
    private TrickAtTableRepository $tricksAtTables;

    public function __construct(TrickAtTableRepository $tricksAtTables)
    {
        $this->tricksAtTables = $tricksAtTables;
    }

    public function applyCardPlayed(CardPlayed $event): void
    {
        $trickAtTable = $this->tricksAtTables->getTrickAtTable($event->getTableIdentifier());
        $this->tricksAtTables->replaceTrickAtTable(
            $event->getTableIdentifier(),
            $trickAtTable->withPlayedCard(
                $event->getPlayerIdentifier(),
                $event->getCard()
            )
        );
    }

    public function applyCardPickedUp(CardPickedUp $event): void
    {
        $trickAtTable = $this->tricksAtTables->getTrickAtTable($event->getTableIdentifier());
        $this->tricksAtTables->replaceTrickAtTable(
            $event->getTableIdentifier(),
            $trickAtTable->withCardWithdrawn($event->getPlayerIdentifier())
        );
    }

    public function applyTrickWon(TrickWon $event): void
    {
        $this->tricksAtTables->replaceTrickAtTable(
            $event->getTableIdentifier(),
            Trick::empty()
        );
    }

    public function applyCardsCollected(CardsCollected $event): void
    {
        $this->tricksAtTables->replaceTrickAtTable(
            $event->getTableIdentifier(),
            Trick::empty()
        );
    }

    public function applyGamePrepared(GamePrepared $event): void
    {
        $this->tricksAtTables->replaceTrickAtTable(
            $event->getTableIdentifier(),
            Trick::empty()
        );
    }

    public function applyTrickReopened(TrickReopened $event): void
    {
        $this->tricksAtTables->replaceTrickAtTable(
            $event->getTableIdentifier(),
            $event->getTrick()
        );
    }
}
