<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TrickAtTable;

use App\Domain\ValueObject\Game\Trick;
use App\Domain\WriteModel\Table\TableIdentifier;

interface TricksAtTables
{
    public function getTrickAtTable(TableIdentifier $tableIdentifier): Trick;
}
