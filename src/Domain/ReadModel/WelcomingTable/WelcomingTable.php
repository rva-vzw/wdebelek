<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\WelcomingTable;

use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Invitation\Invitations;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\WriteModel\Table\TableIdentifier;

final class WelcomingTable
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var PlayerName */
    private $host;
    /** @var \DateTimeImmutable */
    private $latestInvitationPublishedAt;
    /** @var Invitations */
    private $publishedInvitations;
    /** @var int */
    private $playerCount;
    /** @var CardGame */
    private $cardGame;
    /** @var Language */
    private $language;

    public function __construct(
        TableIdentifier $tableIdentifier,
        PlayerName $host,
        \DateTimeImmutable $latestInvitationPublishedAt,
        Invitations $publishedInvitations,
        int $playerCount,
        CardGame $cardGame,
        Language $language
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->host = $host;
        $this->latestInvitationPublishedAt = $latestInvitationPublishedAt;
        $this->publishedInvitations = $publishedInvitations;
        $this->playerCount = $playerCount;
        $this->cardGame = $cardGame;
        $this->language = $language;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getHost(): PlayerName
    {
        return $this->host;
    }

    public function getLatestInvitationPublishedAt(): \DateTimeImmutable
    {
        return $this->latestInvitationPublishedAt;
    }

    public function getPublishedInvitations(): Invitations
    {
        return $this->publishedInvitations;
    }

    public function getPlayerCount(): int
    {
        return $this->playerCount;
    }

    public function withAdditionalInvitation(
        Invitation $invitation,
        \DateTimeImmutable $publicationDate
    ): self {
        $result = clone $this;
        $result->publishedInvitations = $result->publishedInvitations->with($invitation);
        $result->latestInvitationPublishedAt = $publicationDate;

        return $result;
    }

    public function getCardGame(): CardGame
    {
        return $this->cardGame;
    }

    public function withInvitationRemoved(Invitation $invitation): self
    {
        $result = clone $this;
        $result->publishedInvitations = $result->publishedInvitations->without($invitation);

        return $result;
    }

    public function hasPublishedInvitations(): bool
    {
        return !$this->publishedInvitations->isEmpty();
    }

    public function playing(CardGame $cardGame): self
    {
        $result = clone $this;
        $result->cardGame = $cardGame;

        return $result;
    }

    public function withPlayerLeft(): self
    {
        $result = clone $this;
        --$result->playerCount;

        return $result;
    }

    public function withAdditionalPlayer(): self
    {
        $result = clone $this;
        ++$result->playerCount;

        return $result;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }
}
