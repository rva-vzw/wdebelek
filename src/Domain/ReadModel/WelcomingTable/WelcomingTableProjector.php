<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\WelcomingTable;

use App\Domain\ReadModel\CardGames\TableCardGames;
use App\Domain\ReadModel\PlayerProfile\PlayerProfiles;
use App\Domain\ReadModel\PlayingPlayer\PlayerCounts;
use App\Domain\ValueObject\Invitation\Invitations;
use App\Domain\WriteModel\Table\Event\GameChosen;
use App\Domain\WriteModel\Table\Event\InvitationAccepted;
use App\Domain\WriteModel\Table\Event\InvitationPublished;
use App\Domain\WriteModel\Table\Event\InvitationUnpublished;
use App\Domain\WriteModel\Table\Event\PlayerKicked;
use RvaVzw\KrakBoem\EventSourcing\Projector\AbstractProjector;

final readonly class WelcomingTableProjector extends AbstractProjector
{
    public function __construct(
        private WelcomingTableRepository $repository,
        private PlayerCounts $playerCounts,
        private PlayerProfiles $playerProfiles,
        private TableCardGames $tableCardGames
    ) {
    }

    public function applyInvitationPublished(InvitationPublished $event): void
    {
        $tableIdentifier = $event->getTableIdentifier();
        if ($this->repository->hasWelcomingTable($tableIdentifier)) {
            $table = $this->repository->getWelcomingTable($tableIdentifier);
            $this->repository->saveWelcomingTable(
                $table->withAdditionalInvitation($event->getInvitation(), $event->getPublicationDate())
            );

            return;
        }

        $playerProfile = $this->playerProfiles->getPlayerProfile(
            $tableIdentifier,
            $event->getPlayerIdentifier()
        );
        $this->repository->saveWelcomingTable(
            new WelcomingTable(
                $tableIdentifier,
                $playerProfile->getPlayerName(),
                $event->getPublicationDate(),
                new Invitations($event->getInvitation()),
                $this->playerCounts->getPlayerCount($tableIdentifier),
                $this->tableCardGames->getCardGameByTable($tableIdentifier),
                $playerProfile->getLanguage()
            )
        );
    }

    public function applyInvitationUnpublished(InvitationUnpublished $event): void
    {
        $tableIdentifier = $event->getTableIdentifier();

        $table = $this->repository->getWelcomingTable($tableIdentifier)
            ->withInvitationRemoved($event->getInvitation());

        if ($table->hasPublishedInvitations()) {
            $this->repository->saveWelcomingTable($table);

            return;
        }

        $this->repository->removeWelcomingTable($tableIdentifier);
    }

    public function applyGameChosen(GameChosen $event): void
    {
        $tableIdentifier = $event->getTableIdentifier();
        if ($this->repository->hasWelcomingTable($tableIdentifier)) {
            $table = $this->repository->getWelcomingTable($tableIdentifier);

            $this->repository->saveWelcomingTable(
                $table->playing($event->getCardGame())
            );
        }
    }

    public function applyInvitationAccepted(InvitationAccepted $event): void
    {
        $tableIdentifier = $event->getTableIdentifier();
        if ($this->repository->hasWelcomingTable($tableIdentifier)) {
            $table = $this->repository->getWelcomingTable($tableIdentifier);

            $this->repository->saveWelcomingTable(
                $table->withAdditionalPlayer()
            );
        }
    }

    public function applyPlayerKicked(PlayerKicked $event): void
    {
        $tableIdentifier = $event->getTableIdentifier();
        if ($this->repository->hasWelcomingTable($tableIdentifier)) {
            $table = $this->repository->getWelcomingTable($tableIdentifier);
            $this->repository->saveWelcomingTable(
                $table->withPlayerLeft()
            );
        }
    }
}
