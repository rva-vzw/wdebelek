<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\WelcomingTable;

use App\Domain\WriteModel\Table\TableIdentifier;

interface WelcomingTableRepository extends WelcomingTables
{
    public function hasWelcomingTable(TableIdentifier $tableIdentifier): bool;

    public function saveWelcomingTable(WelcomingTable $table): void;

    public function removeWelcomingTable(TableIdentifier $tableIdentifier): void;
}
