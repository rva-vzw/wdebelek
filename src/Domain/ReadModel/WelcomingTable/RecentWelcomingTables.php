<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\WelcomingTable;

interface RecentWelcomingTables
{
    /**
     * @return \Traversable<WelcomingTable>
     */
    public function getRecentWelcomingTables(\DateTimeImmutable $startingDate): \Traversable;
}
