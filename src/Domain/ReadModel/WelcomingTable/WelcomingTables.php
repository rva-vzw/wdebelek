<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\WelcomingTable;

use App\Domain\WriteModel\Table\TableIdentifier;

interface WelcomingTables
{
    public function getWelcomingTable(TableIdentifier $tableIdentifier): WelcomingTable;
}
