<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\CardGames;

use App\Domain\ValueObject\Table\CardGame;
use App\Domain\WriteModel\Table\TableIdentifier;

interface TableCardGames
{
    public function getCardGameByTable(TableIdentifier $tableIdentifier): CardGame;
}
