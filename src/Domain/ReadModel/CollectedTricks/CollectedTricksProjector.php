<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\CollectedTricks;

use App\Domain\WriteModel\Game\Event\AllCollectedTricksDisclosed;
use App\Domain\WriteModel\Game\Event\GameStarted;
use App\Domain\WriteModel\Game\Event\TrickReopened;
use App\Domain\WriteModel\Game\Event\TrickWon;
use RvaVzw\KrakBoem\EventSourcing\Projector\AbstractProjector;

final readonly class CollectedTricksProjector extends AbstractProjector
{
    public function __construct(
        private CollectedTricks $collectedTricks
    ) {
    }

    public function applyGameStarted(GameStarted $event): void
    {
        // This is not really necessary, but since tricks of previous games are not used,
        // I throw them away for now.
        $this->collectedTricks->discardTricks($event->getTableIdentifier());
    }

    public function applyTrickWon(TrickWon $event): void
    {
        $this->collectedTricks->collectTrick(
            $event->getTableIdentifier(),
            $event->getGameIdentifier(),
            $event->getPlayedTrick(),
        );
    }

    public function applyAllCollectedTricksDisclosed(AllCollectedTricksDisclosed $event): void
    {
        $this->collectedTricks->discloseAllTricks($event->getGameIdentifier());
    }

    public function applyTrickReopened(TrickReopened $event): void
    {
        $this->collectedTricks->uncollectTrick($event->getGameIdentifier(), $event->getTrickNumber());
    }
}
