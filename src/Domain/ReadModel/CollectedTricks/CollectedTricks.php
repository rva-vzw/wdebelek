<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\CollectedTricks;

use App\Domain\ValueObject\Game\PlayedTrick;
use App\Domain\ValueObject\Game\PlayedTricks;
use App\Domain\ValueObject\Player\Team;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;

interface CollectedTricks
{
    public function collectTrick(
        TableIdentifier $tableIdentifier,
        GameIdentifier $gameIdentifier,
        PlayedTrick $playedTrick
    ): void;

    public function discloseAllTricks(GameIdentifier $gameIdentifier): void;

    public function getDisclosedTricks(GameIdentifier $gameIdentifier, Team $team): PlayedTricks;

    public function discardTricks(TableIdentifier $tableIdentifier): void;

    public function uncollectTrick(GameIdentifier $gameIdentifier, int $trickNumber): void;
}
