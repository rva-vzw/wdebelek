<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\GameCounter;

interface ActiveTablesCounter
{
    public function getActiveTablesCount(
        \DateTimeImmutable $startedBefore,
        \DateTimeImmutable $stillBusyAt,
        int $minimumGameCount
    ): int;
}
