<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\GameCounter;

interface MonthlyTotals
{
    /**
     * @return \Traversable<MonthlyTotal>
     */
    public function getByMonth(
        Month $from,
        Month $to,
        int $minimumGameCount
    ): \Traversable;
}
