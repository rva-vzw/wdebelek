<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\GameCounter;

use App\Domain\ReadModel\GameCounter\Exception\IncorrectlyCounted;
use App\Domain\WriteModel\Table\TableIdentifier;

final class GameCounter
{
    private function __construct(
        private TableIdentifier $tableIdentifier,
        private \DateTimeImmutable $firstGameStartedAt,
        private \DateTimeImmutable $latestGameStartedAt,
        private int $gameCount
    ) {
    }

    public static function createNew(
        TableIdentifier $tableIdentifier,
        \DateTimeImmutable $startedAt
    ): self {
        return new self($tableIdentifier, $startedAt, $startedAt, 0);
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getFirstGameStartedAt(): \DateTimeImmutable
    {
        return $this->firstGameStartedAt;
    }

    public function getLatestGameStartedAt(): \DateTimeImmutable
    {
        return $this->latestGameStartedAt;
    }

    public function getGameCount(): int
    {
        return $this->gameCount;
    }

    public function incrementedAt(\DateTimeImmutable $dateTime): self
    {
        $result = clone $this;
        $result->latestGameStartedAt = $dateTime;
        ++$result->gameCount;

        return $result;
    }

    public function withGamesCounted(int $gameCount, \DateTimeImmutable $latestGameStartedAt): self
    {
        if ($gameCount > 0 && $latestGameStartedAt >= $this->firstGameStartedAt) {
            $result = clone $this;
            $result->gameCount = $gameCount;
            $result->latestGameStartedAt = $latestGameStartedAt;

            return $result;
        }

        throw IncorrectlyCounted::create($this->tableIdentifier, $gameCount, $latestGameStartedAt);
    }
}
