<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\GameCounter;

use App\Domain\WriteModel\Game\Event\GameStarted;
use RvaVzw\KrakBoem\EventSourcing\Projector\AbstractProjector;

final readonly class GameCounterProjector extends AbstractProjector
{
    public function __construct(
        private GameCounterRepository $gameCounterRepository,
    ) {
    }

    public function applyGameStarted(GameStarted $event): void
    {
        $counter = $this->gameCounterRepository->findCounterForTable($event->getTableIdentifier());
        if (null === $counter) {
            $counter = GameCounter::createNew($event->getTableIdentifier(), $event->getStartedAt());
        }

        $this->gameCounterRepository->saveGameCounter(
            $counter->incrementedAt($event->getStartedAt())
        );
    }
}
