<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\GameCounter;

use App\Domain\WriteModel\Table\TableIdentifier;

interface GameCounterRepository
{
    public function findCounterForTable(TableIdentifier $tableIdentifier): ?GameCounter;

    public function saveGameCounter(GameCounter $counter): void;
}
