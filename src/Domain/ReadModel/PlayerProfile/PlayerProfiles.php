<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\PlayerProfile;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;

interface PlayerProfiles
{
    public function getPlayerProfile(TableIdentifier $tableIdentifier, PlayerIdentifier $playerIdentifier): PlayerProfile;
}
