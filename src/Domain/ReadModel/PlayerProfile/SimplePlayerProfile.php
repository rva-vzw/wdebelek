<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\PlayerProfile;

use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;

final class SimplePlayerProfile implements PlayerProfile
{
    /** @var PlayerIdentifier */
    private $playerIdentifier;
    /** @var PlayerName */
    private $playerName;
    /** @var Language */
    private $language;

    public function __construct(
        PlayerIdentifier $playerIdentifier,
        PlayerName $playerName,
        Language $language
    ) {
        $this->playerIdentifier = $playerIdentifier;
        $this->playerName = $playerName;
        $this->language = $language;
    }

    public static function fromPlayerProfile(PlayerProfile $playerProfile): self
    {
        return new self(
            $playerProfile->getPlayerIdentifier(),
            $playerProfile->getPlayerName(),
            $playerProfile->getLanguage()
        );
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getPlayerName(): PlayerName
    {
        return $this->playerName;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }
}
