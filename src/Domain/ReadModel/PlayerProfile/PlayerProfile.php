<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\PlayerProfile;

use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\NamedPlayer;

interface PlayerProfile extends NamedPlayer
{
    public function getLanguage(): Language;
}
