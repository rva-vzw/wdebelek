<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\SeatedPlayer;

use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\ValueObject\Table\Seat;

interface SeatedPlayer extends PlayerProfile
{
    public function getSeat(): Seat;
}
