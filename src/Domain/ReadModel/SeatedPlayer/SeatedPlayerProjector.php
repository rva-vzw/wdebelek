<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\SeatedPlayer;

use App\Domain\WriteModel\Table\Event\InvitationAccepted;
use App\Domain\WriteModel\Table\Event\TableClaimed;
use RvaVzw\KrakBoem\EventSourcing\Projector\AbstractProjector;

final readonly class SeatedPlayerProjector extends AbstractProjector
{
    public function __construct(private SeatedPlayerRepository $repository)
    {
    }

    public function applyInvitationAccepted(InvitationAccepted $event): void
    {
        $this->repository->putPlayerAtTable(
            $event->getPlayerProfile(),
            $event->getTableIdentifier(),
            $event->getSeat()
        );
    }

    public function applyTableClaimed(TableClaimed $event): void
    {
        $this->repository->putPlayerAtTable(
            $event->getPlayerProfile(),
            $event->getTableIdentifier(),
            $event->getSeat()
        );
    }
}
