<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\SeatedPlayer;

use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\TableIdentifier;

interface SeatedPlayerRepository extends SeatedPlayers
{
    public function putPlayerAtTable(
        PlayerProfile $playerProfile,
        TableIdentifier $tableIdentifier,
        Seat $seat
    ): void;
}
