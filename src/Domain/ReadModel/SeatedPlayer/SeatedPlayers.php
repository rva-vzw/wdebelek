<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\SeatedPlayer;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;

interface SeatedPlayers
{
    public function getSeatedPlayer(TableIdentifier $tableIdentifier, PlayerIdentifier $playerIdentifier): SeatedPlayer;
}
