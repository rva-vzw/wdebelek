<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\SeatedPlayer;

use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Table\Seat;

final class SimpleSeatedPlayer implements SeatedPlayer
{
    /** @var PlayerIdentifier */
    private $playerIdentifier;
    /** @var PlayerName */
    private $playerName;
    /** @var Language */
    private $language;
    /** @var Seat */
    private $seat;

    public function __construct(
        PlayerIdentifier $playerIdentifier,
        PlayerName $playerName,
        Language $language,
        Seat $seat
    ) {
        $this->playerIdentifier = $playerIdentifier;
        $this->playerName = $playerName;
        $this->language = $language;
        $this->seat = $seat;
    }

    public static function fromProfile(PlayerProfile $profile, Seat $seat): self
    {
        return new self(
            $profile->getPlayerIdentifier(),
            $profile->getPlayerName(),
            $profile->getLanguage(),
            $seat
        );
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getPlayerName(): PlayerName
    {
        return $this->playerName;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }

    public function getSeat(): Seat
    {
        return $this->seat;
    }
}
