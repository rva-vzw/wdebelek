<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\Exception;

use RvaVzw\KrakBoem\Id\Identifier;

final class NotFoundInReadModel extends \Exception
{
    public static function create(string $class, Identifier $identifier): self
    {
        return new self(
            "Item {$identifier->toString()} not found in $class read model."
        );
    }
}
