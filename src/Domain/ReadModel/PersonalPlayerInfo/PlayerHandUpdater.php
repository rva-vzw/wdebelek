<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\PersonalPlayerInfo;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\PileOfCards;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;

interface PlayerHandUpdater
{
    public function emptyHand(
        PlayerIdentifier $playerIdentifier,
        TableIdentifier $tableIdentifier
    ): void;

    public function receiveCards(
        PlayerIdentifier $playerIdentifier,
        PileOfCards $cards,
        GameIdentifier $gameIdentifier
    ): void;

    public function replaceCards(
        PlayerIdentifier $playerIdentifier,
        PileOfCards $pileOfCards,
        GameIdentifier $gameIdentifier
    ): void;

    public function removeCard(
        PlayerIdentifier $playerIdentifier,
        Card $card,
        TableIdentifier $tableIdentifier
    ): void;
}
