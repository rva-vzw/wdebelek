<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\PersonalPlayerInfo\Exception;

use App\Domain\ValueObject\Player\PlayerIdentifier;

final class NotPlaying extends \Exception
{
    public static function create(PlayerIdentifier $playerIdentifier): self
    {
        return new self(
            "Player {$playerIdentifier->toString()} is not playing."
        );
    }
}
