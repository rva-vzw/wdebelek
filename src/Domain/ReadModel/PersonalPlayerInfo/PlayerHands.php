<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\PersonalPlayerInfo;

use App\Domain\ValueObject\Card\Hand;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;

interface PlayerHands
{
    public function getPlayerHand(
        PlayerIdentifier $playerIdentifier,
        TableIdentifier $tableIdentifier,
        GameIdentifier $gameIdentifier
    ): Hand;
}
