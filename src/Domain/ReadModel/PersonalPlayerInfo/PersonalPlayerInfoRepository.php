<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\PersonalPlayerInfo;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;

interface PersonalPlayerInfoRepository
{
    public function getPersonalPlayerInfo(
        PlayerIdentifier $playerIdentifier,
        TableIdentifier $tableIdentifier
    ): PersonalPlayerInfo;

    public function savePersonalPlayerInfo(
        PersonalPlayerInfo $playerInfo
    ): void;

    /**
     * @return \Traversable<PersonalPlayerInfo>
     */
    public function getPlayerInfosByTable(TableIdentifier $tableIdentifier): \Traversable;
}
