<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\PersonalPlayerInfo;

use App\Domain\ValueObject\Card\Hand;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\WriteModel\Game\Event\CardPickedUp;
use App\Domain\WriteModel\Game\Event\CardPlayed;
use App\Domain\WriteModel\Game\Event\CardsReceived;
use App\Domain\WriteModel\Game\Event\CardsUnrevealed;
use App\Domain\WriteModel\Game\Event\Dealt;
use App\Domain\WriteModel\Game\Event\GameStarted;
use App\Domain\WriteModel\Game\Event\HandRevealed;
use App\Domain\WriteModel\Game\Event\HandSorted;
use App\Domain\WriteModel\Game\Event\InitialHandShown;
use App\Domain\WriteModel\Table\Event\CardsCollected;
use App\Domain\WriteModel\Table\Event\InvitationAccepted;
use App\Domain\WriteModel\Table\Event\TableClaimed;
use RvaVzw\KrakBoem\EventSourcing\Projector\AbstractProjector;

final readonly class PersonalPlayerInfoProjector extends AbstractProjector
{
    public function __construct(
        private PlayerHandUpdater $playerHandUpdater,
        private PersonalPlayerInfoRepository $repository
    ) {
    }

    public function applyGameStarted(GameStarted $event): void
    {
        foreach ($event->getPlayers() as $player) {
            $personalPlayerInfo = $this->repository->getPersonalPlayerInfo(
                $player,
                $event->getTableIdentifier()
            );
            $this->repository->savePersonalPlayerInfo(
                $personalPlayerInfo->playingGame($event->getGameIdentifier())
            );
        }
    }

    public function applyCardsReceived(CardsReceived $event): void
    {
        $this->playerHandUpdater->receiveCards(
            $event->getPlayerIdentifier(),
            $event->getCards(),
            $event->getGameIdentifier()
        );
    }

    public function applyHandSorted(HandSorted $event): void
    {
        $this->playerHandUpdater->replaceCards(
            $event->getPlayerIdentifier(),
            $event->getHand(),
            $event->getGameIdentifier()
        );
    }

    public function applyCardPlayed(CardPlayed $event): void
    {
        $this->playerHandUpdater->removeCard(
            $event->getPlayerIdentifier(),
            $event->getCard(),
            // FIXME: maybe revert this to game identifier to be consistent.
            $event->getTableIdentifier()
        );
    }

    public function applyCardPickedUp(CardPickedUp $event): void
    {
        $this->playerHandUpdater->receiveCards(
            $event->getPlayerIdentifier(),
            SimplePileOfCards::singleCard($event->getCard()),
            $event->getGameIdentifier()
        );
    }

    public function applyInvitationAccepted(InvitationAccepted $event): void
    {
        $this->repository->savePersonalPlayerInfo(
            PersonalPlayerInfo::forPlayerAtTable(
                Hand::empty(),
                $event->getPlayerIdentifier(),
                $event->getTableIdentifier()
            )
        );
    }

    public function applyTableClaimed(TableClaimed $event): void
    {
        $this->repository->savePersonalPlayerInfo(
            PersonalPlayerInfo::forPlayerAtTable(
                Hand::empty(),
                $event->getPlayerIdentifier(),
                $event->getTableIdentifier()
            )
        );
    }

    public function applyDealt(Dealt $event): void
    {
        $personalPlayerInfo = $this->repository->getPersonalPlayerInfo(
            $event->getLastPlayer(),
            $event->getTableIdentifier()
        );

        $this->repository->savePersonalPlayerInfo(
            $personalPlayerInfo->withTrumpCard($event->getLastCardDealt())
        );
    }

    public function applyInitialHandShown(InitialHandShown $event): void
    {
        $this->playerHandUpdater->replaceCards(
            $event->getPlayerIdentifier(),
            $event->getHand(),
            $event->getGameIdentifier()
        );
    }

    public function applyHandRevealed(HandRevealed $event): void
    {
        $personalPlayerInfo = $this->repository->getPersonalPlayerInfo(
            $event->getPlayerIdentifier(),
            $event->getTableIdentifier()
        );

        $this->repository->savePersonalPlayerInfo(
            $personalPlayerInfo->withCardsOpen()
        );
    }

    public function applyCardsUnrevealed(CardsUnrevealed $event): void
    {
        $personalPlayerInfo = $this->repository->getPersonalPlayerInfo(
            $event->getPlayerIdentifier(),
            $event->getTableIdentifier()
        );

        $this->repository->savePersonalPlayerInfo(
            $personalPlayerInfo->withCardsUnrevealed()
        );
    }

    public function applyCardsCollected(CardsCollected $event): void
    {
        $playerInfos = $this->repository->getPlayerInfosByTable($event->getTableIdentifier());

        /** @var PersonalPlayerInfo $playerInfo */
        foreach ($playerInfos as $playerInfo) {
            $this->repository->savePersonalPlayerInfo($playerInfo->emptyHanded());
        }
    }
}
