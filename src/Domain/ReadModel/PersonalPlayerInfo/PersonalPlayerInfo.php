<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\PersonalPlayerInfo;

use App\Domain\ReadModel\PersonalPlayerInfo\Exception\NotPlaying;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\Hand;
use App\Domain\ValueObject\Player\Exception\NoTrumpCard;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;

final class PersonalPlayerInfo
{
    // Maybe the hand shouldn't be here? It could be moved to RevealedCards.
    /** @var Hand */
    private $hand;
    /** @var Card|null */
    private $trumpCard;
    /** @var GameIdentifier|null */
    private $gameIdentifier;
    /** @var PlayerIdentifier */
    private $playerIdentifier;
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var bool */
    private $cardsOpen;

    public function __construct(
        Hand $hand,
        ?Card $trumpCard,
        ?GameIdentifier $gameIdentifier,
        PlayerIdentifier $playerIdentifier,
        TableIdentifier $tableIdentifier,
        bool $cardsOpen
    ) {
        $this->hand = $hand;
        $this->trumpCard = $trumpCard;
        $this->gameIdentifier = $gameIdentifier;
        $this->playerIdentifier = $playerIdentifier;
        $this->tableIdentifier = $tableIdentifier;
        $this->cardsOpen = $cardsOpen;
    }

    public static function forPlayerAtTable(
        Hand $hand,
        PlayerIdentifier $playerIdentifier,
        TableIdentifier $tableIdentifier
    ): self {
        return new self($hand, null, null, $playerIdentifier, $tableIdentifier, false);
    }

    public function getHand(): Hand
    {
        return $this->hand;
    }

    public function getTrumpCard(): Card
    {
        if ($this->trumpCard instanceof Card) {
            return $this->trumpCard;
        }

        throw NoTrumpCard::create();
    }

    public function hasTrumpCard(): bool
    {
        return $this->trumpCard instanceof Card;
    }

    public function withTrumpCard(Card $card): self
    {
        $result = clone $this;
        $result->trumpCard = $card;

        return $result;
    }

    public function playingGame(GameIdentifier $gameIdentifier): self
    {
        $result = clone $this;
        $result->gameIdentifier = $gameIdentifier;

        return $result;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function isPlaying(): bool
    {
        return $this->gameIdentifier instanceof GameIdentifier;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        if ($this->gameIdentifier instanceof GameIdentifier) {
            return $this->gameIdentifier;
        }

        throw NotPlaying::create($this->playerIdentifier);
    }

    public function withHand(Hand $hand): self
    {
        $result = clone $this;
        $result->hand = $hand;

        return $result;
    }

    public function withoutTrumpCard(): self
    {
        $result = clone $this;
        $result->trumpCard = null;

        return $result;
    }

    public function hasCardsOpen(): bool
    {
        return $this->cardsOpen;
    }

    public function withCardsOpen(): self
    {
        $result = clone $this;
        $result->cardsOpen = true;

        return $result;
    }

    public function emptyHanded(): self
    {
        $result = clone $this;
        $result->hand = Hand::empty();
        $result->trumpCard = null;
        $result->cardsOpen = false;

        return $result;
    }

    public function withCardsUnrevealed(): self
    {
        $result = clone $this;
        $result->cardsOpen = false;

        return $result;
    }
}
