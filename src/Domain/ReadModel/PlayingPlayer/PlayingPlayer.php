<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\PlayingPlayer;

use App\Domain\ReadModel\SeatedPlayer\SeatedPlayer;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Game\Exception\TrickNotFound;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\TableIdentifier;

/**
 * A player that's currently playing at the table.
 */
final class PlayingPlayer
{
    /** @var int */
    private $cardsInHand;
    /** @var int */
    private $tricksWon;
    /** @var bool */
    private $lastPlayer;
    /** @var PlayerName */
    private $playerName;
    /** @var PlayerIdentifier */
    private $playerIdentifier;
    /** @var Card|null */
    private $shownTrump;
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var Seat */
    private $seat;

    public function __construct(
        int $cardsInHand,
        int $tricksWon,
        bool $lastPlayer,
        PlayerName $playerName,
        PlayerIdentifier $playerIdentifier,
        ?Card $shownTrump,
        TableIdentifier $tableIdentifier,
        Seat $seat
    ) {
        if ($cardsInHand < 0 || $tricksWon < 0) {
            throw new \InvalidArgumentException();
        }

        $this->cardsInHand = $cardsInHand;
        $this->tricksWon = $tricksWon;
        $this->lastPlayer = $lastPlayer;
        $this->playerName = $playerName;
        $this->playerIdentifier = $playerIdentifier;
        $this->shownTrump = $shownTrump;
        $this->tableIdentifier = $tableIdentifier;
        $this->seat = $seat;
    }

    public function getCardsInHand(): int
    {
        return $this->cardsInHand;
    }

    public function getTricksWon(): int
    {
        return $this->tricksWon;
    }

    public static function create(
        TableIdentifier $tableIdentifier,
        SeatedPlayer $seatedPlayer
    ): self {
        return new self(
            0,
            0,
            false,
            $seatedPlayer->getPlayerName(),
            $seatedPlayer->getPlayerIdentifier(),
            null,
            $tableIdentifier,
            $seatedPlayer->getSeat()
        );
    }

    public function withCardPlayed(Card $card): self
    {
        $result = clone $this;
        --$result->cardsInHand;

        if ($result->shownTrump == $card) {
            $result->shownTrump = null;
        }

        return $result;
    }

    public function withAdditionalCards(int $numberOfCards): self
    {
        $result = clone $this;
        $result->cardsInHand += $numberOfCards;

        return $result;
    }

    public function withAdditionalTrick(): self
    {
        $result = clone $this;
        ++$result->tricksWon;

        return $result;
    }

    public function isLastPlayer(): bool
    {
        return $this->lastPlayer;
    }

    public function asLastPlayer(): self
    {
        $result = clone $this;
        $result->lastPlayer = true;

        return $result;
    }

    public function asNonLastPlayer(): self
    {
        $result = clone $this;
        $result->lastPlayer = false;

        return $result;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getPlayerName(): PlayerName
    {
        return $this->playerName;
    }

    public function withAdditionalCard(): self
    {
        $result = clone $this;
        ++$result->cardsInHand;

        return $result;
    }

    /**
     * Returns trump card if it is shown to the other players.
     *
     * FIXME: I don't like nullable properties. Maybe I need something as Card::none()?
     */
    public function getShownTrump(): ?Card
    {
        return $this->shownTrump;
    }

    public function withTrumpRevealed(Card $card): self
    {
        $result = clone $this;
        $result->shownTrump = $card;

        return $result;
    }

    public function withTrumpHidden(): self
    {
        $result = clone $this;
        $result->shownTrump = null;

        return $result;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getSeat(): Seat
    {
        return $this->seat;
    }

    public function withOneTrickLess(): self
    {
        if ($this->tricksWon > 0) {
            $result = clone $this;
            --$result->tricksWon;

            return $result;
        }

        throw TrickNotFound::noTricks();
    }
}
