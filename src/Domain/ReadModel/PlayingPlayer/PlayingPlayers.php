<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\PlayingPlayer;

use App\Domain\WriteModel\Table\TableIdentifier;

interface PlayingPlayers
{
    /**
     * @return \Traversable<PlayingPlayer>
     */
    public function getOrderedPlayingPlayers(TableIdentifier $tableIdentifier): \Traversable;
}
