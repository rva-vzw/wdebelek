<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\PlayingPlayer;

use App\Domain\WriteModel\Table\TableIdentifier;

interface PlayerCounts
{
    public function getPlayerCount(TableIdentifier $tableIdentifier): int;
}
