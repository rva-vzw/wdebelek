<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\PlayingPlayer;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;

interface PlayingPlayerRepository extends PlayingPlayers
{
    public function getPlayingPlayer(
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $playerIdentifier
    ): PlayingPlayer;

    public function savePlayingPlayer(PlayingPlayer $fellowPlayer): void;

    public function deleteForTable(TableIdentifier $tableIdentifier): void;
}
