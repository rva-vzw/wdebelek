<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\PlayingPlayer;

use App\Domain\ReadModel\SeatedPlayer\SeatedPlayers;
use App\Domain\ValueObject\Card\Card;
use App\Domain\WriteModel\Game\Event\CardPickedUp;
use App\Domain\WriteModel\Game\Event\CardPlayed;
use App\Domain\WriteModel\Game\Event\CardsReceived;
use App\Domain\WriteModel\Game\Event\Dealt;
use App\Domain\WriteModel\Game\Event\GameAborted;
use App\Domain\WriteModel\Game\Event\GameCanceled;
use App\Domain\WriteModel\Game\Event\GameEnded;
use App\Domain\WriteModel\Game\Event\GameStarted;
use App\Domain\WriteModel\Game\Event\TrickReopened;
use App\Domain\WriteModel\Game\Event\TrickWon;
use App\Domain\WriteModel\Game\Event\TrumpRevealed;
use RvaVzw\KrakBoem\EventSourcing\Projector\AbstractProjector;

final readonly class PlayingPlayerProjector extends AbstractProjector
{
    public function __construct(
        private PlayingPlayerRepository $repository,
        private SeatedPlayers $seatedPlayers
    ) {
    }

    public function applyCardPlayed(CardPlayed $event): void
    {
        $player = $this->repository->getPlayingPlayer(
            $event->getTableIdentifier(),
            $event->getPlayerIdentifier()
        );

        $this->repository->savePlayingPlayer(
            $player->withCardPlayed($event->getCard())
        );
    }

    public function applyCardPickedUp(CardPickedUp $event): void
    {
        $player = $this->repository->getPlayingPlayer(
            $event->getTableIdentifier(),
            $event->getPlayerIdentifier()
        );

        $this->repository->savePlayingPlayer(
            $player->withAdditionalCard()
        );
    }

    public function applyGameStarted(GameStarted $event): void
    {
        foreach ($event->getPlayers() as $playerIdentifier) {
            $this->repository->savePlayingPlayer(
                PlayingPlayer::create(
                    $event->getTableIdentifier(),
                    $this->seatedPlayers->getSeatedPlayer(
                        $event->getTableIdentifier(),
                        $playerIdentifier
                    )
                )
            );
        }
    }

    public function applyCardsReceived(CardsReceived $event): void
    {
        $player = $this->repository->getPlayingPlayer(
            $event->getTableIdentifier(),
            $event->getPlayerIdentifier()
        );

        $this->repository->savePlayingPlayer(
            $player->withAdditionalCards($event->getCards()->count())
        );
    }

    public function applyDealt(Dealt $event): void
    {
        $player = $this->repository->getPlayingPlayer(
            $event->getTableIdentifier(),
            $event->getLastPlayer()
        );

        $this->repository->savePlayingPlayer(
            $player->asLastPlayer()
        );
    }

    public function applyTrumpRevealed(TrumpRevealed $event): void
    {
        $player = $this->repository->getPlayingPlayer(
            $event->getTableIdentifier(),
            $event->getLastPlayer()
        );

        $this->repository->savePlayingPlayer(
            $player->withTrumpRevealed($event->getTrump())
        );
    }

    public function applyGameEnded(GameEnded $event): void
    {
        $this->repository->deleteForTable($event->getTableIdentifier());
    }

    public function applyGameCanceled(GameCanceled $event): void
    {
        $this->repository->deleteForTable($event->getTableIdentifier());
    }

    public function applyGameAborted(GameAborted $event): void
    {
        $this->repository->deleteForTable($event->getTableIdentifier());
    }

    public function applyTrickWon(TrickWon $event): void
    {
        $players = $this->repository->getOrderedPlayingPlayers($event->getTableIdentifier());

        /** @var PlayingPlayer $player */
        foreach ($players as $player) {
            if ($player->getPlayerIdentifier() == $event->getWinner()) {
                $this->repository->savePlayingPlayer(
                    $player->withAdditionalTrick()->withTrumpHidden()
                );
            } elseif ($player->getShownTrump() instanceof Card) {
                $this->repository->savePlayingPlayer(
                    $player->withTrumpHidden()
                );
            }
        }
    }

    public function applyTrickReopened(TrickReopened $event): void
    {
        $player = $this->repository->getPlayingPlayer(
            $event->getTableIdentifier(),
            $event->getWinner()
        );

        $this->repository->savePlayingPlayer(
            $player->withOneTrickLess()
        );
    }
}
