<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\OpenInvitations;

use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\WriteModel\Table\TableIdentifier;

interface OpenInvitations
{
    public function hasOpenInvitation(TableIdentifier $tableIdentifier, Invitation $invitation): bool;
}
