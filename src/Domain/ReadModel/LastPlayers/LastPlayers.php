<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\LastPlayers;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;

interface LastPlayers
{
    public function getLastPlayerByGame(GameIdentifier $gameIdentifier): PlayerIdentifier;
}
