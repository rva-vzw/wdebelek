<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\PlayersAtTable;

use App\Domain\ValueObject\Player\SimpleNamedPlayer;
use App\Domain\WriteModel\Table\TableIdentifier;

interface PlayersAtTable
{
    /**
     * @return \Traversable<SimpleNamedPlayer>
     */
    public function getPlayersAtTable(TableIdentifier $tableIdentifier): \Traversable;
}
