<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\OccupiedSeats;

use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\ValueObject\Invitation\Exception\InvitationNotAccepted;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Invitation\InvitationPrivacy;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\Exception\InvitationNotFound;
use App\Domain\WriteModel\Table\TableIdentifier;

final class OccupiedSeat implements PlayerProfile
{
    /** @var PlayerIdentifier|null */
    private $playerIdentifier;
    /** @var PlayerName */
    private $playerName;
    /** @var string */
    private $invitationUrl;
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var Seat */
    private $seat;
    /** @var Invitation|null */
    private $invitation;
    /** @var PlayerName */
    private $hostName;
    /** @var Language */
    private $language;
    /** @var InvitationPrivacy */
    private $invitationPrivacy;

    public function __construct(
        TableIdentifier $tableIdentifier,
        ?PlayerIdentifier $playerIdentifier,
        PlayerName $playerName,
        string $invitationUrl,
        Seat $seat,
        ?Invitation $invitation,
        PlayerName $hostName,
        Language $language,
        InvitationPrivacy $invitationPrivacy
    ) {
        $this->playerIdentifier = $playerIdentifier;
        $this->playerName = $playerName;
        $this->invitationUrl = $invitationUrl;
        $this->tableIdentifier = $tableIdentifier;
        $this->seat = $seat;
        $this->invitation = $invitation;
        $this->hostName = $hostName;
        $this->language = $language;
        $this->invitationPrivacy = $invitationPrivacy;
    }

    public static function initial(
        TableIdentifier $tableIdentifier,
        PlayerProfile $playerProfile
    ): self {
        return new self(
            $tableIdentifier,
            $playerProfile->getPlayerIdentifier(),
            $playerProfile->getPlayerName(),
            '',
            Seat::zero(),
            null,
            $playerProfile->getPlayerName(),
            $playerProfile->getLanguage(),
            InvitationPrivacy::expired()
        );
    }

    public static function invited(
        TableIdentifier $tableIdentifier,
        string $invitationLink,
        Seat $seat,
        Invitation $invitation,
        PlayerName $hostName,
        Language $language
    ): self {
        return new self(
            $tableIdentifier,
            null,
            PlayerName::unknown(),
            $invitationLink,
            $seat,
            $invitation,
            $hostName,
            $language,
            InvitationPrivacy::privateInvitation()
        );
    }

    public static function accepted(
        TableIdentifier $tableIdentifier,
        PlayerProfile $playerProfile,
        PlayerName $hostName,
        Seat $seat
    ): self {
        return new self(
            $tableIdentifier,
            $playerProfile->getPlayerIdentifier(),
            $playerProfile->getPlayerName(),
            '',
            $seat,
            null,
            $hostName,
            $playerProfile->getLanguage(),
            InvitationPrivacy::expired()
        );
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        if ($this->playerIdentifier instanceof PlayerIdentifier) {
            return $this->playerIdentifier;
        }

        throw InvitationNotAccepted::create();
    }

    public function getPlayerName(): PlayerName
    {
        return $this->playerName;
    }

    public function getInvitationUrl(): string
    {
        return $this->invitationUrl;
    }

    public function hasAccepted(): bool
    {
        return $this->playerIdentifier instanceof PlayerIdentifier;
    }

    public function getSeat(): Seat
    {
        return $this->seat;
    }

    public function acceptedBy(PlayerIdentifier $playerIdentifier, PlayerName $playerName): self
    {
        $result = clone $this;

        $result->invitationUrl = '';
        $result->playerIdentifier = $playerIdentifier;
        $result->playerName = $playerName;
        $result->invitationPrivacy = InvitationPrivacy::expired();

        return $result;
    }

    public function invitationPending(): bool
    {
        return $this->invitation instanceof Invitation;
    }

    public function getInvitation(): Invitation
    {
        if ($this->invitation instanceof Invitation) {
            return $this->invitation;
        }

        throw InvitationNotFound::forUnknownInvitation();
    }

    public function getHostName(): PlayerName
    {
        return $this->hostName;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }

    public function getInvitationPrivacy(): InvitationPrivacy
    {
        return $this->invitationPrivacy;
    }

    public function withPublishedInvitation(): self
    {
        $result = clone $this;
        $result->invitationPrivacy = InvitationPrivacy::publicInvitation();

        return $result;
    }

    public function withUnpublishedInvitation(): self
    {
        $result = clone $this;
        $result->invitationPrivacy = InvitationPrivacy::privateInvitation();

        return $result;
    }
}
