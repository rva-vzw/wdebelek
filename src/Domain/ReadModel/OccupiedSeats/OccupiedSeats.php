<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\OccupiedSeats;

use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;

interface OccupiedSeats
{
    /**
     * @return \Traversable<OccupiedSeat>
     */
    public function getOccupiedSeats(TableIdentifier $tableIdentifier): \Traversable;

    public function getSeatByOpenInvitation(TableIdentifier $tableIdentifier, Invitation $invitation): OccupiedSeat;

    public function getSeatByPlayer(TableIdentifier $tableIdentifier, PlayerIdentifier $playerIdentifier): OccupiedSeat;
}
