<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\OccupiedSeats;

interface OccupiedSeatRepository extends OccupiedSeats, OccupiedSeatUpdater
{
}
