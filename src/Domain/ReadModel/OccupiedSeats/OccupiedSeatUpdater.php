<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\OccupiedSeats;

interface OccupiedSeatUpdater
{
    public function saveOccupiedSeat(OccupiedSeat $occupiedSeat): void;

    public function delete(OccupiedSeat $occupiedSeat): void;
}
