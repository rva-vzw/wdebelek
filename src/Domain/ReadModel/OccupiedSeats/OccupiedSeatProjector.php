<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\OccupiedSeats;

use App\Domain\ReadModel\PlayerProfile\PlayerProfiles;
use App\Domain\WriteModel\Table\Event\InvitationAccepted;
use App\Domain\WriteModel\Table\Event\InvitationPublished;
use App\Domain\WriteModel\Table\Event\InvitationUnpublished;
use App\Domain\WriteModel\Table\Event\PlayerInvited;
use App\Domain\WriteModel\Table\Event\PlayerKicked;
use App\Domain\WriteModel\Table\Event\TableClaimed;
use RvaVzw\KrakBoem\EventSourcing\Projector\AbstractProjector;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

final readonly class OccupiedSeatProjector extends AbstractProjector
{
    public function __construct(
        private OccupiedSeatRepository $repository,
        private RouterInterface $router,
        private PlayerProfiles $playerProfiles
    ) {
    }

    public function applyTableClaimed(TableClaimed $event): void
    {
        $this->repository->saveOccupiedSeat(
            OccupiedSeat::initial(
                $event->getTableIdentifier(),
                $event->getPlayerProfile()
            )
        );
    }

    public function applyPlayerInvited(PlayerInvited $event): void
    {
        // Manually prefixing the invitation link with https is ugly,
        // but I can't think of a better way right now.
        $this->repository->saveOccupiedSeat(
            OccupiedSeat::invited(
                $event->getTableIdentifier(),
                'https:'.$this->router->generate(
                    'wdebelek.accept.private',
                    [
                        'invitation' => $event->getInvitation()->toString(),
                        'tableId' => $event->getTableIdentifier()->toString(),
                        '_locale' => $event->getLanguage()->toString(),
                    ],
                    UrlGeneratorInterface::NETWORK_PATH
                ),
                $event->getSeat(),
                $event->getInvitation(),
                $this->playerProfiles->getPlayerProfile($event->getTableIdentifier(), $event->getHost())->getPlayerName(),
                $event->getLanguage()
            )
        );
    }

    public function applyInvitationAccepted(InvitationAccepted $event): void
    {
        $seat = $this->repository->getSeatByOpenInvitation(
            $event->getTableIdentifier(),
            $event->getInvitation()
        );

        $this->repository->saveOccupiedSeat(
            $seat->acceptedBy(
                $event->getPlayerIdentifier(),
                $event->getPlayerName()
            )
        );
    }

    public function applyPlayerKicked(PlayerKicked $event): void
    {
        $seat = $this->repository->getSeatByPlayer(
            $event->getTableIdentifier(),
            $event->getPlayerIdentifier()
        );
        $this->repository->delete($seat);
    }

    public function applyInvitationPublished(InvitationPublished $event): void
    {
        $seat = $this->repository->getSeatByOpenInvitation(
            $event->getTableIdentifier(),
            $event->getInvitation()
        );

        $this->repository->saveOccupiedSeat(
            $seat->withPublishedInvitation()
        );
    }

    public function applyInvitationUnpublished(InvitationUnpublished $event): void
    {
        $seat = $this->repository->getSeatByOpenInvitation(
            $event->getTableIdentifier(),
            $event->getInvitation()
        );

        $this->repository->saveOccupiedSeat(
            $seat->withUnpublishedInvitation()
        );
    }
}
