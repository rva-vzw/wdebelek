<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\RevealedCards;

use App\Domain\ReadModel\PersonalPlayerInfo\PlayerHands;
use App\Domain\ReadModel\PlayersAtTable\PlayersAtTable;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Player\SimpleNamedPlayer;
use App\Domain\WriteModel\Game\Event\CardPickedUp;
use App\Domain\WriteModel\Game\Event\CardPlayed;
use App\Domain\WriteModel\Game\Event\CardsUnrevealed;
use App\Domain\WriteModel\Game\Event\HandRevealed;
use App\Domain\WriteModel\Game\Event\HandShownToSpectator;
use App\Domain\WriteModel\Game\Event\InitialHandShown;
use App\Domain\WriteModel\Table\Event\CardsCollected;
use RvaVzw\KrakBoem\EventSourcing\Projector\AbstractProjector;

/**
 * FIXME: This projector needs to do a lot of work when simple things happen (throwing or picking up cards).
 */
final readonly class RevealedCardsProjector extends AbstractProjector
{
    public function __construct(
        private PlayerHands $playerHands,
        private RevealedCardsRepository $repository,
        private PlayersAtTable $playersAtTable
    ) {
    }

    public function applyHandShownToSpectator(HandShownToSpectator $event): void
    {
        $shownHand = new RevealedCards(
            $event->getGameIdentifier(),
            $event->getPlayerIdentifier(),
            $event->getSpectatorIdentifier(),
            $event->getTableIdentifier(),
            SimplePileOfCards::fromCards($this->playerHands->getPlayerHand(
                $event->getPlayerIdentifier(),
                $event->getTableIdentifier(),
                $event->getGameIdentifier()
            ))
        );

        $this->repository->saveRevealedCards($shownHand);
    }

    public function applyCardPlayed(CardPlayed $event): void
    {
        /** @var RevealedCards $shownHand */
        foreach ($this->repository->getRevealedByPlayer($event->getPlayerIdentifier(), $event->getGameIdentifier()) as $shownHand) {
            $this->repository->saveRevealedCards(
                $shownHand->withoutCard($event->getCard())
            );
        }
    }

    public function applyCardPickedUp(CardPickedUp $event): void
    {
        /* @var SimpleNamedPlayer $viewer */
        foreach ($this->playersAtTable->getPlayersAtTable($event->getTableIdentifier()) as $viewer) {
            $revealedCards = $this->repository->getOrCreateRevealedCards(
                $event->getGameIdentifier(),
                $event->getPlayerIdentifier(),
                $viewer->getPlayerIdentifier(),
                $event->getTableIdentifier()
            );
            $this->repository->saveRevealedCards($revealedCards->withCard($event->getCard()));
        }
    }

    public function applyCardsCollected(CardsCollected $event): void
    {
        /** @var RevealedCards $shownHand */
        foreach ($this->repository->getRevealedCardsAtTable($event->getTableIdentifier()) as $shownHand) {
            $this->repository->deleteRevealedCards(
                $shownHand
            );
        }
    }

    public function applyHandRevealed(HandRevealed $event): void
    {
        /** @var SimpleNamedPlayer $viewer */
        foreach ($this->playersAtTable->getPlayersAtTable($event->getTableIdentifier()) as $viewer) {
            $revealedCards = new RevealedCards(
                $event->getGameIdentifier(),
                $event->getPlayerIdentifier(),
                $viewer->getPlayerIdentifier(),
                $event->getTableIdentifier(),
                SimplePileOfCards::fromCards($event->getHand())
            );

            $this->repository->saveRevealedCards($revealedCards);
        }
    }

    public function applyInitialHandShown(InitialHandShown $event): void
    {
        /** @var SimpleNamedPlayer $viewer */
        foreach ($this->playersAtTable->getPlayersAtTable($event->getTableIdentifier()) as $viewer) {
            $revealedCards = new RevealedCards(
                $event->getGameIdentifier(),
                $event->getPlayerIdentifier(),
                $viewer->getPlayerIdentifier(),
                $event->getTableIdentifier(),
                SimplePileOfCards::fromCards($event->getHand())
            );

            $this->repository->saveRevealedCards($revealedCards);
        }
    }

    public function applyCardsUnrevealed(CardsUnrevealed $event): void
    {
        foreach ($this->repository->getRevealedByPlayer(
            $event->getPlayerIdentifier(),
            $event->getGameIdentifier()
        ) as $revealedCards) {
            $this->repository->deleteRevealedCards($revealedCards);
        }
    }
}
