<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\RevealedCards;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;

interface CardsRevealed
{
    /**
     * @return \Traversable<RevealedCards>
     */
    public function getCardsRevealedTo(PlayerIdentifier $playerIdentifier, GameIdentifier $gameIdentifier): \Traversable;
}
