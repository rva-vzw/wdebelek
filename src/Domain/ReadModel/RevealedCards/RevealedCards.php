<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\RevealedCards;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;

final class RevealedCards
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var PlayerIdentifier */
    private $playerIdentifier;
    /** @var PlayerIdentifier */
    private $revealedToPlayerIdentifier;
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var SimplePileOfCards */
    private $cards;

    public function __construct(
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $playerIdentifier,
        PlayerIdentifier $revealedToPlayerIdentifier,
        TableIdentifier $tableIdentifier,
        SimplePileOfCards $cards
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->playerIdentifier = $playerIdentifier;
        $this->revealedToPlayerIdentifier = $revealedToPlayerIdentifier;
        $this->tableIdentifier = $tableIdentifier;
        $this->cards = $cards;
    }

    public static function none(
        TableIdentifier $tableIdentifier,
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $playerIdentifier,
        PlayerIdentifier $revealedToPlayerIdentifier
    ): self {
        return new self($gameIdentifier, $playerIdentifier, $revealedToPlayerIdentifier, $tableIdentifier, SimplePileOfCards::empty());
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getRevealedToPlayerIdentifier(): PlayerIdentifier
    {
        return $this->revealedToPlayerIdentifier;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function withoutCard(Card $card): self
    {
        $result = clone $this;
        $result->cards = $result->cards->without($card);

        return $result;
    }

    public function withCard(Card $card): self
    {
        $result = clone $this;
        $result->cards = $result->cards->with($card);

        return $result;
    }

    public function getCards(): SimplePileOfCards
    {
        return $this->cards;
    }

    public function isEmpty(): bool
    {
        return $this->cards->isEmpty();
    }
}
