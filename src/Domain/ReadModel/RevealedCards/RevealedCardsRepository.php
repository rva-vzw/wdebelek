<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\RevealedCards;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;

interface RevealedCardsRepository
{
    public function saveRevealedCards(RevealedCards $revealedCards): void;

    /**
     * @return \Traversable<RevealedCards>
     */
    public function getRevealedByPlayer(
        PlayerIdentifier $playerIdentifier,
        GameIdentifier $gameIdentifier
    ): \Traversable;

    /**
     * @return \Traversable<RevealedCards>
     */
    public function getRevealedCardsAtTable(TableIdentifier $tableIdentifier): \Traversable;

    public function deleteRevealedCards(RevealedCards $shownHand): void;

    public function getOrCreateRevealedCards(
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $playerIdentifier,
        PlayerIdentifier $revealedToPlayerIdentifier,
        TableIdentifier $tableIdentifier
    ): RevealedCards;
}
