<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\ActiveTables;

use App\Domain\ReadModel\TableState\TableState;
use App\Domain\WriteModel\Table\TableIdentifier;

interface ActiveTables
{
    public function getTableState(TableIdentifier $tableIdentifier): TableState;

    public function hasActiveTable(TableIdentifier $tableIdentifier): bool;
}
