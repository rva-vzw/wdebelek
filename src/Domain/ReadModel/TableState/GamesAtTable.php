<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TableState;

use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;

interface GamesAtTable
{
    public function getTableByCurrentGame(GameIdentifier $gameIdentifier): TableIdentifier;
}
