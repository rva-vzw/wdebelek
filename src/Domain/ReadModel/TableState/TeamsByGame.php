<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TableState;

use App\Domain\ValueObject\Player\Teams;
use App\Domain\WriteModel\Game\GameIdentifier;

interface TeamsByGame
{
    public function getTeamsByGame(GameIdentifier $gameIdentifier): Teams;
}
