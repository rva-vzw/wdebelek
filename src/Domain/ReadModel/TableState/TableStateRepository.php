<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TableState;

use App\Domain\ReadModel\ActiveTables\ActiveTables;

interface TableStateRepository extends TableStates, ActiveTables
{
    public function saveTableState(TableState $tableState): void;
}
