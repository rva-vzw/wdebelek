<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TableState;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;

/**
 * @deprecated let's use the TableStateRepository
 */
interface TableStateUpdater
{
    public function logTrick(
        GameIdentifier $gameIdentifier,
        int $trickNumber,
        PlayerIdentifier $winner
    ): void;

    public function prepareGame(
        TableIdentifier $tableIdentifier,
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $dealerIdentifier,
        PlayerIdentifier $lastPlayer
    ): void;
}
