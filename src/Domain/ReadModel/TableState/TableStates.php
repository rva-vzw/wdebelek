<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TableState;

use App\Domain\ReadModel\ActiveTables\ActiveTables;
use App\Domain\WriteModel\Game\GameIdentifier;

/**
 * Table states by game.
 *
 * FIXME: This is a confusing interface name, because there's also {@see ActiveTables}.
 * Maybe rename this again as GameState of GameTableState?
 */
interface TableStates
{
    public function getTableStateByGame(GameIdentifier $gameIdentifier): TableState;
}
