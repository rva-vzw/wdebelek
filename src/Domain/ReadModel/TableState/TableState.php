<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TableState;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Game\Exception\TrickNotFound;
use App\Domain\ValueObject\Game\GameConclusion;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Player\Exception\NoTrumpCard;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Player\Teams;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;

/**
 * Information about what's going on at a table.
 */
final class TableState
{
    public function __construct(
        private GameIdentifier $gameIdentifier,
        private int $trickNumber,
        private TableIdentifier $tableIdentifier,
        private int $doneVotes,
        private int $redealVotes,
        private Teams $playerTeams,
        private CardGame $cardGame,
        private bool $suspended,
        private ?Card $revealedTrump,
        private bool $waitingForDealer,
        private PlayerIdentifier $dealer,
        private PlayerIdentifier $lastPlayer,
        private int $totalNumberOfCards,
        private bool $collectedTricksDisclosed,
    ) {
    }

    public static function forNewTable(TableIdentifier $tableIdentifier,
        PlayerIdentifier $claimedBy
    ): self {
        return new self(
            GameIdentifier::fromNumber($tableIdentifier, GameNumber::first()),
            0,
            $tableIdentifier,
            0,
            0,
            Teams::empty(),
            CardGame::unspecified(),
            true,
            null,
            false,
            $claimedBy,
            $claimedBy,
            0,
            false
        );
    }

    public function forNewGame(
        GameIdentifier $gameIdentifier,
        SimplePlayerIdentifiers $players,
        int $totalNumberOfCards,
        PlayerIdentifier $dealer
    ): self {
        $result = clone $this;
        $result->gameIdentifier = $gameIdentifier;
        $result->trickNumber = 1;
        $result->doneVotes = 0;
        $result->redealVotes = 0;
        $result->playerTeams = Teams::allIndividual($players);
        $result->revealedTrump = null;
        $result->totalNumberOfCards = $totalNumberOfCards;
        $result->dealer = $dealer;
        $result->lastPlayer = $players->getLast();
        $result->suspended = false;
        $result->collectedTricksDisclosed = false;

        return $result;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getTrickNumber(): int
    {
        return $this->trickNumber;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function withConclusionVote(GameConclusion $gameConclusion): self
    {
        $result = clone $this;
        switch ((string) $gameConclusion) {
            case (string) GameConclusion::DONE():
                ++$result->doneVotes;
                break;

            case (string) GameConclusion::REDEAL():
                ++$result->redealVotes;
                break;

            default:
                throw new \LogicException('This should never happen');
        }

        return $result;
    }

    public function getDoneVotes(): int
    {
        return $this->doneVotes;
    }

    public function getRedealVotes(): int
    {
        return $this->redealVotes;
    }

    public function withConclusionVoteWithdrawn(GameConclusion $gameConclusion): self
    {
        $result = clone $this;
        switch ((string) $gameConclusion) {
            case (string) GameConclusion::DONE():
                --$result->doneVotes;
                break;

            case (string) GameConclusion::REDEAL():
                --$result->redealVotes;
                break;

            default:
                throw new \LogicException('This should never happen');
        }

        return $result;
    }

    public function withTrickNumber(int $trickNumber): self
    {
        $result = clone $this;
        $result->trickNumber = $trickNumber;

        return $result;
    }

    public function getPlayerTeams(): Teams
    {
        return $this->playerTeams;
    }

    public function withPlayersTeamedUp(Teams $teams): self
    {
        $result = clone $this;
        $result->playerTeams = $teams;

        return $result;
    }

    public function getCardGame(): CardGame
    {
        return $this->cardGame;
    }

    public function playing(CardGame $cardGame): self
    {
        $result = clone $this;
        $result->cardGame = $cardGame;

        return $result;
    }

    public function isSuspended(): bool
    {
        return $this->suspended;
    }

    public function asSuspended(): self
    {
        $result = clone $this;
        $result->suspended = true;

        return $result;
    }

    public function isFirstGame(): bool
    {
        return $this->gameIdentifier == GameIdentifier::fromNumber($this->tableIdentifier, GameNumber::first());
    }

    public function getRevealedTrump(): Card
    {
        if ($this->revealedTrump instanceof Card) {
            return $this->revealedTrump;
        }

        throw NoTrumpCard::create();
    }

    public function hasRevealedTrump(): bool
    {
        return $this->revealedTrump instanceof Card;
    }

    public function withRevealedTrump(Card $trump): self
    {
        $result = clone $this;
        $result->revealedTrump = $trump;

        return $result;
    }

    public function isWaitingForDealer(): bool
    {
        return $this->waitingForDealer;
    }

    public function waitingForDealer(): self
    {
        $result = clone $this;

        $result->waitingForDealer = true;

        return $result;
    }

    public function dealt(): self
    {
        $result = clone $this;
        $result->waitingForDealer = false;

        return $result;
    }

    public function getDealer(): PlayerIdentifier
    {
        return $this->dealer;
    }

    public function getLastPlayer(): PlayerIdentifier
    {
        return $this->lastPlayer;
    }

    public function getTotalNumberOfCards(): int
    {
        return $this->totalNumberOfCards;
    }

    public function withDealer(PlayerIdentifier $dealer): self
    {
        $result = clone $this;
        $result->dealer = $dealer;

        return $result;
    }

    public function forPreviousTrick(): self
    {
        if ($this->trickNumber > 0) {
            $result = clone $this;
            --$result->trickNumber;

            return $result;
        }

        throw TrickNotFound::noTricks();
    }

    public function isCollectedTricksDisclosed(): bool
    {
        return $this->collectedTricksDisclosed;
    }

    public function withAllCollectedTricksDisclosed(): self
    {
        $result = clone $this;
        $result->collectedTricksDisclosed = true;

        return $result;
    }

    public function withNoCollectedTricksDisclosed(): self
    {
        $result = clone $this;
        $result->collectedTricksDisclosed = false;

        return $result;
    }
}
