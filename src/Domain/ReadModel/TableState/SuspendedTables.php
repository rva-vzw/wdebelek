<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TableState;

use App\Domain\WriteModel\Table\TableIdentifier;

interface SuspendedTables
{
    public function hasSuspendedTable(TableIdentifier $tableIdentifier): bool;
}
