<?php

declare(strict_types=1);

namespace App\Domain\ReadModel\TableState;

use App\Domain\WriteModel\Game\Event\AllCollectedTricksDisclosed;
use App\Domain\WriteModel\Game\Event\CardsCutAfter;
use App\Domain\WriteModel\Game\Event\ConclusionVoteInvalidated;
use App\Domain\WriteModel\Game\Event\Dealt;
use App\Domain\WriteModel\Game\Event\GameStarted;
use App\Domain\WriteModel\Game\Event\NonPlayingTrumpRevealed;
use App\Domain\WriteModel\Game\Event\PlayersTeamedUp;
use App\Domain\WriteModel\Game\Event\TrickReopened;
use App\Domain\WriteModel\Game\Event\TrickWon;
use App\Domain\WriteModel\Game\Event\TrumpRevealed;
use App\Domain\WriteModel\Game\Event\VotedConclusion;
use App\Domain\WriteModel\Table\Event\DealerChanged;
use App\Domain\WriteModel\Table\Event\GameChosen;
use App\Domain\WriteModel\Table\Event\GamePrepared;
use App\Domain\WriteModel\Table\Event\TableClaimed;
use App\Domain\WriteModel\Table\Event\TableSuspended;
use RvaVzw\KrakBoem\EventSourcing\Projector\AbstractProjector;

/**
 * Game state projector.
 *
 * Probably some functionality from TableStateReadModel should move into the projector.
 * After all those months, I'm still not sure about how to properly
 * organise a read model. 😞
 *
 * FIXME (#51): So let's remove the TableStateUpdater interface, and rewrite this projector so that it only needs a TableStateRepository.
 */
final readonly class TableStateProjector extends AbstractProjector
{
    private TableStateUpdater $tableStateUpdater;
    private TableStateRepository $tableStateRepository;

    public function __construct(
        TableStateUpdater $tableStateUpdater,
        TableStateRepository $tableStateRepository
    ) {
        $this->tableStateUpdater = $tableStateUpdater;
        $this->tableStateRepository = $tableStateRepository;
    }

    public function applyTrickWon(TrickWon $event): void
    {
        $this->tableStateUpdater->logTrick(
            $event->getGameIdentifier(),
            $event->getTrickNumber(),
            $event->getWinner()
        );
    }

    public function applyGameStarted(GameStarted $event): void
    {
        $tableState = $this->tableStateRepository->getTableState($event->getTableIdentifier());

        $this->tableStateRepository->saveTableState(
            $tableState->forNewGame(
                $event->getGameIdentifier(),
                $event->getPlayers()->withLastPlayer($event->getLastPlayer()),
                $event->getCards()->count(),
                $event->getDealer()
            )
        );
    }

    public function applyTableClaimed(TableClaimed $event): void
    {
        $this->tableStateRepository->saveTableState(
            TableState::forNewTable($event->getTableIdentifier(), $event->getPlayerIdentifier())
        );
    }

    public function applyGamePrepared(GamePrepared $event): void
    {
        $this->tableStateUpdater->prepareGame(
            $event->getTableIdentifier(),
            $event->getGameIdentifier(),
            $event->getDealer(),
            $event->getPlayersAtTable()->getLastPlayerForDealer($event->getDealer())
        );
    }

    public function applyVotedConclusion(VotedConclusion $event): void
    {
        $gameState = $this->tableStateRepository->getTableStateByGame($event->getGameIdentifier());
        $this->tableStateRepository->saveTableState($gameState->withConclusionVote($event->getConclusion()));
    }

    public function applyConclusionVoteInvalidated(ConclusionVoteInvalidated $event): void
    {
        $tableState = $this->tableStateRepository->getTableStateByGame($event->getGameIdentifier());
        $this->tableStateRepository->saveTableState($tableState->withConclusionVoteWithdrawn($event->getConclusion()));
    }

    public function applyDealt(Dealt $event): void
    {
        $tableState = $this->tableStateRepository->getTableStateByGame($event->getGameIdentifier());
        $this->tableStateRepository->saveTableState(
            $tableState->dealt()->withTrickNumber(1)
        );
    }

    public function applyPlayersTeamedUp(PlayersTeamedUp $event): void
    {
        $tableState = $this->tableStateRepository->getTableStateByGame($event->getGameIdentifier());
        $this->tableStateRepository->saveTableState(
            $tableState->withPlayersTeamedUp($event->getTeams())
        );
    }

    public function applyTableSuspended(TableSuspended $event): void
    {
        $tableState = $this->tableStateRepository->getTableState($event->getTableIdentifier());
        $this->tableStateRepository->saveTableState(
            $tableState->asSuspended()
        );
    }

    public function applyGameChosen(GameChosen $event): void
    {
        $tableState = $this->tableStateRepository->getTableState($event->getTableIdentifier());
        $this->tableStateRepository->saveTableState(
            $tableState->playing($event->getCardGame())
        );
    }

    public function applyTrumpRevealed(TrumpRevealed $event): void
    {
        $tableState = $this->tableStateRepository->getTableState($event->getTableIdentifier());
        $this->tableStateRepository->saveTableState(
            $tableState->withRevealedTrump($event->getTrump())
        );
    }

    public function applyNonPlayingTrumpRevealed(NonPlayingTrumpRevealed $event): void
    {
        $tableState = $this->tableStateRepository->getTableState($event->getTableIdentifier());
        $this->tableStateRepository->saveTableState(
            $tableState->withRevealedTrump($event->getTrump())
        );
    }

    public function applyCardsCutAfter(CardsCutAfter $event): void
    {
        $tableState = $this->tableStateRepository->getTableStateByGame($event->getGameIdentifier());
        $this->tableStateRepository->saveTableState(
            $tableState->waitingForDealer()
        );
    }

    public function applyDealerChanged(DealerChanged $event): void
    {
        $tableState = $this->tableStateRepository->getTableState($event->getTableIdentifier());
        $this->tableStateRepository->saveTableState(
            $tableState->withDealer($event->getNewDealer())
        );
    }

    public function applyTrickReopened(TrickReopened $event): void
    {
        $tableState = $this->tableStateRepository->getTableState($event->getTableIdentifier());
        $this->tableStateRepository->saveTableState(
            $tableState->forPreviousTrick()
        );
    }

    public function applyAllCollectedTricksDisclosed(AllCollectedTricksDisclosed $event): void
    {
        $tableState = $this->tableStateRepository->getTableState($event->getTableIdentifier());
        $this->tableStateRepository->saveTableState(
            $tableState->withAllCollectedTricksDisclosed()
        );
    }
}
