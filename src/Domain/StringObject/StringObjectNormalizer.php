<?php

declare(strict_types=1);

namespace App\Domain\StringObject;

use App\Domain\ValueObject\StringObject\StringObject;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class StringObjectNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed[] $context
     *
     * @return StringObject
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = [])
    {
        if (!is_string($data)) {
            throw new \InvalidArgumentException('String expected');
        }
        /** @var StringObject $result */
        $result = $type::fromString($data);

        return $result;
    }

    public function supportsDenormalization($data, string $type, ?string $format = null)
    {
        $interfaces = class_implements($type);

        return is_array($interfaces) && in_array(StringObject::class, $interfaces);
    }

    /**
     * @param mixed[] $context
     *
     * @return string
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        if (!$object instanceof StringObject) {
            throw new \InvalidArgumentException('StringObject expected');
        }

        return $object->toString();
    }

    public function supportsNormalization($data, ?string $format = null)
    {
        return $data instanceof StringObject;
    }
}
