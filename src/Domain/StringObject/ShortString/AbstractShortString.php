<?php

declare(strict_types=1);

namespace App\Domain\StringObject\ShortString;

use App\Domain\ValueObject\StringObject\Exception\StringTooLong;
use App\Domain\ValueObject\StringObject\StringObject;

abstract class AbstractShortString implements StringObject
{
    /** @var string */
    private $value;

    protected function __construct(string $value)
    {
        if (strlen($value) > $this->getMaxLength()) {
            throw StringTooLong::create($this->getMaxLength(), strlen($value));
        }

        $this->value = $value;
    }

    abstract protected function getMaxLength(): int;

    public function toString(): string
    {
        return $this->value;
    }
}
