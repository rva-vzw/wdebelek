<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\ProcessManager;

use App\Domain\WriteModel\Game\Command\AbortGame;
use App\Domain\WriteModel\Game\Command\CutCardsAfter;
use App\Domain\WriteModel\Game\Command\StartGame;
use App\Domain\WriteModel\Game\Event\GameAborted;
use App\Domain\WriteModel\Game\Event\GameCanceled;
use App\Domain\WriteModel\Game\Event\GameEnded;
use App\Domain\WriteModel\Table\Command\CollectCards;
use App\Domain\WriteModel\Table\Command\PrepareNextGame;
use App\Domain\WriteModel\Table\Command\PrepareRedeal;
use App\Domain\WriteModel\Table\Event\GamePrepared;
use App\Domain\WriteModel\Table\Event\TableSuspended;
use App\NonDeterministic\DateTimeProvider;
use App\NonDeterministic\RandomProvider;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use RvaVzw\KrakBoem\Cqrs\ProcessManager\AbstractProcessManager;

final readonly class TableProcessManager extends AbstractProcessManager
{
    private RandomProvider $randomNumbers;
    private DateTimeProvider $dateTimeProvider;

    public function __construct(CommandBus $commandBus,
        RandomProvider $randomNumbers,
        DateTimeProvider $dateTimeProvider
    ) {
        parent::__construct($commandBus);
        $this->randomNumbers = $randomNumbers;
        $this->dateTimeProvider = $dateTimeProvider;
    }

    public function applyGamePrepared(GamePrepared $event): void
    {
        $this->commandBus->dispatch(
            new StartGame(
                $event->getGameIdentifier(),
                $event->getCurrentlyPlaying(),
                $event->getCards(),
                $event->getTableIdentifier(),
                $event->getDealer(),
                $this->dateTimeProvider->getNow(),
            ),
            new CutCardsAfter(
                $event->getGameIdentifier(),
                $this->randomNumbers->getRandomNumber(4, $event->getCards()->count() - 4)
            )
        );
    }

    public function applyGameEnded(GameEnded $event): void
    {
        $this->commandBus->dispatch(
            new CollectCards(
                $event->getTableIdentifier(),
                $event->getCards()
            ),
            new PrepareNextGame(
                $event->getTableIdentifier()
            )
        );
    }

    public function applyGameCanceled(GameCanceled $event): void
    {
        $this->commandBus->dispatch(
            new CollectCards(
                $event->getTableIdentifier(),
                $event->getCards()
            ),
            new PrepareRedeal(
                $event->getTableIdentifier()
            )
        );
    }

    public function applyTableSuspended(TableSuspended $event): void
    {
        $this->commandBus->dispatch(
            new AbortGame($event->getGameIdentifier())
        );
    }

    public function applyGameAborted(GameAborted $event): void
    {
        $this->commandBus->dispatch(
            new CollectCards(
                $event->getTableIdentifier(),
                $event->getCards()
            )
        );
    }
}
