<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table;

use App\Domain\ValueObject\Secret;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;
use RvaVzw\KrakBoem\Id\Uuid5Identifier;

final readonly class TableIdentifier extends Uuid5Identifier implements AggregateRootIdentifier
{
    protected static function getNamespace(): string
    {
        return '3dd45336-542c-4cb0-ae72-51eb549dfb1e';
    }

    public static function withSecret(Secret $tableSecret): self
    {
        return self::fromName($tableSecret->toString());
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
