<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Exception;

use App\Domain\WriteModel\Table\TableIdentifier;

final class NoCards extends \Exception
{
    public static function create(TableIdentifier $tableIdentifier): self
    {
        return new self("No cards at table {$tableIdentifier->toString()}.");
    }
}
