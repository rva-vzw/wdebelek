<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Exception;

use App\Domain\WriteModel\Table\TableIdentifier;

final class ActiveTable extends \Exception
{
    public static function create(TableIdentifier $tableIdentifier): self
    {
        return new self(
            "Table {$tableIdentifier->toString()} is already active."
        );
    }
}
