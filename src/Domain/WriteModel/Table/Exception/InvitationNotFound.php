<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Exception;

use App\Domain\ValueObject\Invitation\Invitation;

final class InvitationNotFound extends \Exception
{
    public static function create(Invitation $invitation): self
    {
        return new self(
            "Invitation {$invitation->toString()} not found."
        );
    }

    public static function forUnknownInvitation(): self
    {
        return new self(
            'Invitation not found.'
        );
    }

    public static function forPublishedInvitation(Invitation $invitation): self
    {
        return new self(
            "Published invitation {$invitation->toString()} not found."
        );
    }
}
