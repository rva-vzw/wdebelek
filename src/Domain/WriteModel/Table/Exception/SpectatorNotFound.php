<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Exception;

use App\Domain\ValueObject\Player\PlayerIdentifier;

final class SpectatorNotFound extends \Exception
{
    public static function create(PlayerIdentifier $playerIdentifier): self
    {
        return new self(
            "Spectator {$playerIdentifier->toString()} not found."
        );
    }
}
