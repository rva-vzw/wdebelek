<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Exception;

final class TableFull extends \Exception
{
    public static function create(int $capacity): self
    {
        return new self(
            "All $capacity places on your table are reserved."
        );
    }
}
