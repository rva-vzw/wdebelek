<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Exception;

use App\Domain\WriteModel\Table\TableIdentifier;

final class NoDealer extends \Exception
{
    public static function at(TableIdentifier $tableIdentifier): self
    {
        return new self(
            "No dealer at table {$tableIdentifier->toString()}"
        );
    }
}
