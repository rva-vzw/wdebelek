<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Exception;

use App\Domain\WriteModel\Table\TableIdentifier;

final class CantStartTwice extends \Exception
{
    public static function create(TableIdentifier $tableIdentifier): self
    {
        return new self("Cannot start playing twice at table {$tableIdentifier->toString()}.");
    }
}
