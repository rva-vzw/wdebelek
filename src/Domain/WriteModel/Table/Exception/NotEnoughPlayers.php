<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Exception;

final class NotEnoughPlayers extends \Exception
{
    public static function create(int $expectedAmount, int $actualAmount): self
    {
        return new self("Not enough players. Expected {$expectedAmount}, got {$actualAmount}.");
    }
}
