<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Exception;

use App\Domain\WriteModel\Table\TableIdentifier;

final class NoGame extends \Exception
{
    public static function at(TableIdentifier $tableIdentifier): self
    {
        return new self(
            "Table {$tableIdentifier->toString()} is still waiting for the first game to be played."
        );
    }
}
