<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\WriteModel\Table\Table;

final class InvitePlayerHandler extends AbstractTableCommandHandler
{
    public function __invoke(InvitePlayer $command): void
    {
        /** @var Table $table */
        $table = $this->tableRepository->get(
            $command->getTableIdentifier()
        );

        $table->invitePlayer(
            $command->getInvitation(),
            $command->getHost(),
            $command->getLanguage()
        );

        $this->tableRepository->save($table);
    }
}
