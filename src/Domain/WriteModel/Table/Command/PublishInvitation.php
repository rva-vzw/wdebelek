<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class PublishInvitation implements Command
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var Invitation */
    private $invitation;
    /** @var PlayerIdentifier */
    private $playerIdentifier;
    /** @var \DateTimeImmutable */
    private $publicationDate;

    public function __construct(
        TableIdentifier $tableIdentifier,
        Invitation $invitation,
        PlayerIdentifier $playerIdentifier,
        \DateTimeImmutable $publicationDate
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->invitation = $invitation;
        $this->playerIdentifier = $playerIdentifier;
        $this->publicationDate = $publicationDate;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getInvitation(): Invitation
    {
        return $this->invitation;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getPublicationDate(): \DateTimeImmutable
    {
        return $this->publicationDate;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }
}
