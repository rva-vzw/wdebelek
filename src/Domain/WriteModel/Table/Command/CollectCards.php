<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class CollectCards implements Command
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var SimplePileOfCards */
    private $cards;

    public function __construct(
        TableIdentifier $tableIdentifier,
        SimplePileOfCards $cards
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->cards = $cards;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getCards(): SimplePileOfCards
    {
        return $this->cards;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }
}
