<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\WriteModel\Table\Table;
use App\Domain\WriteModel\Table\TableRepository;
use App\NonDeterministic\Shuffler;

final class PrepareGameHandler extends AbstractTableCommandHandler
{
    private Shuffler $shuffler;

    public function __construct(TableRepository $tableRepository, Shuffler $shuffler)
    {
        parent::__construct($tableRepository);
        $this->shuffler = $shuffler;
    }

    public function __invoke(PrepareGame $command): void
    {
        /** @var Table $table */
        $table = $this->tableRepository->get(
            $command->getTableIdentifier()
        );

        $table->prepareGameWithSpecificDealer(
            $command->getDealer(),
            $command->getHost(),
            $command->getNumberOfCards(),
            $this->shuffler
        );

        $this->tableRepository->save($table);
    }

    public function changeShuffler(Shuffler $shuffler): void
    {
        $this->shuffler = $shuffler;
    }
}
