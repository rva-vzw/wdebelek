<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\ReadModel\PlayerProfile\SimplePlayerProfile;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class AcceptInvitation implements Command
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var Invitation */
    private $invitation;
    /** @var PlayerIdentifier */
    private $playerIdentifier;
    /** @var PlayerName */
    private $playerName;
    /** @var \DateTimeImmutable */
    private $acceptationDate;
    /** @var Language */
    private $language;

    public function __construct(
        TableIdentifier $tableIdentifier,
        Invitation $invitation,
        PlayerIdentifier $playerIdentifier,
        PlayerName $playerName,
        \DateTimeImmutable $acceptationDate,
        Language $language
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->invitation = $invitation;
        $this->playerIdentifier = $playerIdentifier;
        $this->playerName = $playerName;
        $this->acceptationDate = $acceptationDate;
        $this->language = $language;
    }

    public static function byPlayer(
        TableIdentifier $tableIdentifier,
        Invitation $invitation,
        PlayerProfile $playerProfile,
        \DateTimeImmutable $acceptationDate): self
    {
        return new self(
            $tableIdentifier,
            $invitation,
            $playerProfile->getPlayerIdentifier(),
            $playerProfile->getPlayerName(),
            $acceptationDate,
            $playerProfile->getLanguage()
        );
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getInvitation(): Invitation
    {
        return $this->invitation;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getPlayerName(): PlayerName
    {
        return $this->playerName;
    }

    public function getAcceptationDate(): \DateTimeImmutable
    {
        return $this->acceptationDate;
    }

    public function getPlayerProfile(): PlayerProfile
    {
        return new SimplePlayerProfile(
            $this->playerIdentifier,
            $this->playerName,
            $this->language
        );
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }
}
