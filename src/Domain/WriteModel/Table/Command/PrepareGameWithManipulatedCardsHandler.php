<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\WriteModel\Table\Table;

final class PrepareGameWithManipulatedCardsHandler extends AbstractTableCommandHandler
{
    public function __invoke(PrepareGameWithManipulatedCards $command): void
    {
        /** @var Table $table */
        $table = $this->tableRepository->get(
            $command->getTableIdentifier()
        );

        $table->prepareGameWithCardsReplaced(
            $command->getDealer(),
            $command->getHost(),
            $command->getCards()
        );

        $this->tableRepository->save($table);
    }
}
