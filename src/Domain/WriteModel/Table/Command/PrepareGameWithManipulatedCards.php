<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

/**
 * Fiddle with given cards. For testing only!
 */
final class PrepareGameWithManipulatedCards implements Command
{
    private TableIdentifier $tableIdentifier;
    private PlayerIdentifier $dealer;
    private PlayerIdentifier $host;
    private SimplePileOfCards $cards;

    public function __construct(
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $dealer,
        PlayerIdentifier $host,
        SimplePileOfCards $cards
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->dealer = $dealer;
        $this->host = $host;
        $this->cards = $cards;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getDealer(): PlayerIdentifier
    {
        return $this->dealer;
    }

    public function getHost(): PlayerIdentifier
    {
        return $this->host;
    }

    public function getCards(): SimplePileOfCards
    {
        return $this->cards;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }
}
