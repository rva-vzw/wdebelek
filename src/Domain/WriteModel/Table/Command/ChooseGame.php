<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class ChooseGame implements Command
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var CardGame */
    private $cardGame;
    /** @var PlayerIdentifier */
    private $playerIdentifier;

    public function __construct(
        TableIdentifier $tableIdentifier,
        CardGame $cardGame,
        PlayerIdentifier $playerIdentifier
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->cardGame = $cardGame;
        $this->playerIdentifier = $playerIdentifier;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getCardGame(): CardGame
    {
        return $this->cardGame;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }
}
