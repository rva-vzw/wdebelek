<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\WriteModel\Table\Table;

final class PrepareRedealHandler extends AbstractTableCommandHandler
{
    public function __invoke(PrepareRedeal $command): void
    {
        /** @var Table $table */
        $table = $this->tableRepository->get(
            $command->getTableIdentifier()
        );

        $table->prepareRedeal();

        $this->tableRepository->save($table);
    }
}
