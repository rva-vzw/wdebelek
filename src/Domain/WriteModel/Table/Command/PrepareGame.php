<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class PrepareGame implements Command
{
    private PlayerIdentifier $dealer;
    private TableIdentifier $tableIdentifier;
    private PlayerIdentifier $host;
    private int $numberOfCards;

    public function __construct(
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $dealer,
        PlayerIdentifier $host,
        int $numberOfCards
    ) {
        $this->dealer = $dealer;
        $this->tableIdentifier = $tableIdentifier;
        $this->host = $host;
        $this->numberOfCards = $numberOfCards;
    }

    public function getDealer(): PlayerIdentifier
    {
        return $this->dealer;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getHost(): PlayerIdentifier
    {
        return $this->host;
    }

    public function getNumberOfCards(): int
    {
        return $this->numberOfCards;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }
}
