<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\WriteModel\Table\Table;

final class ClaimTableHandler extends AbstractTableCommandHandler
{
    public function __invoke(ClaimTable $command): void
    {
        $table = Table::claim(
            $command->getTableIdentifier(),
            $command->getPlayerProfile()
        );

        $this->tableRepository->save($table);
    }
}
