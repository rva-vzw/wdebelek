<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\WriteModel\Table\Table;

final class ChooseGameHandler extends AbstractTableCommandHandler
{
    public function __invoke(ChooseGame $command): void
    {
        /** @var Table $table */
        $table = $this->tableRepository->get(
            $command->getTableIdentifier()
        );

        $table->chooseCardGame(
            $command->getCardGame(),
            $command->getPlayerIdentifier()
        );

        $this->tableRepository->save($table);
    }
}
