<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\WriteModel\Table\Table;

final class CollectCardsHandler extends AbstractTableCommandHandler
{
    public function __invoke(CollectCards $command): void
    {
        /** @var Table $table */
        $table = $this->tableRepository->get(
            $command->getTableIdentifier()
        );

        $table->collectCards($command->getCards());

        $this->tableRepository->save($table);
    }
}
