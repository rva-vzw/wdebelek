<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\WriteModel\Table\Table;

final class PublishInvitationHandler extends AbstractTableCommandHandler
{
    public function __invoke(PublishInvitation $command): void
    {
        /** @var Table $table */
        $table = $this->tableRepository->get(
            $command->getTableIdentifier()
        );

        $table->publishInvitation(
            $command->getInvitation(),
            $command->getPlayerIdentifier(),
            $command->getPublicationDate()
        );

        $this->tableRepository->save($table);
    }
}
