<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\WriteModel\Table\Table;

final class AcceptInvitationHandler extends AbstractTableCommandHandler
{
    public function __invoke(AcceptInvitation $command): void
    {
        /** @var Table $table */
        $table = $this->tableRepository->get(
            $command->getTableIdentifier()
        );

        $table->acceptInvitation(
            $command->getInvitation(),
            $command->getAcceptationDate(),
            $command->getPlayerProfile()
        );

        $this->tableRepository->save($table);
    }
}
