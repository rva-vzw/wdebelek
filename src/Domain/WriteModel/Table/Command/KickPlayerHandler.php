<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\WriteModel\Table\Table;

final class KickPlayerHandler extends AbstractTableCommandHandler
{
    public function __invoke(KickPlayer $command): void
    {
        /** @var Table $table */
        $table = $this->tableRepository->get($command->getTableIdentifier());
        $table->kickPlayer($command->getPlayerToBeKicked(), $command->getKickingPlayer());
        $this->tableRepository->save($table);
    }
}
