<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class KickPlayer implements Command
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var PlayerIdentifier */
    private $playerToBeKicked;
    /** @var PlayerIdentifier */
    private $kickingPlayer;

    public function __construct(
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $playerToBeKicked,
        PlayerIdentifier $kickingPlayer
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->playerToBeKicked = $playerToBeKicked;
        $this->kickingPlayer = $kickingPlayer;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getPlayerToBeKicked(): PlayerIdentifier
    {
        return $this->playerToBeKicked;
    }

    public function getKickingPlayer(): PlayerIdentifier
    {
        return $this->kickingPlayer;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }
}
