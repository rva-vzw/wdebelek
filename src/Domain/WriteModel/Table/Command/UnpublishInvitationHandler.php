<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\WriteModel\Table\Table;

final class UnpublishInvitationHandler extends AbstractTableCommandHandler
{
    public function __invoke(UnpublishInvitation $command): void
    {
        /** @var Table $table */
        $table = $this->tableRepository->get(
            $command->getTableIdentifier()
        );

        $table->unpublishInvitation(
            $command->getInvitation(),
            $command->getPlayerIdentifier(),
            $command->getUnpublicationDate()
        );

        $this->tableRepository->save($table);
    }
}
