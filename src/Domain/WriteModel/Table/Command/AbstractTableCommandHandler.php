<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\WriteModel\Table\TableRepository;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandHandler;

abstract class AbstractTableCommandHandler implements CommandHandler
{
    /** @var TableRepository */
    protected $tableRepository;

    public function __construct(TableRepository $tableRepository)
    {
        $this->tableRepository = $tableRepository;
    }
}
