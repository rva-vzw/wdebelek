<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class InvitePlayer implements Command
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var Invitation */
    private $invitation;
    /** @var PlayerIdentifier */
    private $host;
    /** @var Language */
    private $language;

    public function __construct(
        TableIdentifier $tableIdentifier,
        Invitation $invitation,
        PlayerIdentifier $host,
        Language $language
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->invitation = $invitation;
        $this->host = $host;
        $this->language = $language;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getInvitation(): Invitation
    {
        return $this->invitation;
    }

    public function getHost(): PlayerIdentifier
    {
        return $this->host;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }
}
