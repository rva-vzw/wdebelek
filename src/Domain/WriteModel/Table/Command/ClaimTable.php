<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Command;

use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class ClaimTable implements Command
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var PlayerProfile */
    private $playerProfile;

    public function __construct(
        TableIdentifier $tableIdentifier,
        PlayerProfile $playerProfile
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->playerProfile = $playerProfile;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getPlayerProfile(): PlayerProfile
    {
        return $this->playerProfile;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }
}
