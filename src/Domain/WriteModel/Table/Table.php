<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table;

use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Invitation\Invitations;
use App\Domain\ValueObject\Player\Exception\DuplicatePlayer;
use App\Domain\ValueObject\Player\Exception\PlayerNotFound;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\ValueObject\Table\SeatsAtTable;
use App\Domain\WriteModel\Game\Game;
use App\Domain\WriteModel\Table\Event\CardsCollected;
use App\Domain\WriteModel\Table\Event\DealerChanged;
use App\Domain\WriteModel\Table\Event\GameChosen;
use App\Domain\WriteModel\Table\Event\GamePrepared;
use App\Domain\WriteModel\Table\Event\InvitationAccepted;
use App\Domain\WriteModel\Table\Event\InvitationPublished;
use App\Domain\WriteModel\Table\Event\InvitationUnpublished;
use App\Domain\WriteModel\Table\Event\PlayerInvited;
use App\Domain\WriteModel\Table\Event\PlayerKicked;
use App\Domain\WriteModel\Table\Event\TableClaimed;
use App\Domain\WriteModel\Table\Event\TableSuspended;
use App\Domain\WriteModel\Table\Exception\InvitationNotFound;
use App\Domain\WriteModel\Table\Exception\NoDealer;
use App\Domain\WriteModel\Table\Exception\NoGame;
use App\Domain\WriteModel\Table\Exception\NotEnoughPlayers;
use App\Domain\WriteModel\Table\Exception\TableFull;
use App\NonDeterministic\Shuffler;
use RvaVzw\KrakBoem\Cqrs\Aggregate\AbstractAggregate;

final class Table extends AbstractAggregate
{
    public const CAPACITY = 6;

    private TableIdentifier $tableIdentifier;
    private SeatsAtTable $seats;
    private ?GameNumber $gameNumber;
    private SimplePileOfCards $cards;
    private ?PlayerIdentifier $dealer;
    private SimplePlayerIdentifiers $currentlyPlaying;
    private CardGame $cardGame;
    private Invitations $publishedInvitations;
    private SimplePlayerIdentifiers $formerPlayers;

    public static function claim(
        TableIdentifier $tableIdentifier,
        PlayerProfile $playerProfile
    ): self {
        $result = new static($tableIdentifier);
        $result->applyAndRecord(new TableClaimed(
            $tableIdentifier,
            $playerProfile->getPlayerIdentifier(),
            $playerProfile->getPlayerName(),
            Seat::zero(),
            $playerProfile->getLanguage()
        ));

        return $result;
    }

    public function applyTableClaimed(TableClaimed $event): void
    {
        $this->tableIdentifier = $event->getTableIdentifier();
        $this->seats = SeatsAtTable::empty(self::CAPACITY)
            ->withPlayerOnSeat($event->getSeat(), $event->getPlayerIdentifier());
        $this->gameNumber = null;
        $this->cards = SimplePileOfCards::empty();
        $this->dealer = null;
        $this->currentlyPlaying = SimplePlayerIdentifiers::none();
        $this->cardGame = CardGame::unspecified();
        $this->publishedInvitations = Invitations::none();
        $this->formerPlayers = SimplePlayerIdentifiers::none();
    }

    public function invitePlayer(
        Invitation $invitation,
        PlayerIdentifier $host,
        Language $language
    ): void {
        if ($this->seats->hasInvitation($invitation)) {
            return;
        }

        $this->guardTableSize();
        // Check whether the host (who is creating the invitation) is known.
        $this->guardKnownPlayer($host);

        $this->applyAndRecord(new PlayerInvited(
            $this->tableIdentifier,
            $invitation,
            $this->seats->getFreeSeat(),
            $host,
            $language
        ));
    }

    private function guardTableSize(): void
    {
        if ($this->seats->hasFreeSeat()) {
            return;
        }
        throw TableFull::create(self::CAPACITY);
    }

    public function applyPlayerInvited(PlayerInvited $event): void
    {
        $this->seats = $this->seats->withInvitation($event->getSeat(), $event->getInvitation());
    }

    public function acceptInvitation(
        Invitation $invitation,
        \DateTimeImmutable $acceptationDate,
        PlayerProfile $playerProfile
    ): void {
        $this->guardKnownInvitation($invitation);
        $this->guardNewPlayer($playerProfile->getPlayerIdentifier());

        if ($this->publishedInvitations->has($invitation)) {
            $this->applyAndRecord(new InvitationUnpublished(
                $this->tableIdentifier,
                $invitation,
                $playerProfile->getPlayerIdentifier(),
                $acceptationDate
            ));
        }
        $this->applyAndRecord(InvitationAccepted::byPlayer(
            $this->tableIdentifier,
            $invitation,
            $this->seats->getReservedSeat($invitation),
            $playerProfile
        ));
    }

    private function guardKnownInvitation(Invitation $invitation): void
    {
        if ($this->seats->hasInvitation($invitation)) {
            return;
        }

        throw InvitationNotFound::create($invitation);
    }

    private function guardNewPlayer(PlayerIdentifier $playerIdentifier): void
    {
        if ($this->seats->hasPlayer($playerIdentifier)) {
            throw DuplicatePlayer::fromPlayer($playerIdentifier);
        }
    }

    public function applyInvitationAccepted(InvitationAccepted $event): void
    {
        $this->seats = $this->seats->withPlayerOnSeat(
            $event->getSeat(),
            $event->getPlayerIdentifier()
        );
        $this->formerPlayers = $this->formerPlayers->without($event->getPlayerIdentifier());
    }

    public function prepareGameWithSpecificDealer(
        PlayerIdentifier $dealer,
        PlayerIdentifier $host,
        int $numberOfCards,
        Shuffler $shuffler
    ): void {
        $cards = $this->cards->count() === $numberOfCards
            ? $this->cards : $shuffler->shuffle($this->getNewDeck($numberOfCards));
        $this->prepareGameWithCardsReplaced($dealer, $host, $cards);
    }

    private function guardNumberOfPlayers(): void
    {
        if ($this->seats->playerCount() < Game::MIN_PLAYER_COUNT) {
            throw NotEnoughPlayers::create(Game::MIN_PLAYER_COUNT, $this->seats->playerCount());
        }
    }

    private function guardKnownPlayer(PlayerIdentifier $playerIdentifier): void
    {
        if ($this->seats->hasPlayer($playerIdentifier)) {
            return;
        }

        // Even if I leave, I can still manage the table (that is, if I have the table secret).
        // See https://gitlab.com/rva-vzw/wdebelek/-/issues/114
        if ($this->formerPlayers->has($playerIdentifier)) {
            return;
        }

        throw PlayerNotFound::create($playerIdentifier);
    }

    public function applyGamePrepared(GamePrepared $event): void
    {
        $this->gameNumber = $event->getGameNumber();
        $this->cards = $event->getCards();
        $this->dealer = $event->getDealer();
        $this->currentlyPlaying = $event->getCurrentlyPlaying();
    }

    /**
     * Prepare next game with the next dealer.
     */
    public function prepareNextGame(): void
    {
        $this->guardNumberOfPlayers();
        $this->guardGame();
        $this->guardDealer();

        /** @var PlayerIdentifier $previousDealer */
        $previousDealer = $this->dealer;
        $newDealer = $this->seats->getPlayerNextTo($previousDealer);

        /** @var GameNumber $previousGame */
        $previousGame = $this->gameNumber;

        $this->applyAndRecord(new GamePrepared(
            $this->tableIdentifier,
            $newDealer,
            $this->seats->getPlayerBefore($newDealer),
            $previousGame->next(),
            $this->cards,
            $this->seats->getTableMates(),
            // FIXME: #19, allow games with more than 4 players.
            $this->seats->getTableMates()->getAtMostFourPlayers($newDealer)
        ));
    }

    private function guardDealer(): void
    {
        if ($this->dealer instanceof PlayerIdentifier) {
            return;
        }

        throw NoDealer::at($this->tableIdentifier);
    }

    private function guardGame(): void
    {
        if ($this->gameNumber instanceof GameNumber) {
            return;
        }

        throw NoGame::at($this->tableIdentifier);
    }

    /**
     * Prepare next game with the same dealer.
     */
    public function prepareRedeal(): void
    {
        $this->guardNumberOfPlayers();
        $this->guardGame();
        $this->guardDealer();

        /** @var PlayerIdentifier $previousDealer */
        $previousDealer = $this->dealer;

        /** @var GameNumber $previousGame */
        $previousGame = $this->gameNumber;

        // Although it is called 'game restart', we still increase the game number,
        // because I don't want to end up with too many events for a single game
        // aggregate.

        $this->applyAndRecord(new GamePrepared(
            $this->tableIdentifier,
            $previousDealer,
            $this->seats->getPlayerBefore($previousDealer),
            $previousGame->next(),
            $this->cards,
            $this->seats->getTableMates(),
            // FIXME: #19, allow games with more than 4 players.
            $this->seats->getTableMates()->getAtMostFourPlayers($previousDealer)
        ));
    }

    public function kickPlayer(PlayerIdentifier $playerToBeKicked, PlayerIdentifier $kickingPlayer): void
    {
        $this->guardKnownPlayer($playerToBeKicked);
        $this->guardKnownPlayer($kickingPlayer);

        $playerBeforeKickedOne = $this->seats->getPlayerBefore($playerToBeKicked);
        $originalDealer = $this->dealer;

        $this->applyAndRecord(new PlayerKicked(
            $this->tableIdentifier,
            $playerToBeKicked,
            $kickingPlayer
        ));

        if ($this->currentlyPlaying->has($playerToBeKicked)) {
            if (!$this->gameNumber instanceof GameNumber) {
                throw new \LogicException('This should not happen. Cannot have playing players without game number.');
            }
            // Table will be suspended; organiser will have to resume the table
            // to start the next game.
            $this->applyAndRecord(new TableSuspended($this->tableIdentifier, $this->gameNumber));
        } elseif ($originalDealer == $playerToBeKicked) {
            $this->applyAndRecord(new DealerChanged(
                $this->tableIdentifier,
                $playerBeforeKickedOne
            ));
        }
    }

    public function applyPlayerKicked(PlayerKicked $event): void
    {
        if ($this->dealer == $event->getPlayerIdentifier()) {
            $this->dealer = 1 === $this->seats->playerCount() ? null : $this->seats->getPlayerBefore($event->getPlayerIdentifier());
        }

        $this->seats = $this->seats->withPlayerGone($event->getPlayerIdentifier());
        $this->formerPlayers = $this->formerPlayers->with($event->getPlayerIdentifier());
    }

    public function applyTableSuspended(TableSuspended $event): void
    {
        $this->currentlyPlaying = SimplePlayerIdentifiers::none();
        $this->dealer = null;
    }

    public function collectCards(SimplePileOfCards $cards): void
    {
        $this->applyAndRecord(new CardsCollected($this->tableIdentifier, $cards));
    }

    public function applyCardsCollected(CardsCollected $event): void
    {
        $this->cards = $event->getCards();
    }

    public function prepareGameWithCardsReplaced(
        PlayerIdentifier $dealer,
        PlayerIdentifier $host,
        SimplePileOfCards $cards
    ): void {
        $this->guardNumberOfPlayers();
        $this->guardKnownPlayer($dealer);
        $this->guardKnownPlayer($host);

        $gameNumber = $this->gameNumber instanceof GameNumber
            ? $this->gameNumber->next()
            : GameNumber::first();

        $this->applyAndRecord(new GamePrepared(
            $this->tableIdentifier,
            $dealer,
            $this->seats->getPlayerBefore($dealer),
            $gameNumber,
            $cards,
            $this->seats->getTableMates(),
            // FIXME: #19, allow games with more than 4 players.
            $this->seats->getTableMates()->getAtMostFourPlayers($dealer)
        ));
    }

    public function chooseCardGame(
        CardGame $cardGame,
        PlayerIdentifier $playerIdentifier
    ): void {
        if ($this->cardGame == $cardGame) {
            return;
        }

        $this->applyAndRecord(new GameChosen(
            $this->tableIdentifier,
            $cardGame,
            $playerIdentifier
        ));
    }

    public function publishInvitation(
        Invitation $invitation,
        PlayerIdentifier $playerIdentifier,
        \DateTimeImmutable $publicationDate
    ): void {
        $this->guardKnownPlayer($playerIdentifier);
        $this->guardKnownInvitation($invitation);

        if ($this->publishedInvitations->has($invitation)) {
            return;
        }

        $this->applyAndRecord(new InvitationPublished(
            $this->tableIdentifier,
            $invitation,
            $playerIdentifier,
            $publicationDate
        ));
    }

    public function applyInvitationPublished(InvitationPublished $event): void
    {
        $this->publishedInvitations = $this->publishedInvitations->with($event->getInvitation());
    }

    public function unpublishInvitation(
        Invitation $invitation,
        PlayerIdentifier $playerIdentifier,
        \DateTimeImmutable $unpublicationDate
    ): void {
        $this->guardKnownPlayer($playerIdentifier);
        $this->guardKnownInvitation($invitation);

        if ($this->publishedInvitations->has($invitation)) {
            $this->applyAndRecord(new InvitationUnpublished(
                $this->tableIdentifier,
                $invitation,
                $playerIdentifier,
                $unpublicationDate
            ));
        }
    }

    public function applyInvitationUnpublished(InvitationUnpublished $event): void
    {
        $this->publishedInvitations = $this->publishedInvitations->without($event->getInvitation());
    }

    public function applyDealerChanged(DealerChanged $event): void
    {
        $this->dealer = $event->getNewDealer();
    }

    private function getNewDeck(int $numberOfCards): SimplePileOfCards
    {
        return match ($numberOfCards) {
            32 => SimplePileOfCards::thirtyTwo(),
            52 => SimplePileOfCards::complete(),
            default => throw new \InvalidArgumentException('Only default piles of 52 cards or 32 cards are supported right now.'),
        };
    }
}
