<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Event;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class DealerChanged implements ClientRelevantTableEvent
{
    private TableIdentifier $tableIdentifier;
    private PlayerIdentifier $newDealer;

    public function __construct(
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $newDealer
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->newDealer = $newDealer;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getNewDealer(): PlayerIdentifier
    {
        return $this->newDealer;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }
}
