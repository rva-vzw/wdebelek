<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Event;

use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Player\TableMates;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class GamePrepared implements Event
{
    private TableIdentifier $tableIdentifier;
    private PlayerIdentifier $dealer;
    private PlayerIdentifier $cutter;
    private GameNumber $gameNumber;
    private SimplePileOfCards $cards;
    private TableMates $playersAtTable;
    private SimplePlayerIdentifiers $currentlyPlaying;

    public function __construct(
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $dealer,
        PlayerIdentifier $cutter,
        GameNumber $gameNumber,
        SimplePileOfCards $cards,
        TableMates $playersAtTable,
        SimplePlayerIdentifiers $currentlyPlaying
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->dealer = $dealer;
        $this->cutter = $cutter;
        $this->gameNumber = $gameNumber;
        $this->cards = $cards;
        $this->playersAtTable = $playersAtTable;
        $this->currentlyPlaying = $currentlyPlaying;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return GameIdentifier::fromNumber(
            $this->tableIdentifier,
            $this->gameNumber
        );
    }

    public function getDealer(): PlayerIdentifier
    {
        return $this->dealer;
    }

    public function getCutter(): PlayerIdentifier
    {
        return $this->cutter;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getGameNumber(): GameNumber
    {
        return $this->gameNumber;
    }

    public function getCards(): SimplePileOfCards
    {
        return $this->cards;
    }

    public function getPlayersAtTable(): TableMates
    {
        return $this->playersAtTable;
    }

    public function getCurrentlyPlaying(): SimplePlayerIdentifiers
    {
        return $this->currentlyPlaying;
    }
}
