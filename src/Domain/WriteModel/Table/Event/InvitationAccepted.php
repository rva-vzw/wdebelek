<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Event;

use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\ReadModel\PlayerProfile\SimplePlayerProfile;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class InvitationAccepted implements ClientRelevantTableEvent
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var Invitation */
    private $invitation;
    /** @var PlayerIdentifier */
    private $playerIdentifier;
    /** @var PlayerName */
    private $playerName;
    /** @var Seat */
    private $seat;
    /** @var Language */
    private $playerLanguage;

    public function __construct(
        TableIdentifier $tableIdentifier,
        Invitation $invitation,
        PlayerIdentifier $playerIdentifier,
        PlayerName $playerName,
        Seat $seat,
        Language $playerLanguage
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->invitation = $invitation;
        $this->playerIdentifier = $playerIdentifier;
        $this->playerName = $playerName;
        $this->seat = $seat;
        $this->playerLanguage = $playerLanguage;
    }

    public static function byPlayer(
        TableIdentifier $tableIdentifier,
        Invitation $invitation,
        Seat $seat,
        PlayerProfile $playerProfile
    ): self {
        return new self(
            $tableIdentifier,
            $invitation,
            $playerProfile->getPlayerIdentifier(),
            $playerProfile->getPlayerName(),
            $seat,
            $playerProfile->getLanguage()
        );
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getInvitation(): Invitation
    {
        return $this->invitation;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getPlayerName(): PlayerName
    {
        return $this->playerName;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getSeat(): Seat
    {
        return $this->seat;
    }

    public function getPlayerProfile(): PlayerProfile
    {
        return new SimplePlayerProfile(
            $this->getPlayerIdentifier(),
            $this->getPlayerName(),
            $this->getPlayerLanguage()
        );
    }

    private function getPlayerLanguage(): Language
    {
        return $this->playerLanguage;
    }
}
