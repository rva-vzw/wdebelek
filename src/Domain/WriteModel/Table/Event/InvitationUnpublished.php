<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Event;

use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class InvitationUnpublished implements Event
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var Invitation */
    private $invitation;
    /** @var PlayerIdentifier */
    private $playerIdentifier;
    /** @var \DateTimeImmutable */
    private $unpublicationDate;

    public function __construct(
        TableIdentifier $tableIdentifier,
        Invitation $invitation,
        PlayerIdentifier $playerIdentifier,
        \DateTimeImmutable $unpublicationDate
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->invitation = $invitation;
        $this->playerIdentifier = $playerIdentifier;
        $this->unpublicationDate = $unpublicationDate;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getInvitation(): Invitation
    {
        return $this->invitation;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getUnpublicationDate(): \DateTimeImmutable
    {
        return $this->unpublicationDate;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }
}
