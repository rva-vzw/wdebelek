<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Event;

use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class TableSuspended implements ClientRelevantTableEvent
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var GameNumber */
    private $gameNumber;

    public function __construct(TableIdentifier $tableIdentifier, GameNumber $gameNumber)
    {
        $this->tableIdentifier = $tableIdentifier;
        $this->gameNumber = $gameNumber;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getGameNumber(): GameNumber
    {
        return $this->gameNumber;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return GameIdentifier::fromNumber($this->tableIdentifier, $this->gameNumber);
    }
}
