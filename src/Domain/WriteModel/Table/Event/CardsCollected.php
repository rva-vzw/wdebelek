<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Event;

use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class CardsCollected implements ClientRelevantTableEvent
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var SimplePileOfCards */
    private $cards;

    public function __construct(
        TableIdentifier $tableIdentifier,
        SimplePileOfCards $cards
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->cards = $cards;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getCards(): SimplePileOfCards
    {
        return $this->cards;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }
}
