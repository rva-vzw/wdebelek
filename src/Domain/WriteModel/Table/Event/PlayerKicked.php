<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Event;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class PlayerKicked implements ClientRelevantTableEvent
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var PlayerIdentifier */
    private $playerIdentifier;
    /** @var PlayerIdentifier */
    private $kickedBy;

    public function __construct(
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $playerIdentifier,
        PlayerIdentifier $kickedBy
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->playerIdentifier = $playerIdentifier;
        $this->kickedBy = $kickedBy;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getKickedBy(): PlayerIdentifier
    {
        return $this->kickedBy;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }
}
