<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Event;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class GameChosen implements Event
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var CardGame */
    private $cardGame;
    /** @var PlayerIdentifier */
    private $playerIdentifier;

    public function __construct(
        TableIdentifier $tableIdentifier,
        CardGame $cardGame,
        PlayerIdentifier $playerIdentifier
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->cardGame = $cardGame;
        $this->playerIdentifier = $playerIdentifier;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getCardGame(): CardGame
    {
        return $this->cardGame;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }
}
