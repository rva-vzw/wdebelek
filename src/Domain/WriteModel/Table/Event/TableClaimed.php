<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Event;

use App\Domain\ReadModel\PlayerProfile\PlayerProfile;
use App\Domain\ReadModel\PlayerProfile\SimplePlayerProfile;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class TableClaimed implements Event
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var PlayerIdentifier */
    private $playerIdentifier;
    /** @var PlayerName */
    private $playerName;
    /** @var Seat */
    private $seat;
    /** @var Language */
    private $language;

    public function __construct(
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $playerIdentifier,
        PlayerName $playerName,
        Seat $seat,
        Language $language
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->playerIdentifier = $playerIdentifier;
        $this->playerName = $playerName;
        $this->seat = $seat;
        $this->language = $language;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getPlayerName(): PlayerName
    {
        return $this->playerName;
    }

    public function getSeat(): Seat
    {
        return $this->seat;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }

    public function getPlayerProfile(): PlayerProfile
    {
        return new SimplePlayerProfile(
            $this->getPlayerIdentifier(),
            $this->getPlayerName(),
            $this->language
        );
    }
}
