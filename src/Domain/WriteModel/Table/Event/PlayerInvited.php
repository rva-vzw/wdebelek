<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Table\Event;

use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Table\Seat;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class PlayerInvited implements ClientRelevantTableEvent
{
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var Invitation */
    private $invitation;
    /** @var Seat */
    private $seat;
    /** @var PlayerIdentifier */
    private $host;
    /** @var Language */
    private $language;

    public function __construct(
        TableIdentifier $tableIdentifier,
        Invitation $invitation,
        Seat $seat,
        PlayerIdentifier $host,
        Language $language
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->invitation = $invitation;
        $this->seat = $seat;
        $this->host = $host;
        $this->language = $language;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getInvitation(): Invitation
    {
        return $this->invitation;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getSeat(): Seat
    {
        return $this->seat;
    }

    public function getHost(): PlayerIdentifier
    {
        return $this->host;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }
}
