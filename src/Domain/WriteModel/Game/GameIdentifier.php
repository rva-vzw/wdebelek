<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game;

use App\Domain\ValueObject\Game\GameNumber;
use App\Domain\WriteModel\Table\TableIdentifier;
use Ramsey\Uuid\Uuid;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;
use RvaVzw\KrakBoem\Id\Uuid4Identifier;

final readonly class GameIdentifier extends Uuid4Identifier implements AggregateRootIdentifier
{
    public static function fromNumber(
        TableIdentifier $tableIdentifier,
        GameNumber $gameNumber
    ): self {
        // Everybody loves reproducible UUIDs.
        $uuid = Uuid::uuid5($tableIdentifier->toString(), (string) $gameNumber->toInteger())->toString();

        return self::fromString($uuid);
    }

    public static function first(TableIdentifier $tableIdentifier): self
    {
        return self::fromNumber(
            $tableIdentifier,
            GameNumber::first()
        );
    }
}
