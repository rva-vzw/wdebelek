<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\WriteModel\Game\Game;

final class RevealTrumpHandler extends AbstractGameCommandHandler
{
    public function __invoke(RevealTrump $command): void
    {
        /** @var Game $game */
        $game = $this->gameRepository->get($command->getGameIdentifier());
        $game->revealTrump($command->getPlayerIdentifier());
        $this->gameRepository->save($game);
    }
}
