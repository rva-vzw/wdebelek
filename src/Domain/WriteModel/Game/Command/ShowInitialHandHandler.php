<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\WriteModel\Game\Game;

final class ShowInitialHandHandler extends AbstractGameCommandHandler
{
    public function __invoke(ShowInitialHand $command): void
    {
        /** @var Game $game */
        $game = $this->gameRepository->get($command->getGameIdentifier());
        $game->showInitialHand($command->getPlayerIdentifier());
        $this->gameRepository->save($game);
    }
}
