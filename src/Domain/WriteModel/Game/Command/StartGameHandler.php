<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\WriteModel\Game\Game;

final class StartGameHandler extends AbstractGameCommandHandler
{
    public function __invoke(StartGame $command): void
    {
        $game = Game::start(
            $command->getGameIdentifier(),
            $command->getPlayers(),
            $command->getCards(),
            $command->getTableIdentifier(),
            $command->getDealer(),
            $command->getStartedAt()
        );

        $this->gameRepository->save($game);
    }
}
