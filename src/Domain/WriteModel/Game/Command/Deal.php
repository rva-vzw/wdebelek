<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\ValueObject\DealingStrategy;
use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class Deal implements Command
{
    private GameIdentifier $gameIdentifier;
    private DealingStrategy $dealingStrategy;

    public function __construct(
        GameIdentifier $gameIdentifier,
        DealingStrategy $dealingStrategy
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->dealingStrategy = $dealingStrategy;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getDealingStrategy(): DealingStrategy
    {
        return $this->dealingStrategy;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
