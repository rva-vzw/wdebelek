<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

// Maybe something like this is needed in KrakBoem.
use App\Domain\WriteModel\Game\GameRepository;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandHandler;

abstract class AbstractGameCommandHandler implements CommandHandler
{
    /** @var GameRepository */
    protected $gameRepository;

    // This constructor should be public for the handler to be found by Symfony, I guess.
    public function __construct(GameRepository $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }
}
