<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class ReopenTrick implements Command
{
    public function __construct(
        private GameIdentifier $gameIdentifier,
        private PlayerIdentifier $reopeningPlayer,
        private int $trickNumber,
    ) {
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getReopeningPlayer(): PlayerIdentifier
    {
        return $this->reopeningPlayer;
    }

    public function getTrickNumber(): int
    {
        return $this->trickNumber;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
