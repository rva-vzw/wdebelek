<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\ValueObject\Game\GameConclusion;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class VoteGameConclusion implements Command
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var PlayerIdentifier */
    private $voter;
    /** @var GameConclusion */
    private $gameConclusion;

    public function __construct(
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $voter,
        GameConclusion $gameConclusion
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->voter = $voter;
        $this->gameConclusion = $gameConclusion;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getVoter(): PlayerIdentifier
    {
        return $this->voter;
    }

    public function getGameConclusion(): GameConclusion
    {
        return $this->gameConclusion;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
