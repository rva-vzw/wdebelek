<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\WriteModel\Game\Game;

final class ReviewPreviousTrickHandler extends AbstractGameCommandHandler
{
    public function __invoke(ReviewPreviousTrick $command): void
    {
        /** @var Game $game */
        $game = $this->gameRepository->get($command->getGameIdentifier());
        $game->reviewPreviousTrick($command->getReviewer(), $command->getTrickNumber());
        $this->gameRepository->save($game);
    }
}
