<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class PickUpCard implements Command
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var PlayerIdentifier */
    private $playerIdentifier;
    /** @var int */
    private $trickNumber;

    public function __construct(
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $playerIdentifier,
        int $trickNumber
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->playerIdentifier = $playerIdentifier;
        $this->trickNumber = $trickNumber;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getTrickNumber(): int
    {
        return $this->trickNumber;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
