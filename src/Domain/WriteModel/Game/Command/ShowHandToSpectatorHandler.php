<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\WriteModel\Game\Game;

final class ShowHandToSpectatorHandler extends AbstractGameCommandHandler
{
    public function __invoke(ShowHandToSpectator $command): void
    {
        /** @var Game $game */
        $game = $this->gameRepository->get(
            $command->getGameIdentifier()
        );

        $game->showHandToSpectator(
            $command->getPlayer(),
            $command->getSpectator()
        );

        $this->gameRepository->save($game);
    }
}
