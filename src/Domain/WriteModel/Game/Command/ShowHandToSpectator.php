<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class ShowHandToSpectator implements Command
{
    private PlayerIdentifier $player;
    private PlayerIdentifier $spectator;
    private GameIdentifier $gameIdentifier;

    public function __construct(
        PlayerIdentifier $player,
        PlayerIdentifier $spectator,
        GameIdentifier $gameIdentifier
    ) {
        $this->player = $player;
        $this->spectator = $spectator;
        $this->gameIdentifier = $gameIdentifier;
    }

    public function getPlayer(): PlayerIdentifier
    {
        return $this->player;
    }

    public function getSpectator(): PlayerIdentifier
    {
        return $this->spectator;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
