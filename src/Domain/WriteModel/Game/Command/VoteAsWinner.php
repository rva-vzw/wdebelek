<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class VoteAsWinner implements Command
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var PlayerIdentifier */
    private $voter;
    /** @var int */
    private $trickNumber;
    /** @var PlayerIdentifier */
    private $winner;

    public function __construct(
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $voter,
        int $trickNumber,
        PlayerIdentifier $winner
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->voter = $voter;
        $this->trickNumber = $trickNumber;
        $this->winner = $winner;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getVoter(): PlayerIdentifier
    {
        return $this->voter;
    }

    public function getTrickNumber(): int
    {
        return $this->trickNumber;
    }

    public function getWinner(): PlayerIdentifier
    {
        return $this->winner;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
