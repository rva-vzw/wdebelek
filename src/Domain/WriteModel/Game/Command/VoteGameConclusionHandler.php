<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\WriteModel\Game\Game;

final class VoteGameConclusionHandler extends AbstractGameCommandHandler
{
    public function __invoke(VoteGameConclusion $command): void
    {
        /** @var Game $game */
        $game = $this->gameRepository->get($command->getGameIdentifier());
        $game->voteGameConclusion(
            $command->getVoter(), $command->getGameConclusion()
        );
        $this->gameRepository->save($game);
    }
}
