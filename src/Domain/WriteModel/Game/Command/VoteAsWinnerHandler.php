<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\WriteModel\Game\Game;

final class VoteAsWinnerHandler extends AbstractGameCommandHandler
{
    public function __invoke(VoteAsWinner $command): void
    {
        /** @var Game $game */
        $game = $this->gameRepository->get($command->getGameIdentifier());
        $game->voteWinner(
            $command->getVoter(),
            $command->getWinner(),
            $command->getTrickNumber()
        );
        $this->gameRepository->save($game);
    }
}
