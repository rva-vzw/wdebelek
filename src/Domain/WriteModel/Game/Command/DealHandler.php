<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\WriteModel\Game\Game;

final class DealHandler extends AbstractGameCommandHandler
{
    public function __invoke(Deal $command): void
    {
        /** @var Game $game */
        $game = $this->gameRepository->get($command->getGameIdentifier());
        $game->deal($command->getDealingStrategy());
        $this->gameRepository->save($game);
    }
}
