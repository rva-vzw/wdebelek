<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\WriteModel\Game\Game;

final class PickUpCardHandler extends AbstractGameCommandHandler
{
    public function __invoke(PickUpCard $command): void
    {
        /** @var Game $game */
        $game = $this->gameRepository->get($command->getGameIdentifier());
        $game->pickUpCard(
            $command->getPlayerIdentifier(),
            $command->getTrickNumber()
        );
        $this->gameRepository->save($game);
    }
}
