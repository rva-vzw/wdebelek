<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\WriteModel\Game\Game;

final class RevealHandHandler extends AbstractGameCommandHandler
{
    public function __invoke(RevealHand $command): void
    {
        /** @var Game $game */
        $game = $this->gameRepository->get($command->getGameIdentifier());
        $game->revealHand($command->getPlayerIdentifier());
        $this->gameRepository->save($game);
    }
}
