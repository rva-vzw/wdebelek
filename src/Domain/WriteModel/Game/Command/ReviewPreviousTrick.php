<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class ReviewPreviousTrick implements Command
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var PlayerIdentifier */
    private $reviewer;
    /** @var int */
    private $trickNumber;

    public function __construct(
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $reviewer,
        int $trickNumber
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->reviewer = $reviewer;
        $this->trickNumber = $trickNumber;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getReviewer(): PlayerIdentifier
    {
        return $this->reviewer;
    }

    public function getTrickNumber(): int
    {
        return $this->trickNumber;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
