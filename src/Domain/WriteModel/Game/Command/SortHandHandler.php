<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\WriteModel\Game\Game;

final class SortHandHandler extends AbstractGameCommandHandler
{
    public function __invoke(SortHand $command): void
    {
        /** @var Game $game */
        $game = $this->gameRepository->get($command->getGameIdentifier());
        $game->sortHand(
            $command->getPlayerIdentifier(),
            $command->getHand()
        );
        $this->gameRepository->save($game);
    }
}
