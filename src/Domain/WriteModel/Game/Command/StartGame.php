<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class StartGame implements Command
{
    private GameIdentifier $gameIdentifier;
    private SimplePlayerIdentifiers $players;
    private SimplePileOfCards $cards;
    private TableIdentifier $tableIdentifier;
    private PlayerIdentifier $dealer;
    private \DateTimeImmutable $startedAt;

    public function __construct(
        GameIdentifier $gameIdentifier,
        SimplePlayerIdentifiers $players,
        SimplePileOfCards $cards,
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $dealer,
        \DateTimeImmutable $startedAt
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->players = $players;
        $this->cards = $cards;
        $this->tableIdentifier = $tableIdentifier;
        $this->dealer = $dealer;
        $this->startedAt = $startedAt;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getPlayers(): SimplePlayerIdentifiers
    {
        return $this->players;
    }

    public function getCards(): SimplePileOfCards
    {
        return $this->cards;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getDealer(): PlayerIdentifier
    {
        return $this->dealer;
    }

    public function getStartedAt(): \DateTimeImmutable
    {
        return $this->startedAt;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
