<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\Teams;
use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class TeamUpPlayers implements Command
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var Teams */
    private $teams;
    /** @var PlayerIdentifier */
    private $playerIdentifier;

    public function __construct(
        GameIdentifier $gameIdentifier,
        Teams $teams,
        PlayerIdentifier $playerIdentifier
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->teams = $teams;
        $this->playerIdentifier = $playerIdentifier;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getTeams(): Teams
    {
        return $this->teams;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
