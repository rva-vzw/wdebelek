<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class CutCardsAfter implements Command
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var int */
    private $numberOfCards;

    public function __construct(GameIdentifier $gameIdentifier, int $numberOfCards)
    {
        $this->gameIdentifier = $gameIdentifier;
        $this->numberOfCards = $numberOfCards;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getNumberOfCards(): int
    {
        return $this->numberOfCards;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
