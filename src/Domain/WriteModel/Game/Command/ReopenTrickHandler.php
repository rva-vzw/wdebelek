<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\WriteModel\Game\Game;

final class ReopenTrickHandler extends AbstractGameCommandHandler
{
    public function __invoke(ReopenTrick $command): void
    {
        /** @var Game $game */
        $game = $this->gameRepository->get($command->getGameIdentifier());
        $game->reopenTrick(
            $command->getReopeningPlayer(),
            $command->getTrickNumber()
        );
        $this->gameRepository->save($game);
    }
}
