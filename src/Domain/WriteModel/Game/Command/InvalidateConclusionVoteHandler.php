<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\WriteModel\Game\Game;

final class InvalidateConclusionVoteHandler extends AbstractGameCommandHandler
{
    public function __invoke(InvalidateConclusionVote $command): void
    {
        /** @var Game $game */
        $game = $this->gameRepository->get($command->getGameIdentifier());
        $game->invalidateConclusionVote($command->getPlayerIdentifier());
        $this->gameRepository->save($game);
    }
}
