<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Command;

use App\Domain\WriteModel\Game\Game;

final class UnrevealCardsHandler extends AbstractGameCommandHandler
{
    public function __invoke(UnrevealCards $command): void
    {
        /** @var Game $game */
        $game = $this->gameRepository->get($command->getGameIdentifier());
        $game->unrevealCards($command->getPlayerIdentifier());
        $this->gameRepository->save($game);
    }
}
