<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Event;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class VotedAsWinner implements Event
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var PlayerIdentifier */
    private $voter;
    /** @var PlayerIdentifier */
    private $winner;
    /** @var int */
    private $trickNumber;

    public function __construct(
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $voter,
        PlayerIdentifier $winner,
        int $trickNumber
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->voter = $voter;
        $this->winner = $winner;
        $this->trickNumber = $trickNumber;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getVoter(): PlayerIdentifier
    {
        return $this->voter;
    }

    public function getWinner(): PlayerIdentifier
    {
        return $this->winner;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getTrickNumber(): int
    {
        return $this->trickNumber;
    }
}
