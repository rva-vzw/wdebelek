<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Event;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

/**
 * This event is emitted when the revealed trump is not part of the game.
 */
final class NonPlayingTrumpRevealed implements ClientRelevantTableEvent
{
    private GameIdentifier $gameIdentifier;
    private Card $trump;
    private TableIdentifier $tableIdentifier;
    private PlayerIdentifier $revealingPlayer;

    public function __construct(
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $revealingPlayer,
        Card $trump,
        TableIdentifier $tableIdentifier
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->trump = $trump;
        $this->tableIdentifier = $tableIdentifier;
        $this->revealingPlayer = $revealingPlayer;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getTrump(): Card
    {
        return $this->trump;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getRevealingPlayer(): PlayerIdentifier
    {
        return $this->revealingPlayer;
    }
}
