<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Event;

use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Player\Exception\PlayerNotFound;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

// Make sure to avoid reloading the player's cards every time a GameStarted, because this caused #97
// https://gitlab.com/rva-vzw/wdebelek/-/issues/97

final class GameStarted implements ClientRelevantTableEvent
{
    private GameIdentifier $gameIdentifier;
    private SimplePlayerIdentifiers $players;
    private SimplePileOfCards $cards;
    private TableIdentifier $tableIdentifier;
    private PlayerIdentifier $dealer;
    private PlayerIdentifier $lastPlayer;
    private \DateTimeImmutable $startedAt;

    public function __construct(
        GameIdentifier $gameIdentifier,
        SimplePlayerIdentifiers $players,
        SimplePileOfCards $cards,
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $dealer,
        PlayerIdentifier $lastPlayer,
        \DateTimeImmutable $startedAt
    ) {
        if (!$players->has($lastPlayer)) {
            throw PlayerNotFound::create($lastPlayer);
        }

        $this->gameIdentifier = $gameIdentifier;
        $this->players = $players;
        $this->cards = $cards;
        $this->tableIdentifier = $tableIdentifier;
        $this->dealer = $dealer;
        $this->lastPlayer = $lastPlayer;
        $this->startedAt = $startedAt;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getPlayers(): SimplePlayerIdentifiers
    {
        return $this->players;
    }

    public function getCards(): SimplePileOfCards
    {
        return $this->cards;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getDealer(): PlayerIdentifier
    {
        return $this->dealer;
    }

    public function getLastPlayer(): PlayerIdentifier
    {
        return $this->lastPlayer;
    }

    public function getStartedAt(): \DateTimeImmutable
    {
        return $this->startedAt;
    }
}
