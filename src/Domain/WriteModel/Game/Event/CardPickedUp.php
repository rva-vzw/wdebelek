<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Event;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class CardPickedUp implements ClientRelevantTableEvent
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var PlayerIdentifier */
    private $playerIdentifier;
    /** @var int */
    private $trickNumber;
    /** @var Card */
    private $card;
    /** @var TableIdentifier */
    private $tableIdentifier;

    public function __construct(
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $playerIdentifier,
        Card $card,
        int $trickNumber,
        TableIdentifier $tableIdentifier
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->playerIdentifier = $playerIdentifier;
        $this->trickNumber = $trickNumber;
        $this->card = $card;
        $this->tableIdentifier = $tableIdentifier;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getTrickNumber(): int
    {
        return $this->trickNumber;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getCard(): Card
    {
        return $this->card;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }
}
