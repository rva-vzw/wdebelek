<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Event;

use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class WinnerVotesInvalidated implements Event
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var int */
    private $trickNumber;

    public function __construct(GameIdentifier $gameIdentifier, int $trickNumber)
    {
        $this->gameIdentifier = $gameIdentifier;
        $this->trickNumber = $trickNumber;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getTrickNumber(): int
    {
        return $this->trickNumber;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
