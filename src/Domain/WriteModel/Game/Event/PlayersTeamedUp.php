<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Event;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\Teams;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class PlayersTeamedUp implements ClientRelevantTableEvent
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var Teams */
    private $teams;
    /** @var PlayerIdentifier */
    private $playerIdentifier;

    public function __construct(
        GameIdentifier $gameIdentifier,
        TableIdentifier $tableIdentifier,
        Teams $teams,
        PlayerIdentifier $playerIdentifier
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->tableIdentifier = $tableIdentifier;
        $this->teams = $teams;
        $this->playerIdentifier = $playerIdentifier;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getTeams(): Teams
    {
        return $this->teams;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }
}
