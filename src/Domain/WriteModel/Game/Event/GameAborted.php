<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Event;

use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

/**
 * Game aborted because a player went away during the game.
 */
final class GameAborted implements Event
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var TableIdentifier */
    private $tableIdentifier;
    /** @var SimplePileOfCards */
    private $cards;

    public function __construct(
        GameIdentifier $gameIdentifier,
        TableIdentifier $tableIdentifier,
        SimplePileOfCards $cards
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->tableIdentifier = $tableIdentifier;
        $this->cards = $cards;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getCards(): SimplePileOfCards
    {
        return $this->cards;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
