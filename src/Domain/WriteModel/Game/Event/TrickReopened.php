<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Event;

use App\Domain\ValueObject\Game\Trick;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class TrickReopened implements ClientRelevantTableEvent
{
    public function __construct(
        private GameIdentifier $gameIdentifier,
        private PlayerIdentifier $reopenedBy,
        private PlayerIdentifier $winner,
        private int $trickNumber,
        private Trick $trick,
        private TableIdentifier $tableIdentifier
    ) {
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getReopenedBy(): PlayerIdentifier
    {
        return $this->reopenedBy;
    }

    public function getWinner(): PlayerIdentifier
    {
        return $this->winner;
    }

    public function getTrickNumber(): int
    {
        return $this->trickNumber;
    }

    public function getTrick(): Trick
    {
        return $this->trick;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->getGameIdentifier();
    }
}
