<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Event;

use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

/**
 * Cards cut.
 *
 * Note that this event doesn't care about the player that actually cut the
 * cards. It's possible that the one cutting the cards isn't involved in the
 * game, especially on a table with 6 players.
 * See: https://www.rijkvanafdronk.be/wiezen/delen/
 */
final class CardsCutAfter implements Event
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var int */
    private $numberOfCards;

    public function __construct(
        GameIdentifier $gameIdentifier,
        int $numberOfCards
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->numberOfCards = $numberOfCards;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getNumberOfCards(): int
    {
        return $this->numberOfCards;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
