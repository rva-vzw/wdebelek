<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Event;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class Dealt implements ClientRelevantTableEvent
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var PlayerIdentifier */
    private $lastPlayer;
    /** @var Card */
    private $lastCardDealt;
    /** @var TableIdentifier */
    private $tableIdentifier;

    public function __construct(
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $lastPlayer,
        Card $lastCardDealt,
        TableIdentifier $tableIdentifier
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->lastPlayer = $lastPlayer;
        $this->lastCardDealt = $lastCardDealt;
        $this->tableIdentifier = $tableIdentifier;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getLastPlayer(): PlayerIdentifier
    {
        return $this->lastPlayer;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getLastCardDealt(): Card
    {
        return $this->lastCardDealt;
    }
}
