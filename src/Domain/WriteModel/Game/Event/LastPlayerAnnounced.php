<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Event;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

/**
 * Last player (for dealing) announced.
 *
 * @deprecated This event isn't used anymore. I keep it around because it may still exist in the event store.
 */
final class LastPlayerAnnounced implements Event
{
    private GameIdentifier $gameIdentifier;
    private PlayerIdentifier $playerIdentifier;

    public function __construct(
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $playerIdentifier
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->playerIdentifier = $playerIdentifier;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
