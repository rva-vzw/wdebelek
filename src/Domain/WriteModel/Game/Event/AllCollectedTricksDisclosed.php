<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Event;

use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class AllCollectedTricksDisclosed implements ClientRelevantTableEvent
{
    public function __construct(
        private TableIdentifier $tableIdentifier,
        private GameIdentifier $gameIdentifier,
    ) {
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
