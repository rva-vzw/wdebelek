<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Event;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

/**
 * This event is emitted when the revealed trump is actually in the hand of a player.
 */
final class TrumpRevealed implements ClientRelevantTableEvent
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var PlayerIdentifier */
    private $lastPlayer;
    /** @var Card */
    private $trump;
    /** @var TableIdentifier */
    private $tableIdentifier;

    public function __construct(
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $lastPlayer,
        Card $trump,
        TableIdentifier $tableIdentifier
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->lastPlayer = $lastPlayer;
        $this->trump = $trump;
        $this->tableIdentifier = $tableIdentifier;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getLastPlayer(): PlayerIdentifier
    {
        return $this->lastPlayer;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getTrump(): Card
    {
        return $this->trump;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }
}
