<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Event;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class HandShownToSpectator implements ClientRelevantTableEvent
{
    private TableIdentifier $tableIdentifier;
    private PlayerIdentifier $playerIdentifier;
    private PlayerIdentifier $spectatorIdentifier;
    private GameIdentifier $gameIdentifier;

    public function __construct(
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $playerIdentifier,
        PlayerIdentifier $spectatorIdentifier,
        GameIdentifier $gameIdentifier
    ) {
        $this->tableIdentifier = $tableIdentifier;
        $this->playerIdentifier = $playerIdentifier;
        $this->spectatorIdentifier = $spectatorIdentifier;
        $this->gameIdentifier = $gameIdentifier;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getPlayerIdentifier(): PlayerIdentifier
    {
        return $this->playerIdentifier;
    }

    public function getSpectatorIdentifier(): PlayerIdentifier
    {
        return $this->spectatorIdentifier;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->gameIdentifier;
    }
}
