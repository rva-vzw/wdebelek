<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Event;

use App\Domain\ValueObject\Game\PlayedTrick;
use App\Domain\ValueObject\Game\Trick;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use App\Publishing\ClientRelevantTableEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class TrickWon implements ClientRelevantTableEvent
{
    /** @var GameIdentifier */
    private $gameIdentifier;
    /** @var PlayerIdentifier */
    private $winner;
    /** @var int */
    private $trickNumber;
    /** @var Trick */
    private $trick;
    /** @var TableIdentifier */
    private $tableIdentifier;

    public function __construct(
        GameIdentifier $gameIdentifier,
        PlayerIdentifier $winner,
        int $trickNumber,
        Trick $trick,
        TableIdentifier $tableIdentifier
    ) {
        $this->gameIdentifier = $gameIdentifier;
        $this->winner = $winner;
        $this->trickNumber = $trickNumber;
        $this->trick = $trick;
        $this->tableIdentifier = $tableIdentifier;
    }

    public function getGameIdentifier(): GameIdentifier
    {
        return $this->gameIdentifier;
    }

    public function getWinner(): PlayerIdentifier
    {
        return $this->winner;
    }

    public function getTrickNumber(): int
    {
        return $this->trickNumber;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->getGameIdentifier();
    }

    public function getTrick(): Trick
    {
        return $this->trick;
    }

    public function getTableIdentifier(): TableIdentifier
    {
        return $this->tableIdentifier;
    }

    public function getPlayedTrick(): PlayedTrick
    {
        return new PlayedTrick(
            $this->trick,
            $this->trickNumber,
            $this->winner
        );
    }
}
