<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Exception;

final class InvalidCut extends \Exception
{
    public static function create(int $minAllowed, int $maxAllowed): self
    {
        return new self("Cut should be at least $minAllowed cards, at most $maxAllowed cards");
    }
}
