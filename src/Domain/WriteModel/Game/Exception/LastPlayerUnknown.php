<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Exception;

use App\Domain\WriteModel\Game\GameIdentifier;

final class LastPlayerUnknown extends \Exception
{
    public static function forGame(GameIdentifier $gameIdentifier): self
    {
        return new self(
            "No last player known for game {$gameIdentifier->toString()}."
        );
    }
}
