<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Exception;

use App\Domain\WriteModel\Game\GameIdentifier;

final class IncorrectTrickNumber extends \Exception
{
    public static function create(
        GameIdentifier $gameIdentifier,
        int $expectedTrickNumber,
        int $actualTrickNumber
    ): self {
        return new self(
            "Incorrect trick number for game {$gameIdentifier->toString()}: "
            ."expected {$expectedTrickNumber}, got {$actualTrickNumber}."
        );
    }
}
