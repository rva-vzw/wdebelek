<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Exception;

use App\Domain\WriteModel\Game\GameIdentifier;

final class CannotReopenTrick extends \Exception
{
    public static function create(GameIdentifier $gameIdentifier, int $trickNumber): self
    {
        return new self("Cannot reopen trick {$trickNumber} for game {$gameIdentifier->toString()}");
    }
}
