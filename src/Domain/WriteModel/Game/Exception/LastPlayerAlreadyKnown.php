<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Exception;

use App\Domain\ValueObject\Player\PlayerIdentifier;

final class LastPlayerAlreadyKnown extends \Exception
{
    public static function create(PlayerIdentifier $existing, PlayerIdentifier $new): self
    {
        return new self(
            "Could not make player {$new->toString()} last player; player {$existing->toString()} is already last player."
        );
    }
}
