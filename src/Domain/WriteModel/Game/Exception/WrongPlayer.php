<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Exception;

use App\Domain\ValueObject\Player\PlayerIdentifier;

final class WrongPlayer extends \Exception
{
    public static function create(PlayerIdentifier $expectedPlayer, PlayerIdentifier $actualPlayer): self
    {
        return new self(
            "Wrong player. Expected {$expectedPlayer->toString()}, got {$actualPlayer->toString()}."
        );
    }
}
