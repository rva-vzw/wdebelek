<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Exception;

use App\Domain\ValueObject\Player\PlayerIdentifier;

final class HandNotRevealed extends \Exception
{
    public static function create(PlayerIdentifier $playerIdentifier): self
    {
        return new self(
            "Player {$playerIdentifier->toString()} should reveal their cards before showing their initial hand."
        );
    }
}
