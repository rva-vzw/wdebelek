<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Exception;

use App\Domain\ValueObject\Game\GameStatus;

final class StatusInvalid extends \Exception
{
    public static function create(GameStatus $expectedStatus, GameStatus $actualStatus): self
    {
        $message = "Status invalid. Expected $expectedStatus, got $actualStatus.";

        return new self($message);
    }
}
