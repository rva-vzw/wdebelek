<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game\Exception;

use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;

final class CheatingPlayer extends \Exception
{
    public static function create(
        PlayerIdentifier $playerIdentifier,
        TableIdentifier $tableIdentifier
    ): self {
        return new self(
            "Unexpected cards or cards missing for player {$playerIdentifier->toString()} at table {$tableIdentifier->toString()}."
        );
    }

    public static function showingCardsToFellowPlayer(): self
    {
        return new self(
            'Player tried to show hand to fellow player'
        );
    }
}
