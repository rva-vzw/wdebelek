<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game;

use RvaVzw\KrakBoem\Cqrs\Aggregate\EventSourcedWriteModelRepository;
use RvaVzw\KrakBoem\EventSourcing\EventBus\EventBus;
use RvaVzw\KrakBoem\EventSourcing\EventStore\EventStore;

/**
 * @extends EventSourcedWriteModelRepository<Game,GameIdentifier>
 */
final readonly class GameRepository extends EventSourcedWriteModelRepository
{
    public function __construct(EventStore $eventStore, EventBus $eventBus)
    {
        parent::__construct($eventStore, $eventBus, Game::class);
    }
}
