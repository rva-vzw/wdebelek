<?php

declare(strict_types=1);

namespace App\Domain\WriteModel\Game;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Card\Hand;
use App\Domain\ValueObject\Card\Hands;
use App\Domain\ValueObject\Card\PileOfCards;
use App\Domain\ValueObject\Card\SimplePileOfCards;
use App\Domain\ValueObject\Card\SimplePiles;
use App\Domain\ValueObject\DealingStrategy;
use App\Domain\ValueObject\Election\Election;
use App\Domain\ValueObject\Game\Exception\NotDealt;
use App\Domain\ValueObject\Game\Exception\PlayerDidNotPlay;
use App\Domain\ValueObject\Game\Exception\PlayerPlayedTwice;
use App\Domain\ValueObject\Game\Exception\TrickIncomplete;
use App\Domain\ValueObject\Game\GameConclusion;
use App\Domain\ValueObject\Game\GameStatus;
use App\Domain\ValueObject\Game\PlayedTrick;
use App\Domain\ValueObject\Game\PlayedTricks;
use App\Domain\ValueObject\Game\Trick;
use App\Domain\ValueObject\Player\Exception\PlayerNotFound;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\SimplePlayerIdentifiers;
use App\Domain\ValueObject\Player\Teams;
use App\Domain\WriteModel\Game\Event\AllCollectedTricksDisclosed;
use App\Domain\WriteModel\Game\Event\CardPickedUp;
use App\Domain\WriteModel\Game\Event\CardPlayed;
use App\Domain\WriteModel\Game\Event\CardsCutAfter;
use App\Domain\WriteModel\Game\Event\CardsReceived;
use App\Domain\WriteModel\Game\Event\CardsUnrevealed;
use App\Domain\WriteModel\Game\Event\ConclusionVoteInvalidated;
use App\Domain\WriteModel\Game\Event\Dealt;
use App\Domain\WriteModel\Game\Event\GameAborted;
use App\Domain\WriteModel\Game\Event\GameCanceled;
use App\Domain\WriteModel\Game\Event\GameEnded;
use App\Domain\WriteModel\Game\Event\GameStarted;
use App\Domain\WriteModel\Game\Event\HandRevealed;
use App\Domain\WriteModel\Game\Event\HandShownToSpectator;
use App\Domain\WriteModel\Game\Event\HandSorted;
use App\Domain\WriteModel\Game\Event\InitialHandShown;
use App\Domain\WriteModel\Game\Event\NonPlayingTrumpRevealed;
use App\Domain\WriteModel\Game\Event\PlayersTeamedUp;
use App\Domain\WriteModel\Game\Event\TrickReopened;
use App\Domain\WriteModel\Game\Event\TrickReviewed;
use App\Domain\WriteModel\Game\Event\TrickWon;
use App\Domain\WriteModel\Game\Event\TrumpRevealed;
use App\Domain\WriteModel\Game\Event\VotedAsWinner;
use App\Domain\WriteModel\Game\Event\VotedConclusion;
use App\Domain\WriteModel\Game\Event\WinnerVotesInvalidated;
use App\Domain\WriteModel\Game\Exception\CannotReopenTrick;
use App\Domain\WriteModel\Game\Exception\CheatingPlayer;
use App\Domain\WriteModel\Game\Exception\HandNotRevealed;
use App\Domain\WriteModel\Game\Exception\IncorrectTrickNumber;
use App\Domain\WriteModel\Game\Exception\InvalidCut;
use App\Domain\WriteModel\Game\Exception\StatusInvalid;
use App\Domain\WriteModel\Game\Exception\WrongPlayer;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\Aggregate\AbstractAggregate;

/**
 * Game aggregate.
 *
 * This is a big class, I was wondering if something could be extracted, but I don't see it.
 */
final class Game extends AbstractAggregate
{
    // FIXME: Support games for 2 players, #40.
    public const MIN_PLAYER_COUNT = 3;

    private GameIdentifier $gameIdentifier;
    private SimplePlayerIdentifiers $players;
    private SimplePileOfCards $undealtCards;
    private GameStatus $status;
    private Hands $hands;
    private Trick $currentTrick;
    private int $currentTrickNumber;
    private Election $winnerElection;
    private TableIdentifier $tableIdentifier;
    private PlayedTricks $playedTricks;
    private Election $conclusionElection;
    private SimplePlayerIdentifiers $playersWithHandRevealed;
    private Teams $teams;
    private ?Card $trumpCard;
    private PlayerIdentifier $lastPlayer;
    private Hands $initialHands;

    public static function start(
        GameIdentifier $gameIdentifier,
        SimplePlayerIdentifiers $players,
        SimplePileOfCards $cards,
        TableIdentifier $tableIdentifier,
        PlayerIdentifier $dealer,
        \DateTimeImmutable $startedAt
    ): self {
        $result = new self($gameIdentifier);
        $result->applyAndRecord(new GameStarted(
            $gameIdentifier,
            $players,
            $cards,
            $tableIdentifier,
            $dealer,
            $players->getLast(),
            $startedAt
        ));

        return $result;
    }

    public function applyGameStarted(GameStarted $event): void
    {
        $this->gameIdentifier = $event->getGameIdentifier();
        $this->players = $event->getPlayers();
        $this->undealtCards = SimplePileOfCards::fromCards($event->getCards());
        $this->status = GameStatus::NEW();
        $this->hands = Hands::empty();
        $this->winnerElection = Election::start($this->players);
        $this->tableIdentifier = $event->getTableIdentifier();
        $this->playedTricks = PlayedTricks::empty();
        $this->conclusionElection = Election::start($this->players);
        $this->playersWithHandRevealed = new SimplePlayerIdentifiers();
        $this->teams = Teams::allIndividual($event->getPlayers());
        $this->trumpCard = null;
        $this->lastPlayer = $event->getLastPlayer();
    }

    public function cutCards(int $numberOfCards): void
    {
        $this->guardStatus(GameStatus::NEW());
        $this->guardCutAfter($numberOfCards);

        $this->applyAndRecord(new CardsCutAfter($this->gameIdentifier, $numberOfCards));
    }

    public function applyCardsCutAfter(CardsCutAfter $event): void
    {
        $this->status = GameStatus::CUT();
        $this->undealtCards = $this->undealtCards->cutAfter($event->getNumberOfCards());
    }

    private function guardStatus(GameStatus $expectedStatus): void
    {
        if ($this->status == $expectedStatus) {
            return;
        }

        throw StatusInvalid::create($expectedStatus, $this->status);
    }

    public function abort(): void
    {
        $merged = $this->getCollectedCards();

        $this->applyAndRecord(new GameAborted(
            $this->gameIdentifier,
            $this->tableIdentifier,
            $merged
        ));
    }

    private function guardCutAfter(int $numberOfCards): void
    {
        // Not sure whether I should move this functionality to PileOfCards.
        $playersCount = $this->players->count();
        $cardsCount = $this->undealtCards->count();

        $minAllowed = 0;
        $maxAllowed = $cardsCount;

        if ($playersCount * 2 >= $cardsCount) {
            $minAllowed = $playersCount;
            $maxAllowed = $cardsCount - $playersCount;
        }

        if ($numberOfCards >= $minAllowed && $numberOfCards <= $maxAllowed) {
            return;
        }

        throw InvalidCut::create($minAllowed, $maxAllowed);
    }

    public function deal(DealingStrategy $strategy): void
    {
        $this->guardStatus(GameStatus::CUT());

        $lastPlayer = $this->lastPlayer;

        $lastCard = $this->undealtCards->getLast();

        $stock = $this->undealtCards;

        foreach ($strategy as $numberOfCardsToTake) {
            foreach ($this->players->withLastPlayer($lastPlayer) as $player) {
                $piles = $stock->splitAfter($numberOfCardsToTake);
                $this->applyAndRecord(new CardsReceived(
                    $this->gameIdentifier,
                    $player,
                    SimplePileOfCards::fromCards($piles->first()),
                    $this->tableIdentifier
                ));
                $stock = $piles->second();
            }
        }

        $trump = $stock->isEmpty() ? $lastCard : $stock->getFirst();

        $this->applyAndRecord(new Dealt($this->gameIdentifier, $lastPlayer, $trump, $this->tableIdentifier));
    }

    public function applyCardsReceived(CardsReceived $event): void
    {
        $this->hands = $this->hands->withAdditionalCardsForPlayer(
            $event->getPlayerIdentifier(),
            $event->getCards()
        );

        $this->undealtCards = $this->undealtCards->withCardsRemoved($event->getCards());
    }

    public function sortHand(PlayerIdentifier $playerIdentifier, Hand $cards): void
    {
        if ($this->hands->getHand($playerIdentifier)->hasSameCardsAs($cards)) {
            $this->applyAndRecord(new HandSorted(
                $this->gameIdentifier,
                $playerIdentifier,
                $cards
            ));

            return;
        }

        throw CheatingPlayer::create($playerIdentifier, $this->tableIdentifier);
    }

    public function applyHandSorted(HandSorted $event): void
    {
        $this->hands = $this->hands->withHand($event->getPlayerIdentifier(), $event->getHand());
    }

    public function applyDealt(Dealt $event): void
    {
        $this->currentTrick = Trick::empty();
        $this->currentTrickNumber = 1;
        $this->status = GameStatus::PLAYING();
        $this->trumpCard = $event->getLastCardDealt();
        $this->lastPlayer = $event->getLastPlayer();
        $this->initialHands = $this->hands;
    }

    public function play(PlayerIdentifier $playerIdentifier, Card $card, int $trickNumber): void
    {
        $this->guardStatus(GameStatus::PLAYING());
        $this->guardTrickNumber($trickNumber);
        $this->guardCardInHand($playerIdentifier, $card);
        $this->guardHasNotPlayed($playerIdentifier);

        $this->applyAndRecord(new CardPlayed(
            $this->gameIdentifier,
            $playerIdentifier,
            $card,
            $trickNumber,
            $this->tableIdentifier
        ));
    }

    private function guardTrickNumber(int $trickNumber): void
    {
        if ($this->currentTrickNumber === $trickNumber) {
            return;
        }

        throw IncorrectTrickNumber::create($this->gameIdentifier, $this->currentTrickNumber, $trickNumber);
    }

    private function guardCardInHand(PlayerIdentifier $playerIdentifier, Card $card): void
    {
        if ($this->hands->getHand($playerIdentifier)->has($card)) {
            return;
        }

        throw CheatingPlayer::create($playerIdentifier, $this->tableIdentifier);
    }

    private function guardHasNotPlayed(PlayerIdentifier $playerIdentifier): void
    {
        if ($this->currentTrick->hasCardFromPlayer($playerIdentifier)) {
            throw PlayerPlayedTwice::create($playerIdentifier);
        }
    }

    public function applyCardPlayed(CardPlayed $event): void
    {
        $this->currentTrick = $this->currentTrick->withPlayedCard(
            $event->getPlayerIdentifier(),
            $event->getCard()
        );
        $this->hands = $this->hands->withoutPlayerCard(
            $event->getPlayerIdentifier(),
            $event->getCard()
        );
    }

    public function pickUpCard(PlayerIdentifier $playerIdentifier, int $trickNumber): void
    {
        $this->guardStatus(GameStatus::PLAYING());
        $this->guardTrickNumber($trickNumber);
        $this->guardPlayedCard($playerIdentifier);

        if ($this->winnerElection->hasVote()) {
            $this->applyAndRecord(new WinnerVotesInvalidated(
                $this->gameIdentifier,
                $trickNumber
            ));
        }

        $this->applyAndRecord(new CardPickedUp(
            $this->gameIdentifier,
            $playerIdentifier,
            $this->currentTrick->getCardPlayedBy($playerIdentifier),
            $trickNumber,
            $this->tableIdentifier
        ));
    }

    private function guardPlayedCard(PlayerIdentifier $playerIdentifier): void
    {
        if ($this->currentTrick->hasCardFromPlayer($playerIdentifier)) {
            return;
        }

        throw PlayerDidNotPlay::create($playerIdentifier);
    }

    public function applyCardPickedUp(CardPickedUp $event): void
    {
        $this->currentTrick = $this->currentTrick->withCardWithdrawn(
            $event->getPlayerIdentifier()
        );
        $this->hands = $this->hands->withAdditionalCardForPlayer(
            $event->getPlayerIdentifier(),
            $event->getCard()
        );
    }

    public function voteWinner(PlayerIdentifier $voter, PlayerIdentifier $winner, int $trickNumber): void
    {
        $this->guardTrickNumber($trickNumber);
        $this->guardTrickComplete();
        $this->guardPlayer($voter);
        $this->guardPlayer($winner);

        if ($this->winnerElection->playerHasVoted($voter) && $this->winnerElection->getVote($voter) === $winner->toString()) {
            // Don't record the vote twice.
            return;
        }

        $this->applyAndRecord(new VotedAsWinner(
            $this->gameIdentifier,
            $voter,
            $winner,
            $trickNumber
        ));

        if ($this->winnerElection->hasWinner()) {
            $this->collectTrick();
        }
    }

    private function guardTrickComplete(): void
    {
        if ($this->currentTrick->count() === $this->players->count()) {
            return;
        }

        throw TrickIncomplete::create($this->currentTrick, $this->players);
    }

    private function guardPlayer(PlayerIdentifier $playerIdentifier): void
    {
        if ($this->players->has($playerIdentifier)) {
            return;
        }

        throw PlayerNotFound::create($playerIdentifier);
    }

    public function applyVotedAsWinner(VotedAsWinner $event): void
    {
        $this->winnerElection = $this->winnerElection->withVote(
            $event->getVoter(),
            $event->getWinner()->toString()
        );
    }

    public function applyTrickWon(TrickWon $event): void
    {
        // Should keep track of all tricks, because we'll need them when
        // recollecting the cards.
        $this->playedTricks = $this->playedTricks->withPlayedTrick(
            new PlayedTrick(
                $this->currentTrick,
                $event->getTrickNumber(),
                $event->getWinner()
            )
        );
        $this->currentTrick = Trick::empty();
        ++$this->currentTrickNumber;
        $this->winnerElection = Election::start($this->players);
    }

    public function applyWinnerVotesInvalidated(WinnerVotesInvalidated $event): void
    {
        $this->winnerElection = Election::start($this->players);
    }

    public function endGame(): void
    {
        $this->guardStatus(GameStatus::PLAYING());
        $merged = $this->getCollectedCards();

        $this->applyAndRecord(new GameEnded(
            $this->gameIdentifier,
            $this->tableIdentifier,
            $merged
        ));
    }

    private function getPilesFromPlayedTricks(): SimplePiles
    {
        /** @var SimplePileOfCards[] $piles */
        $piles = [];

        // Played tricks are lifo, so use `getReverseIterator`.
        /** @var PlayedTrick $playedTrick */
        foreach ($this->playedTricks->getReverseIterator() as $playedTrick) {
            $teamNumber = $this->teams->getTeamNumber($playedTrick->getWinner());
            if (!array_key_exists($teamNumber, $piles)) {
                $piles[$teamNumber] = SimplePileOfCards::empty();
            }
            $piles[$teamNumber] = $piles[$teamNumber]->mergedWith($playedTrick);
        }

        return new SimplePiles(...array_values($piles));
    }

    public function applyGameEnded(GameEnded $event): void
    {
        $this->status = GameStatus::DONE();
    }

    public function voteGameConclusion(PlayerIdentifier $voter, GameConclusion $conclusion): void
    {
        $this->guardPlayer($voter);
        $this->guardStatus(GameStatus::PLAYING());

        if ($this->conclusionElection->playerHasVoted($voter) && $this->conclusionElection->getVote($voter) == $conclusion) {
            return;
        }

        $this->applyAndRecord(new VotedConclusion(
            $this->gameIdentifier,
            $voter,
            $conclusion,
            $this->tableIdentifier
        ));

        if ($this->conclusionElection->hasWinner()) {
            switch ($this->conclusionElection->getWinningOption()) {
                case (string) GameConclusion::DONE():
                    $this->endGame();
                    break;
                case (string) GameConclusion::REDEAL():
                    $this->cancelGame();
                    break;
                default:
                    throw new \LogicException('This should never happen.');
            }
        }
    }

    public function applyVotedConclusion(VotedConclusion $event): void
    {
        $this->conclusionElection = $this->conclusionElection->withVote(
            $event->getPlayerIdentifier(),
            $event->getConclusion()->toString(),
        );
    }

    public function invalidateConclusionVote(PlayerIdentifier $playerIdentifier): void
    {
        $this->guardPlayer($playerIdentifier);
        $this->guardStatus(GameStatus::PLAYING());

        if (!$this->conclusionElection->playerHasVoted($playerIdentifier)) {
            return;
        }

        $vote = GameConclusion::fromString($this->conclusionElection->getVote($playerIdentifier));

        $this->applyAndRecord(new ConclusionVoteInvalidated(
            $this->gameIdentifier,
            $playerIdentifier,
            $vote,
            $this->tableIdentifier
        ));
    }

    public function applyConclusionVoteInvalidated(ConclusionVoteInvalidated $event): void
    {
        $this->conclusionElection = $this->conclusionElection->withVoteWithdrawn($event->getPlayerIdentifier());
    }

    public function cancelGame(): void
    {
        $this->guardStatus(GameStatus::PLAYING());
        $merged = $this->getCollectedCards();

        $this->applyAndRecord(new GameCanceled(
            $this->gameIdentifier,
            $this->tableIdentifier,
            $merged
        ));
    }

    public function applyGameCanceled(GameCanceled $event): void
    {
        $this->status = GameStatus::DONE();
    }

    public function revealHand(PlayerIdentifier $playerIdentifier): void
    {
        $this->guardPlayer($playerIdentifier);
        // Let's not allow to reveal cards when not dealt.
        $this->guardDealt();

        if ($this->playersWithHandRevealed->has($playerIdentifier)) {
            return;
        }

        $this->applyAndRecord(new HandRevealed(
            $this->gameIdentifier,
            $playerIdentifier,
            $this->hands->getHand($playerIdentifier),
            $this->tableIdentifier
        ));
    }

    public function applyHandRevealed(HandRevealed $event): void
    {
        $this->playersWithHandRevealed = $this->playersWithHandRevealed->with($event->getPlayerIdentifier());
    }

    public function teamUpPlayers(Teams $teams, PlayerIdentifier $playerIdentifier): void
    {
        $this->guardPlayer($playerIdentifier);
        $this->guardPlayers($teams->getIndividualPlayers());
        $this->applyAndRecord(new PlayersTeamedUp(
            $this->gameIdentifier, $this->tableIdentifier, $teams, $playerIdentifier
        ));
    }

    public function applyPlayersTeamedUp(PlayersTeamedUp $event): void
    {
        $this->teams = $event->getTeams();
    }

    /**
     * @param \Traversable<PlayerIdentifier> $individualPlayers
     */
    private function guardPlayers(\Traversable $individualPlayers): void
    {
        foreach ($individualPlayers as $playerIdentifier) {
            $this->guardPlayer($playerIdentifier);
        }
    }

    public function applyGameAborted(GameAborted $event): void
    {
        $this->status = GameStatus::DONE();
    }

    public function revealTrump(PlayerIdentifier $playerIdentifier): void
    {
        // Only the last player can reveal trump.
        $this->guardLastPlayer($playerIdentifier);
        $this->guardStatus(GameStatus::PLAYING());

        // Since the game status is 'playing', we can be sure there is actually a trump card.
        /** @var Card $trumpCard */
        $trumpCard = $this->trumpCard;

        if ($this->undealtCards->has($trumpCard)) {
            $this->applyAndRecord(new NonPlayingTrumpRevealed(
                $this->gameIdentifier,
                // pass the player to the event to record who initiated the action.
                $playerIdentifier,
                $trumpCard,
                $this->tableIdentifier
            ));

            return;
        }

        $this->applyAndRecord(new TrumpRevealed(
            $this->gameIdentifier,
            $this->hands->getCardHolder($trumpCard),
            $trumpCard,
            $this->tableIdentifier
        ));
    }

    private function guardLastPlayer(PlayerIdentifier $playerIdentifier): void
    {
        if ($playerIdentifier == $this->lastPlayer) {
            return;
        }

        throw WrongPlayer::create($this->lastPlayer, $playerIdentifier);
    }

    public function showInitialHand(PlayerIdentifier $playerIdentifier): void
    {
        $this->guardPlayer($playerIdentifier);
        $this->guardDealt();

        if ($this->playersWithHandRevealed->has($playerIdentifier)) {
            // One should put their current cards at the table first, before
            // all the cards can be shown (safety measure).

            // FIXME: guard that initial hand isn't shown multiple times?
            $this->applyAndRecord(new InitialHandShown(
                $this->gameIdentifier,
                $playerIdentifier,
                $this->initialHands->getHand($playerIdentifier),
                $this->tableIdentifier
            ));

            return;
        }

        throw HandNotRevealed::create($playerIdentifier);
    }

    private function guardDealt(): void
    {
        if ($this->status == GameStatus::NEW()) {
            throw NotDealt::create($this->gameIdentifier);
        }
    }

    public function reviewPreviousTrick(PlayerIdentifier $reviewer, int $trickNumber): void
    {
        $this->guardPlayer($reviewer);

        if ($this->currentTrickNumber - 1 === $trickNumber) {
            $latestTrick = $this->playedTricks->getByTrickNumber(
                $trickNumber
            );

            $this->applyAndRecord(
                new TrickReviewed(
                    $this->gameIdentifier,
                    $reviewer,
                    $latestTrick->getWinner(),
                    $latestTrick->getTrickNumber(),
                    $latestTrick->getTrick(),
                    $this->tableIdentifier
                )
            );

            return;
        }

        throw IncorrectTrickNumber::create($this->gameIdentifier, $this->currentTrickNumber - 1, $trickNumber);
    }

    public function applyCardsUnrevealed(CardsUnrevealed $event): void
    {
        $this->playersWithHandRevealed = $this->playersWithHandRevealed->without($event->getPlayerIdentifier());
    }

    public function unrevealCards(PlayerIdentifier $playerIdentifier): void
    {
        $this->guardPlayer($playerIdentifier);

        if ($this->playersWithHandRevealed->has($playerIdentifier)) {
            $this->applyAndRecord(new CardsUnrevealed(
                $this->gameIdentifier,
                $playerIdentifier,
                $this->tableIdentifier
            ));
        }
    }

    private function getCollectedCards(): SimplePileOfCards
    {
        $piles = $this->getPilesFromPlayedTricks()
            ->withAdditionalPiles($this->hands)
            ->withAdditionalPile($this->currentTrick)
            ->withAdditionalPile($this->undealtCards)
            ->shuffled();

        // something here is more convoluted than it should be...
        return SimplePileOfCards::fromCards($piles->merged());
    }

    public function showHandToSpectator(
        PlayerIdentifier $playerIdentifier,
        PlayerIdentifier $spectator
    ): void {
        // Let's prevent that a player shows their cards to another playing player
        $this->guardNotPlaying($spectator);

        $this->applyAndRecord(new HandShownToSpectator(
            $this->tableIdentifier,
            $playerIdentifier,
            $spectator,
            $this->gameIdentifier
        ));
    }

    private function guardNotPlaying(PlayerIdentifier $spectator): void
    {
        if ($this->players->has($spectator)) {
            throw CheatingPlayer::showingCardsToFellowPlayer();
        }
    }

    public function reopenTrick(PlayerIdentifier $playerIdentifier, int $trickNumber): void
    {
        $this->guardPlayer($playerIdentifier);

        if ($this->currentTrickNumber - 1 === $trickNumber && $this->currentTrick->isEmpty()) {
            $latestTrick = $this->playedTricks->getByTrickNumber($trickNumber);

            $this->applyAndRecord(
                new WinnerVotesInvalidated(
                    $this->gameIdentifier,
                    $this->currentTrickNumber - 1
                )
            );
            $this->applyAndRecord(new TrickReopened(
                $this->gameIdentifier,
                $playerIdentifier,
                $latestTrick->getWinner(),
                $latestTrick->getTrickNumber(),
                $latestTrick->getTrick(),
                $this->tableIdentifier
            ));

            return;
        }

        throw CannotReopenTrick::create($this->gameIdentifier, $trickNumber);
    }

    public function applyTrickReopened(TrickReopened $event): void
    {
        $this->currentTrick = $event->getTrick();
        $this->playedTricks = $this->playedTricks->withoutTrick($event->getTrickNumber());
        --$this->currentTrickNumber;
    }

    private function collectTrick(): void
    {
        $this->applyAndRecord(
            new TrickWon(
                $this->gameIdentifier,
                PlayerIdentifier::fromString($this->winnerElection->getWinningOption()),
                $this->currentTrickNumber,
                $this->currentTrick,
                $this->tableIdentifier
            )
        );
        if ($this->hands->allEmpty()) {
            // For now we'll automatically disclose all collected tricks if all hands are empty.
            $this->applyAndRecord(new AllCollectedTricksDisclosed($this->tableIdentifier, $this->gameIdentifier));
        }
    }
}
