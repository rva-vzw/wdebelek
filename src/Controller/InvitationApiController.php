<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Table\Command\InvitePlayer;
use App\Domain\WriteModel\Table\Command\PublishInvitation;
use App\Domain\WriteModel\Table\Command\UnpublishInvitation;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class InvitationApiController extends AbstractController
{
    /** @var CommandBus */
    private $commandBus;

    public function __construct(
        CommandBus $commandBus
    ) {
        $this->commandBus = $commandBus;
    }

    /**
     * @param non-empty-string $tableSecret
     * @param non-empty-string $invitation
     * @param non-empty-string $hostSecret
     *
     * @Route("/api/invitations/private/{tableSecret}/{invitation}/{hostSecret}", name="wdebelek.api.invite_player", methods="PUT", options={"expose"=true})
     */
    public function invitePlayer(
        string $tableSecret,
        string $invitation,
        string $hostSecret,
        Request $request
    ): Response {
        /** @var string $content */
        $content = $request->getContent();
        $params = json_decode($content, true);
        /** @var string $lang */
        $lang = $params['lang'];

        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString($tableSecret));
        $invitation = Invitation::fromString($invitation);

        $this->commandBus->dispatch(
            new InvitePlayer(
                $tableIdentifier,
                $invitation,
                PlayerIdentifier::withSecret(Secret::fromString($hostSecret)),
                Language::fromString($lang)
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $tableSecret
     * @param non-empty-string $invitation
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/invitations/public/{tableSecret}/{invitation}/{playerSecret}", name="wdebelek.api.publish_invitation", methods="PUT", options={"expose"=true})
     */
    public function publishInvitation(
        string $tableSecret,
        string $invitation,
        // FIXME: Let's make this a post parameter, see #118.
        string $playerSecret
    ): Response {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString($tableSecret));
        $invitation = Invitation::fromString($invitation);
        $playerIdentifier = PlayerIdentifier::withSecret(Secret::fromString($playerSecret));

        $this->commandBus->dispatch(
            new PublishInvitation(
                $tableIdentifier,
                $invitation,
                $playerIdentifier,
                new \DateTimeImmutable()
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $tableSecret
     * @param non-empty-string $invitation
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/invitations/public/{tableSecret}/{invitation}/{playerSecret}", name="wdebelek.api.unpublish_invitation", methods="DELETE", options={"expose"=true})
     */
    public function unpublishInvitation(
        string $tableSecret,
        string $invitation,
        // FIXME: Let's make this a post parameter, see #118.
        string $playerSecret
    ): Response {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString($tableSecret));
        $invitation = Invitation::fromString($invitation);
        $playerIdentifier = PlayerIdentifier::withSecret(Secret::fromString($playerSecret));

        $this->commandBus->dispatch(
            new UnpublishInvitation(
                $tableIdentifier,
                $invitation,
                $playerIdentifier,
                new \DateTimeImmutable()
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
