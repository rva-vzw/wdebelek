<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\ReadModel\ActiveTables\ActiveTables;
use App\Domain\ReadModel\PlayersAtTable\PlayersAtTable;
use App\Domain\ReadModel\PlayingPlayer\PlayingPlayers;
use App\Domain\ReadModel\WelcomingTable\RecentWelcomingTables;
use App\Domain\WriteModel\Table\TableIdentifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\WebLink\Link;

final class TableApiController extends AbstractController
{
    public const INVITATION_DATE_THRESHOLD = '-6 hours';

    /** @var NormalizerInterface */
    private $normalizer;

    public function __construct(NormalizerInterface $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    /**
     * @param non-empty-string $tableId
     *
     * @Route("/api/table/{tableId}", name="wdebelek.api.get_table", options={"expose"=true})
     */
    public function getTableState(
        string $tableId,
        Request $request,
        ActiveTables $activeTables
    ): Response {
        // Publish mercure link
        $hubUrl = $this->getParameter('mercure.default_hub');
        assert(is_string($hubUrl));
        $this->addLink($request, new Link('mercure', $hubUrl));

        $tableIdentifier = TableIdentifier::fromString($tableId);

        if ($activeTables->hasActiveTable($tableIdentifier)) {
            $tableState = $activeTables->getTableState(
                TableIdentifier::fromString($tableId)
            );

            return new JsonResponse($this->normalizer->normalize($tableState));
        }

        return new JsonResponse([]);
    }

    /**
     * @param non-empty-string $tableId
     *
     * @Route("/api/table/{tableId}/players", name="wdebelek.api.get_players", options={"expose"=true})
     */
    public function getActivePlayers(
        string $tableId,
        PlayersAtTable $playersAtTable
    ): Response {
        $players = iterator_to_array($playersAtTable->getPlayersAtTable(
            TableIdentifier::fromString($tableId)
        ));

        return new JsonResponse($this->normalizer->normalize($players));
    }

    /**
     * @param non-empty-string $tableId
     *
     * @Route("/api/table/{tableId}/players/playing", name="wdebelek.api.get_playing_players", options={"expose"=true})
     */
    public function getPlayingPlayers(
        PlayingPlayers $fellowPlayers,
        string $tableId
    ): Response {
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $playersArray = iterator_to_array(
            $fellowPlayers->getOrderedPlayingPlayers($tableIdentifier)
        );

        return new JsonResponse(
            $this->normalizer->normalize($playersArray),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/api/table", name="wdebelek.api.get_welcoming_tables", options={"expose"=true})
     */
    public function getWelcomingTables(
        RecentWelcomingTables $welcomingTables,
        Request $request
    ): Response {
        // Publish mercure link
        $hubUrl = $this->getParameter('mercure.default_hub');
        assert(is_string($hubUrl));
        $this->addLink($request, new Link('mercure', $hubUrl));

        $tables = iterator_to_array(
            $welcomingTables->getRecentWelcomingTables(
                new \DateTimeImmutable(self::INVITATION_DATE_THRESHOLD)
            )
        );

        return new JsonResponse($this->normalizer->normalize($tables));
    }
}
