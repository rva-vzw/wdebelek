<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\ReadModel\OccupiedSeats\OccupiedSeats;
use App\Domain\ReadModel\PlayerProfile\PlayerProfiles;
use App\Domain\ReadModel\TableState\SuspendedTables;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Secret;
use App\Domain\ValueObject\Table\CardGame;
use App\Domain\WriteModel\Table\Command\ChooseGame;
use App\Domain\WriteModel\Table\Command\InvitePlayer;
use App\Domain\WriteModel\Table\Command\KickPlayer;
use App\Domain\WriteModel\Table\Command\PrepareGame;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

final class OrganizerApiController extends AbstractController
{
    /** @var NormalizerInterface */
    private $normalizer;
    /** @var CommandBus */
    private $commandBus;

    public function __construct(
        CommandBus $commandBus,
        NormalizerInterface $normalizer
    ) {
        $this->normalizer = $normalizer;
        $this->commandBus = $commandBus;
    }

    /**
     * Retrieve occupied seats.
     *
     * Needs table secret, because this also returns invitation links.
     *
     * @param non-empty-string $tableSecret
     *
     * @Route("/api/admin/{tableSecret}", name="wdebelek.api.get_seats", options={"expose"=true})
     */
    public function getSeats(
        string $tableSecret,
        OccupiedSeats $playersAtTables
    ): Response {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString($tableSecret));
        $players = $playersAtTables->getOccupiedSeats($tableIdentifier);

        return new JsonResponse($this->normalizer->normalize($players));
    }

    /**
     * @param non-empty-string $tableSecret
     *
     * @Route("/api/admin/{tableSecret}/game", name="wdebelek.api.prepare_game", methods="POST", options={"expose"=true})
     */
    public function prepareGame(
        Request $request,
        string $tableSecret,
        SuspendedTables $suspendedTables
    ): Response {
        /** @var string $content */
        $content = $request->getContent();
        $params = json_decode($content, true);

        /** @var string $dealer */
        $dealer = $params['dealer'];
        Assert::stringNotEmpty($dealer);
        /** @var string $hostSecret */
        $hostSecret = $params['hostSecret'];
        Assert::stringNotEmpty($hostSecret);

        $numberOfCards = 52;
        if (array_key_exists('cards', $params)) {
            $numberOfCards = (int) $params['cards'];
        }

        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString($tableSecret));

        // TODO: Check for first game to choose between resume playing and prepare first game
        $this->commandBus->dispatch(
            new PrepareGame(
                $tableIdentifier,
                PlayerIdentifier::fromString($dealer),
                PlayerIdentifier::withSecret(Secret::fromString($hostSecret)),
                $numberOfCards
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $hostSecret
     * @param non-empty-string $playerId
     * @param non-empty-string $tableSecret
     *
     * @Route("/api/admin/{tableSecret}/players/{hostSecret}/{playerId}", name="wdebelek.api.kick_player", methods="DELETE", options={"expose"=true})
     */
    public function kickPlayer(
        string $tableSecret,
        string $playerId,
        string $hostSecret,
        PlayerProfiles $playerProfiles
    ): Response {
        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString($tableSecret));
        $playerToBeKicked = PlayerIdentifier::fromString($playerId);
        $kickingPlayer = PlayerIdentifier::withSecret(Secret::fromString($hostSecret));

        $kickingPlayerProfile = $playerProfiles->getPlayerProfile($tableIdentifier, $kickingPlayer);

        $this->commandBus->dispatch(
            new KickPlayer($tableIdentifier, $playerToBeKicked, $kickingPlayer),
            // Let's create a new invite immediately, see also #148.
            new InvitePlayer($tableIdentifier, Invitation::create(), $kickingPlayer, $kickingPlayerProfile->getLanguage())
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $tableSecret
     *
     * @Route("/api/admin/{tableSecret}/config/game", name="wdebelek.api.choose_game", methods="PUT", options={"expose"=true})
     */
    public function chooseCardGame(
        Request $request,
        string $tableSecret
    ): Response {
        /** @var string $content */
        $content = $request->getContent();
        $params = json_decode($content, true);

        /** @var string $playerSecret */
        $playerSecret = $params['playerSecret'];
        Assert::stringNotEmpty($playerSecret);

        /** @var string $cardGameName */
        $cardGameName = $params['gameName'];

        $tableIdentifier = TableIdentifier::withSecret(Secret::fromString($tableSecret));
        $playerIdentifier = PlayerIdentifier::withSecret(Secret::fromString($playerSecret));
        $cardGame = CardGame::fromString($cardGameName);

        $this->commandBus->dispatch(
            new ChooseGame($tableIdentifier, $cardGame, $playerIdentifier)
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
