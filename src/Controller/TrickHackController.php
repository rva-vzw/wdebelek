<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\Command\PickUpCard;
use App\Domain\WriteModel\Game\Command\PlayCard;
use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Hack to work around issue with selenium and sortable.js.
 *
 * This controller allows throwing a card using an ugly web form.
 * See https://gitlab.com/rva-vzw/wdebelek/-/issues/22
 */
final class TrickHackController extends AbstractController
{
    /** @var CommandBus */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/hack/{gameId}/play/{trickNumber}/{playerSecret}", name="wdebelek.play_card_hack")
     */
    public function playCardHack(
        Request $request,
        string $gameId,
        int $trickNumber,
        string $playerSecret
    ): Response {
        $cardNumber = $request->request->get('cardNumber');

        if (is_numeric($cardNumber)) {
            $this->commandBus->dispatch(
                new PlayCard(
                    GameIdentifier::fromString($gameId),
                    PlayerIdentifier::withSecret(Secret::fromString($playerSecret)),
                    Card::fromCardNumber((int) $cardNumber),
                    $trickNumber
                )
            );
        }

        return $this->render(
            'trickHack.html.twig'
        );
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/hack/{gameId}/pick-up/{trickNumber}/{playerSecret}", name="wdebelek.pick_up_card_hack")
     */
    public function pickUpCardHack(
        Request $request,
        string $gameId,
        int $trickNumber,
        string $playerSecret
    ): Response {
        if ($request->isMethod(Request::METHOD_POST)) {
            $this->commandBus->dispatch(
                new PickUpCard(
                    GameIdentifier::fromString($gameId),
                    PlayerIdentifier::withSecret(Secret::fromString($playerSecret)),
                    $trickNumber
                )
            );
        }

        return $this->render(
            'trickHack.html.twig'
        );
    }
}
