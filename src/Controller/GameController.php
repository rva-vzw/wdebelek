<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\ReadModel\TableState\TableStates;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\GameIdentifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class GameController extends AbstractController
{
    /**
     * Show a game.
     *
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/{_locale}/play/{gameId}/{playerSecret}", options={"expose"=true}, name="wdebelek.play")
     *
     * @deprecated Please use {@see TableController::currentGame()}
     */
    public function game(
        string $gameId,
        TableStates $tableStates,
        ?string $playerSecret = null,
        string $_locale = 'en'
    ): Response {
        if (null === $playerSecret) {
            throw new \Exception('Not yet implemented. See #25.');
        }

        // Convert to value objects to enforce UUIDs.
        $gameIdentifier = GameIdentifier::fromString($gameId);
        $secret = Secret::fromString($playerSecret);
        $playerIdentifier = PlayerIdentifier::withSecret($secret);

        $gameState = $tableStates->getTableStateByGame($gameIdentifier);

        return $this->render(
            'table.html.twig',
            [
                'lang' => $_locale,
                'playerSecret' => $secret->toString(),
                'playerId' => $playerIdentifier->toString(),
                'tableId' => $gameState->getTableIdentifier()->toString(),
            ]
        );
    }
}
