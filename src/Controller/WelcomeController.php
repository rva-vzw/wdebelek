<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\ReadModel\PlayerProfile\SimplePlayerProfile;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Table\Command\ClaimTable;
use App\Domain\WriteModel\Table\Command\InvitePlayer;
use App\Domain\WriteModel\Table\Table;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class WelcomeController extends AbstractController
{
    /**
     * @Route("{_locale}/", name="wdebelek.start", options={"expose"=true})
     * @Route("/nieuw")
     * @Route("/new")
     * @Route("/")
     */
    public function __invoke(Request $request, CommandBus $commandBus, ?string $_locale = ''): Response
    {
        if (empty($_locale)) {
            return $this->redirectToRoute(
                'wdebelek.start',
                ['_locale' => 'nl']
            );
        }

        $name = $request->request->get('name');

        if (empty($name) || !is_string($name)) {
            return $this->render(
                'start.html.twig',
                ['lang' => $_locale]
            );
        }

        $playerSecret = Secret::create();
        $tableSecret = Secret::create();

        $tableIdentifier = TableIdentifier::withSecret($tableSecret);
        $playerIdentifier = PlayerIdentifier::withSecret($playerSecret);
        $language = Language::fromString($_locale);

        $commandBus->dispatch(
            new ClaimTable(
                $tableIdentifier,
                new SimplePlayerProfile(
                    $playerIdentifier,
                    PlayerName::fromString($name),
                    $language
                )
            )
        );

        // Automatically create 5 invites, see #148
        for ($i = 1; $i < Table::CAPACITY; ++$i) {
            $commandBus->dispatch(
                new InvitePlayer(
                    $tableIdentifier,
                    Invitation::create(),
                    $playerIdentifier,
                    $language
                )
            );
        }

        return $this->redirectToRoute(
            'wdebelek.table', [
                'tableSecret' => $tableSecret->toString(),
                'playerSecret' => $playerSecret->toString(),
                '_locale' => $_locale,
            ]
        );
    }

    /**
     * @Route("/{_locale}/timecheck", name="timecheck");
     */
    public function timeCheck(): Response
    {
        /** @var string $timeDump */
        /** @phpstan-ignore-next-line */
        $timeDump = var_dump(new \DateTimeImmutable(), true);

        return new Response($timeDump);
    }
}
