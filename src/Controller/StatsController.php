<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\ReadModel\GameCounter\MonthlyTotals;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class StatsController extends AbstractController
{
    /**
     * @Route("{_locale}/stats", name="wdebelek.stats", options={"expose"=true})
     */
    public function __invoke(MonthlyTotals $monthlyTotals, string $_locale): Response
    {
        return $this->render(
            'stats.html.twig',
            ['lang' => $_locale]
        );
    }
}
