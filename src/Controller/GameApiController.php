<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\ReadModel\TableState\TeamsByGame;
use App\Domain\ValueObject\DealingStrategy;
use App\Domain\ValueObject\Game\GameConclusion;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\Teams;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\Command\Deal;
use App\Domain\WriteModel\Game\Command\InvalidateConclusionVote;
use App\Domain\WriteModel\Game\Command\RevealTrump;
use App\Domain\WriteModel\Game\Command\TeamUpPlayers;
use App\Domain\WriteModel\Game\Command\VoteGameConclusion;
use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class GameApiController extends AbstractController
{
    /** @var NormalizerInterface */
    private $normalizer;
    /** @var CommandBus */
    private $commandBus;
    /** @var DenormalizerInterface */
    private $denormalizer;

    public function __construct(
        NormalizerInterface $normalizer,
        CommandBus $commandBus,
        DenormalizerInterface $denormalizer
    ) {
        $this->normalizer = $normalizer;
        $this->commandBus = $commandBus;
        $this->denormalizer = $denormalizer;
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/game/{gameId}/conclusion/vote/{playerSecret}", name="wdebelek.api.vote_conclusion", methods="PUT", options={"expose"=true})
     */
    public function voteConclusion(
        Request $request,
        string $gameId,
        string $playerSecret
    ): Response {
        /** @var string $content */
        $content = $request->getContent();
        $params = json_decode($content, true);

        $conclusion = GameConclusion::fromString((string) $params['conclusion']);
        $this->commandBus->dispatch(
            new VoteGameConclusion(
                GameIdentifier::fromString($gameId),
                PlayerIdentifier::withSecret(Secret::fromString($playerSecret)),
                $conclusion
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/game/{gameId}/conclusion/vote/{playerSecret}", name="wdebelek.api.invalidate_conclusion_vote", methods="DELETE", options={"expose"=true})
     */
    public function invalidateConclusionVote(string $gameId, string $playerSecret): Response
    {
        $this->commandBus->dispatch(
            new InvalidateConclusionVote(
                GameIdentifier::fromString($gameId),
                PlayerIdentifier::withSecret(Secret::fromString($playerSecret))
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/game/{gameId}/teams/{playerSecret}", name="wdebelek.api.teams.put", methods="PUT", options={"expose"=true})
     */
    public function teamUpPlayers(
        Request $request,
        string $gameId,
        string $playerSecret
    ): Response {
        /** @var string $content */
        $content = $request->getContent();
        $params = json_decode($content, true);

        /** @var Teams $teams */
        $teams = $this->denormalizer->denormalize($params, Teams::class);
        $this->commandBus->dispatch(
            new TeamUpPlayers(
                GameIdentifier::fromString($gameId), $teams, PlayerIdentifier::withSecret(Secret::fromString($playerSecret))
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/game/{gameId}/trump/{playerSecret}", name="wdebelek.api.reveal_trump", methods="PUT", options={"expose"=true})
     */
    public function revealTrump(string $gameId, string $playerSecret): Response
    {
        $this->commandBus->dispatch(
            new RevealTrump(
                GameIdentifier::fromString($gameId),
                PlayerIdentifier::withSecret(Secret::fromString($playerSecret))
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $gameId
     *
     * @Route("/api/game/{gameId}/teams", name="wdebelek.api.teams.get", methods="GET", options={"expose"=true})
     */
    public function getPlayerTeams(
        TeamsByGame $teamsByGame,
        string $gameId
    ): Response {
        $gameIdentifier = GameIdentifier::fromString($gameId);

        $teams = $teamsByGame->getTeamsByGame($gameIdentifier);

        return new JsonResponse(
            $this->normalizer->normalize($teams),
            Response::HTTP_OK
        );
    }

    /**
     * @param non-empty-string $gameId
     *
     * @Route("/api/game/{gameId}/deal/{playerSecret}", name="wdebelek.api.deal", methods="POST", options={"expose"=true})
     */
    public function deal(
        Request $request,
        string $gameId,
        string $playerSecret
    ): Response {
        /** @var string $content */
        $content = $request->getContent();
        $params = json_decode($content, true);

        /** @var DealingStrategy $strategy */
        $strategy = $this->denormalizer->denormalize($params['strategy'], DealingStrategy::class);

        $gameIdentifier = GameIdentifier::fromString($gameId);

        // FIXME: I shouldn't need the LastPlayer interface here. The aggregate should know
        // by itself who will be the last player.
        $this->commandBus->dispatch(
            new Deal(
                $gameIdentifier,
                $strategy
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
