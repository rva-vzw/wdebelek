<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\ReadModel\RevealedCards\CardsRevealed;
use App\Domain\ReadModel\TableState\GamesAtTable;
use App\Domain\ValueObject\Card\Hand;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\Command\RevealHand;
use App\Domain\WriteModel\Game\Command\ShowHandToSpectator;
use App\Domain\WriteModel\Game\Command\ShowInitialHand;
use App\Domain\WriteModel\Game\Command\SortHand;
use App\Domain\WriteModel\Game\Command\UnrevealCards;
use App\Domain\WriteModel\Game\GameIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class HandApiController extends AbstractController
{
    /** @var CommandBus */
    private $commandBus;
    /** @var DenormalizerInterface */
    private $denormalizer;
    /** @var NormalizerInterface */
    private $normalizer;

    public function __construct(
        DenormalizerInterface $denormalizer,
        CommandBus $commandBus,
        NormalizerInterface $normalizer
    ) {
        $this->commandBus = $commandBus;
        $this->denormalizer = $denormalizer;
        $this->normalizer = $normalizer;
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/game/{gameId}/hand/{playerSecret}", name="wdebelek.api.sort_hand", methods="PUT", options={"expose"=true})
     */
    public function sortPlayerHand(
        Request $request,
        string $gameId,
        string $playerSecret
    ): Response {
        /** @var string $content */
        $content = $request->getContent();
        $params = json_decode($content, true);

        /** @var Hand $hand */
        $hand = $this->denormalizer->denormalize($params['cards'], Hand::class);

        $this->commandBus->dispatch(
            new SortHand(
                GameIdentifier::fromString($gameId),
                PlayerIdentifier::withSecret(
                    Secret::fromString($playerSecret)
                ),
                $hand
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/game/{gameId}/hand/{playerSecret}/public/current", name="wdebelek.api.reveal_hand", methods="PUT", options={"expose"=true})
     */
    public function revealPlayerHand(
        string $gameId,
        string $playerSecret
    ): Response {
        $this->commandBus->dispatch(
            new RevealHand(
                GameIdentifier::fromString($gameId),
                PlayerIdentifier::withSecret(
                    Secret::fromString($playerSecret)
                )
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     * @param non-empty-string $spectatorId
     *
     * @Route("/api/game/{gameId}/hand/{playerSecret}/shown/{spectatorId}", name="wdebelek.api.show_to_spectator", methods="PUT", options={"expose"=true})
     */
    public function showHandToSpectator(
        string $gameId,
        string $playerSecret,
        string $spectatorId,
        GamesAtTable $gamesAtTable
    ): Response {
        $gameIdentifier = GameIdentifier::fromString($gameId);
        $this->commandBus->dispatch(
            new ShowHandToSpectator(
                PlayerIdentifier::withSecret(Secret::fromString($playerSecret)),
                PlayerIdentifier::fromString($spectatorId),
                $gameIdentifier
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/game/{gameId}/cards/{playerSecret}", name="wdebelek.api.get_shown", methods="GET", options={"expose"=true})
     */
    public function getHandsShownToPlayer(
        string $gameId,
        string $playerSecret,
        CardsRevealed $shownHands
    ): Response {
        return new JsonResponse(
            $this->normalizer->normalize(
                iterator_to_array($shownHands->getCardsRevealedTo(
                    PlayerIdentifier::withSecret(Secret::fromString($playerSecret)),
                    GameIdentifier::fromString($gameId)
                ))
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/game/{gameId}/hand/{playerSecret}/public/initial", name="wdebelek.api.show_initial", methods="PUT", options={"expose"=true})
     */
    public function showInitialCards(
        string $gameId,
        string $playerSecret
    ): Response {
        $this->commandBus->dispatch(
            new ShowInitialHand(
                GameIdentifier::fromString($gameId),
                PlayerIdentifier::withSecret(
                    Secret::fromString($playerSecret)
                )
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/game/{gameId}/hand/{playerSecret}/public/current", name="wdebelek.api.unreveal_cards", methods="DELETE", options={"expose"=true})
     */
    public function unrevealCards(
        string $gameId,
        string $playerSecret
    ): Response {
        $this->commandBus->dispatch(
            new UnrevealCards(
                GameIdentifier::fromString($gameId),
                PlayerIdentifier::withSecret(Secret::fromString($playerSecret))
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
