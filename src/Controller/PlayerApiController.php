<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\ReadModel\PersonalPlayerInfo\PersonalPlayerInfoRepository;
use App\Domain\ValueObject\Card\Hand;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Table\TableIdentifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class PlayerApiController extends AbstractController
{
    /** @var NormalizerInterface */
    private $normalizer;

    public function __construct(
        NormalizerInterface $normalizer
    ) {
        $this->normalizer = $normalizer;
    }

    /**
     * @param non-empty-string $tableId
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/table/{tableId}/player/{playerSecret}", name="wdebelek.api.get_hand", methods="GET", options={"expose"=true})
     */
    public function getPlayerInfo(
        PersonalPlayerInfoRepository $personalPlayerInfos,
        string $playerSecret,
        string $tableId
    ): Response {
        $tableIdentifier = TableIdentifier::fromString($tableId);
        $playerIdentifier = PlayerIdentifier::withSecret(
            Secret::fromString($playerSecret)
        );

        $info = $personalPlayerInfos->getPersonalPlayerInfo($playerIdentifier, $tableIdentifier);

        return new JsonResponse($this->normalizer->normalize($info));
    }
}
