<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\ReadModel\CardGames\TableCardGames;
use App\Domain\ReadModel\Exception\NotFoundInReadModel;
use App\Domain\ReadModel\OccupiedSeats\OccupiedSeats;
use App\Domain\ReadModel\PlayerProfile\SimplePlayerProfile;
use App\Domain\ReadModel\WelcomingTable\WelcomingTables;
use App\Domain\ValueObject\Invitation\Invitation;
use App\Domain\ValueObject\Player\Language;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Player\PlayerName;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\Game;
use App\Domain\WriteModel\Table\Command\AcceptInvitation;
use App\Domain\WriteModel\Table\Command\ClaimTable;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Routing\Annotation\Route;

final class TableController extends AbstractController
{
    /**
     * This shows just some screen with some info and links for testing.
     *
     * @Route("/test")
     */
    public function hello(): Response
    {
        return $this->render('welcome.html.twig');
    }

    /**
     * @param non-empty-string $tableSecret
     * @param non-empty-string $playerSecret
     *
     * @Route("{_locale}/admin/seats/{tableSecret}/manage/{playerSecret}", name="wdebelek.table")
     */
    public function manageSeats(string $_locale, string $tableSecret, string $playerSecret): Response
    {
        $tableSecret = Secret::fromString($tableSecret);
        $playerSecret = Secret::fromString($playerSecret);

        return $this->render(
            'seats.html.twig',
            [
                'locale' => $_locale,
                'tableSecret' => $tableSecret->toString(),
                'playerSecret' => $playerSecret->toString(),
                'tableId' => TableIdentifier::withSecret($tableSecret)->toString(),
                'minPlayerCount' => Game::MIN_PLAYER_COUNT,
            ]
        );
    }

    /**
     * @param non-empty-string $tableId
     * @param non-empty-string $invitation
     *
     * @Route("{_locale}/table/{tableId}/invitation/{invitation}", name="wdebelek.accept.private", options={"expose"=true})
     */
    public function acceptPrivateInvitation(
        Request $request,
        string $invitation,
        string $tableId,
        CommandBus $commandBus,
        OccupiedSeats $occupiedSeats,
        string $_locale,
        TableCardGames $tableGames
    ): Response {
        // FIXME: improve this with an OpenInvitation read model or something.

        $name = $request->request->get('name');
        $language = Language::fromString($_locale);

        if (empty($name) || !is_string($name)) {
            try {
                $occupiedSeat = $occupiedSeats->getSeatByOpenInvitation(
                    TableIdentifier::fromString($tableId),
                    Invitation::fromString($invitation)
                );
            } catch (NotFoundInReadModel $ex) {
                return $this->render('noSuchInvitation.html.twig');
            }

            return $this->render(
                'accept.html.twig',
                [
                    'hostName' => $occupiedSeat->getHostName()->toString(),
                    'gameName' => $tableGames->getCardGameByTable($occupiedSeat->getTableIdentifier()),
                    'lang' => $language->toString(),
                ]
            );
        }

        $playerSecret = Secret::create();

        try {
            $commandBus->dispatch(
                AcceptInvitation::byPlayer(
                    TableIdentifier::fromString($tableId),
                    Invitation::fromString($invitation),
                    new SimplePlayerProfile(
                        PlayerIdentifier::withSecret($playerSecret),
                        PlayerName::fromString($name),
                        $language
                    ),
                    new \DateTimeImmutable()
                )
            );
        } catch (HandlerFailedException $ex) {
            return $this->render('noSuchInvitation.html.twig');
        }

        return $this->redirectToRoute(
            'wdebelek.player', [
                                 'tableId' => $tableId,
                                 'playerSecret' => $playerSecret->toString(),
                                 '_locale' => $language->toString(),
                             ]
        );
    }

    /**
     * @param non-empty-string $tableId
     * @param non-empty-string $playerSecret
     *
     * @Route("{_locale}/table/{tableId}/play/{playerSecret}", name="wdebelek.player", options={"expose"=true})
     */
    public function currentGame(string $tableId, string $playerSecret, string $_locale): Response
    {
        $playerSecret = Secret::fromString($playerSecret);
        $playerIdentifier = PlayerIdentifier::withSecret($playerSecret);

        return $this->render(
            'table.html.twig',
            [
                'tableId' => $tableId,
                'lang' => $_locale,
                'playerId' => $playerIdentifier->toString(),
                'playerSecret' => $playerSecret->toString(),
            ]
        );
    }

    /**
     * @param non-empty-string $tableId
     *
     * @Route("{_locale}/table/{tableId}/join", name="wdebelek.accept.public", options={"expose"=true})
     */
    public function acceptPublishedInvitation(
        Request $request,
        string $tableId,
        CommandBus $commandBus,
        string $_locale,
        WelcomingTables $welcomingTables
    ): Response {
        // Let's reuse the code of 'claimTable' for now.
        // We don't want to use Symfony forms, because that's too much hassle.

        $name = $request->request->get('name');
        $language = Language::fromString($_locale);

        $table = $welcomingTables->getWelcomingTable(
            TableIdentifier::fromString($tableId)
        );

        if (!$table->hasPublishedInvitations()) {
            return $this->render('noSuchInvitation.html.twig');
        }

        if (empty($name) || !is_string($name)) {
            return $this->render(
                'accept.html.twig',
                [
                    'hostName' => $table->getHost()->toString(),
                    'gameName' => $table->getCardGame(),
                    'lang' => $language->toString(),
                ]
            );
        }

        $playerSecret = Secret::create();
        $invitation = $table->getPublishedInvitations()->getRandomInvitation();

        try {
            $commandBus->dispatch(
                AcceptInvitation::byPlayer(
                    TableIdentifier::fromString($tableId),
                    $invitation,
                    new SimplePlayerProfile(
                        PlayerIdentifier::withSecret($playerSecret),
                        PlayerName::fromString($name),
                        $language
                    ),
                    new \DateTimeImmutable()
                )
            );
        } catch (HandlerFailedException $ex) {
            return $this->render('noSuchInvitation.html.twig');
        }

        return $this->redirectToRoute(
            'wdebelek.player', [
                                 'tableId' => $tableId,
                                 'playerSecret' => $playerSecret->toString(),
                                 '_locale' => $language->toString(),
                             ]
        );
    }
}
