<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\ReadModel\GameCounter\ActiveTablesCounter;
use App\Domain\ReadModel\GameCounter\Month;
use App\Domain\ReadModel\GameCounter\MonthlyTotals;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class StatisticsApiController extends AbstractController
{
    private const A_WHILE_AGO = '-15 minutes';
    private const MIN_GAMES_FOR_RELEVANT_TABLE = 4;

    public function __construct(
        private NormalizerInterface $normalizer
    ) {
    }

    /**
     * @Route("/api/stats/active", name="wdebelek.api.stats.active_tables", options={"expose"=true})
     */
    public function getNumberOfActiveTables(ActiveTablesCounter $activeTablesCounter): Response
    {
        $aWhileAgo = new \DateTimeImmutable(self::A_WHILE_AGO);

        return new JsonResponse(
            $activeTablesCounter->getActiveTablesCount(
                new \DateTimeImmutable(),
                $aWhileAgo,
                1
            )
        );
    }

    /**
     * @Route("/api/stats/monthly", name="wdebelek.api.stats.monthly", options={"expose"=true})
     */
    public function getMonthlyTotals(MonthlyTotals $monthlyTotals): Response
    {
        $currentMonth = Month::current();
        $lastYear = Month::fromDateTimeImmutable(new \DateTimeImmutable('-1 year'));

        $totals = iterator_to_array(
            $monthlyTotals->getByMonth(
                $lastYear,
                $currentMonth,
                self::MIN_GAMES_FOR_RELEVANT_TABLE
            )
        );

        return new JsonResponse(
            $this->normalizer->normalize($totals)
        );
    }
}
