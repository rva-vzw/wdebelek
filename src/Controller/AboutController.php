<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class AboutController extends AbstractController
{
    /**
     * @Route("/{_locale}/about", name="wdebelek.about", options={"expose"=true})
     */
    public function __invoke(): Response
    {
        return $this->render('about.html.twig');
    }
}
