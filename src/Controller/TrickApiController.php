<?php

declare(strict_types=1);

namespace App\Controller;

use App\Domain\ReadModel\CollectedTricks\CollectedTricks;
use App\Domain\ReadModel\TableState\TeamsByGame;
use App\Domain\ReadModel\TrickAtTable\TricksAtTables;
use App\Domain\ValueObject\Card\Card;
use App\Domain\ValueObject\Player\PlayerIdentifier;
use App\Domain\ValueObject\Secret;
use App\Domain\WriteModel\Game\Command\PickUpCard;
use App\Domain\WriteModel\Game\Command\PlayCard;
use App\Domain\WriteModel\Game\Command\ReopenTrick;
use App\Domain\WriteModel\Game\Command\ReviewPreviousTrick;
use App\Domain\WriteModel\Game\Command\VoteAsWinner;
use App\Domain\WriteModel\Game\GameIdentifier;
use App\Domain\WriteModel\Table\TableIdentifier;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\WebLink\Link;
use Webmozart\Assert\Assert;

final class TrickApiController extends AbstractController
{
    /** @var CommandBus */
    private $commandBus;
    /** @var NormalizerInterface */
    private $normalizer;

    public function __construct(
        CommandBus $commandBus,
        NormalizerInterface $normalizer
    ) {
        $this->commandBus = $commandBus;
        $this->normalizer = $normalizer;
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/game/{gameId}/trick/{trickNumber}/{playerSecret}", name="wdebelek.api.play_card", methods="POST", options={"expose"=true})
     */
    public function playCard(
        Request $request,
        string $gameId,
        string $playerSecret,
        int $trickNumber
    ): Response {
        /** @var string $content */
        $content = $request->getContent();
        $params = json_decode($content, true);

        /** @var int $cardNumber */
        $cardNumber = $params['card'];

        $this->commandBus->dispatch(
            new PlayCard(
                GameIdentifier::fromString($gameId),
                PlayerIdentifier::withSecret(
                    Secret::fromString($playerSecret)
                ),
                Card::fromCardNumber($cardNumber),
                $trickNumber
            )
        );

        // If this fails, try reloading the test data!
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $tableId
     *
     * @Route("/api/table/{tableId}/trick", name="wdebelek.api.current_trick", methods="GET", options={"expose"=true})
     */
    public function getTrick(
        string $tableId,
        TricksAtTables $tricksAtTables,
        Request $request
    ): Response {
        // This parameter is automatically created by the MercureBundle
        $hubUrl = $this->getParameter('mercure.default_hub');
        assert(is_string($hubUrl));

        // Link: <http://localhost:3000/.well-known/mercure>; rel="mercure"
        $this->addLink($request, new Link('mercure', $hubUrl));

        $result = $this->normalizer->normalize(
            $tricksAtTables->getTrickAtTable(TableIdentifier::fromString($tableId))
        );

        return new JsonResponse($result, Response::HTTP_OK);
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/game/{gameId}/trick/{trickNumber}/{playerSecret}", name="wdebelek.api.pick_up_card", methods="DELETE", options={"expose"=true})
     */
    public function pickUpCard(
        string $gameId,
        string $playerSecret,
        int $trickNumber
    ): Response {
        $this->commandBus->dispatch(
            new PickUpCard(
                GameIdentifier::fromString($gameId),
                PlayerIdentifier::withSecret(
                    Secret::fromString($playerSecret)
                ),
                $trickNumber
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/game/{gameId}/winner/{trickNumber}/vote/{playerSecret}", name="wdebelek.api.vote_winner", methods="PUT", options={"expose"=true})
     */
    public function voteWinner(
        Request $request,
        string $gameId,
        string $playerSecret,
        int $trickNumber
    ): Response {
        /** @var string $content */
        $content = $request->getContent();
        $params = json_decode($content, true);

        $winnerId = $params['winner'];
        Assert::stringNotEmpty($winnerId);

        $this->commandBus->dispatch(
            new VoteAsWinner(
                GameIdentifier::fromString($gameId),
                PlayerIdentifier::withSecret(
                    Secret::fromString($playerSecret)
                ),
                $trickNumber,
                PlayerIdentifier::fromString($winnerId)
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $reviewerSecret
     *
     * @Route("/api/game/{gameId}/review/{trickNumber}/{reviewerSecret}", name="wdebelek.api.review_previous_trick", methods="POST", options={"expose"=true})
     */
    public function reviewPrevious(
        string $gameId,
        string $reviewerSecret,
        int $trickNumber
    ): Response {
        $this->commandBus->dispatch(
            new ReviewPreviousTrick(
                GameIdentifier::fromString($gameId),
                PlayerIdentifier::withSecret(Secret::fromString($reviewerSecret)),
                $trickNumber
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerSecret
     *
     * @Route("/api/game/{gameId}/winner/{trickNumber}/vote/{playerSecret}", name="wdebelek.api.reopen_trick", methods="DELETE", options={"expose"=true})
     */
    public function reopenTrick(
        string $gameId,
        string $playerSecret,
        int $trickNumber
    ): Response {
        $this->commandBus->dispatch(
            new ReopenTrick(
                GameIdentifier::fromString($gameId),
                PlayerIdentifier::withSecret(Secret::fromString($playerSecret)),
                $trickNumber
            )
        );

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param non-empty-string $gameId
     * @param non-empty-string $playerId
     *
     * @Route("/api/game/{gameId}/collected-by-teams/{playerId}", name="wdebelek.api.tricks_by_team", methods="GET", options={"expose"=true})
     */
    public function getCollectedTricksByPlayerTeam(
        string $gameId,
        string $playerId,
        CollectedTricks $collectedTricks,
        TeamsByGame $teamsByGame,
    ): Response {
        $gameIdentifier = GameIdentifier::fromString($gameId);
        $playerIdentifier = PlayerIdentifier::fromString($playerId);

        $teams = $teamsByGame->getTeamsByGame($gameIdentifier);
        $team = $teams->getTeamByPlayer($playerIdentifier);

        // The normalizer will only normalize the tricks, not the winners, which is fine
        // for now.
        return new JsonResponse(
            $this->normalizer->normalize(
                array_values(iterator_to_array($collectedTricks->getDisclosedTricks($gameIdentifier, $team)))
            )
        );
    }
}
