<?php

declare(strict_types=1);

namespace App\Controller;

use App\Controller\Exception\VideoCallUrlPatternMissing;
use App\Domain\WriteModel\Table\TableIdentifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class VideoCallController extends AbstractController
{
    /** @var string */
    private $videoCallUrlPattern;

    public function __construct(string $videoCallUrlPattern)
    {
        if (empty($videoCallUrlPattern)) {
            throw VideoCallUrlPatternMissing::create();
        }

        $this->videoCallUrlPattern = $videoCallUrlPattern;
    }

    /**
     * @param non-empty-string $tableId
     *
     * @Route("{_locale}/table/{tableId}/chat", name="wdebelek.chat", options={"expose"=true})
     */
    public function __invoke(string $tableId): Response
    {
        // Make sure we have something that could be a table-ID.
        $tableIdentifier = TableIdentifier::fromString($tableId);

        return $this->redirect(
            sprintf($this->videoCallUrlPattern, $tableIdentifier->toString())
        );
    }
}
