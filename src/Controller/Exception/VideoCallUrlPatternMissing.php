<?php

declare(strict_types=1);

namespace App\Controller\Exception;

final class VideoCallUrlPatternMissing extends \Exception
{
    public static function create(): self
    {
        return new self('Please provide a VIDEO_CALL_URL in your settings file.');
    }
}
