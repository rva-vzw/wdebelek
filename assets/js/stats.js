import Vue from "vue";
import VueI18n from "vue-i18n";
import MonthlyStats from "./Components/MonthlyStats";

Vue.use(VueI18n);

new Vue({
  el: '#stats',
  components: {MonthlyStats}
})
