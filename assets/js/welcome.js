import Vue from "vue";
import VueI18n from "vue-i18n";
import BootstrapVue from "bootstrap-vue";
import Welcome from "./Components/Welcome";

import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'

Vue.use(BootstrapVue);
Vue.use(VueI18n);

new Vue({
    el: '#app',
    components: {Welcome},
})
