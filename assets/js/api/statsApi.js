import axios from 'axios';
import routing from '../routing';

export default {

  getActiveTableCount() {
    let url = routing.getRoute('wdebelek.api.stats.active_tables');
    return axios.get(url).then(response => {
      return response.data
    })
  },
  getMonthlyTotals() {
    let url = routing.getRoute('wdebelek.api.stats.monthly');
    return axios.get(url).then(response => {
      return response.data
    })
  }
}