import axios from 'axios';
import routing from '../routing';

export default {
    getHubUrl(tableId) {
        let url = routing.getRoute(
            'wdebelek.api.get_table', {
                tableId: tableId
            }
        );

        return axios.get(url).then(response => {
            console.log(response);
            const hubUrl = response.headers.link.match(/<([^>]+)>;\s+rel=(?:mercure|"[^"]*mercure[^"]*")/)[1];

            console.log(hubUrl);
            return hubUrl;
        })
    },
    getTableState(tableId) {
        let url = routing.getRoute(
            'wdebelek.api.get_table', {
                tableId: tableId
            }
        );

        return axios.get(url).then(response => {
            return response.data;
        })
    },
    getPlayers(tableId) {
        let url = routing.getRoute(
            'wdebelek.api.get_players', {
                tableId: tableId
            }
        );

        return axios.get(url).then(response => {
            return response.data;
        })
    },
    getPlayingPlayers(tableId) {
        let url = routing.getRoute(
            'wdebelek.api.get_playing_players', {
                tableId: tableId
            }
        );

        return axios.get(url).then(response => {
            return response.data;
        })
    }
}
