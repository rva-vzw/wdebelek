import axios from 'axios';
import routing from '../routing';

export default {
    playCard(gameId, trickNumber, playerSecret, cardNumber) {
        let url = routing.getRoute(
            'wdebelek.api.play_card', {
                gameId: gameId,
                trickNumber: trickNumber,
                playerSecret: playerSecret
            }
        );

        return axios.post(url, {
            'card': cardNumber
        });
    },
    getCurrentTrick(tableId) {
        let url = routing.getRoute(
            'wdebelek.api.current_trick', {
                tableId: tableId
            }
        );

        return axios.get(url).then(response => {
            return response.data
        })
    },
    pickUpCard(gameId, trickNumber, playerSecret) {
        let url = routing.getRoute(
            'wdebelek.api.pick_up_card', {
                gameId: gameId,
                trickNumber: trickNumber,
                playerSecret: playerSecret
            }
        );

        return axios.delete(url);
    },
    voteWinner(gameId, trickNumber, voterSecret, winnerId) {
        let url = routing.getRoute(
            'wdebelek.api.vote_winner', {
                gameId: gameId,
                trickNumber: trickNumber,
                playerSecret: voterSecret
            }
        );

        return axios.put(url, {
            'winner': winnerId
        });
    },
    reviewPreviousTrick(gameId, trickNumber, reviewerSecret) {
        let url = routing.getRoute(
            'wdebelek.api.review_previous_trick', {
                gameId: gameId,
                trickNumber: trickNumber,
                reviewerSecret: reviewerSecret
            }
        );

        return axios.post(url);
    },
    reopenTrick(gameId, trickNumber, playerSecret) {
        let url = routing.getRoute(
          'wdebelek.api.reopen_trick', {
            gameId: gameId,
            trickNumber: trickNumber,
            playerSecret: playerSecret
          }
        );

        return axios.delete(url);
    },
    getCollectedTricksByTeam(gameId, playerId) {
        let url = routing.getRoute(
            'wdebelek.api.tricks_by_team', {
                gameId: gameId,
                playerId: playerId
            }
        );
        return axios.get(url).then(response => response.data);
    }
}
