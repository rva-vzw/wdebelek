import axios from 'axios';
import routing from '../routing';

export default {
    getHand(gameId, playerSecret) {
        let url = routing.getRoute(
            'wdebelek.api.get_hand', {
                gameId: gameId,
                playerSecret: playerSecret
            }
        );

        return axios.get(url).then(response => {
            return response.data
        })
    },
    sortHand(gameId, playerSecret, cardNumbers) {
        let url = routing.getRoute(
            'wdebelek.api.sort_hand', {
                gameId: gameId,
                playerSecret: playerSecret
            }
        );

        return axios.put(url, {
            'cards': cardNumbers
        });
    },
    // FIXME: maybe move these three to revealedCardsApi?
    revealHand(gameId, playerSecret) {
        let url = routing.getRoute(
            'wdebelek.api.reveal_hand', {
                gameId: gameId,
                playerSecret: playerSecret
            }
        );

        return axios.put(url);
    },
    showInitial(gameId, playerSecret) {
        let url = routing.getRoute(
            'wdebelek.api.show_initial', {
                gameId: gameId,
                playerSecret: playerSecret
            }
        );

        return axios.put(url);
    },
    unrevealCards(gameId, playerSecret) {
        let url = routing.getRoute(
            'wdebelek.api.unreveal_cards', {
                gameId: gameId,
                playerSecret: playerSecret
            }
        );

        return axios.delete(url);
    }
}
