import axios from 'axios';
import routing from '../routing';

export default {
    teamUpPlayers(gameId, teams, playerSecret) {
        let url = routing.getRoute(
            'wdebelek.api.teams.put', {
                gameId: gameId,
                playerSecret: playerSecret
            }
        );

        return axios.put(url, teams);
    },
    getTeams(gameId) {
        let url = routing.getRoute(
            'wdebelek.api.teams.get', {
                gameId: gameId
            }
        );

        return axios.get(url).then(res => res.data);
    }
}
