import axios from 'axios';
import routing from '../routing';

export default {
    invitePlayer(tableSecret, invitation, hostSecret, lang) {
        let url = routing.getRoute(
            'wdebelek.api.invite_player', {
                tableSecret: tableSecret,
                invitation: invitation,
                hostSecret: hostSecret
            }
        );

        return axios.put(url, {
            lang: lang
        });
    },
    publishInvitation(tableSecret, invitation, playerSecret) {
        let url = routing.getRoute(
            'wdebelek.api.publish_invitation', {
                tableSecret: tableSecret,
                invitation: invitation,
                playerSecret: playerSecret
            }
        );

        return axios.put(url)
    },
    unpublishInvitation(tableSecret, invitation, playerSecret) {
        let url = routing.getRoute(
            'wdebelek.api.unpublish_invitation', {
                tableSecret: tableSecret,
                invitation: invitation,
                playerSecret: playerSecret
            }
        );

        return axios.delete(url)
    },
    getWelcomingTables() {
        let url = routing.getRoute('wdebelek.api.get_welcoming_tables');

        return axios.get(url).then(response => response.data)
    },
    getHubUrl() {
        let url = routing.getRoute('wdebelek.api.get_welcoming_tables');

        return axios.get(url).then(response => {
            return response.headers.link.match(/<([^>]+)>;\s+rel=(?:mercure|"[^"]*mercure[^"]*")/)[1];
        })
    }
}
