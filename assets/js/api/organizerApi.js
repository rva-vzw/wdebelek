import axios from 'axios';
import routing from '../routing';

export default {
    getPlayers(tableSecret) {
        let url = routing.getRoute(
            'wdebelek.api.get_seats',
            {
                tableSecret: tableSecret
            }
        );

        return axios.get(url).then(response => {
            return response.data
        })
    },
    prepareGame(tableSecret, dealer, hostSecret, numberOfCards) {
        let url = routing.getRoute(
            'wdebelek.api.prepare_game', {
                tableSecret: tableSecret
            }
        );

        return axios.post(url, {
            dealer: dealer,
            hostSecret: hostSecret,
            cards: numberOfCards
        })
    },
    kickPlayer(tableSecret, playerId, hostSecret) {
        let url = routing.getRoute(
            'wdebelek.api.kick_player', {
                tableSecret: tableSecret,
                playerId: playerId,
                hostSecret: hostSecret
            }
        );

        return axios.delete(url);
    },
    chooseGame(tableSecret, playerSecret, gameName) {
        let url = routing.getRoute(
            'wdebelek.api.choose_game', {
                tableSecret: tableSecret
            }
        );

        return axios.put(url, {
            playerSecret: playerSecret,
            gameName: gameName
        })
    }
}
