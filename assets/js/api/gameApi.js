import axios from 'axios';
import routing from '../routing';

export default {
    voteConclusion(gameId, voterSecret, conclusion) {
        let url = routing.getRoute(
            'wdebelek.api.vote_conclusion', {
                gameId: gameId,
                playerSecret: voterSecret
            }
        );

        return axios.put(url, {
            'conclusion': conclusion
        })
    },
    invalidateConclusionVote(gameId, voterSecret) {
        let url = routing.getRoute(
            'wdebelek.api.invalidate_conclusion_vote', {
                gameId: gameId,
                playerSecret: voterSecret
            }
        );

        return axios.delete(url);
    },
    revealTrump(gameId, playerSecret) {
        let url = routing.getRoute(
            'wdebelek.api.reveal_trump', {
                gameId: gameId,
                playerSecret: playerSecret
            }
        );

        return axios.put(url);
    },
    deal(gameId, strategy, playerSecret) {
        let url = routing.getRoute(
            'wdebelek.api.deal', {
                gameId: gameId,
                playerSecret: playerSecret
            }
        );

        return axios.post(
            url,
            {
                strategy: strategy
            }
        )
    }
}
