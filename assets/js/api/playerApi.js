import axios from 'axios';
import routing from '../routing';

export default {
    getPersonalInfo(tableId, playerSecret) {
        let url = routing.getRoute(
            'wdebelek.api.get_hand', {
                tableId: tableId,
                playerSecret: playerSecret
            }
        );

        return axios.get(url).then(response => {
            return response.data
        })
    },
}
