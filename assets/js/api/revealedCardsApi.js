import axios from 'axios';
import routing from '../routing';

export default {
    showHandToSpectator(gameId, playerSecret, spectatorId) {
        let url = routing.getRoute(
            'wdebelek.api.show_to_spectator', {
                gameId: gameId,
                playerSecret: playerSecret,
                spectatorId: spectatorId
            }
        );

        return axios.put(url);
    },
    getRevealedCards(gameId, playerSecret) {
        let url = routing.getRoute(
            'wdebelek.api.get_shown', {
                gameId: gameId,
                playerSecret: playerSecret
            }
        );

        return axios.get(url).then(response => {
            return response.data
        })
    }
}
