export default {
    compareCards(c1, c2) {
        let suit1 = [4, 1, 3, 2][Math.floor((c1 - 1) / 13)];
        let suit2 = [4, 1, 3, 2][Math.floor((c2 - 1) / 13)];

        if (suit1 !== suit2) {
            return suit1 < suit2 ? -1 : 1;
        }

        // Make sure ace is highest card
        let rank1 = [(c1 + 11) % 13];
        let rank2 = [(c2 + 11) % 13];

        return rank1 - rank2;
    }
}
