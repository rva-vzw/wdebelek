/*
 * CardMat main javascript file. (Should have given it another name)
 */

import Vue from "vue";
import VueI18n from "vue-i18n";
import BootstrapVue from "bootstrap-vue";
import CardMat from "./Components/CardMat";

import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'

// Let's import @goback136's style sheet
import '../css/cardMat.css';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';

Vue.use(BootstrapVue);
Vue.use(VueI18n);

new Vue({
    el: '#app',
    components: {CardMat},
})

