<?php

$config = new PhpCsFixer\Config();

$finder = PhpCsFixer\Finder::create()
    ->exclude([
        'var',
        'vendor',
        'tests/Support/_generated',
    ])
;

return $config
    ->setRules([
        '@Symfony' => true,
        'array_syntax' => ['syntax' => 'short'],
        'phpdoc_to_comment' => false,
        'nullable_type_declaration_for_default_null_value' => true,
    ])
    ->setFinder($finder)
;
