# WDEBELEK :beer: 🃏

WDEBELEK is an event sourced web application, written in PHP, that you can use
to play online card games, especially
[solo whist](https://en.wikipedia.org/wiki/Solo_whist)
([wiezen](https://www.rijkvanafdronk.be/wiezen)). 

This web application should focus on playing realistic card games,
especially with the cards being cut, not shuffled.

Players should be able to sort their cards themselves,
allowing to emulate a 'steekspelletje'.

And the possibility should also exist
to end a game with some cards unplayed, putting all cards again together as-is.

Initially, the idea of this project is just enabling remote card playing, without
any keeping track of the score. I guess, when playing cards online, you will
probably be using some video conferencing tool to discuss the game. So one of
the players can be responsible for writing down the points.

You can try this out, on [kaart.rijkvanafdronk.be](https://kaart.rijkvanafdronk.be), 
but please be gentle to my VPS. It is recommended to
[read the manual](https://www.rijkvanafdronk.be/apps/wdebelek) (only in Dutch for now).

If you play wiezen, [we also have an application](https://www.rijkvanafdronk.be/apps/dikdikdik)
to help you keeping track of the points.

## How to run this locally?

### Install dependencies using docker:

Run the following from your project root:

```
docker run --rm --interactive --tty -v "$PWD":/app composer install
docker run -it --rm  -v "$PWD":/usr/src/app -w /usr/src/app node:14 yarn install
docker run -it --rm  -v "$PWD":/usr/src/app -w /usr/src/app node:14 yarn encore dev
```

### (Alternatively:) Install dependencies using locally installed composer and yarn:

Run the following from your project root:

```
docker-compose up -d
composer install
yarn install
yarn encore dev
```

### Create the database

You can run the following from the outside of your php container:
```
docker-compose exec php make reset
```

### Mercure

The application will listen to http://mercure:3000 for server sent events.
So for this to work, you'll have to modify your `/etc/hosts` files so that
mercure resolves to 127.0.0.1.

### Access the application

Your application should now be at http://localhost:8080

### Run the tests

```
docker-compose exec php make test
```

### Use XDebug with PHPStorm

I always forget how to configure XDebug. But you'll probably need to define a PHP web
server (Preferences, PHP, Servers) and configure the path mappings. Listen for incoming
connections, and it might work.

Debugging command line scripts is also something that sometimes works. To debug a console
command, run it from within your container as follows:

```
XDEBUG_CONFIG="idekey=PHPSTORM" ./bin/console messenger:consume amqp_commands -vv --limit=1
```

### Run the make scripts from your host os

If you want to run `make reset` or `make test` from your host os, without
docker, you can if you modify `/etc/hosts` as follows:

```
127.0.0.1	localhost	db webserver chrome mercure
```

## Deploy on a production server

I've added some instructions about
[how you can install wdebelek on a production server](doc/deploy-production-manually.md).
Once your server is prepared, you can use gitlab CI/CD as well. I have no documentation
for that at the moment, but you can check out [.gitlab-ci.yml](.gitlab-ci.yml).